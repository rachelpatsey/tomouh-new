<?php 

require 'vendor/autoload.php';
require "vendor/zohocrm/php-sdk/src/index.php";

$init = ZCRMRestClient::initialize();


// $oAuthClient = ZohoOAuth::getClientInstance();

// $grantToken = "1000.31f9ad491bb4f214a2bfe57329ae801c.519c55c242a4fb2cc979f9300d205c5e";
// $oAuthTokens = $oAuthClient->generateAccessToken($grantToken);

// echo "<pre>";
// print_r($oAuthTokens);exit;


// try{
//     $ins=ZCRMRestClient::getInstance();
//     $moduleArr=$ins->getAllModules()->getData();
//     echo "<pre>";
//     print_r($moduleArr);
//     foreach ($moduleArr as $module) {
//         echo "ModuleName:".$module->getModuleName();
//         echo "SingLabel:".$module->getSingularLabel();
//         echo "PluLabel:".$module->getPluralLabel();
//         echo "BusinesscardLimit:".$module->getBusinessCardFieldLimit();
//         echo "ApiName:".$module->getAPIName();
//         $fields=$module->getFields();
//         if($fields==null) {
//             continue;
//         }
//         foreach ($fields as $field) {
//             echo $field->getApiName().", ";
//             echo $field->getLength().", ";
//             echo $field->IsVisible().", ";
//             echo $field->getFieldLabel().", ";
//             echo $field->getCreatedSource().", ";
//             echo $field->isMandatory().", ";
//             echo $field->getSequenceNumber().", ";
//             echo $field->isReadOnly().", ";
//             echo $field->getDataType().", ";
//             echo $field->getId().", ";
//             echo $field->isCustomField().", ";
//            echo $field->isBusinessCardSupported().", ";
//           echo $field->getDefaultValue().", ";
//         }
//     }
// } catch (ZCRMException $e) {
//     echo $e->getCode();
//     echo $e->getMessage();
//     echo $e->getExceptionCode();
// }


// try{
//           $bulkAPIResponse=ZCRMOrganization::getInstance()->getAllUsers();
//           $users=$bulkAPIResponse->getData();
//           foreach($users as $userInstance)
//           {
//           echo $userInstance->getCountry();
//           $roleInstance=$userInstance->getRole();
//           echo $roleInstance->getId();
//           echo $roleInstance->getName();
//           $customizeInstance=$userInstance->getCustomizeInfo();
//           if($customizeInstance!=null)
//           {
//           echo $customizeInstance->getNotesDesc();
//           echo $customizeInstance->getUnpinRecentItem();
//           echo $customizeInstance->isToShowRightPanel();
//           echo $customizeInstance->isBcView();
//           echo $customizeInstance->isToShowHome();
//           echo $customizeInstance->isToShowDetailView();
//           }
//           echo $userInstance->getCity();
//           echo $userInstance->getSignature();
//           echo $userInstance->getNameFormat();
//           echo $userInstance->getLanguage();
//           echo $userInstance->getLocale();
//           echo $userInstance->isPersonalAccount();
//           echo $userInstance->getDefaultTabGroup();
//           echo $userInstance->getAlias();
//           echo $userInstance->getStreet();
//           $themeInstance=$userInstance->getTheme();
//           if($themeInstance!=null)
//           {
//           echo $themeInstance->getNormalTabFontColor();
//           echo $themeInstance->getNormalTabBackground();
//           echo $themeInstance->getSelectedTabFontColor();
//           echo $themeInstance->getSelectedTabBackground();
//           }
//           echo $userInstance->getState();
//           echo $userInstance->getCountryLocale();
//           echo $userInstance->getFax();
//           echo $userInstance->getFirstName();
//           echo $userInstance->getEmail();
//           echo $userInstance->getZip();
//           echo $userInstance->getDecimalSeparator();
//           echo $userInstance->getWebsite();
//           echo $userInstance->getTimeFormat();
//           $profile= $userInstance->getProfile();
//           echo $profile->getId();
//           echo $profile->getName();
//           echo $userInstance->getMobile();
//           echo $userInstance->getLastName();
//           echo $userInstance->getTimeZone();
//           echo $userInstance->getZuid();
//           echo $userInstance->isConfirm();
//           echo $userInstance->getFullName();
//           echo $userInstance->getPhone();
//           echo $userInstance->getDob();
//           echo $userInstance->getDateFormat();
//           echo $userInstance->getStatus();
//           }
//           }
//           catch(ZCRMException $e)
//           {
//           echo $e->getMessage();
//           echo $e->getExceptionCode();
//           echo $e->getCode();
//           }


// insert 
$moduleIns=ZCRMRestClient::getInstance()->getModuleInstance("Contacts");//to get the instance of the module
$records=array();
$record=ZCRMRecord::getInstance("Contacts",null); //To get ZCRMRecord instance
$record->setFieldValue("Last_Name","Last_Name"); //This function use to set FieldApiName and value similar to all other FieldApis and Custom field
$record->setFieldValue("Email","demo12122@email.com");
$record->setFieldValue("First_Name","First_Name");
$record->setFieldValue("Date_Of_Birth","10/10/1996");
$record->setFieldValue("Gender","Male"); 

$record->setFieldValue("Expired_On","10/10/2020");
$record->setFieldValue("Home_Country","Test");
$record->setFieldValue("Home_City","Test");
$record->setFieldValue("Mobile_No.","9876543210"); 

$record->setFieldValue("Country_Of_Residence","Test");
$record->setFieldValue("City_Of_Residence'","Test");
$record->setFieldValue("Mobile_No._2","9876543210");
$record->setFieldValue("Educational_Info.","test"); 

$record->setFieldValue("Experience_Info","Test");
$record->setFieldValue("AAH'","Test");
$record->setFieldValue("Passions_and_interests","test");
$record->setFieldValue("Subscription_Info","test");

$record->setFieldValue("Status'","Active");
$record->setFieldValue("Plan","Free");
$record->setFieldValue("Label","Member");


array_push($records, $record);//pushing the record to the array 
// echo "<pre>";
// print_r($records);
// exit;
$responseIn=$moduleIns->createRecords($records);
foreach($responseIn->getEntityResponses() as $responseIns){
echo "HTTP Status Code:".$responseIn->getHttpStatusCode(); //To get http response code
echo "Status:".$responseIns->getStatus(); //To get response status
echo "Message:".$responseIns->getMessage(); //To get response message
echo "Code:".$responseIns->getCode(); //To get status code
echo "Details:".json_encode($responseIns->getDetails());
}

exit;


?>