<?php 

require 'vendor/autoload.php';
require "vendor/zohocrm/php-sdk/src/index.php";

ZCRMRestClient::initialize();

// insert 
$moduleIns=ZCRMRestClient::getInstance()->getModuleInstance("Contacts");//to get the instance of the module
$records=array();
$record=ZCRMRecord::getInstance("Contacts",null); //To get ZCRMRecord instance
$record->setFieldValue("Last_Name","Last_Name"); //This function use to set FieldApiName and value similar to all other FieldApis and Custom field
$record->setFieldValue("Email","demo33@email.com");
$record->setFieldValue("First_Name","First_Name");
$record->setFieldValue("Date_Of_Birth","10/10/1996");
$record->setFieldValue("Gender","Male"); 

$record->setFieldValue("Expired_On","10/10/2020");
$record->setFieldValue("Home_Country","Test");
$record->setFieldValue("Home_City","Test");
$record->setFieldValue("Mobile_No.","9876543210"); 

$record->setFieldValue("Country_Of_Residence","Test");
$record->setFieldValue("City_Of_Residence'","Test");
$record->setFieldValue("Mobile_No._2","9876543210");
$record->setFieldValue("Educational_Info.","test"); 

$record->setFieldValue("Experience_Info","Test");
$record->setFieldValue("AAH'","Test");
$record->setFieldValue("Passions_and_interests","test");
$record->setFieldValue("Subscription_Info","test");

$record->setFieldValue("Status'","Active");
$record->setFieldValue("Plan","Free");
$record->setFieldValue("Label","Member");


array_push($records, $record);//pushing the record to the array 


$responseIn=$moduleIns->createRecords($records);
foreach($responseIn->getEntityResponses() as $responseIns){
echo "HTTP Status Code:".$responseIn->getHttpStatusCode(); //To get http response code
echo "Status:".$responseIns->getStatus(); //To get response status
echo "Message:".$responseIns->getMessage(); //To get response message
echo "Code:".$responseIns->getCode(); //To get status code
echo "Details:".json_encode($responseIns->getDetails());
}

exit;


?>