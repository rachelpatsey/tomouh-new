<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./application/helpers/general_helper.php');
require_once('./zoho_v2/vendor/autoload.php');

class Crm_contact_data extends CI_Controller
{
	public function __construct(){
		parent::__construct();

		$this->load->model('tomouh_model');
		$this->load->model('messages_model');
		$this->load->model('admin/admin_model');
		$this->load->library('session');
		$this->load->library('form_validation');
		ZCRMRestClient::initialize();
	}

	public function index(){

		$tbl = "tbl_crm_contact_data";
		// mail("victoriahale.cis@gmail.com","Crm_contact_data Notification","Crm_contact_data webhook run");

		$data = $_POST;
		$handle = fopen('./zoho_called.txt','a+');
        $logtext = "******************".date('Y-m-d H:i:s')."******************\n\n";
             
        $logtext .= "========== parameter list contactdata======== ";
            
        $logtext .= "<pre>".print_r($data, true)."</pre>";
             
        $logtext .= "\n\n**********************************************************************\n\n";
        $errorlog = fwrite($handle,$logtext);
        fclose($handle);
    
		$contact_id = $data['Contact_Id'];
	
		if(!empty($contact_id)){

			// $crm_data =  $this->tomouh_model->getCrmContactDataById($contact_id);
			$crm_data =  $this->tomouh_model->getUserByContactId($contact_id);
			$apiResponse=ZCRMModule::getInstance('Contacts')->getRecord($contact_id); 
			$record=$apiResponse->getData();
			
			$contactdata=$record->getData();

			$handle = fopen('./zoho_called.txt','a+');
	        $logtext = "******************".date('Y-m-d H:i:s')."******************\n\n";
	             
	        $logtext .= "========== parameter list contactdata======== ";
	            
	        $logtext .= "<pre>".print_r($contactdata, true)."</pre>";
	             
	        $logtext .= "\n\n**********************************************************************\n\n";
	        $errorlog = fwrite($handle,$logtext);
	        fclose($handle);

			if(empty($crm_data)){

				$insert_array = array(
					'v_contact_id'=>$data['Contact_Id'],
					'v_email'=>$data['Email'],
					'l_contact_data'=>json_encode($data),
					'd_added'=>date('Y-m-d H:i:s'),
					'd_modified'=>date('Y-m-d H:i:s')
				);

				$add_contact_data = $this->admin_model->add_entry($insert_array,$tbl);
				$geo = [];
				$latitude = '';
				$longitude = '';
				// if(isset($data['Residence_city']) && isset($data['Residence_country']) && $data['Residence_country'] != '' && $data['Residence_city'] != ''){
				// 	$address = $data['Residence_city'].' '.$data['Residence_country'];
				
				// 	$google_api_key = $this->tomouh_model->getSetting('GOOGLE_MAP_KEY');
				// 	$geo = $this->file_get_contents_curl('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key='.$google_api_key);

				// 	$geo = json_decode($geo, true);
	
				// }

				
				// if (isset($geo['status']) && ($geo['status'] == 'OK')) {

				// 	$latitude = $geo['results'][0]['geometry']['location']['lat'];
				// 	$longitude = $geo['results'][0]['geometry']['location']['lng'];

				// }else{
				// 	$latitude = '';
				// 	$longitude = '';
				// }
				$staus ='inactive';
				if(isset($contactdata['Member_Status'])){
					if($contactdata['Member_Status'] == 'Active') $staus = 'active';
					if($contactdata['Member_Status'] == 'Cancelled') $staus = 'inactive';
				}

				$insert_data = array(
						'v_email'   	 => $data['Email'],
						'v_password' 	 => '',
						'v_firstname'	 => $contactdata['First_Name'],
						'v_lastname' 	 => $contactdata['Last_Name'],
						'd_dob'      	 => date('Y-m-d', strtotime($contactdata['Date_of_Birth'])),
						'e_gender'    	 => $contactdata['Gender'],
						'v_home_country' => $contactdata['Country'],
						'v_home_city'    => $contactdata['Home_City'],
						'v_residence_country' => $contactdata['Country_of_Residence'],
						'v_residence_city'    => $contactdata['City_Of_Residence'],
						'v_telephone' 	 => $contactdata['Mobile_No'],
						'v_telephone_2'  => $contactdata['Mobile_No_2'],
						'v_twitter_link '=> '',
						'v_linkedin_link'=> '',
						'e_referral' 	 => '',
						'v_referral_email' => '',
						't_passion_interest'  => $contactdata['Passions_and_interests'],
						't_description'  => $contactdata['Description'],
						'v_main_occupation' => '',
						'v_longitude'=>'',
						'v_latitude'=>'',
						'e_status'=>$staus,
						'e_member_of_month'=>0,
						'v_subscription_id'=>'',
						'e_type'=>'',
						'v_label'=>$contactdata['Label'],
						'e_plan_type'=>$contactdata['Plan'],
						'v_crm_contact_id'=>$contact_id,
						'e_payment_type'=>'',
						'd_subscription_exp_date'=>(isset($contactdata['Profile_Expiry']) && $contactdata['Profile_Expiry'] != '') ? $contactdata['Profile_Expiry'] : null,
						'd_last_profile_update'=>(isset($contactdata['Last_profile_update']) && $contactdata['Last_profile_update'] != '') ? $contactdata['Last_profile_update'] : null,
					);	

					$tbl = "tbl_members";
					$add_user_data = $this->admin_model->add_entry($insert_data,$tbl);
					
					if($staus == 'active'){
						$merge_vars =[];
						// mailchimp api call to subscribe start

						$merge_vars['EMAIL'] = $data['Email'];
						$merge_vars['FNAME'] = $data['First_Name'];
						$merge_vars['LNAME'] = $data['Last_Name'];

						mailChimpSubscribe($merge_vars);
						
						// mailchimp api call to subscribe end
					}
						

			}else{

				$insert_array = array(
					'v_contact_id'=>$data['Contact_Id'],
					'v_email'=>$data['Email'],
					'l_contact_data'=>json_encode($data),
					'd_modified'=>date('Y-m-d H:i:s')
				);

				$this->admin_model->update_crm_contact_entry($contact_id,$insert_array,$tbl);

				
				// $this->db->select('*');
				// $this->db->from('tbl_members');
				// $this->db->where('v_crm_contact_id', $contact_id);
				// $query = $this->db->get();
				// $row = $query->result_array();

				$staus ='inactive';
				if($contactdata['Member_Status'] == 'Active') {
					$staus = 'active';
				}
				if($contactdata['Member_Status'] == 'Cancelled') {
					$staus = 'inactive';
				}


				$update_data = array(
					'v_email'   	 => $contactdata['Email'],
					'v_firstname'	 => $contactdata['First_Name'],
					'v_lastname' 	 => $contactdata['Last_Name'],
					'd_dob'      	 => date('Y-m-d', strtotime($contactdata['Date_of_Birth'])),
					'e_gender'    	 => $contactdata['Gender'],
					'v_home_country' => $contactdata['Country'],
					'v_home_city'    => $contactdata['Home_City'],
					'v_residence_country' => $contactdata['Country_of_Residence'],
					'v_residence_city'    => $contactdata['City_Of_Residence'],
					'v_telephone' 	 => $contactdata['Mobile_No'],
					'v_telephone_2'  => $contactdata['Mobile_No_2'],
					't_passion_interest'  => $contactdata['Passions_and_interests'],
					't_description'  => $contactdata['Description'],
				//	'v_longitude'=>'',
				//	'v_latitude'=>'',
					'e_status'=>$staus,
				//	'e_member_of_month'=>0,
					'v_label'=>$contactdata['Label'],
					'e_plan_type'=>$contactdata['Plan'],
					'v_crm_contact_id'=>$contact_id,
					'd_subscription_exp_date'=>$contactdata['Profile_Expiry'],
					'd_last_profile_update'=>$contactdata['Last_profile_update'],
				);

			$tbl = "tbl_members";
			$add_user_data=$this->admin_model->update_member_entry($contact_id,$update_data,$tbl);
			$arr=array($crm_data['v_email'],$data['Email'],$staus);
			$handle = fopen('./zoho_called1.txt','a+');
	        $logtext = "******************".date('Y-m-d H:i:s')."******************\n\n";
	             
	        $logtext .= "========== parameter list contactdata======== ";
	            
	        $logtext .= "<pre>".print_r($arr, true)."</pre>";
	             
	        $logtext .= "\n\n**********************************************************************\n\n";
	        $errorlog = fwrite($handle,$logtext);
	        fclose($handle);
				
				if($crm_data['v_email'] != $data['Email']){
					try{
						mailChimpChangeEmail($data['Email'],$crm_data['v_email']);
					}catch(Exception $e){

					}
					
				}else if($staus == 'active'){
					$merge_vars = [];
					// mailchimp api call to subscribe start

					$merge_vars['EMAIL'] = $data['Email'];
					$merge_vars['FNAME'] = $data['First_Name'];
					$merge_vars['LNAME'] = $data['Last_Name'];

					try{
						mailChimpSubscribe($merge_vars);
					}catch(Exception $e){

					}
					
					// else{
    				// 	try{
					// 		mailChimpSubscribe($merge_vars);
					// 	}catch(Exception $e){

					// 	}
    				// }
				}else if($staus == 'inactive'){
					$merge_vars = [];
					// mailchimp api call to subscribe start

					$merge_vars['EMAIL'] = $data['Email'];
					$merge_vars['FNAME'] = $data['First_Name'];
					$merge_vars['LNAME'] = $data['Last_Name'];
					try{
						mailChimpUnsubscribe($merge_vars);
					}catch(Exception $e){

					}
				}

			


			}
		}

		// exit();
	}

	public function delete_crm_contact(){

		$data = $_POST;

		$data = $_POST;
		$handle = fopen('./zoho_called.txt','a+');
        $logtext = "******************".date('Y-m-d H:i:s')."******************\n\n";
             
        $logtext .= "========== parameter list contactdata======== ";
            
        $logtext .= "<pre>".print_r($data, true)."</pre>";
             
        $logtext .= "\n\n**********************************************************************\n\n";
        $errorlog = fwrite($handle,$logtext);
        fclose($handle);
        

		$contact_data = $this->tomouh_model->getCrmContactDataById($data['Contact_id']);

		$this->admin_model->delete_crm_contact($data['Contact_id'],"tbl_crm_contact_data");

		if(isset( $contact_data['v_email'] ) && !empty( $contact_data['v_email'] )){

			$this->admin_model->delete_member_temp_data( $contact_data['v_email'] , 'tbl_members_temp');

			$this->admin_model->delete_member_before_data( $contact_data['v_email'] , 'tbl_before_member');
		}

		$this->admin_model->delete_crm_member($data['Contact_id'],"tbl_crm_members_data");

		$members_data = $this->tomouh_model->getUserByContactId($data['Contact_id']);

		if( isset($members_data['id']) && $members_data['id'] != '' ){

				$this->tomouh_model->deleteUserAccount($members_data['id']);
		}

		if( isset($members_data['v_email']) && $members_data['v_email'] != '' ){

			$this->admin_model->delete_member_temp_data( $members_data['v_email'] , 'tbl_members_temp');

			$this->admin_model->delete_member_before_data( $members_data['v_email'] , 'tbl_before_member');
		}

		if( isset($members_data['v_crm_contact_id']) && $members_data['v_crm_contact_id'] != '' ){

			$this->admin_model->delete_crm_contact($members_data['v_crm_contact_id'],"tbl_crm_contact_data");
				
			$this->delete_zoho_crm_contact($members_data['v_crm_contact_id']);
			
		}
		if( isset($members_data['v_email']) && $members_data['v_email'] != '' ){
			
			$res = mailChimpDelete($members_data['v_email']);

			
    	}

	}


	public function delete_zoho_crm_member( $member_id ){

		// if(!empty($member_id)){

		// 	$zcrmRecordIns = ZCRMRecord::getInstance("Members", $member_id); 
		// 	$apiResponse=$zcrmRecordIns->delete();

		// }
		
	}
}
