<?php 

$token="6b86fc1baf974eaa2690ebcc8fe6fd2b";
require 'vendor/autoload.php';
use CristianPontes\ZohoCRMClient\ZohoCRMClient;

$member = new ZohoCRMClient('CustomModule1', "6b86fc1baf974eaa2690ebcc8fe6fd2b","eu");

$education = new ZohoCRMClient('CustomModule2', "6b86fc1baf974eaa2690ebcc8fe6fd2b","eu");

$education_info1 ='University : Oxford, Degree : MSc 1, Major : 60%, Year : Oxford';
$education_info2 ='University : Oxford, Degree : MSc 2, Major : 65%, Year : Oxford';
$education_info3 ='University : Oxford, Degree : MSc 3, Major : 70%, Year : Oxford';
$education_info4 ='University : Oxford, Degree : MSc 4, Major : 75%, Year : Oxford';

$experience_info1 ='Company : IT Company, Job Title : Php Developer 1, Industry : Software, Period : 2000-2002';
$experience_info2 ='Company : IT Company, Job Title : Php Developer 2, Industry : Software, Period : 2002-2007';
$experience_info3 ='Company : IT Company, Job Title : Php Developer 3, Industry : Software, Period : 2007-2009';
$experience_info4 ='Company : IT Company, Job Title : Php Developer 4, Industry : Software, Period : 2009-2012';

$AAH ='Achived Gold Medal in 2010, Got Awared in 2012';

$mResult = $member->insertRecords()
	->setRecords([
		array(
			'First Name' => 'Wp',
			'Last Name' => 'Developer',
			'Email' => 'wp@gmail.com',
			'Date Of Birth' => '07/15/1987', //MMDDYYY
			'Gender'	=> 'Male',
			'Expired On' => '02/28/2018', //MMDDYYY
			'Home Country'	=> 'United States',
			'Home City'	=>'New York',
			'Mobile No.'	=>'1122334455',
			'Country of Residence'	=>'United States',
			'City Of Residence'	=>'Albana',
			'Mobile No. 2'	=>'5544332211',
			'Educational Info 1'	=> $education_info1,
			'Educational Info 2'	=> $education_info2,
			'Educational Info 3'	=> $education_info3,
			'Educational Info 4'	=> $education_info4,
			'Experience Info 1' => $experience_info1,
			'Experience Info 2' => $experience_info2,
			'Experience Info 3' => $experience_info3,
			'Experience Info 4' => $experience_info4,
			'AAH'				=> $AAH,
			'Passions and interests' => 'I have no Intrest'
		)
	])
	->onDuplicateError()
	->triggerWorkflow()
	->request();

echo "<pre>";
print_r($mResult[1]->id);
echo "</pre>";	
?>