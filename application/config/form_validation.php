<?php 

$config = array(
		'login_details' => array(

        				array(
            						'field' => 'v_name',
            						'label' => 'Username',
            						'rules' => 'required'
        					),
        				array(
                        'field' => 'v_password',
            						'label' => 'Password',
            						'rules' => 'required'
                    ),
		),
    'settings' => array(

                array(
                        'field' => 'SITE_NAME',
                        'label' => 'Site Name',
                        'rules' => 'required'
                    ),
                array(
                        'field' => 'SITE_URL',
                        'label' => 'Site URL',
                        'rules' => 'required'
                    ),
                array(
                        'field' => 'SITE_NAME',
                        'label' => 'Site Name',
                        'rules' => 'required'
                    ),
                array(
                        'field' => 'CONTACT_INFO',
                        'label' => 'Constant Info', 
                        'rules' => 'required'
                    ),
                array(
                        'field' => 'ADDRESS',
                        'label' => 'Address',
                        'rules' => 'required'
                    ),
                array(
                        'field' => 'FOOTER_TEXT',
                        'label' => 'Footer Text',
                        'rules' => 'required'
                    ),
                array(
                        'field' => 'INSTAGRAM_LINK',
                        'label' => 'Instagram',
                        'rules' => 'required'
                    ),
                array(
                        'field' => 'LINKEDIN_LINK',
                        'label' => 'Linkedin',
                        'rules' => 'required'
                    ),
                array(
                        'field' => 'TWITTER_LINK',
                        'label' => 'Twitter',
                        'rules' => 'required'
                    ),
                array(
                        'field' => 'YOUTUBE_LINK',
                        'label' => 'Youtube',
                        'rules' => 'required'
                    ),
    ),

		'manage_admin' => array(

				        array(
                        'field' => 'v_username',
                        'label' => 'Username',
                        'rules' => 'required|alpha'
                    ),
                array(
                        'field' => 'v_password',
                        'label' => 'Password',
                        'rules' => 'required'
                    ),
                array(
                        'field' => 'v_email',
                        'label' => 'Email',
                        'rules' => 'required|valid_email'
                    ),
		),
		'pages' => array(

              	array(
                          'field' => 'v_title',
                          'label' => 'Page Title',
                          'rules' => 'required'
                  	),
                array(
                          'field' => 'l_description',
                          'label' => 'Page Description',
                          'rules' => 'required'
                    ),
                array(
                          'field' => 'v_meta_title',
                          'label' => 'Meta Title',
                          'rules' => 'required'
                    ),

                array(
                          'field' => 'v_meta_keyword',
                          'label' => 'Meta Keyword',
                          'rules' => 'required'
                    ),

                array(
                          'field' => 'l_meta_description',
                          'label' => 'Meta Description',
                          'rules' => 'required'
                    ),
                array(
                          'field' => 'e_status',
                          'label' => 'Status',
                          'rules' => 'required'
                    )
        ),
		'news' => array(

              	array(
                      'field' => 'v_title',
                      'label' => 'News Title',
                      'rules' => 'required'
                  	),
              	array(
                      'field' => 'l_description',
                      'label' => 'News Description',
                      'rules' => 'required'
                	),
             	array(
                      'field' => 'v_meta_title',
                      'label' => 'Meta Title',
                      'rules' => 'required'
                   	),

              	array(
                      'field' => 'v_meta_keyword',
                      'label' => 'Meta Keyword',
                      'rules' => 'required'
                   	),

              	array(
                      'field' => 'l_meta_description',
                      'label' => 'Meta Description',
                      'rules' => 'required'
                   	),
              	array(
                      'field' => 'e_status',
                      'label' => 'Status',
                      'rules' => 'required'
                  )
            ),
		'email_template' => array(

                array(

                      'field' => 'v_title',
                      'label' => 'Email Title',
                      'rules' => 'required'
                    ),
                array(

                      'field' => 'v_subject', 
                      'label' => 'Email Subject',
                      'rules' => 'required'
                  	),
                array(

                      'field' => 'v_reply_to', 
                      'label' => 'Reply To Email',
                      'rules' => 'required|valid_email'
                    ),
                array(

                    'field' => 'l_body',
                    'label' => 'Email_body',
                    'rules' => 'required'
                    ),
                array(

              		'field' => 'e_status',
              		'label' => 'Status',
              		'rules' => 'required'
           		)
         ),
		'meta_fields' => array(

				        array(

                      'field' => 'v_title',
                      'label' => 'Meta Fields Title',
                      'rules' => 'required'
                    ),
                array(

                      'field' => 'l_description', 
                      'label' => 'Meta Fields Description',
                      'rules' => 'required'
                  	),
                array(

                      'field' => 'i_order', 
                      'label' => 'Meta Fields Order',
                      'rules' => 'required'
                    ),
                array(

              		'field' => 'e_status',
              		'label' => 'Status',
              		'rules' => 'required'
           		),
		),
		'faqs' => array(

				      array(

                    'field' => 'v_question',
                    'label' => 'Question',
                    'rules' => 'required'
                ),
            	array(

                  	'field' => 'l_answer', 
                 	'label' => 'Answer',
                  	'rules' => 'required'
              	),
            	array(

                  	'field' => 'e_status', 
                  	'label' => 'Status',
                  	'rules' => 'required'
                )
		),
		'testimonials' => array(

				      array(

                    'field' => 'v_name',
                    'label' => 'Name',
                    'rules' => 'required'
                ),
            	array(

                  	'field' => 'l_text', 
                 	'label' => 'Description',
                  	'rules' => 'required'
              	),
            	array(

                  	'field' => 'e_status', 
                  	'label' => 'Status',
                  	'rules' => 'required'
                )
		),
		'photos' => array(

				      array(
                    'field' => 'v_name',
                    'label' => 'Name',
                    'rules' => 'required'
                ),
              array(
                    'field' => 'v_image',
                    'label' => 'Image',
                    'rules' => 'required'
                ),
            	array(

                  	'field' => 'e_status', 
                  	'label' => 'Status',
                  	'rules' => 'required'
                )
		),
		'videos' => array(

				        array(

                    'field' => 'v_name',
                    'label' => 'Name',
                    'rules' => 'required'
                ),
                array(

                    'field' => 'v_url',
                    'label' => 'URL',
                    'rules' => 'required'
                ),
            	  array(

                  	'field' => 'e_status', 
                  	'label' => 'Status',
                  	'rules' => 'required'
                )
		),
		'team' => array(

				        array(

                    'field' => 'e_type',
                    'label' => 'Member Type',
                    'rules' => 'required'
                ),
				        array(

                    'field' => 'v_name',
                    'label' => 'Name',
                    'rules' => 'required'
                ),
                array(

                    'field' => 'v_position',
                    'label' => 'Position',
                    'rules' => 'required'
                ),
                array(

                    'field' => 'v_image',
                    'label' => 'Image',
                    'rules' => 'required'
                ),
                array(

                    'field' => 'l_description',
                    'label' => 'Description',
                    'rules' => 'required'
                ),
            	  array(

                  	'field' => 'e_status', 
                  	'label' => 'Status',
                  	'rules' => 'required'
                )
		),
		'books' => array(

				        array(

                    'field' => 'v_title',
                    'label' => 'Title',
                    'rules' => 'required'
                ),
                array(

                    'field' => 'v_image',
                    'label' => 'Image',
                    'rules' => 'required'
                ),
				        array(

                    'field' => 'l_description',
                    'label' => 'Description',
                    'rules' => 'required'
                ),
            	  array(

                  	'field' => 'e_status', 
                  	'label' => 'Status',
                  	'rules' => 'required'
                )
		),

    'home_slider' => array(

                // array(

                //     'field' => 'l_main_text',
                //     'label' => 'Main Text',
                //     'rules' => 'required'
                // ),
                // array(

                //     'field' => 'v_text',
                //     'label' => 'Text',
                //     'rules' => 'required'
                // ),
                array(

                    'field' => 'v_image',
                    'label' => 'Image',
                    'rules' => 'required'
                ),
                array(

                    'field' => 'e_status', 
                    'label' => 'Status',
                    'rules' => 'required'
                )
    ),

    'members' => array(

                array(

                    'field' => 'v_firstname',
                    'label' => 'Firstname',
                    'rules' => 'required'
                ),
                array(

                    'field' => 'v_lastname',
                    'label' => 'Lastname',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'v_email',
                    'label' => 'Email',
                    'rules' => 'required|valid_email'
                ),
                array(
                    'field' => 'e_gender',
                    'label' => 'Gender',
                    'rules' => 'required'
                ),
    ),

    'manage_fellows' => array(

                array(
                      'field' => 'v_title',
                      'label' => 'Name',
                      'rules' => 'required'
                    ),
                array(
                      'field' => 'l_bio',
                      'label' => 'Bio',
                      'rules' => 'required'
                  ),
              array(
                      'field' => 'd_year',
                      'label' => 'Year',
                      'rules' => 'required'
                    ),

                array(
                      'field' => 'l_description',
                      'label' => 'Description',
                      'rules' => 'required'
                    ),
               
                array(
                      'field' => 'e_status',
                      'label' => 'Status',
                      'rules' => 'required'
                  )
            ),
);
?>