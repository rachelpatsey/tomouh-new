<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Payment extends CI_Controller
{
	public function __construct(){
		parent::__construct();

		$this->load->model('admin/admin_model');
		$this->load->model('messages_model');
		$this->load->library('session');
		$this->load->library('form_validation');
	}
	public function index(){

			$this->load->helper('url');
			$this->load->view('header');
			$this->load->view('payment');
			$this->load->view('footer');
	}
}