<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['media/photos/(:any)'] = 'category/index/$1';
$route['media/videos/(:any)'] = 'video_detail/index/$1';
$route['profile_upto_date'] = 'page/profile_upto_date';

$route['profile/(:any)'] = 'profile/index/$1';
$route['about_us'] = 'page/view/about_us';
$route['about_us_demo'] = 'page/view/about_us_demo';
$route['tcl_fellow'] = 'page/view/tcl-fellow';
$route['tcl_awards'] = 'page/view/tcl-awards';
$route['sponsorship'] = 'page/view/sponsorship';
$route['founders'] = 'page/view/founders';
$route['our_members'] = 'page/view/our_members';
$route['initiative'] = 'page/view/initiative';
$route['contact_us']  = 'page/view/contact_us';
$route['my_home']  = 'page/view/my_home';
$route['faq']  = 'page/view/faq';
$route['why_join'] = 'page/view/why_join_tomouh';
$route['team'] = 'page/view/team';
$route['honorary_member'] = 'page/view/honorary_member';
$route['council_of_senior_advisers'] = 'page/view/council-of-senior-advisers';
$route['board_member'] = 'page/view/board-members';
$route['membership_committee'] = 'page/view/membership_committee';
$route['media'] = 'page/view/media';
$route['signup_2'] = 'page/signup_2';
$route['signup'] = 'page/signup';
$route['blog/(:any)'] = 'blog/detail/$1';
$route['logout'] = 'page/logout';
$route['edit_profile'] = 'page/edit_profile';
$route['edit_personal_info'] = 'page/edit_profile_1';
$route['edit_educational_info'] = 'page/edit_profile_2';
$route['payment_success'] = 'page/payment_success';
$route['why_join_detail/(:any)'] = 'why_join_detail/index/$1';
$route['terms-and-conditions'] = 'page/view/terms-and-conditions';
$route['login%C2%A0'] = 'login/index';
$route['member_updates'] = 'membership';
$route['member_updates'] = 'membership';
$route['membership_process'] = 'page/view/membership_process';
$route['getCountryFromName'] = 'page/getCountryFromName';
$route['signup_success']  = 'page/signup_success';
$route['delete_account']  = 'page/delete_account';
$route['request_website_data']  = 'page/request_website_data';
$route['delete_crm_contact'] = 'Crm_contact_data/delete_crm_contact';
$route['delete_crm_member'] = 'Crm_member_data/delete_crm_member';

// $route['my-stripe'] = "StripeController";
$route['stripePost']['post'] = "Stripe_process/stripePost";
$route['returnWebhookStripe']= "Stripe_process/returnWebhookStripe";

