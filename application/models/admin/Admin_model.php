<?php 
class Admin_model extends CI_Model {

	public function __construct(){
			
		parent::__construct();
	}

	public function authenticate($username, $password){
		
		$this->db->select('*');
    	$this->db->from('tbl_admin');
		$this->db->where('(v_username ="'. $username.'" OR v_email="'.$username.'")');
		$this->db->where('v_password', md5($password));
		$this->db->where('e_status', 'active');
		$query = $this->db->get();
		$row = $query->result_array();
		
		return $row[0];
	}

	public function getSetting($key){
		
		$query = $this->db->query("SELECT l_value FROM tbl_sitesetting WHERE v_name ='".$key."'");
		$results = $query->result_array();
		return $results[0]["l_value"];
	}

	public function getAdminList($arg){
		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( (v_username LIKE '%".$keyword."%') OR (v_email LIKE '%".$keyword."%') )";
		}		
		$this->db->select('*');
		$this->db->from('tbl_admin');
		
		if($wh != ''){
			$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_admin');
		
		if($wh != ''){
			$this->db->where($wh);
		}
		$this->db->order_by('d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		
		return $row;
	}

	public function getFrontMembersList($arg){
		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( (t1.v_firstname LIKE '%".$keyword."%') OR 
					(t1.v_lastname LIKE '%".$keyword."%') OR 
					(t1.t_passion_interest LIKE '%".$keyword."%') OR 
					(t1.v_residence_country LIKE '%".$keyword."%') OR 
					(t1.v_residence_city LIKE '%".$keyword."%') OR 
					(t1.v_home_country LIKE '%".$keyword."%') OR 
					(t1.v_home_city LIKE '%".$keyword."%') OR
					(t2.v_university LIKE '%".$keyword."%') OR
					(t2.v_major LIKE '%".$keyword."%') OR
					(t2.v_degree LIKE '%".$keyword."%') OR
					(t3.v_industry LIKE '%".$keyword."%') OR
					(t3.v_company LIKE '%".$keyword."%'))";
		}	

		$this->db->select('*');
		$this->db->from('tbl_members t1'); 
	    $this->db->join('tbl_educational_data t2', 't1.id=t2.user_id', 'left');
	    $this->db->join('tbl_professional_data t3', 't1.id=t3.user_id', 'left');

		if($wh != ''){
			$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_members t1'); 
	    $this->db->join('tbl_educational_data t2', 't1.id=t2.user_id', 'left');
	    $this->db->join('tbl_professional_data t3', 't1.id=t3.user_id', 'left');
		
		if($wh != ''){
			$this->db->where($wh);
		}
		
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['member'] = $query->result_array();
		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["member"] = $query->result_array();

		return $row;
		
	}

	public function getPageList($arg){
		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( (v_title LIKE '%".$keyword."%') OR (v_slug LIKE '%".$keyword."%') )";
		}		
		$this->db->select('*');
		$this->db->from('tbl_pages');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_pages');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$this->db->order_by('d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		
		return $row;
	}

	public function getPhotoList($arg){
		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( (v_name LIKE '%".$keyword."%') )";
		}		
		$this->db->select('*');
		$this->db->from('tbl_photos');
	
		if($wh != ''){
			$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_photos');

		if($wh != ''){
			$this->db->where($wh);
		}
		$this->db->order_by('d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		
		return $row;
	}

	public function getVideoList($arg){
		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( (v_name LIKE '%".$keyword."%') )";
		}		
		$this->db->select('*');
		$this->db->from('tbl_videos');
		// $this->db->group_by('v_name');
		if($wh != ''){
			$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_videos');
		// $this->db->group_by('v_name');

		if($wh != ''){
			$this->db->where($wh);
		}
		$this->db->order_by('d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		
		return $row;
	}

	public function getTeamList($arg){

		extract($arg);

		$wh = '';
		if($keyword != ''){
			$wh = "( (v_name LIKE '%".$keyword."%')) ";
		}		

		if($members_type != ''){
			if( $wh != '' ) $wh .= ' AND ';
				$wh .= " e_type = '".$members_type."' ";
		}

		$this->db->select('*');
		$this->db->from('tbl_team');

		if($wh != ''){
			$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_team');

		if($wh != ''){
			$this->db->where($wh);
		}
		$this->db->order_by('d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		
		return $row;
	}

	public function getBooksList($arg){

		extract($arg);

		$wh = '';
		
		if($keyword != ''){
			$wh = "( (v_title LIKE '%".$keyword."%') )";
		}		
		$this->db->select('*');
		$this->db->from('tbl_books');
		// $this->db->group_by('v_title');
		if($wh != ''){
			$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_books');
		// $this->db->group_by('v_title');

		if($wh != ''){
			$this->db->where($wh);
		}
		$this->db->order_by('d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		
		return $row;
	}

	public function getSliderList($arg){

		extract($arg);

		$wh = '';
		
		if($keyword != ''){
			$wh = "( (v_text LIKE '%".$keyword."%') )";
		}		
		$this->db->select('*');
		$this->db->from('tbl_slider');

		if($wh != ''){
			$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_slider');

		if($wh != ''){
			$this->db->where($wh);
		}
		$this->db->order_by('d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
	
		return $row;
	}

	public function getMemberList($arg){

		extract($arg);

		// $label = array('Member',
		// 				'Junior Member',
		// 				'Honorary Member',
		// 				'Senior Advisory Board Member',
		// 				'Speaker',
		// 				'Did not pay renewal',
		// 				'Expired',
		// 				'Senior Members',
		// 				'Removed',
		// 				'Former Member');

		$wh = "v_label IN ('Member',
						'Junior Member',
						'Honorary Member',
						'Senior Advisory Board Member',
						'Speaker',
						'Did not pay renewal',
						'Expired',
						'Senior Members',
						'Removed',
						'Former Member')";
						

		// $wh = "";
		
		if($keyword != ''){
			$wh .= " AND ( (v_firstname LIKE '%".$keyword."%') 
					OR (v_lastname LIKE '%".$keyword."%')
					OR (v_email LIKE '%".$keyword."%'))";
			// $wh .= " v_firstname LIKE '%".$keyword."%' 
			// OR v_lastname LIKE '%".$keyword."%'
			// OR v_email LIKE '%".$keyword."%' ";
		}		
		
		$this->db->select('*');
		$this->db->from('tbl_members');
		
		if(isset($wh) && !empty($wh)){
			$this->db->where($wh);
		}
		if($status != ''){

			$this->db->where("e_status = '".$status."'");
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_members');

		if(isset($wh) && !empty($wh)){
			$this->db->where($wh);
		}
		if($status != ''){

			$this->db->where("e_status = '".$status."'");
		}
		$this->db->order_by('id','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		
		return $row;
	}

	public function getMetaList($arg){
		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( (tbl_meta_fields.v_title LIKE '%".$keyword."%') OR (tbl_meta_fields.v_type LIKE '%".$keyword."%') )";
		}		
		$this->db->select('*');
		$this->db->from('tbl_meta_fields');
		$this->db->join('tbl_pages', 'tbl_meta_fields.i_item_id = tbl_pages.id');

		if($wh != ''){
			$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('tbl_meta_fields.*, tbl_pages.v_title as page_title');
		$this->db->from('tbl_meta_fields');
		$this->db->join('tbl_pages', 'tbl_meta_fields.i_item_id = tbl_pages.id');
		
		if($wh != ''){
			$this->db->where($wh);
		}
		$this->db->order_by('tbl_meta_fields.d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		return $row;
	}

	public function getMetaItems($arg, $id){
	
		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( (tbl_meta_fields.v_title LIKE '%".$keyword."%') OR (tbl_meta_fields.v_type LIKE '%".$keyword."%') )";
		}		
		$this->db->select('tbl_meta_fields.v_title as page_title, tbl_meta_items.*');
		$this->db->from('tbl_meta_items');
		$this->db->join('tbl_meta_fields', 'tbl_meta_items.i_meta_id = tbl_meta_fields.id');
		$this->db->where('tbl_meta_items.i_meta_id', $id);
		
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('tbl_meta_fields.v_title as page_title, tbl_meta_items.*');
		$this->db->from('tbl_meta_items');
		$this->db->join('tbl_meta_fields', 'tbl_meta_items.i_meta_id = tbl_meta_fields.id');
		$this->db->where('tbl_meta_items.i_meta_id', $id);

		if($wh != ''){
		$this->db->where($wh);
		}
		$this->db->order_by('tbl_meta_items.d_added','DESC');
		$this->db->limit($limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $form ,"black");
		$row["rows"] = $query->result_array();
		return $row;
	}


	public function getNewsList($arg){
		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( (v_title LIKE '%".$keyword."%') OR (v_slug LIKE '%".$keyword."%') )";
		}		
		$this->db->select('*');
		$this->db->from('tbl_news');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_news');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$this->db->order_by('d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		return $row;
	}
	public function getFellowList($arg){
		extract($arg);

		$wh = '';
		
		if($keyword != ''){
			$wh = "( (v_title LIKE '%".$keyword."%') OR (v_slug LIKE '%".$keyword."%') )";
		}		
		$this->db->select('d_year')->from('tbl_fellow');
		// $this->db->from('tbl_fellow');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$this->db->where('e_type',$e_type)->where('e_status',$e_status)->order_by('d_year','ASC')->group_by('d_year');
		$query1 = $this->db->get(); 
		// $row["nototal"] = $query1->num_rows();
		$row["years"] = $query1->result_array();

		$this->db->select('*');
		$this->db->from('tbl_fellow');

		if($wh != ''){
			$this->db->where($wh);
		}
		$query2 = $this->db->get(); 
		$row["nototal"] = $query2->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_fellow');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		if(isset($e_status) && $e_status!= ''){
			$this->db->where('e_status',$e_status);
		}
		// if(isset($d_year) && $d_year!= ''){
		// 	$this->db->where('d_year',$d_year);
		// }
		
		if(isset($e_type) && $e_type!= ''){
			$this->db->where('e_type',$e_type);
		}
		
		if(isset($row["years"]) && count($row["years"]) && !isset($d_year) && $d_year == ''){

			foreach ($row["years"] as $key => $value) {
				if($value['d_year'] < date('Y')){
					$lastYearkey = $key;
				}
			}			
			$currentYearkey = array_search(date('Y'), array_column($row["years"], 'd_year'));
			if(isset($currentYearkey) && $currentYearkey != '' ){
				$row['selectedYear']=$row["years"][$currentYearkey]['d_year'];
			}else{
				$row['selectedYear']=$row["years"][$lastYearkey]['d_year'];
			}
		}

		if(isset($d_year) && $d_year != ''){			
			$this->db->where('d_year',$d_year);
			$row['selectedYear']=$d_year;
		}
		// $this->db->order_by('d_added','DESC');
		$this->db->order_by('d_year','ASC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		

		return $row;
	}
	

	public function getFounderList($arg){
		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( (v_title LIKE '%".$keyword."%') )";
		}		
		$this->db->select('*');
		$this->db->from('tbl_founders');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_founders');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		if(isset($e_status) && $e_status!= ''){
			$this->db->where('e_status',$e_status);
		}
		$this->db->order_by('d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		return $row;
	}

	public function getEventList($arg){
		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( (t_title LIKE '%".$keyword."%') )";
		}		
		$this->db->select('*');
		$this->db->from('tbl_events');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_events');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		if(isset($e_status) && $e_status!= ''){
			$this->db->where('e_status',$e_status);
		}
		$this->db->order_by('d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		return $row;
	}


	public function getMembersUpdateList($arg){
		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( (v_user_name LIKE '%".$keyword."%') OR (l_description LIKE '%".$keyword."%') )";
		}		
		$this->db->select('*');
		$this->db->from('tbl_members_updates');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_members_updates');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$this->db->order_by('d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		return $row;
	}

	public function getFaqsList($arg){
		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( (v_question LIKE '%".$keyword."%') OR (l_answer LIKE '%".$keyword."%') )";
		}		
		$this->db->select('*');
		$this->db->from('tbl_faqs');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_faqs');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$this->db->order_by('d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		return $row;
	}
	
	public function getEmailTemplateList($arg){
		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( (	v_title LIKE '%".$keyword."%') OR (	v_subject LIKE '%".$keyword."%') )";
		}		
		$this->db->select('*');
		$this->db->from('tbl_email_templates');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_email_templates');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$this->db->order_by('d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		return $row;
	}

	public function getTestimonialsList($arg){
		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( (	v_name LIKE '%".$keyword."%') )";
		}		
		$this->db->select('*');
		$this->db->from('tbl_testimonials');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_testimonials');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$this->db->order_by('d_added','DESC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		return $row;
	}

	public function getPaymentList($arg){

		extract($arg);
		
		$wh = '';
		
		if($keyword != ''){
			$wh = "( ( tbl_members.v_firstname LIKE '%".$keyword."%') 
					OR ( tbl_members.v_lastname LIKE '%".$keyword."%')
					OR ( tbl_members.v_email LIKE '%".$keyword."%'))";
		}		
		$this->db->select('*');
		$this->db->from('tbl_subscription_data');
		$this->db->join('tbl_members', 'tbl_subscription_data.user_id = tbl_members.id');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_subscription_data');
		$this->db->join('tbl_members', 'tbl_subscription_data.user_id = tbl_members.id');
		
		if($wh != ''){
		$this->db->where($wh);
		}
		$this->db->order_by('tbl_subscription_data.id','DESC');
		$this->db->group_by('tbl_subscription_data.user_id');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		return $row;
	}

	public function add_entry($ins, $tbl)
	{	
	
		$this->db->insert($tbl, $ins);
		
		$sql = $this->db->last_query();
		
		$last_id =$this->db->insert_id();
		 
		return $last_id;
	}

	public function add_multi_entry($tbl,$ins) {
		$this->db->insert_batch($tbl, $ins); 
		return 1;
	}

	public function select_entry($tbl,$id){


		$this->db->select('*');
		$this->db->from($tbl);
		$this->db->where('id', $id);
		$row = $this->db->get()->result_array();

		return $row[0];

	}

	public function select_payment($tbl,$id){

		$this->db->select('*');
		$this->db->from($tbl);
		$this->db->where('id', $id);

		$temp = $this->db->get()->result_array();

		$data['members_data'] = isset($temp) && !empty($temp) && isset($temp[0]) ? $temp[0] : [];

		$user_id = $id;

		$subscription_data = $this->db->query("SELECT * FROM `tbl_subscription_data` as t1 LEFT JOIN `tbl_members` as t2 ON t1.user_id = t2.id WHERE t1.user_id = ".$user_id."");

		$data['subscription_data'] = $subscription_data->result();

		return $data;
	}

	public function select_members($tbl,$id){


		$this->db->select('*');
		$this->db->from($tbl);
		$this->db->where('id', $id);

		$temp = $this->db->get()->result_array();

		$data['members_data'] = isset($temp) && !empty($temp) && isset($temp[0]) ? $temp[0] : [];

		$user_id = $id;

		$educational_data = $this->db->query("SELECT * FROM `tbl_educational_data` WHERE user_id = ".$user_id."");

		$data['educational_data'] = $educational_data->result();

		$professional_data = $this->db->query("SELECT * FROM `tbl_professional_data` WHERE user_id = ".$user_id."");

		$data['professional_data'] = $professional_data->result();

		$achievement_data = $this->db->query("SELECT * FROM `tbl_achievements_data` WHERE user_id = ".$user_id."");
		        
		$data['achievement_data'] = $achievement_data->result();

		return $data;
	}

	public function select_member($tbl,$id){


		$this->db->select('*');
		$this->db->from('tbl_educational_data');
		$this->db->where('user_id', $id);
		$rows = $this->db->get()->result_array();
		return $rows;
		
	}

	public function select_photo($tbl,$id){

		$this->db->select('*');
		$this->db->from('tbl_photos');
		$this->db->join('tbl_photosimage', 'tbl_photos.id = tbl_photosimage.i_photos_id');
		$this->db->where('i_photos_id', $id);
		$rows = $this->db->get()->result_array();
		return $rows;
	}

	public function update_entry($id,$ins,$tbl){

		$this->db->where('id', $id);
		$this->db->update($tbl, $ins);
		return 1;
	}
	public function update_member_entry($id,$ins,$tbl){
		// echo "<pre>";
		// print_r($ins);
		// print_r($id);
		// exit;
		$this->db->where('v_crm_contact_id', $id);
		$this->db->update($tbl, $ins);
		

		$this->db->select('id');
		$this->db->from($tbl);
		$this->db->where('v_crm_contact_id', $id);
		$row = $this->db->get()->result_array();

		// echo "<pre>";
		// print_r($row); exit;

		return $row[0]['id'];
		// return 1;
	}

	public function update_crm_contact_entry($id,$ins,$tbl){

		$this->db->where('v_contact_id', $id);
		$this->db->update($tbl, $ins);
		return 1;
	}

	public function update_crm_member_entry($id,$ins,$tbl){

		$this->db->where('v_member_id', $id);
		$this->db->update($tbl, $ins);
		return 1;
	}

	public function update_members_data($id,$ins,$tbl){

		$this->db->where('v_member_crm_id', $id);
		$this->db->update($tbl, $ins);
		return 1;
	}

	public function update_members($id,$ins,$tbl){

		$this->db->where('id', $id);
		$this->db->update($tbl, $ins);
		//$sql = $this->db->last_query();
		//exit;
			
		return $id;
	}

	public function update_payment($id,$ins,$tbl){

		$this->db->where('id', $id);
		$this->db->update($tbl, $ins);
		return $id;
	}

	public function update_photos($v_slug,$ins,$tbl){
		$this->db->where('v_slug', $v_slug);
		$this->db->update($tbl, $ins);
		return 1;
	}

	public function delete_entry($wh,$tbl)
	{
		$this->db->where('id', $wh);
		$this->db->delete($tbl);
		return 1;
	}
	
	public function delete_data($user_id,$tbl)
	{
		$this->db->where('user_id', $user_id);
		$this->db->delete($tbl);
		return 1;
	}

	public function delete_crm_contact($id,$tbl)
	{
		$this->db->where('v_contact_id', $id);
		$this->db->delete($tbl);
		return 1;
	}

	public function delete_member_temp_data($email,$tbl){

		$this->db->where('v_email', $email);
		$this->db->delete($tbl);
		return 1;
	}

	public function delete_member_before_data($email,$tbl){

		$this->db->where('v_email', $email);
		$this->db->delete($tbl);
		return 1;
	}

	public function delete_crm_member($id,$tbl){
		$this->db->where('v_member_id', $id);
		$this->db->delete($tbl);
		return 1;
	}

	public function delete_photo($id,$tbl)
	{
		$this->db->where('id', $id);
		$this->db->delete($tbl);
		return 1;
	}

	public function delete_image($id,$tbl)
	{
		$this->db->where('i_photos_id', $id);
		$this->db->delete($tbl);
		return 1;
	}

	public function add_meta_item($ins, $tbl)
	{
		$this->db->insert($tbl, $ins);
		return 1;
	}
	public function update_meta_item($id,$ins,$tbl){

		$this->db->where('i_meta_id', $id);
		$this->db->update($tbl, $ins);
		return 1;
	}
	public function getMetaFieldsWithItems($type, $item_id){

		$this->db->select('*');
		$this->db->from('tbl_meta_fields');
		$this->db->where('v_type', $type);
		$this->db->where('i_item_id', $item_id);
		$this->db->or_where('i_item_id', 0);
		$this->db->order_by('i_order', 'ASC');
		$query = $this->db->get();
		$rows = $query->result_array();

		if(!empty($rows)){
			foreach($rows as $rKey=>$row){
				$rows[$rKey]["items"] = $this->getMetaItem($row["id"]);
			}
		}
		
		return $rows;
		
	}
	public function getMetaItem($id){
		
		$this->db->select('*');
		$this->db->from('tbl_meta_items');
		$this->db->where('i_meta_id', $id);
		$this->db->order_by('i_order','ASC');
		$query = $this->db->get();
		$rows = $query->result_array();
		
		return $rows;
	}
	public function getMetaValue($key, $id){
		$row = $this->getMetaValueRow($key, $id);
		if(!empty($row)){
			return $row['l_value'];
		}
	}
	public function getMetaValueRow($key, $id){
		
		$this->db->select('*');
		$this->db->from('tbl_meta_values');
		$this->db->where('i_post_id', $id);
		$this->db->where('v_key', $key);
		$query = $this->db->get();
		$rows = $query->result_array();
		if(!empty($rows)){
			return $rows[0];
		}
	}
	public function update_meta($key, $value, $id){
		
		$row = $this->getMetaValueRow($key, $id);	
		
		if(!empty($row)){
			$ins = array("l_value"=>$value);
			$this->update_entry($row["id"], $ins, "tbl_meta_values");
		}else{
			$ins = array("i_post_id"=>$id, "v_key"=>$key, "l_value"=>$value);
			$this->add_entry($ins, "tbl_meta_values");
		}	
	}

	public function getTotalMembers(){
		$wh = "v_label IN ('Member',
						'Junior Member',
						'Honorary Member',
						'Senior Advisory Board Member',
						'Speaker',
						'Did not pay renewal',
						'Expired',
						'Senior Members',
						'Removed',
						'Former Member')";
			
		$this->db->from('tbl_members');
                        	
		if(isset($wh) && !empty($wh)){
			$this->db->where($wh);
		}
		$query = $this->db->get();
		$rowcount = $query->num_rows();
		return $rowcount;
	}
	public function getTotalActiveMembers(){
		$wh = "v_label IN ('Member',
						'Junior Member',
						'Honorary Member',
						'Senior Advisory Board Member',
						'Speaker',
						'Did not pay renewal',
						'Expired',
						'Senior Members',
						'Removed',
						'Former Member')";
		$this->db->from('tbl_members');
		$this->db->where('e_status','active');
		if(isset($wh) && !empty($wh)){
			$this->db->where($wh);
		}
		$query = $this->db->get();
		$rowcount = $query->num_rows();
		return $rowcount;
	}
	public function getTotalInactiveMembers(){
		$wh = "v_label IN ('Member',
						'Junior Member',
						'Honorary Member',
						'Senior Advisory Board Member',
						'Speaker',
						'Did not pay renewal',
						'Expired',
						'Senior Members',
						'Removed',
						'Former Member')";
		$this->db->from('tbl_members');
		$this->db->where('e_status','inactive');
		if(isset($wh) && !empty($wh)){
			$this->db->where($wh);
		}
		$query = $this->db->get();
		$rowcount = $query->num_rows();
		return $rowcount;
	}
	public function getTotalPayment(){

		$this->db->from('tbl_subscription_data');
		$query = $this->db->get();
		$rowcount = $query->num_rows();
		return $rowcount;
	}

	public function getCountryList($arg){

		extract($arg);

		$wh = '';
		if($keyword != ''){
			$wh = "( (country_name LIKE '%".strtolower($keyword)."%')) ";
		}		

		// $this->db->select('*');
		$this->db->select('tbl_country.*, COUNT(tbl_members.id) as totalMember');
		$this->db->from('tbl_country');
		$this->db->join('tbl_members', 'tbl_country.country_name=tbl_members.v_residence_country', 'left');
		$this->db->where('tbl_members.e_status','active');
		$this->db->group_by('tbl_country.id');
		if($wh != ''){
			$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		// $this->db->select('*');
		$this->db->select('tbl_country.*,COUNT(tbl_members.id) as totalMember');
		$this->db->from('tbl_country');
		$this->db->join('tbl_members', 'tbl_country.country_name=tbl_members.v_residence_country', 'left');
		$this->db->where('tbl_members.e_status','active');
		
		

		if($wh != ''){
			$this->db->where($wh);
		}
		$this->db->order_by('id','ASC');
		$this->db->group_by('tbl_country.id');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		// echo "<pre>";
		// print_r($row);
		return $row;
	}

	public function getIndustryList($arg){

		extract($arg);

		$wh = '';
		if($keyword != ''){
			$wh = "( (v_name LIKE '%".strtolower($keyword)."%')) ";
		}		

		$this->db->select('*');
		$this->db->from('tbl_industry');

		if($wh != ''){
			$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_industry');

		if($wh != ''){
			$this->db->where($wh);
		}
		$this->db->order_by('id','ASC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		
		return $row;
	}

	public function getInitiativesList($arg){

		extract($arg);

		$wh = '';
		if($keyword != ''){
			$wh = "( (v_name LIKE '%".strtolower($keyword)."%')) ";
		}		

		$this->db->select('*');
		$this->db->from('tbl_initiatives');

		if($wh != ''){
			$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_initiatives');

		if($wh != ''){
			$this->db->where($wh);
		}
		$this->db->order_by('id','ASC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		
		return $row;
	}

	public function getRequestUser($arg){

		extract($arg);

		$wh = '';
		if($keyword != ''){
			$wh = "( (v_firstname LIKE '%".strtolower($keyword)."%') OR (v_email LIKE '%".strtolower($keyword)."%') OR (v_lastname LIKE '%".strtolower($keyword)."%')) ";
		}		

		$this->db->select('*');
		$this->db->from('tbl_request_user_data');

		if($wh != ''){
			$this->db->where($wh);
		}
		$query1 = $this->db->get(); 
		$row["nototal"] = $query1->num_rows();

		$this->db->select('*');
		$this->db->from('tbl_request_user_data');

		if($wh != ''){
			$this->db->where($wh);
		}
		$this->db->order_by('id','ASC');
		$this->db->limit($limit, $limitstart);
		$query = $this->db->get();
		$row['row'] = $query->result_array();

		$this->paging_model->vmPageNav($row["nototal"], $limitstart, $limit, $form ,"black");
		$row["rows"] = $query->result_array();
		
		return $row;
	}

	public function getRow($tbl,$id){

		
		$this->db->select('*');
		$this->db->where('id',$id);
		$this->db->from($tbl);
		$query = $this->db->get();
		
		$row = $query->result_array();
		
		return $row[0];
	}

	public function getResult($tbl){
		
		$this->db->select('*');
		$this->db->from($tbl);
		$query = $this->db->get();
		
		$rows = $query->result_array();
		
		return $rows;
	}
	
	public function select_member_data($tbl,$email){


		$this->db->select('*');
		$this->db->from($tbl);
		$this->db->where('v_email', $email);
		$this->db->order_by('id','DESC');
		$row = $this->db->get()->result_array();

		return $row[0];

	}

	public function select_member_subscription_data($tbl,$id){


		$this->db->select('*');
		$this->db->from($tbl);
		$this->db->where('user_id', $id);
		$this->db->order_by('id','DESC');
		$this->db->limit(1);
		$row = $this->db->get()->result_array();
		return $row[0];

	}

}
?>
