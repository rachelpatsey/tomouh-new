<?php
class Messages_model extends CI_Model {

        
	public function __construct(){
			// Call the CI_Model constructor
			parent::__construct();
			$this->messages=array(
									"invalid"=>"Invalid Username/Password",
									"logout"=>"You are logged out successfully.",
									"sameuser"=>"Username/Email name already exist!",
									"invalidemail"=>"Username/Email is invalid",
									"msgslug"=>"Slug Name already exits!!",
									
									"login"=>"You are logged in successfully.",
									"del"=>"Record has been deleted successfully.",
									"multidel"=>"Selected records has been deleted successfully.",
									"multiact"=>"Selected records has been activated successfully.",
									"multiinact"=>"Selected records has been deactivated successfully.",
									"useract"=>"Your TCL account is verified.",
									
									"multiapprovedt"=>"Selected records has been approved successfully.",
									"multipending"=>"Selected records has been pending successfully.",
									"multiactdeclined"=>"Selected records has been declined successfully.",
									
									"add"=>"Record has been added successfully.",
									"edit"=>"Record has been updated successfully.",
									"pedit"=>"Profile has been updated successfully.",
									"idel"=>"Image has been deleted successfully.",
									
									"addPayment"=>"Payment details has been added successfully.",
									"editPayment"=>"Payment details has been updated successfully.",

									"nameexists"=>"Entered username already exists.",
									"pagetitleexists"=>"Entered page title already exists.",
									"cattitleexists"=>"Entered category already exists.",
									"urlexists"=>"Entered Url already exists.",
									"emailnotvalid"=>"Please enter a valid email address.",
									"emailexists"=>"Entered Email is already existed.",
									"photosnameexists"=>"Entered photo name already exists.",
									"couponexists"=>"Entered Coupon already exists.",
									"passwordmismatch"=>"Password fields are not matching.",
									"invaliduser"=>"Username does not exists. Please Try Again",
									"editinfo"=>"Your details has been changed successfully.",
									"chgstus"=>"Content has been publised successfully!!",
									"restore"=>"The page was restored successfully.",
									"order"=>"Menu Items Reordered Successfully!!",
									"newssent"=>"Newsletter has been sent successfully!!",
									"error"=>"Error Occured! Mail has not  sent!",
									"sessionexpire"=>"Your session has been expire! Please login again.",
									"filedel"=>"File has been Removed!",
									"logfirst"=>"Please, Login first!",
									"nochpass"=>"You have entered wrong a password. Please try again",
									"chpass"=>"Your password has been changed successfully!!",
									"setsave"=>"Settings has been changed successfully!",
									"newssend"=>"Newsletter has been sent to subscribe users.",
									"wlogin"=>"You entered wrong email or password",
									"rmode"=>"Your account in review mode. Wait still admin approve it", 

									"courseComplite"=>"Your course is completed successfully.", 
									
									"reg"=>"You have successfully registered. Please check your email for more details.",
									"cmpt"=>"COMPETITIONS FORM successfully submitted.",
									"evnt"=>"Event form successfully submitted.",
									"loginfirst"=>"Please, login first !!",
									"passsent"=>"Your password has been sent to your email address.",
									"emailnotexist"=>"The email address you have entered does not exist.",
									"usernamenotexist"=>"Username you have entered does not exist.",
									"emailsent"=>"Thank you! Your enquiry has been sent to administrator",
									"orsentdel"=>"Order has been sent! member info has been deleted from database.",
									"reviewadd"=>"Thank You! Your review has been submitted.",
									"csvuploaded"=>"Order CSV uploaded successfully!!",
									"addimg"=>"Image uploaded successfully!!",
									"delimg"=>"Image deleted successfully!!",
									"active"=>"Record has been activated",
									"imageonly"=>"Please upload image only with extension .jpg .jpeg .gif .png ",
									"imagenotup"=>"Your image has not been uploaded.Please try again",
									"sameemail"=>"Email address already exists",
									"userinactive"=>"Your account is inactive. Please contact administrator.",
									"queexist"=>"Question already exists.",
									"alreadybookmarked"=>"You have already saved this article in your list",
									"bookmarksaved"=>"Article has been saved to your list",
									"inqsent"=>"<strong>Message Sent!</strong> Your message has been sent successfully. We will respond back as soon as possible",
									"artsub"=>"Success! Thanks for your comments. It will be visible once administrator will approve it.",
									"invsent"=>"Invitation sent successfully.",
									"ratesucc"=>"Thanks for your rating!",
									"commentsucc"=>"Thanks For your comments. Once administrator will approve your comment, it will display on site",
									"cmtflgd"=>"Thank you! Your report has been submitted successfully.",
									"frdexist"=>"You have already added this user as a friend.",
									"frdadded"=>"You have successfully added this user as a Friend.",
									"itsyou"=>"You can't add yourself as a Friend.",
									"required"=>"Please enter all fields.",
									"subscribeconf"=>"Please check your email for confirmation.",
									"subscribesuccess"=>"You have successfully subscribed to our newsletter.",
									"alreadysubscribed"=>"You are already registered.",
									"permissiondenied"=>"You have not  access permission.",
									"titleexist"=>"Title already exists.",
									"Fill"=>"Please Fill Form Properly.",
									"MIN_CHAR_REQUIRED"=>"Your Password Must Be More Then 7 Digit.",
									"NOT_SAME_PASSWORD"=>"Password Not Same.",
									"ACTIVE_ACCOUNT"=>"You have been registered successfully. Please check your email to activate your account",
									
									"UNIQUE"=>"Email OR Username Is Exist.",
									"EMAILNOTEXIST"=>"Email Not Exist.",
									"booked" 	=> "Your booking has been successfully submited.",
									"allreq" 	=> "All fields are required.",
									"blimit"    => "Your booking limitation has been exceeded for this week",
									"bexist"    => "You already booked this class.",
									"bnexist"   => "E-mail you have entered does not exist. Please Try Again.",
									"canceled"  => "Booking cancelation link has been sent to your email",
									"plink" 	=> "Change password link has been sent to email",
									"emailnotsent" => "Email sending failed.Please contact to administrator.",
									"usernamenotsent" => "Username sending failed.Please contact to administrator.",
									"accountsuccess" => "Your account has been activated successfully.",
									"waitverify" => "Your account has been activated successfully, now please wait for admin approval",
									"Forgot_Mail_Sent"=>"A password reset link has been sent to your TCL registered email. Please check the spam folder if you don't receive it shortly.",
									"Forgot_Username_Sent"=>"Your reset username link has been sent. Please check email for Reset Username",
									"Code_Forgot_Mail_Sent"=>"Your reset password verification code has been sent to your email.",
									"no_company_exist" 	=> "Please submit your company info",
									"change_visible" 	=> "Your profile visibility has been change successfully",
									"passNotEmpty" 	=> "Please enter password",
									"notverify" 	=> "Please verify your email to activate your account.",
									"yourecruiter" 	=> "You are registered as recruiter in our system, You can't apply for this job.",
									"profilewarnning" 	=> "Your profile is missing critical information. Please complete your profile so that recruiters can contact you for relevant jobs.",
									"profilecontent" 	=> "Please update your personal and educational information for complete profile.",
									"ssnexist" 	=> "This Social Security Code is already exist, Please Enter unique Code.",
									"paysuccess" 	=> "Payment done successfully.",
									"bookedinquiry" 	=> "Your inquiry booking has been successfully submitted.",
									"selectstate" 	=> "Please select state first.",
									"accept" 	=> "Patient inquiry accepted successfully.",
									"recommended" 	=> "Recommendation Completed, you can view this recommendation under the patients profile.",
									"schedule" 	=> "Your are successfully schedule your avalability time.",
									//"some" 	=> "Your are successfully schedule your avalability time.",

									"audioupload" => "Audio file uploaded successful.",
									"onlyaudio" => "Only Audio file supported.",
									"onlyvideo" => "Only Audio file supported.",
									"audionotupload" => "Audio file uploading failed.",
									"ruledel" => "Rule deleted successfully.",
									"slidedel" => "Slide deleted successfully.",
									"videodel" => "Video deleted successfully.",
									"imgdel" => "Rule deleted successfully.",
									"audiodel" => "Rule deleted successfully.",
									"audiodel" => "Video file uploading successfully.",
									
									"adduser"=>"User has been inserted successfully.",
									"addbundles"=>"Bundle has been inserted successfully.",
									"addcourses"=>"Courses has been inserted successfully.",
									"addpages"=>"Pages has been inserted successfully.",
									"addnews"=>"News has been inserted successfully.",
									"addfellow"=>"Manage Awards has been inserted successfully.",
									"reqdesfellow"=>"Description is required.",
									"addawards"=>"Awards has been inserted successfully.",
									"addfounder"=>"Founder has been inserted successfully.",
									"addevent"=>"Event has been inserted successfully.",
									"addemailtemplate"=>"EmailTemplate has been inserted successfully.",
									"addadmin"=>"Admin has been inserted successfully.",
									"addpage"=>"Page has been inserted successfully.",
									"addmeta"=>"Mata field has been inserted successfully.",
									"addnote"=>"Slide note has been inserted successfully.",
									"uploadppt"=>"PPT file has been uploaded successfully.",

									"edituser"=>"User has been updated successfully.",
									"editmeta"=>"Meta field has been updated successfully.",
									"editmemberupdate"=>"Memer Updates has been updated successfully.",
									"editbundles"=>"Bundle has been updated successfully.",
									"editcourses"=>"Courses has been updated successfully.",
									"editpages"=>"Pages has been inserted successfully.",
									"editnews"=>"News has been inserted successfully.",
									"editfellow"=>"Manage Awards has been updated successfully.",
									"editawards"=>"Awards has been updated successfully.",
									"editfounder"=>"Founder has been inserted successfully.",
									"editevent"=>"Event has been inserted successfully.",
									"editemailtemplate"=>"EmailTemplate has been inserted successfully.",
									"editadmin"=>"Admin has been inserted successfully.",

									"editpage"=>"Page has been updated successfully.",

									"imgchng"=>"Slide Image has been change successfully.",
									"audchng"=>"Slide Audio has been change successfully.",
									"vidchng"=>"Slide Video has been change successfully.",
									"onlyppt"=>".ppt or .pptx file Only supported.",

									"orderchangesucc"=>"Slides ordering has been change successfully.",
									"orderchangefailed"=>"Changing Slide order has been failed.",

									"fileuploadfail"=>"File uploading failed.",
									"cv_submitted" => "You have submitted CV successfully.",
									"inquiry_submitted" => "You have submitted inquiry successfully.",
									"coupon_failed" => "You have entered wrong coupon code",
									"coupon_success" => "You coupon code has been applied successfully",
									"mispass" => "Password not matches with confirm password",
									"invalid_login"=>"You have typed either a wrong password or email address",
									"password_not_match"=>"The old password you have typed does not match the records, please try with the correct password again.",
									"password_blank"=>"Plaese enter all password correctly",
									"password_change_success"=>"<strong>Success</strong> Your password has been changed successfully.",
									"validate_recaptch"=>"Please validate your reCAPTCHA.",
									"config_email"=>"Our record indicates that either your membership has been canceled or inactive. Please contact us at <a href='mailto:info@tomouh.net'>info@tomouh.net</a> if you are facing any technical issues.",
									"send_contact_msg"=>"Your message has been sent successfully.",
									"send_rsvp_msg"=>"Your RSVP has been sent successfully.",
									"imported"=>"You have imported members successfully",
									"valid_doc"=> "Please upload valid document.",
									"home_country_missing"=>"Please Select Home Country.",
									"residence_country_missing"=>"Please Select Residence Country.",
									"tele_code_missing"=>"Please Select Telephone Code.",
									"emailupdated"=>"<strong>Action completed.</strong> Your email has been successfully updated.",
									"delete_account"=>"Your account is successfully deleted!!!", 
									"request_data"=>"Action Completed. We will shortly send you the information that is being used on the website",
									"sent_email"=>"Email sent successfully!!",
									"not_sent_email"=>"Something went wrong. Please try again.",
									);
	}
	
	public function getSuccessMsg($key){
		$html ='<div class="alert alert-success alert-white rounded">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					<div class="icon"><i class="fa fa-check"></i></div>
					'.$this->messages[$key].'
				 </div>';
		return $html;
	}

	public function getSuccessMsgEvent($key){
		$html ='<div class="alert alert-success alert-white rounded">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					<div class="icon"><i class="fa fa-check"></i>
					'.$this->messages[$key].'
				 </div></div>';
		return $html;
	}

	public function getErrorMsg($key){
		$html ='<div class="alert alert-danger alert-white rounded">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					<div class="icon"><i class="fa fa-times-circle"></i></div>
					<strong>Error!</strong> '.$this->messages[$key].'
				 </div>';
		return $html;
	}
	
	public function getWarningMsg($key){
		$html ='<div class="alert alert-warning alert-white rounded">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					<div class="icon"><i class="fa fa-warning"></i></div>
					<strong>Warning!</strong> '.$this->messages[$key].'
				 </div>';
		return $html;
	}
}
?>