<?php

class General_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getDropdownList($droparray, $selval){
		$str = '';
		foreach($droparray as $key => $val){
			if($selval == $val) { $sel = 'selected="selected"'; }
			else { $sel = ''; }
			$str .= '<option value="'.$val.'" '.$sel.' />'.ucwords($val).'</option>';
		}
		echo $str;
	}

	public function getTCLTypeDropdownList($droparray, $selval){
		$str = '';
		foreach($droparray as $key => $val){
			if($selval == $val) { $sel = 'selected="selected"'; }
			else { $sel = ''; }
			$str .= '<option value="'.$val.'" '.$sel.' />'.ucwords($val).'</option>';
		}
		echo $str;
	}

	public function getDropdownListKey($droparray, $selval){
		$str = '';
		foreach($droparray as $key => $val){
			if($selval == $key) { $sel = 'selected="selected"'; }
			else { $sel = ''; }
			$str .= '<option value="'.$key.'" '.$sel.' />'.ucwords($val).'</option>';
		}
		echo $str;
	}

	public function getItemList($droparray, $selval){

		foreach($droparray as $key => $val){
			if($selval == $val) { $sel = 'selected="selected"'; }
			else { $sel = ''; }
			$str .= '<option value="'.$val.'" '.$sel.' />'.ucwords($val).'</option>';
		}
		echo $str;
	}

	public function get_Admin($key,$value){
		
		$this->db->select('*');
    	$this->db->from('tbl_admin');
		$this->db->where($key, $value );
		$query = $this->db->get();
		$row = $query->result_array();
		if (isset($row[0])) {
			
			return $row[0];
		}
		return array();
	}

	public function get_Page($key,$value){
		
		$this->db->select('*');
    	$this->db->from('tbl_pages');
		$this->db->where($key, $value );
		$query = $this->db->get();
		$row = $query->result_array();
		if (isset($row[0])) {
			
			return $row[0];
		}
		return array();
	}

	public function get_pname($key,$value){
		
		$this->db->select('*');
    	$this->db->from('tbl_photos');
		$this->db->where($key, $value );
		$query = $this->db->get();
		$row = $query->result_array();
		if (isset($row[0])) {
			
			return $row[0];
		}
		return array();
	}

	public function get_Cat($key,$value){
		
		$this->db->select('*');
    	$this->db->from('tbl_photo_category');
		$this->db->where($key, $value );
		$query = $this->db->get();
		$row = $query->result_array();
		if (isset($row[0])) {
			
			return $row[0];
		}
		return array();
	}

	public function get_Admin_Data($key,$value,$id){

		$this->db->select('*');
    	$this->db->from('tbl_admin');
		$this->db->where($key, $value );
		$this->db->where('id != '.$id);
		$query = $this->db->get();
		$row = $query->result_array();
		return $row[0];

	}

	public function get_Page_Data($key,$value,$id){

		$this->db->select('*');
    	$this->db->from('tbl_pages');
		$this->db->where($key, $value );
		$this->db->where('id != '.$id);
		$query = $this->db->get();
		$row = $query->result_array();
		return $row[0];

	}

	public function get_Cat_Data($key,$value,$id){

		$this->db->select('*');
    	$this->db->from('tbl_photo_category');
		$this->db->where($key, $value );
		$this->db->where('id != '.$id);
		$query = $this->db->get();
		$row = $query->result_array();
		return $row[0];

	}

	public	function seoText($str) {
					 $str=trim(strtolower($str));
					 $special_array=array('#','$','\'','"','?','&',':','!','%','&reg;','&trade;','(',')','/',',');
					 $str=str_replace(' ','-',$str);
					 foreach($special_array as $item)
					 {
					  $str=str_replace($item,'-',$str);
					 }
					 //echo $str."<br>";
					 $str=str_replace('--','-',$str);
					 $str=str_replace('---','-',$str);
					 $str=str_replace('--','-',$str);
					 $str=str_replace('--','-',$str);
					 $str=str_replace('--','-',$str);
					 $str=str_replace("’","-",$str);
					 //echo $str."<br>";exit;
					 return trim($str,'-');
					 
	}
	
	public function is_unique($tbl,$field, $val="", $id=""){
		
	  if($id !=""){
		$this->db->select($field);
		$this->db->where($field, $val);
		$this->db->where('id !=',$id);
		
	  }else{	
	  	$this->db->select($field)
	  			 ->where($field, $val);
	  }
	  $result = $this->db->get($tbl);

	  if ( $result->num_rows() > 0 ){
		
		$val = $val."-".rand(5, 10);
		return $this->is_unique($tbl, $field, $val);
		
	  }else{
		
		return $val;
	  	
	  }
	}
	//GENERAL INSERT FUNCTON 
	public function insert($tbl="",$ins = array()){
		
		$this->db->insert($tbl, $ins);
		return $this->db->insert_id();
	
	}
	//GENERAL SQL QUERT FIRE FUNCTION
	public function select_query($ssql=""){
		$query = $this->db->query($ssql);
		return $query->result_array();
	}

}


?>