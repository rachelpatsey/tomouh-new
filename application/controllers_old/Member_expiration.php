<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./zoho/vendor/autoload.php');
use CristianPontes\ZohoCRMClient\ZohoCRMClient;

require_once('./zoho_v2/vendor/autoload.php');


// require_once('./application/mailchimp/MCAPI.class.php');

require_once('./application/helpers/general_helper.php');

class Member_expiration extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('tomouh_model');
		$this->load->model('general_model');
		$this->load->model('messages_model');
		$this->load->model('admin/admin_model');
		$this->load->model('paging_model');
		$this->load->library('form_validation'); 
		$this->load->library('session');

		ZCRMRestClient::initialize();

	}

	public function index(){

		$members = $this->tomouh_model->getUserForUnlink();

		foreach($members as $member){

			if(isset($member['v_crm_contact_id']) && !empty($member['v_crm_contact_id'])){

				$zcrmRecordIns = ZCRMRecord::getInstance("Contacts", $member['v_crm_contact_id']);
				$zcrmRecordIns->setFieldValue("Label", "Not Paid");
				$apiResponse=$zcrmRecordIns->update();


                $merge_vars['EMAIL'] = $member['v_email'];
				$merge_vars['FNAME'] = $member['v_firstname'];
				$merge_vars['LNAME'] = $member['v_lastname'];

				$res = mailChimpUnsubscribe($merge_vars);

				if($res == 1){

	            }else{

	        	}          

			}

		}

		// exit();
	}
}