<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('./zoho/vendor/autoload.php');

use CristianPontes\ZohoCRMClient\ZohoCRMClient;


require_once('./application/helpers/general_helper.php');
require_once('./zoho_v2/vendor/autoload.php');

//require_once('./application/mailchimp/MCAPI.class.php');

class Crm_member_data extends CI_Controller
{
	public function __construct(){
		parent::__construct();

		$this->load->model('tomouh_model');
		$this->load->model('messages_model');
		$this->load->model('admin/admin_model');
		$this->load->library('session');
		$this->load->library('form_validation');
		ZCRMRestClient::initialize();
	}

	public function index(){

		$tbl = "tbl_crm_members_data";
		
		$data = $_POST;
		$member_id = $data['Member_id'];
		$email = $data['Email'];
		
		$handle = fopen('./zoho_called.txt','a+');
        $logtext = "******************".date('Y-m-d H:i:s')."******************\n\n";
             
        $logtext .= "========== parameter list ======== ";
            
        $logtext .= "<pre>".print_r($data, true)."</pre>";;
             
        $logtext .= "\n\n**********************************************************************\n\n";
        $errorlog = fwrite($handle,$logtext);
        fclose($handle);
		
		if(!empty($member_id)){

			$crm_data = $this->tomouh_model->getCrmMemberDataById($member_id);

			$moduleIns=ZCRMRestClient::getInstance()->getModuleInstance("Members");  //To get module instance
			$response=$moduleIns->searchRecordsByCriteria("(Email:equals:$email)");  //To get module records that match the criteria
			$memberData=$response->getData(); 
			$memberData = $memberData[0]->getData(); 



			
			$moduleIns =ZCRMRestClient::getInstance()->getModuleInstance("Contacts");
			$zcrmRecordContacts=array();
			// $zcrmRecordContact = ZCRMRecord::getInstance("Contacts");
			$zcrmRecordContact=ZCRMRecord::getInstance("Contacts",null); 

			$zcrmRecordContact->setFieldValue("Last_Name", $memberData['Last_Name']);
			$zcrmRecordContact->setFieldValue("Email",$memberData['Email']);
			$zcrmRecordContact->setFieldValue("First_Name",$memberData['Name']);
			$zcrmRecordContact->setFieldValue("Date_Of_Birth",date('Y-m-d', strtotime($memberData['Date_Of_Birth'])));
			$zcrmRecordContact->setFieldValue("Gender",$memberData['Gender']);

			$zcrmRecordContact->setFieldValue("Home_Country",$memberData['Home_Country']);
			$zcrmRecordContact->setFieldValue("Home_City",$memberData['Home_City']);
			$zcrmRecordContact->setFieldValue("Mobile_No",$memberData['Mobile_No']); 

			$zcrmRecordContact->setFieldValue("Country_Of_Residence",$memberData['Country_Of_Residence']);
			$zcrmRecordContact->setFieldValue("City_Of_Residence'",$memberData['City_Of_Residence']);
			$zcrmRecordContact->setFieldValue("Mobile_No_2",$memberData['Mobile_No_2']);
			$zcrmRecordContact->setFieldValue("GCC_or_Not'",$memberData['GCC_or_Not']);
			$zcrmRecordContact->setFieldValue("Label",$memberData['Label']);

			array_push($zcrmRecordContacts, $zcrmRecordContact);
			
			$bulkAPIResponse=$moduleIns->upsertRecords($zcrmRecordContacts); // $recordsArray - array of ZCRMRecord instances filled with required data for upsert.
			$entityResponses = $bulkAPIResponse->getEntityResponses();

			
			
			if(empty($crm_data)){

				$insert_array = array(
					'v_member_id'=>$data['Member_id'],
					'v_member_email'=>$data['Email'],
					'l_member_data'=>json_encode($data),
					'd_added'=>date('Y-m-d H:i:s'),
					'd_modified'=>date('Y-m-d H:i:s')
				);

				$add_contact_data = $this->admin_model->add_entry($insert_array,$tbl);	

			}else{

				$insert_array = array(
					'v_member_id'=>$data['Member_id'],
					'v_member_email'=>$data['Email'],
					'l_member_data'=>json_encode($data),
					'd_modified'=>date('Y-m-d H:i:s')
				);

				$this->admin_model->update_crm_member_entry($member_id,$insert_array,$tbl);
			}

			$user_data = $this->tomouh_model->getUserByMemberId($member_id);
			$geo = [];
			
			if(empty($user_data)){

				if(isset($data['Residence_city']) && isset($data['Residence_country']) && $data['Residence_country'] != '' && $data['Residence_city'] != ''){
					$address = $data['Residence_city'].' '.$data['Residence_country'];
				
					$google_api_key = $this->tomouh_model->getSetting('GOOGLE_MAP_KEY');
					$geo = $this->file_get_contents_curl('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key='.$google_api_key);

					$geo = json_decode($geo, true);
	
				}

				
				if (isset($geo['status']) && ($geo['status'] == 'OK')) {

					$latitude = $geo['results'][0]['geometry']['location']['lat'];
					$longitude = $geo['results'][0]['geometry']['location']['lng'];

				}else{
					$latitude = '';
					$longitude = '';
				}

				$member_insert = array(
		                            'v_email'                 => $memberData['Email'],
		                            'v_password'              => '',
		                            'v_firstname'             => $memberData['Name'],
		                            'v_lastname'              => $memberData['Last_Name'],
		                            'd_dob'                   => $memberData['Date_Of_Birth'],
		                            'e_gender'                => $memberData['Gender'],
		                            'v_home_country'          => $memberData['Home_Country'],
		                            'v_home_city'             => $memberData['Home_City'],
		                            'v_residence_country'     => $memberData['Country_of_Residence'],
		                            'v_residence_city'        => $memberData['City_Of_Residence'],
		                            'v_telephone'             => $memberData['Mobile_No'],
		                            'v_telephone_2'           => $memberData['Mobile_No_2'],
		                            'v_twitter_link '         => '',
		                            'v_linkedin_link'         => '',
		                            'v_instagram_link'        => '',
		                            'e_referral'              => '0',
		                            'v_referral_email'        => '',
		                            't_passion_interest'      => '',
		                            't_description'           => '',
		                            'v_main_occupation'       => '',
		                            'v_longitude'             => $longitude,
		                            'v_latitude'              => $latitude,
		                            'e_status'                => 'active',
		                            'e_member_of_month'       => 0,
		                            'v_subscription_id'       => '',
		                            'e_type'                  => 'member',
									'v_label'                  => $memberData['Label'],
		                            'e_plan_type'             => $memberData['Plan'] == 'Free' ? 'free' : 'paid',
		                            'v_crm_contact_id'        => '',
		                            'e_payment_type'          => '',
		                            'v_member_crm_id'		  => $data['Member_id'],	 
		                            'd_subscription_exp_date' => $memberData['Expired_On'],
	                            );
				
				$this->admin_model->add_entry($member_insert,"tbl_members");

				
				
				// mailchimp api call to subscribe start

				$merge_vars['EMAIL'] = $data['Email'];
				$merge_vars['FNAME'] = $data['Firstname'];
				$merge_vars['LNAME'] = $data['Lastname'];

				mailChimpSubscribe($merge_vars);
				
				// mailchimp api call to subscribe end			
			
			}else{

				$member_insert = array(

									'v_email'                 => $memberData['Email'],
		                            'v_firstname'             => $memberData['Name'],
		                            'v_lastname'              => $memberData['Last_Name'],
		                            'd_dob'                   => $memberData['Date_Of_Birth'],
		                            'e_gender'                => $memberData['Gender'],
		                            'v_home_country'          => $memberData['Home_Country'],
		                            'v_home_city'             => $memberData['Home_City'],
		                            'v_residence_country'     => $memberData['Country_of_Residence'],
		                            'v_residence_city'        => $memberData['City_Of_Residence'],
		                            'v_member_crm_id'		  => $data['Member_id'],
		                            'v_telephone'             => $memberData['Mobile_No'],
		                            'v_telephone_2'           => $memberData['Mobile_No_2'],
		                            'e_plan_type'		      => $memberData['Plan'] == 'Free' ? 'free' : 'paid',
									'v_label'                  => $memberData['Label'],
		                            'd_subscription_exp_date' => $memberData['Expired_On'],
								);
			
				$this->admin_model->update_members_data($member_id,$member_insert,"tbl_members");

				

				if($user_data['v_email'] != $data['Email']){

					mailChimpChangeEmail($data['Email'],$user_data['v_email']);
				}
					

			}

		}
			
			
		
		// exit();
	}

	public function delete_crm_member(){

		$data = $_POST;

		$this->admin_model->delete_crm_member($data['Member_id'],"tbl_crm_members_data");

		$members_data = $this->tomouh_model->getUserByMemberId($data['Member_id']);

		if( isset($members_data['id']) && $members_data['id'] != '' ){

				$this->tomouh_model->deleteUserAccount($members_data['id']);
		}

		if( isset($members_data['v_email']) && $members_data['v_email'] != '' ){

			$this->admin_model->delete_member_temp_data( $members_data['v_email'] , 'tbl_members_temp');

			$this->admin_model->delete_member_before_data( $members_data['v_email'] , 'tbl_before_member');
		}

		if( isset($members_data['v_crm_contact_id']) && $members_data['v_crm_contact_id'] != '' ){

			$this->admin_model->delete_crm_contact($members_data['v_crm_contact_id'],"tbl_crm_contact_data");
				
			$this->delete_zoho_crm_contact($members_data['v_crm_contact_id']);
			
		}
		if( isset($members_data['v_email']) && $members_data['v_email'] != '' ){
			
			$res = mailChimpDelete($members_data['v_email']);

			
    	}

		
		// exit();
	}

	public function delete_zoho_crm_contact( $contact_id ){

		//$cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');

		// $client = new ZohoCRMClient('Contacts', $cms_token,"eu");

        if(!empty($contact_id)){
        	
        	$zcrmRecordIns = ZCRMRecord::getInstance("Contacts", $contact_id); 
			$apiResponse=$zcrmRecordIns->delete();
        	/*
        	 $records = $client->deleteRecords()
			            ->id(
							$contact_id	
			            ) 
			            ->request(); */


        }  
	}

	public function file_get_contents_curl($url) {

	    $ch = curl_init();

	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_URL, $url);

	    $data = curl_exec($ch);
	    curl_close($ch);

	    return $data;
	}
}
?>