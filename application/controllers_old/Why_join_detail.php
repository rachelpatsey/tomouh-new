<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Why_join_detail extends CI_Controller
{
	public function __construct(){
		parent::__construct();	
		$this->load->model('tomouh_model');
		$this->load->model('messages_model');
		$this->load->library('session');
		$this->load->library('form_validation');
	}
	public function index($slug=''){
		
		$data['meta_title'] = 'Why Join Detail';
		$data['meta_keyword'] = 'Why Join Detail';
		$data['meta_description'] = 'Why Join Detail';
			
		$this->load->helper('url');

		$data['image'] = $this->tomouh_model->getMetafieldValue('image'.$slug);
		$data['text'] = $this->tomouh_model->getMetafieldValue('text'.$slug);
		$data['description'] = $this->tomouh_model->getMetafieldValue('description'.$slug);

		$this->load->view('why_join_detail',$data);
	}

}