<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller
{
	public function __construct(){
		parent::__construct();

		$this->load->model('messages_model');
		$this->load->model('tomouh_model');
		$this->load->library('session');
		$this->load->library('form_validation');
	}
	public function index(){

		$this->load->helper('url');

		if($this->session->has_userdata('logged_user')){
					redirect(base_url().'my_home');
					exit;
		}

		if($this->input->post()){

			// echo "<pre>";
			// print_r($this->input->post());exit();

			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$keep_logged_in = $this->input->post('keep_logged_in');

			$user['email'] = $email;

			$this->session->set_userdata($user);

			if($this->login($email,$password,$keep_logged_in)){

				$user_id  =$this->session->userdata("logged_user");

				if( $user_id != '' ){

					$results = $this->tomouh_model->getUserByUserId($user_id);

					if( $results['e_plan_type'] == 'paid' && strtotime($results['d_subscription_exp_date']) < strtotime(date('Y-m-d')) ){

						$user = array(
							'user' =>array(
								'email' => $results['v_email'],
								'is_member' => 1,
								)
							);

						$this->session->set_userdata($user);
						$this->session->unset_userdata('logged_user');
						redirect(base_url().'payment');
						exit;

					}
				}

				redirect(base_url().'my_home');
				exit;
			}else{

				redirect(base_url().'login?succ=0&msg=invalid_login');
				exit;
			}
		}

		$data = array();
		$data['email'] = $this->session->userdata('email')?$this->session->userdata('email'):'';

	    $data['meta_title'] = 'Login';
		$data['meta_keyword'] = 'Login';
		$data['meta_description'] = 'Login';

		$this->load->view('login',$data);
	}

	public function login($email, $password, $keep_logged_in) {
	    $user = $this->tomouh_model->loginCheck($email,$password);

	   if ($user) {

	   		 $logged_user = $user[0]['id'];

	   		 $login_data = $this->tomouh_model->getStatusOfUser($logged_user);

			  if($login_data['e_status'] == 'inactive'){
			  	redirect(base_url().'login?succ=0&msg=config_email');
			  	exit();
			  }


	         $user_data['logged_user'] = $logged_user;
	         $this->session->set_userdata($user_data);
			  
			  if($keep_logged_in != ''){
			  	setcookie('tomouh_logged_data', json_encode($user_data), time() + (86400 * 365), "/");
			  }

	         return true;
	   }else {
	         return false;
	   }
	}

	public function activation($code){

		if($code !=""){
			$user = $this->tomouh_model->getUserByMd5Email($code);
			$post["e_status"] = 'active';
			$active = $this->tomouh_model->activeAcc($code,$post);
			redirect(base_url().'login?succ=1&msg=useract');
		}else{
			redirect(base_url().'login');
			exit;
		}
	}
}