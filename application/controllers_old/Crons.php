<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
require_once('./application/helpers/general_helper.php');

class Crons extends CI_Controller{

	public function __construct(){

		parent::__construct();	
		$this->load->model('tomouh_model');

	}

	public function index(){
		// $template = $this->tomouh_model->getEmailTemplate(2);
		// 				$email_subject = $template['v_subject'];
		// 				$content = $template['l_body'];

		// 				echo "<pre>";
		// 				print_r($content);
		// 				exit;

		// $merge_vars['EMAIL'] = 'test5512@test.com';
		// $merge_vars['FNAME'] = 'test55';
		// $merge_vars['LNAME'] = 'test55';

		// mailChimpDelete('test111@test.com');
		// exit;


	}

	public function profile_update_notify($days =''){
		exit;
		
		if($days != ''){
			$after6month = date('Y-m-d', strtotime('+6 months'));
			$after10days = date('Y-m-d', strtotime($after6month.' -10 days'));
			$query = $this->db->query("SELECT * FROM tbl_members WHERE i_updated = 0 AND d_subscription_exp_date = '".$after10days."'");
			$results = $query->result_array();
		}else{
			$after6month = date('Y-m-d', strtotime('+6 months'));
			$query = $this->db->query("SELECT * FROM tbl_members WHERE d_subscription_exp_date = '".$after6month."'");
			$results = $query->result_array();
			$query = $this->db->query("UPDATE tbl_members SET i_updated=0, 'e_updated'= 'no' WHERE d_subscription_exp_date = '".$after6month."'");
		}
		
		if(!empty($results)){
		foreach($results as $member){
			
			$educational_data = $this->tomouh_model->getEducationalData($member['id']);
			$professional_data = $this->tomouh_model->getProfessionalData($member['id']);
			
			
			$v_name	= trim($member['v_firstname'].' '.$member['v_lastname']);
			$v_email = $member['v_email'];
			$v_city = $member['v_residence_city'];
			$v_telephone = $member['v_telephone'];
			$v_telephone_2 = $member['v_telephone_2'];
			$v_mobiles = $v_telephone.'<br>'.$v_telephone_2;
			
			$educational_html = '';
			$educational_html .='<table align="left" cellpadding="10" cellspacing="0" style="border:1px solid #333333;margin-bottom:20px;width: 100%;">
									<thead>
									  <tr>
										<th style="border-right:1px solid #333333;text-align:left;">University</th>
										<th style="border-right:1px solid #333333;text-align:left;">Degree</th>
										<th style="border-right:1px solid #333333;text-align:left;">Major</th>
										<th style="text-align=left;">Year</th>
									  </tr>
									</thead>
								<tbody>';
            if(!empty($educational_data)){	
				foreach($educational_data as $row){
				$educational_html .='<tr>
									<td style="border-top:1px solid #333333;border-right:1px solid #333333;">'.$row['v_university'].'</td>
									<td style="border-top:1px solid #333333;border-right:1px solid #333333;">'.$row['v_degree'].'</td>
									<td style="border-top:1px solid #333333;border-right:1px solid #333333;">'.$row['v_major'].'</td>
									<td style="border-top:1px solid #333333;">'.$row['i_passing_year'].'</td>
								  </tr>';
				}
			}else{
				$educational_html .='<tr>
                                <td colspan="4" style="border-top:1px solid #333333;text-align:center">No recirds found</td>
                                </tr>';
			}
           $educational_html .='</tbody></table>';
		   
		   $professional_html = '';
		   $professional_html .='<table align="left" cellpadding="10" cellspacing="0" style="border:1px solid #333333;margin-bottom:20px;width: 100%;">
									<thead>
									  <tr>
										<th style="border-right:1px solid #333333;text-align:left;">Company</th>
										<th style="border-right:1px solid #333333;text-align:left;">Job Title</th>
										<th style="border-right:1px solid #333333;text-align:left;">Industry</th>
										<th style="border-right:1px solid #333333;text-align:left;">From</th>
										<th style="text-align=left;">To</th>
									  </tr>
									</thead>
								<tbody>';
			if(!empty($professional_data)){					
            foreach($professional_data as $row){
            $professional_html .='<tr>
                                <td style="border-top:1px solid #333333;border-right:1px solid #333333;">'.$row['v_company'].'</td>
                                <td style="border-top:1px solid #333333;border-right:1px solid #333333;">'.$row['v_job_title'].'</td>
                                <td style="border-top:1px solid #333333;border-right:1px solid #333333;">'.$row['v_industry'].'</td>
								<td style="border-top:1px solid #333333;border-right:1px solid #333333;">'.$row['i_company_from'].'</td>
                                <td style="border-top:1px solid #333333;">'.$row['i_company_to'].'</td>
                              </tr>';
            }
		   }else{
		   	$professional_html .='<tr>
                                <td colspan="5" style="border-top:1px solid #333333;text-align:center">No recirds found</td>
                                </tr>';
		   }
           $professional_html .='</tbody></table>';
                          
			$email_to = $v_email;
			$email_from = "";

			$template = $this->tomouh_model->getEmailTemplate(10);
			$email_subject = $template['v_subject'];
			$content = $template['l_body'];

			$content = str_replace("V_NAME", $v_name, $content);
			$content = str_replace("V_EMAIL", $v_email, $content);
			$content = str_replace("V_CITY", $v_city, $content);
			$content = str_replace("V_MOBILES", $v_mobiles, $content);
			$content = str_replace("EDUCATION_INFO", $educational_html, $content);
			$content = str_replace("PROFESSIONAL_INFO", $professional_html, $content);
			$content = str_replace("../../../../", base_url(), $content);
			
			$sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content , $attachments = array() );
						
		}
		}
	}
	
	public function communication_notify(){

		// $before7days = date('Y-m-d');
		// echo $before7days; exit;
		$before7days = date('Y-m-d', strtotime('-7 days'));
		// $before7days = date('Y-m-d', strtotime($after6month.' -7 days'));
		$query = $this->db->query("SELECT * FROM tbl_communications WHERE d_added = '".$before7days."'");
		$results = $query->result_array();

		
		if(!empty($results)){
			foreach($results as $row){
				
				$user_data = $this->tomouh_model->getUserByUserId($row['i_from']);
				$v_name = trim($user_data["v_firstname"].' '.$user_data["v_lastname"]);
				
				$email_to = $user_data['v_email']; 
				$email_from = '';
				$template = $this->tomouh_model->getEmailTemplate(11);
				$email_subject = $template['v_subject'];
				$content = $template['l_body'];
				$content = str_replace("V_NAME", $v_name, $content);
				$content = str_replace("../../../../", base_url(), $content);
				
				$sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content , $attachments = array() );

				// $sent = $this->tomouh_model->sent_email('victoriahale.cis@gmail.com', $email_from, $email_subject, $content , $attachments = array() );
				// $sent = $this->tomouh_model->sent_email('shailesh.crestinfotech@gmail.com', $email_from, $email_subject, $content , $attachments = array() );

				// $handle = fopen('./cron_called.txt','a+');
		  //       $logtext = "******************".date('Y-m-d H:i:s')."******************\n\n";
		             
		  //       $logtext .= "========== parameter list ======== ";
		            
		  //       $logtext .= "<pre>".print_r($sent, true)."</pre>";;
		             
		  //       $logtext .= "\n\n**********************************************************************\n\n";
		  //       $errorlog = fwrite($handle,$logtext);
		  //       fclose($handle);
			
			}
		}
		
	}

	public function customer_notify(){

		mail("shailesh.crestinfotech@gmail.com","Inside customer_notify","Customer Notify cron run");
		mail("victoriahale.cis@gmail.com","Inside customer_notify","Customer Notify cron run");

		$before7days = date('Y-m-d', strtotime($after6month.' -7 days'));
		// $before7days = date('Y-m-d', strtotime('-7 days'));
		// echo $before7days; exit;
		// echo $before7days; exit;
		// $before7days = date('Y-m-d', strtotime($after6month.' -7 days'));
		$query = $this->db->query("SELECT * FROM tbl_communications WHERE d_added = '".$before7days."'");
		$results = $query->result_array();

		
		if(!empty($results)){
			foreach($results as $row){
				
				$user_data = $this->tomouh_model->getUserByUserId($row['i_from']);
				$v_name = trim($user_data["v_firstname"].' '.$user_data["v_lastname"]);
				
				$email_to = $user_data['v_email']; 
				$email_from = '';
				$template = $this->tomouh_model->getEmailTemplate(11);
				$email_subject = $template['v_subject'];
				$content = $template['l_body'];
				$content = str_replace("V_NAME", $v_name, $content);
				$content = str_replace("../../../../", base_url(), $content);
				
				// $sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content , $attachments = array() );

				$sent = $this->tomouh_model->sent_email('victoriahale.cis@gmail.com', $email_from, $email_subject, $content , $attachments = array() );
				$sent = $this->tomouh_model->sent_email('shailesh.crestinfotech@gmail.com', $email_from, $email_subject, $content , $attachments = array() );

				// $handle = fopen('./cron_called.txt','a+');
		  //       $logtext = "******************".date('Y-m-d H:i:s')."******************\n\n";
		             
		  //       $logtext .= "========== parameter list ======== ";
		            
		  //       $logtext .= "<pre>".print_r($sent, true)."</pre>";;
		             
		  //       $logtext .= "\n\n**********************************************************************\n\n";
		  //       $errorlog = fwrite($handle,$logtext);
		  //       fclose($handle);
			
			}
		}
		
	}

}