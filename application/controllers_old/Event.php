<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Event extends CI_Controller
{
	public function __construct(){
		parent::__construct();

		$this->load->model('tomouh_model');
		$this->load->model('messages_model');
		$this->load->library('session');
		$this->load->library('form_validation');
	}
	public function index(){
		
		$data['meta_title'] = 'Events';
		$data['meta_keyword'] = 'Events';
		$data['meta_description'] = 'Events';
			
		$this->load->helper('url');
		
		$this->load->view('event', $data);
	}
}