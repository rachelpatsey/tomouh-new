<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./zoho/vendor/autoload.php');
use CristianPontes\ZohoCRMClient\ZohoCRMClient;

require_once('./zoho_v2/vendor/autoload.php');

// require_once('./application/mailchimp/MCAPI.class.php');
require_once('./application/helpers/general_helper.php');

class Page extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->helper('url');
		$this->load->model('tomouh_model');
		$this->load->model('general_model');
		$this->load->model('messages_model');
		$this->load->model('admin/admin_model');
		$this->load->model('paging_model');
		$this->load->library('form_validation');
		$this->load->library('session');

		ZCRMRestClient::initialize();
	}

	public function view($slug=""){

		$data = array();

		if($_SERVER['REQUEST_METHOD']=='POST'){

			$btn_submit = $this->input->post("btn_submit");

			if($btn_submit && $btn_submit == 'CONTACT US'){

				$insert_data["v_user_name"] = $this->input->post("v_user_name");
				$insert_data["v_user_email"] = $this->input->post("v_user_email");
				$insert_data["l_user_message"] = $this->input->post("l_user_message");

				$contact_info =  $this->tomouh_model->getSetting('CONTACT_INFO');

				$tbl = "tbl_contact_us";
				$add_contact_data = $this->admin_model->add_entry($insert_data,$tbl);

				$email_to = $contact_info;
				$email_from ='';
				$email_subject = 'You have received new inquiry';
				$template = $this->tomouh_model->getEmailTemplate(5);

				$content = $template['l_body'];

				$email_content = '<p>Hello Admin,</p>
								  <p>You have received new inquiry. Please find below more informations.</p>';
				$email_content .= '<table cellpadding="10" align="left">
									<tr>
										<th align="left">Name</th>
										<td>'.$insert_data["v_user_name"].'</td>
									</tr>
									<tr>
										<th align="left">Email</th>
										<td>'.$insert_data["v_user_email"].'</td>
									</tr>
									<tr>
										<th align="left">Message</th>
										<td>'.$insert_data["l_user_message"].'</td>
									</tr>
								  </table>';

				$content = str_replace("EMAIL_CONTENT", $email_content, $content);
				// print_r($content); exit();
				$sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content , $attachments = array() );

				redirect(base_url().'contact_us/?succ=1&msg=inqsent');
				exit;
			}
		}

		$result = $this->tomouh_model->getPageBySlug($slug);
		$data = $result;
		$data['meta']= $this->tomouh_model->getPageMeta($result['id']);
		$data['meta_title'] = ($result['v_meta_title']) ? $result['v_meta_title'] : $result['v_title'];
		$data['meta_keyword'] = $result['v_meta_keyword'];
		$data['meta_description'] = $result['l_meta_description'];
		$data['main_description'] = $result['l_description'];
		$data['body_class'] = 'home page-'.$result['id'];
		$data['title'] = $result['v_title'];

		if($slug == 'about_us'){
			$data['address'] =  $this->tomouh_model->getSetting('ADDRESS');
			$this->load->view("about_us", $data);
		}elseif($slug == 'contact_us'){
			$data['contact_info'] =  $this->tomouh_model->getSetting('CONTACT_INFO');
			$this->load->view("contact_us", $data);
		}elseif($slug == 'faq'){
			$data['faqs'] = $this->tomouh_model->getFaqs();
			$this->load->view("faq", $data);
		}elseif($slug == 'my_home'){
			if($this->session->has_userdata('logged_user')){
				$data['books'] = $this->tomouh_model->getBooks();
				$data['members'] = $this->tomouh_model->memberWithProfessionData();
				$data['members_update'] = $this->tomouh_model->getMembersUpdate();

				$this->load->view("my_home", $data);
			}else{
				redirect(base_url().'login');
				exit();
			}
		}
		elseif($slug == 'why_join_tomouh'){
			$this->load->view("why_join", $data);
		}elseif($slug == 'initiative'){
			if($this->session->has_userdata('logged_user')){
				$data['initiative_data'] = $this->tomouh_model->getInitiativeData();
				$this->load->view("initiative", $data);
			}else{
				redirect(base_url().'login');
				exit();
			}
		}elseif($slug == 'membership_process'){
			$this->load->view("membership_process", $data);
		}elseif($slug == 'team'){
			$data['teams'] = $this->tomouh_model->getTomouhTeam('Team');
			$this->load->view("team", $data);
		}elseif($slug == 'honorary_member'){
			$data['honorary_members'] = $this->tomouh_model->getTomouhTeam('Honorary');
			$this->load->view("honorary_member", $data);
		}elseif($slug == 'membership_committee'){
			$data['memberships'] = $this->tomouh_model->getTomouhTeam('Membership');
			$this->load->view("membership_committee", $data);
		}elseif($slug == 'media'){

			$data['limit'] = $limit = 9;

			if ($this->input->get('page') && $this->input->get('page') !=''){

				$data['page'] = $this->input->get('page');
				$limitstart = $limit * ($this->input->get('page') - 1);

			}else{

				$data['page'] = 1;
				$limitstart = 0;
			}

			if ($this->input->get('type') && $this->input->get('type') !=''){

				$data['type'] = $type = $this->input->get('type');

			}else{

				$data['type'] = $type = 'image';
			}

			if($type == 'image'){

				$photo = $this->tomouh_model->getChildPhotosPagination(0,$limit,$limitstart);

				$data['total_row'] = $photo['total_row'];

				foreach($photo['result'] as $ph){
					$photo_id = $ph['id'];
					$category_name = $ph['v_name'];
					$image = $this->tomouh_model->getPhoto($photo_id);
					$img = $image[0]['v_image'];
					$photos[] = array(

						'id' => $photo_id,
						'v_slug' => $ph['v_slug'],
						'v_name' => $category_name,
						'v_image' =>$img,
					);
				}

				$data['photos'] = $photos;
				$data['videos'] = $this->tomouh_model->getvideosPagination($limit, 0);

			}else{

				$photo = $this->tomouh_model->getChildPhotosPagination(0,$limit,0);

				$data['total_row'] = $photo['total_row'];

				foreach($photo['result'] as $ph){
					$photo_id = $ph['id'];
					$category_name = $ph['v_name'];
					$image = $this->tomouh_model->getPhoto($photo_id);
					$img = $image[0]['v_image'];
					$photos[] = array(

						'id' => $photo_id,
						'v_slug' => $ph['v_slug'],
						'v_name' => $category_name,
						'v_image' =>$img,
					);
				}

				$data['photos'] = $photos;
				$data['videos'] = $this->tomouh_model->getvideosPagination($limit, $limitstart);

			}				
				$this->load->view("media", $data);

			}else{

				$this->load->view("page", $data);
			}
	}

	public function signup($slug="signup"){
		
		$result = $this->tomouh_model->getPageBySlug($slug);

		if($this->session->has_userdata('logged_user')){

			redirect(base_url().'profile');
			exit;
		}

		$data                     = $result;
		$data['meta']             = $this->tomouh_model->getPageMeta($result['id']);
		$data['meta_title']       = ($result['v_meta_title']) ? $result['v_meta_title'] : $result['v_title'];
		$data['meta_keyword']     = $result['v_meta_keyword'];
		$data['meta_description'] = $result['l_meta_description'];
		$data['main_description'] = $result['l_description'];
		$data['body_class']       = $slug.' page-'.$result['id'];
		$data['title']            = $result['v_title'];

		$crm_email = isset($_GET['ref'])?$_GET['ref']:'';

		if(!empty($crm_email)){

			$res = setcookie('user_step1', '', time() - 2592000, '/');

			$res = setcookie('user_step2', '', time() - 2592000, '/');

			$member_data = $this->tomouh_model->getBeforeMemberData($crm_email);
		

			$data['user_step1'] =  isset($member_data['l_user_step1_data']) ? json_decode($member_data['l_user_step1_data'] , true) : '';

			if(!empty($member_data)){


				$user['contact_id'] = $member_data['v_contact_crm_id'];
			
				$data['member'] = $this->getZohoCRMContactsById($member_data['v_contact_crm_id']);
				

				$this->session->set_userdata($user);
			
			}else{

				$data['member'] = $this->getZohoCRMContacts($crm_email,1);

				$user['contact_id'] = $data['member']['contact_id'];

				$this->session->set_userdata($user);

			}
		}else{

			$data['member'] = '';

			if(isset($_COOKIE['user_step1']) && !empty($_COOKIE['user_step1'])){

				$data['user_step1'] = json_decode($_COOKIE['user_step1'],true);
			}
		}

		$data['plan_type'] = isset($_GET['plan_type'])?$_GET['plan_type']:'';

		if($this->input->post()){

			$data = $this->input->post();
			
			setcookie('user_step1', json_encode($data), time() + (86400 * 30), "/");

			$password = $this->input->post('password');
			$confirm_password = $this->input->post('confirm_password');

			if($password != $confirm_password){
				redirect(base_url().'signup/?succ=0&msg=mispass');
				exit;
			}

			$email = $this->input->post('email');

			$results = $this->tomouh_model->getAllMembers();

		    foreach($results as $result){

		      $saved_email = $result['v_email'];

		      if($email == $saved_email){

		      		redirect(base_url().'signup/?succ=0&msg=sameemail');
					exit;
		      }

		    }

			$login_data['user'] = $this->input->post();
			$this->session->set_userdata($login_data);


				
			// echo "<pre>";print_r($this->input->post());exit;
			redirect(base_url().'signup_2');
			exit();
		}

		$data['countries'] = $this->tomouh_model->getCountry();

		

		$this->load->view('signup',$data);

	}
	public function signup_2($slug="signup"){
		
		if($this->session->has_userdata('logged_user')){

			redirect(base_url().'profile');
			exit;
		}

		$result = $this->tomouh_model->getPageBySlug($slug);

		$data                     = $result;
		$data['meta']             = $this->tomouh_model->getPageMeta($result['id']);
		$data['meta_title']       = ($result['v_meta_title']) ? $result['v_meta_title'] : $result['v_title'];
		$data['meta_keyword']     = $result['v_meta_keyword'];
		$data['meta_description'] = $result['l_meta_description'];
		$data['main_description'] = $result['l_description'];
		$data['body_class']       = $slug.' page-'.$result['id'];
		$data['title']            = $result['v_title'];

		$this->load->helper('url');

		$user_id  =$this->session->userdata("logged_user");

			if($this->input->post()){

				$data = $this->input->post();

				
				setcookie('user_step2', json_encode($data), time() + (86400 * 30), "/");

				$user_data = $this->session->userdata('user');
				

				
					if( isset($user_data['plan_type']) && $user_data['plan_type'] == 'free' ){

						$next_subscription_date = date('Y-m-d', strtotime('+1 year'));

				    	$address = $user_data['residence_city'].' '.$user_data['residence_country'];

				    	$google_api_key = $this->tomouh_model->getSetting('GOOGLE_MAP_KEY');

						$geo = $this->file_get_contents_curl('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key='.$google_api_key);

						$geo = json_decode($geo, true);

						if (isset($geo['status']) && ($geo['status'] == 'OK')) {

						  $latitude = $geo['results'][0]['geometry']['location']['lat'];
						  $longitude = $geo['results'][0]['geometry']['location']['lng'];

						}else{

							$latitude = '';
						  	$longitude = '';
						}

						$main_occupation_description = isset($data['main_occu'])?$data['main_occu']:'';

				    	$main_occupation_data = isset($data['occupation'])?$data['occupation']:'';

						$int_main_occupation = intval(preg_replace('/[^0-9]+/', '', $main_occupation_data), 10);

						$passion_interest = $data['passions_interests'];

						if($user_data['referral'] == 0){

							$referral_email = '';
						}else{

							$referral_email = $user_data['referral_email'];
						}

						/*
						email part
						*/

						$user = $user_data['email'];

						$email_to = $user;
						$email_from = "";

						$template = $this->tomouh_model->getEmailTemplate(2);
						$email_subject = $template['v_subject'];
						$content = $template['l_body'];

						$activation_link = '<a href="'.base_url().'login/activation/'.md5($user_data['email']).'" target="_blank">link here</a>';

						$content = str_replace("link here", $activation_link, $content);

						$sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content , $attachments = array() );
						$sent =1;
						if($sent){

								$insert_data = array(

									'v_email'   	 => $user_data['email'],
									'v_password' 	 => md5($user_data['password']),
									'v_firstname'	 => $user_data['firstname'],
									'v_lastname' 	 => $user_data['lastname'],
									'd_dob'      	 => date('Y-m-d', strtotime($user_data['dob'])),
									'e_gender'    	 => $user_data['gender'],
									'v_home_country' => $user_data['home_country'],
									'v_home_city'    => $user_data['home_city'],
									'v_residence_country' => $user_data['residence_country'],
									'v_residence_city'    => $user_data['residence_city'],
									'v_telephone' 	 => $user_data['tele_country_code'].'-'.$user_data['telephone'],
									'v_telephone_2'  => $user_data['tele_2_country_code'].'-'.$user_data['telephone_2'],
									'v_twitter_link '=> $user_data['twitter_link'],
									'v_linkedin_link'=> $user_data['linkedin_link'],
									'e_referral' 	 => $user_data['referral'],
									'v_referral_email' => $referral_email,
									't_passion_interest'  => isset($passion_interest)?$passion_interest:'',
									't_description'  => '',
									'v_main_occupation' => $main_occupation_description,
									'v_longitude'=>$longitude,
									'v_latitude'=>$latitude,
									'e_status'=>'inactive',
									'e_member_of_month'=>0,
									'v_subscription_id'=>'',
									'e_type'=>$user_data['type'],
									'v_label'=>'Member',
									'e_plan_type'=>'free',
									'v_crm_contact_id'=>$user_data['contact_id'],
									'e_payment_type'=>'',
									'd_subscription_exp_date'=>date("Y-m-d", strtotime('-1 day')),
								);

								$tbl = "tbl_members";
								$add_user_data = $this->admin_model->add_entry($insert_data,$tbl);

								$login_data['user_step2'] = $this->input->post();

								$this->session->set_userdata($login_data);

								$this->add_zoho_crm_plan($add_user_data);

								if($user_data['type'] == 'member'){

									$this->add_mailchimp($user_data['email'],$user_data['firstname'],$user_data['lastname']);
								}

								$educ_data_length = sizeof($data['university']);

								for($i=0;$i<$educ_data_length;$i++){
								  if(isset($data['university'][$i]) && !empty($data['university'][$i])){
									if($int_main_occupation <= 10){

										if(($i+1) == $int_main_occupation){
											$main_occupation = 1;
										}else{
											$main_occupation = 0;
										}
									}else{
										$main_occupation = 0;
									}

									$insert_array = array(

										'user_id'=>$add_user_data,
										'v_university' =>$data['university'][$i],
										'v_degree'=>$data['degree'][$i],
										'v_major'=>$data['major'][$i],
										'i_passing_year'=>$data['graduation_year'][$i],
										'i_main_occupation'=>$main_occupation
									);
									$tbl="tbl_educational_data";
									$this->admin_model->add_entry($insert_array,$tbl);
								  }
								}

								$company_data_length = sizeof($data['company']);

								for($i=0;$i<$company_data_length;$i++){

								 if(isset($data['company'][$i]) && !empty($data['company'][$i])){
									if($int_main_occupation >= 10){

										if(($i+11) == $int_main_occupation){
											$main_occupation = 1;
										}else{
											$main_occupation = 0;
										}
									}else{
										$main_occupation = 0;
									}

									$insert_array = array(

										'user_id'=>$add_user_data,
										'v_company' =>$data['company'][$i],
										'v_job_title'=>$data['job_title'][$i],
										'v_industry'=>$data['industry'][$i],
										'i_company_from'=>$data['company_from'][$i],
										'i_company_to'=>$data['company_to'][$i],
										'i_main_occupation'=>$main_occupation
									);
									$tbl="tbl_professional_data";
									$this->admin_model->add_entry($insert_array,$tbl);
								  }
								}

								$award_data_length = sizeof($data['award_details']);

								for($i=0;$i<$award_data_length;$i++){

								   if(isset($data['award_details'][$i]) && !empty($data['award_details'][$i])){
									$insert_array = array(

										'user_id'=>$add_user_data,
										'v_achievement' =>$data['award_details'][$i],
									);
									$tbl="tbl_achievements_data";
									$this->admin_model->add_entry($insert_array,$tbl);

								   }
								}

								// $member_type = $user_data['type'];

								// if($member_type == 'member'){

								// 	$contact_id = $user_data['contact_id'];

								// 	$this->updateContactLabel($contact_id);

								// }

								$this->session->unset_userdata('user');

								$res = setcookie('user_step1', '', time() - 2592000, '/');

	                        	$res = setcookie('user_step2', '', time() - 2592000, '/');

								redirect(base_url().'login?succ=1&msg=notverify');
					  			exit();
							}


					}elseif( isset($user_data['contact_id']) && $user_data['contact_id'] == '' ){

				    	$before_array = array(
				    			'v_email'			=> $user_data['email'],
				    			'l_user_step1_data' => json_encode($user_data),
				    			'l_user_step2_data'	=> json_encode($data),
				    			'd_added' 			=> date('Y-m-d H:i:s'),
				    				);

				    	$inserted_id = $this->admin_model->add_entry( $before_array , 'tbl_before_member' );
						
						$user_data['lead_source'] ='Via Website';
						  
				    	$this->addZohoContactMember( $user_data, $data, $inserted_id );

				    	$res = setcookie('user_step1', '', time() - 2592000, '/');

						$res = setcookie('user_step2', '', time() - 2592000, '/');

						$this->session->unset_userdata('user');
						
						$this->session->unset_userdata('user_step2');

				    	redirect(base_url().'signup_success');
						exit;

					}else{

						$login_data['user_step2'] = $this->input->post();

						$this->session->set_userdata($login_data);

						$member_type = $user_data['type'];

						$us_data['last_page'] = "signup_2";

		         		$this->session->set_userdata($us_data);

							redirect(base_url().'payment');
							exit;
					}
			}

		$login_data = $this->session->userdata('user');

		if(empty($login_data)){

			redirect(base_url().'signup');
			exit;

		}else{

			if(!isset($user_id)){

				$user_id = 0;
			}

			$user_email = isset($login_data['email']) ? $login_data['email'] : '';

			if(!empty($user_email)){

				 $edu_pro_data = $this->tomouh_model->getBeforeMemberData($user_email);

				 $data['user_step2'] = isset($edu_pro_data['l_user_step2_data']) ? json_decode($edu_pro_data['l_user_step2_data'] , true) : '';
			}else{

				if(isset($_COOKIE['user_step2']) && !empty($_COOKIE['user_step2'])){

					$data['user_step2'] = json_decode($_COOKIE['user_step2'],true);

				}
			}
			 
			$data['industry_data'] = $this->tomouh_model->getIndustryActiveData();

		}

		$this->load->view('signup_2',$data);
	}
	public function edit_profile_1($slug="edit_profile_1"){


		$data['meta_title'] = "Edit Profile";
		$data['meta_keyword'] = "Edit Profile";
		$data['meta_description'] = "Edit Profile";

		$user_id  =$this->session->userdata("logged_user");

		if(!$this->session->has_userdata('logged_user')){

			redirect(base_url().'login/?succ=0&msg=logfirst');
			exit;
		}

		if($this->input->post()){

				$data = $this->input->post();

				$user_data = $this->tomouh_model->getUserByUserId($user_id);

				$email = $user_data['v_email'];

				$results = $this->tomouh_model->getAllMembers();

				if($email != $data['email']){

					mailChimpChangeEmail($data['email'],$email);

				    foreach($results as $result){

				      $saved_email = $result['v_email'];

				      if($data['email'] == $saved_email){

				      		redirect(base_url().'edit_personal_info/?succ=0&msg=sameemail');
							exit;
				      }
				 }

				}

				$date_of_birth = date('Y-m-d', strtotime($data['dob']));

				if($data['referral'] == 0){

					$referral_email = '';
				}else{

					$referral_email = $data['referral_email'];
				}

				$address = $data['residence_city'].' '.$data['residence_country'];

				$google_api_key = $this->tomouh_model->getSetting('GOOGLE_MAP_KEY');
				
				$geo = $this->file_get_contents_curl('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key='.$google_api_key);

				$geo = json_decode($geo, true);

				if (isset($geo['status']) && ($geo['status'] == 'OK')) {

					$latitude = $geo['results'][0]['geometry']['location']['lat'];
					$longitude = $geo['results'][0]['geometry']['location']['lng'];

				}else{
					$latitude = '';
					$longitude = '';
				}

				$update_data = array(

						'v_email'   	 => $data['email'],
						'v_firstname'	 => $data['firstname'],
						'v_lastname' 	 => $data['lastname'],
						'd_dob'      	 => $date_of_birth,
						'e_gender'    	 => $data['gender'],
						'v_home_country' => $data['home_country'],
						'v_home_city'    => $data['home_city'],
						'v_residence_country' => $data['residence_country'],
						'v_residence_city'    => $data['residence_city'],
						'v_telephone' 	 => $data['tele_country_code'].'-'.$data['telephone'],
						'v_telephone_2'  => $data['tele_2_country_code'].'-'.$data['telephone_2'],
						'v_twitter_link '=> $data['twitter_link'],
						'v_linkedin_link'=> $data['linkedin_link'],
						'v_instagram_link'=> $data['instagram_link'],
						'e_referral'     => $data['referral'],
						'v_referral_email'=> $referral_email,
						'v_longitude'=>$longitude,
						'v_latitude'=>$latitude,
						'i_updated'=>1,
						'e_updated'=>'yes',
					);

					$tbl = "tbl_members";
					$this->admin_model->update_entry($user_id,$update_data,$tbl);


					$this->update_crm_detail_step1($data, $user_id);

					redirect(base_url().'profile');
					exit();
				}


		$user_data = $this->tomouh_model->getUserByUserId($user_id);
		//echo "<pre>"; print_r($user_data); exit;
		$data['user_data'] = $user_data;
		$data['countries'] = $this->tomouh_model->getCountry();
		$this->load->view('edit_profile_1',$data);

	}

	public function edit_profile_2($slug="edit_profile_2"){

		$data = array();

		$data['meta_title'] = "Edit Profile";
		$data['meta_keyword'] = "Edit Profile";
		$data['meta_description'] = "Edit Profile";

		$user_id  =$this->session->userdata("logged_user");

		if(!$this->session->has_userdata('logged_user')){

			redirect(base_url().'login/?succ=0&msg=logfirst');
			exit;
		}

		$this->load->helper('url');

		if(isset($this->session->userdata["logged_user"])){

			$user_data = $this->tomouh_model->getUserByUserId($user_id);

			$usr_data['user'] = $user_data;

			$this->session->set_userdata($usr_data);

			}

		$data['educational_data'] = $edu_data = $this->tomouh_model->getEducationalData($user_id);

		$data['professional_data'] = $prof_data = $this->tomouh_model->getProfessionalData($user_id);

		$data['achievement_data'] = $achi_data = $this->tomouh_model->getAchievementalData($user_id);

	    $user_data = $this->tomouh_model->getUserByUserId($user_id);

	    $data['industry_data'] = $this->tomouh_model->getIndustryActiveData();

		if($this->input->post()){

					$data = $this->input->post();

					$data['edu_row_id'] = isset($data['edu_row_id']) ? $data['edu_row_id'] : array('0');

					if(isset($data['edu_row_id'])){

						foreach($edu_data as $row){

							if(!in_array($row['id'], $data['edu_row_id'])){

								$this->admin_model->delete_entry($row['id'],"tbl_educational_data");
							}

						}
					}
					
					$data['pro_row_id'] = isset($data['pro_row_id']) ? $data['pro_row_id'] : array('0');

					if(isset($data['pro_row_id'])){

						foreach($prof_data as $row){

							if(!in_array($row['id'], $data['pro_row_id'])){

								$this->admin_model->delete_entry($row['id'],"tbl_professional_data");
							}
						}
					}

					$data['achi_row_id'] = isset($data['achi_row_id']) ? $data['achi_row_id'] : array('0');

					if(isset($data['achi_row_id'])){
						
						foreach($achi_data as $row){

							if(!in_array($row['id'], $data['achi_row_id'])){

								$this->admin_model->delete_entry($row['id'],"tbl_achievements_data");
							}
						}

					}


					$main_occupation_data = $this->input->post('occupation');
					
					$int_main_occupation = intval(preg_replace('/[^0-9]+/', '', $main_occupation_data), 10);

					$passion_interest_data = array(

								't_passion_interest'=>$data['passions_interests'],
								'v_main_occupation'=>$data['main_occu'],
								'i_updated'=>1,
								'e_updated'=>'yes',
								);

					$tbl = "tbl_members";
					$this->admin_model->update_entry($user_id,$passion_interest_data,$tbl);


					$educ_data_length = sizeof($data['university']);

					for($i=0;$i<$educ_data_length;$i++){

					  if(isset($data['university'][$i]) && !empty($data['university'][$i])){

						if($int_main_occupation <= 10){

							if(($i+1) == $int_main_occupation){
								$main_occupation = 1;
							}else{
								$main_occupation = 0;
							}
						}else{
							$main_occupation = 0;
						}

						if(!empty($data['edu_row_id'][$i])){

							$update_array = array(

							'v_university' =>$data['university'][$i],
							'v_degree'=>$data['degree'][$i],
							'v_major'=>$data['major'][$i],
							'i_passing_year'=>$data['graduation_year'][$i],
							'i_main_occupation'=>$main_occupation
						);

						$tbl="tbl_educational_data";
						$this->admin_model->update_entry($data['edu_row_id'][$i],$update_array,$tbl);

						}else{

							$insert_array = array(

							'user_id'=>$user_id,
							'v_university' =>$data['university'][$i],
							'v_degree'=>$data['degree'][$i],
							'v_major'=>$data['major'][$i],
							'i_passing_year'=>$data['graduation_year'][$i],
							'i_main_occupation'=>$main_occupation
						);
						$tbl="tbl_educational_data";
						$this->admin_model->add_entry($insert_array,$tbl);
						}
					  }
					}

					$prof_data_length = sizeof($data['company']);

					for($i=0;$i<$prof_data_length;$i++){
					 if(isset($data['company'][$i]) && !empty($data['company'][$i])){

						if($int_main_occupation >= 10){

							if(($i+11) == $int_main_occupation){
								$main_occupation = 1;
							}else{
								$main_occupation = 0;
							}
						}else{
							$main_occupation = 0;
						}

						if(!empty($data['pro_row_id'][$i])){

							$update_array = array(

							'v_company' =>$data['company'][$i],
							'v_job_title'=>$data['job_title'][$i],
							'v_industry'=>$data['industry'][$i],
							'i_company_from'=>$data['company_from'][$i],
							'i_company_to'=>$data['company_to'][$i],
							'i_main_occupation'=>$main_occupation
						);

						$tbl="tbl_professional_data";
						$this->admin_model->update_entry($data['pro_row_id'][$i],$update_array,$tbl);

						}else{

							$insert_array = array(

							'user_id'           => $user_id,
							'v_company'         => $data['company'][$i],
							'v_job_title'       => $data['job_title'][$i],
							'v_industry'        => $data['industry'][$i],
							'i_company_from'    => $data['company_from'][$i],
							'i_company_to'      => $data['company_to'][$i],
							'i_main_occupation' => $main_occupation
						);
						$tbl="tbl_professional_data";
						$this->admin_model->add_entry($insert_array,$tbl);
						}
					  }
					}

					$achie_data_length = sizeof($data['award_details']);

					for($i=0;$i<$achie_data_length;$i++){

					  if(isset($data['award_details'][$i]) && !empty($data['award_details'][$i])){

						if(!empty($data['achi_row_id'][$i])){

							$update_array = array(
							'v_achievement' =>$data['award_details'][$i],
						);

						$tbl="tbl_achievements_data";
						$this->admin_model->update_entry($data['achi_row_id'][$i],$update_array,$tbl);

						}else{

							$insert_array = array(

							'user_id'=>$user_id,
							'v_achievement' =>$data['award_details'][$i],
						);
						$tbl="tbl_achievements_data";
						$this->admin_model->add_entry($insert_array,$tbl);

						}
					 }
					}

					$this->update_crm_detail_step2($data, $user_id);

					redirect(base_url().'profile');
					exit;

				}

			$data['t_passion_interest'] = isset($user_data['t_passion_interest'])?$user_data['t_passion_interest']:'';
			$data['v_main_occupation'] = isset($user_data['v_main_occupation'])?$user_data['v_main_occupation']:'';

			$this->load->view('edit_profile_2',$data);
	}

	public function isValidEmail($email){ 
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

	public function payment_success($slug="payment_success"){

			$data = array();

			$response = $_POST;

			if(isset($response) && !empty($response)){
				
				$sub_id = $response['subscr_id'];

				if(!empty($sub_id)){

				$sub_data = $this->tomouh_model->getSubscriptionBySubId($sub_id);

	            $sub_date = isset($sub_data['d_subscription_date'])?$sub_data['d_subscription_date']:'';

	            $original_date = '';

                if($sub_date != ''){

                    $original_date = date('Y-m-d', strtotime($sub_date));
                }

				$original_date='2018-05-10';
				
	            if(isset($original_date) && !empty($original_date) && strtotime($original_date) >= strtotime(date('Y-m-d') .' -1 day')){

	                
	            }else{

	            $member = $this->tomouh_model->getUserBySubscriptionId($sub_id);

	            if(!empty($member)){

	                $subscription_array = array(
	                        'user_id'=>$member['id'],
	                        'v_customer_id'=>$response['payer_id'],
	                        'v_subscription_id'=>$sub_id,
	                        'l_subscription_data'=>json_encode($response),
	                        'd_subscription_date'=>date("Y-m-d H:i:s"),
	                        'e_payment_type'=>'paypal',
	                    );
	                $this->admin_model->add_entry($subscription_array,"tbl_subscription_data");

	                $this->update_zoho_crm($member['id'], $sub_id);

	                $update_member = array(
	                        'd_subscription_exp_date'=>date('Y-m-d', strtotime('+1 year')),
	                        );

	                $this->admin_model->update_entry($member['id'],$update_member,"tbl_members");

	            }else{

            	$is_member = $this->isValidEmail($response['custom']);

                    if( $is_member == 1 ){

                        $mem_data = $this->tomouh_model->getUserByEmail($response['custom']);

                        if($mem_data){

                            $subscription_array = array(
                                    'user_id'=>$mem_data['id'],
                                    'v_customer_id'=>$response['payer_id'],
                                    'v_subscription_id'=>$sub_id,
                                    'l_subscription_data'=>json_encode($response),
                                    'd_subscription_date'=>date("Y-m-d H:i:s"),
                                    'e_payment_type'=>'paypal',
                                );
                            $this->admin_model->add_entry($subscription_array,"tbl_subscription_data");

                            $update_member = array(
                                    'e_plan_type' => 'paid',
                                    'd_subscription_exp_date'=>date('Y-m-d', strtotime('+1 year')),
                                    );

                            $this->admin_model->update_entry($mem_data['id'],$update_member,"tbl_members");

                            $this->update_zoho_crm($mem_data['id'], $sub_id);

                            if( isset($mem_data['e_status']) && $mem_data['e_status'] == 'active' ){

                            	$data['is_member'] = 1;

                            }else{

                            	$data['is_member'] = 0;

                            }
                        }

                    }else{

	                $member_temp_data = $this->tomouh_model->getTempUserById($response['custom']);

	                $user_data = json_decode($member_temp_data['l_user_step1_data'],true);

	                $data = json_decode($member_temp_data['l_user_step2_data'],true);


	                        if(isset($user_data['residence_city'])){

	                            $address = $user_data['residence_city'].' '.$user_data['residence_country'];

	                            $google_api_key = $this->tomouh_model->getSetting('GOOGLE_MAP_KEY');
								
								$geo = $this->file_get_contents_curl('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key='.$google_api_key);
								
	                            $geo = json_decode($geo, true);

	                            if (isset($geo['status']) && ($geo['status'] == 'OK')) {

	                              $latitude = $geo['results'][0]['geometry']['location']['lat'];
	                              $longitude = $geo['results'][0]['geometry']['location']['lng'];

	                            }else{

	                                $latitude = '';
	                                $longitude = '';
	                            }
	                        }else{
	                                $latitude = '';
	                                $longitude = '';
	                        }

	                        $main_occupation_description = isset($data['main_occu'])?$data['main_occu']:'';

	                        $main_occupation_data = isset($data['occupation'])?$data['occupation']:'';

	                        $int_main_occupation = intval(preg_replace('/[^0-9]+/', '', $main_occupation_data), 10);

	                        $passion_interest = $data['passions_interests'];

	                        if($user_data['referral'] == 0){

	                            $referral_email = '';
	                        }else{

	                            $referral_email = $user_data['referral_email'];
	                        }

	                        $user = $user_data['email'];

	                        $email_to = $user;
	                        $email_from = "";

	                        $template = $this->tomouh_model->getEmailTemplate(2);
	                        $email_subject = $template['v_subject'];
	                        $content = $template['l_body'];

	                        $activation_link = '<a href="'.base_url().'login/activation/'.md5($user_data['email']).'" target="_blank">link here</a>';

	                        $content = str_replace("link here", $activation_link, $content);

	                        $sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content , $attachments = array() );

	                        if($sent){

	                            $insert_data = array(
		                            'v_email'                 => $user_data['email'],
		                            'v_password'              => md5($user_data['password']),
		                            'v_firstname'             => $user_data['firstname'],
		                            'v_lastname'              => $user_data['lastname'],
		                            'd_dob'                   => date('Y-m-d', strtotime($user_data['dob'])),
		                            'e_gender'                => $user_data['gender'],
		                            'v_home_country'          => $user_data['home_country'],
		                            'v_home_city'             => $user_data['home_city'],
		                            'v_residence_country'     => $user_data['residence_country'],
		                            'v_residence_city'        => $user_data['residence_city'],
		                            'v_telephone'             => $user_data['tele_country_code'].'-'.$user_data['telephone'],
		                            'v_telephone_2'           => $user_data['tele_2_country_code'].'-'.$user_data['telephone_2'],
		                            'v_twitter_link '         => $user_data['twitter_link'],
		                            'v_linkedin_link'         => $user_data['linkedin_link'],
		                            'v_instagram_link'        => $user_data['instagram_link'],
		                            'e_referral'              => $user_data['referral'],
		                            'v_referral_email'        => $referral_email,
		                            't_passion_interest'      => isset($passion_interest)?$passion_interest:'',
		                            't_description'           => '',
		                            'v_main_occupation'       => isset($main_occupation_description)?$main_occupation_description:'',
		                            'v_longitude'             => $longitude,
		                            'v_latitude'              => $latitude,
		                            'e_status'                => 'inactive',
		                            'e_member_of_month'       => 0,
		                            'v_subscription_id'       => $sub_id,
		                            'e_type'                  => $user_data['type'],
									'v_label'             	  => 'Member',
		                            'e_plan_type'             => 'paid',
		                            'v_crm_contact_id'        => $user_data['contact_id'],
		                            'e_payment_type'          => 'paypal',
		                            'd_subscription_exp_date' => date('Y-m-d', strtotime('+1 year')),
	                            );

	                        $tbl = "tbl_members";
	                        $add_user_data = $this->admin_model->add_entry($insert_data,$tbl);

	                        $this->add_zoho_crm($add_user_data,$response['custom']);

	                        if($user_data['type'] == 'member'){

	                            $this->add_mailchimp($user_data['email'],$user_data['firstname'],$user_data['lastname']);
	                        }

	                        $subscription_array = array(
	                                'user_id'=>$add_user_data,
	                                'v_customer_id'=>$response['payer_id'],
	                                'v_subscription_id'=>$sub_id,
	                                'l_subscription_data'=>json_encode($response),
	                                'd_subscription_date'=>date("Y-m-d H:i:s"),
	                                'e_payment_type'=>'paypal',
	                            );
	                        $this->admin_model->add_entry($subscription_array,"tbl_subscription_data");

	                        $educ_data_length = sizeof($data['university']);

	                        for($i=0;$i<$educ_data_length;$i++){
	                          if(isset($data['university'][$i]) && !empty($data['university'][$i])){
	                            if($int_main_occupation <= 10){

	                                if(($i+1) == $int_main_occupation){
	                                    $main_occupation = 1;
	                                }else{
	                                    $main_occupation = 0;
	                                }
	                            }else{
	                                $main_occupation = 0;
	                            }

	                            $insert_array = array(

	                                'user_id'=>$add_user_data,
	                                'v_university' =>$data['university'][$i],
	                                'v_degree'=>$data['degree'][$i],
	                                'v_major'=>$data['major'][$i],
	                                'i_passing_year'=>$data['graduation_year'][$i],
	                                'i_main_occupation'=>$main_occupation
	                            );
	                            $tbl="tbl_educational_data";
	                            $this->admin_model->add_entry($insert_array,$tbl);
	                          }
	                        }

	                        $company_data_length = sizeof($data['company']);

	                        for($i=0;$i<$company_data_length;$i++){

	                         if(isset($data['company'][$i]) && !empty($data['company'][$i])){
	                            if($int_main_occupation >= 10){

	                                if(($i+11) == $int_main_occupation){
	                                    $main_occupation = 1;
	                                }else{
	                                    $main_occupation = 0;
	                                }
	                            }else{
	                                $main_occupation = 0;
	                            }

	                            $insert_array = array(

	                                'user_id'=>$add_user_data,
	                                'v_company' =>$data['company'][$i],
	                                'v_job_title'=>$data['job_title'][$i],
	                                'v_industry'=>$data['industry'][$i],
	                                'i_company_from'=>$data['company_from'][$i],
	                                'i_company_to'=>$data['company_to'][$i],
	                                'i_main_occupation'=>$main_occupation
	                            );
	                            $tbl="tbl_professional_data";
	                            $this->admin_model->add_entry($insert_array,$tbl);
	                          }
	                        }

	                        $award_data_length = sizeof($data['award_details']);

	                        for($i=0;$i<$award_data_length;$i++){

	                           if(isset($data['award_details'][$i]) && !empty($data['award_details'][$i])){
	                            $insert_array = array(

	                                'user_id'=>$add_user_data,
	                                'v_achievement' =>$data['award_details'][$i],
	                            );
	                            $tbl="tbl_achievements_data";
	                            $this->admin_model->add_entry($insert_array,$tbl);

	                           		}
	                           	}
	                        }
	                      }
	                    }
	                }
	            }
	    	}

			$data['meta_title'] = "Payment Success Page";
			$data['meta_keyword'] = "Payment Success Page";
			$data['meta_description'] = "Payment Success Page";

			$res = setcookie('user_step1', '', time() - 2592000, '/');

           	$res = setcookie('user_step2', '', time() - 2592000, '/');

			$this->session->unset_userdata('user');
		    $this->session->unset_userdata('user_step2');

			$this->load->view('payment_success',$data);
	}

	public function logout(){
		$this->session->unset_userdata('logged_user');
		if(isset($_COOKIE['tomouh_logged_data']) && !empty($_COOKIE['tomouh_logged_data'])){
		  setcookie('tomouh_logged_data', '', time() - 31536000, '/');
		}
		redirect(base_url().'login/?succ=1&msg=logout');
		exit;
	}

	public function getZohoCRMContacts($email,$index){
	
		$zcrmModuleIns = ZCRMModule::getInstance("Contacts");
		$bulkAPIResponse=$zcrmModuleIns->getRecords('','Modified_Time','desc',$index,$index+199);
						
		$recordsArray = $bulkAPIResponse->getData();

		if(!empty($recordsArray)){
			foreach ($recordsArray as $rcad) {
				$theRsp = $rcad->getData();
				if(isset($theRsp['Email'])){
					if($theRsp['Email'] == $email){
						$data_members['member'] = $theRsp;
						$data_members['contact_id'] = $rcad->getEntityId();
						return $data_members;
					}
				}
			}
		}else{

			return 0;
		}

		$index = $index+200;

		$result = $this->getZohoCRMContacts($email,$index);

		if(!empty($result)){

			return $result;
		}
		
		// $cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');

		// $client = new ZohoCRMClient('Contacts', $cms_token, "eu");
		// $records = $client->getRecords()
		// 	    ->fromIndex($index)
		// 	    ->toIndex($index+199)
		// 	    ->sortBy('Modified Time')
		// 	    ->sortDesc()
		// 	    ->request();

		// if(!empty($records)){

		// 	foreach($records as $record){
		// 		echo "<pre>";
		// 			print_r($record->data);exit;

		// 		if(isset($record->data['Email'])){
					

		// 			if($record->data['Email'] == $email){

		// 				$data_members = $record->data;

						

		// 				return $data_members;

		// 			}
		// 		}
		// 	}
		// }else{

		// 		return 0;
		// }

		// $index = $index+200;

		// $result = $this->getZohoCRMContacts($email,$index);

		// if(!empty($result)){

		// 	return $result;
		// }
	}

	public function add_zoho_crm($user_id,$temp_user_id){

        $member_data = $this->tomouh_model->getUserByUserId($user_id);

        $payment_data_str = 'Payment Type : '.$member_data['e_payment_type'].', Subscription Id : '.$member_data['v_subscription_id'].', Activation Date : '.date('Y-m-d').', Expiration Date : '.$member_data['d_subscription_exp_date'];

        $member_temp_data = $this->tomouh_model->getTempUserById($temp_user_id);

        $user_step1 = json_decode($member_temp_data['l_user_step1_data'],true);

        $user_step2 = json_decode($member_temp_data['l_user_step2_data'],true);

        if(isset($user_step2['university']) && !empty($user_step2['university'])){

            $educ_data_length = sizeof($user_step2['university']);

            $education_info = array();

            for($i=0;$i<$educ_data_length;$i++){

                if(isset($user_step2['university'][$i]) && !empty($user_step2['university'][$i])){

                $education_info[] = 'University : '.$user_step2['university'][$i].', Degree : '.$user_step2['degree'][$i].', Major : '.$user_step2['major'][$i].', Year : '.$user_step2['graduation_year'][$i];

                }
            }
        }

        if(isset($user_step2['company']) && !empty($user_step2['company'])){

            $company_data_length = sizeof($user_step2['company']);

            $experience_info = array();

            for($i=0;$i<$company_data_length;$i++){

                if(isset($user_step2['company'][$i]) && !empty($user_step2['company'][$i])){

                $experience_info[] = 'Company : '.$user_step2['company'][$i].', Job Title : '.$user_step2['job_title'][$i].', Industry : '.$user_step2['industry'][$i].', Period : '.$user_step2['company_from'][$i].'-'.$user_step2['company_to'][$i];

                }
            }
        }

        if(isset($user_step2['award_details']) && !empty($user_step2['award_details'])){

            $award_data_length = sizeof($user_step2['award_details']);

            $award_detail = '';

            for($i=0;$i<$award_data_length;$i++){

                if(isset($user_step2['award_details'][$i]) && !empty($user_step2['award_details'][$i])){

                 $award_detail .= $user_step2['award_details'][$i].',';

                }
            }
        }

        if(isset($award_detail)){

            $AAH = rtrim($award_detail,',');

        }else{
            $AAH = '';
        }

        $educational_info = implode(' %0A ',$education_info);

        $exp_info = implode(' %0A ', $experience_info);

        $cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');

        // insert into member
		
		//to get the instance of the module
		$moduleMemberIns=ZCRMRestClient::getInstance()->getModuleInstance("Members");

		$recordsMember=array();
		//To get ZCRMRecord instance
		$recordMember=ZCRMRecord::getInstance("Members",null); 
		//This function use to set FieldApiName and value similar to all other FieldApis and Custom field
		$recordMember->setFieldValue("Last_Name",$user_step1['lastname']);
		$recordMember->setFieldValue("Email",$user_step1['email']);
		$recordMember->setFieldValue("Name",$user_step1['firstname']);
		$recordMember->setFieldValue("Date_Of_Birth",date('Y-m-d', strtotime($user_step1['dob'])));
		$recordMember->setFieldValue("Gender",$user_step1['gender']);
		$recordMember->setFieldValue("Expired_On",date('m/d/Y',strtotime('+1 year')));
		$recordMember->setFieldValue("Home_Country",$user_step1['home_country']);
		$recordMember->setFieldValue("Home_City",$user_step1['home_city']);
		$recordMember->setFieldValue("Mobile_No",$user_step1['tele_country_code'].'-'.$user_step1['telephone']); 
		$recordMember->setFieldValue("Country_of_Residence",$user_step1['residence_country']);
		$recordMember->setFieldValue("City_Of_Residence'",$user_step1['residence_city']);
		$recordMember->setFieldValue("Mobile_No_2",$user_step1['tele_2_country_code'].'-'.$user_step1['telephone_2']);
		$recordMember->setFieldValue("Educational_Info_1",isset($educational_info)? urldecode($educational_info):''); 
		$recordMember->setFieldValue("Experience_Info_1",isset($exp_info)? urldecode($exp_info):'');
		$recordMember->setFieldValue("AAH",$AAH);
		$recordMember->setFieldValue("Passions_and_interests",$user_step2['passions_interests']);
		$recordMember->setFieldValue("Subscription_Info",$payment_data_str);
		$recordMember->setFieldValue("Status'","Active");
		$recordMember->setFieldValue("Plan","Paid");
		$recordMember->setFieldValue("Label","Member");

		array_push($recordsMember, $recordMember);//pushing the record to the array 
		$responseIn=$moduleMemberIns->createRecords($recordsMember);
		$responseIn = $responseIn->getEntityResponses();
		$responseIn = $responseIn[0]->getDetails();
		$responseCrmID = $responseIn['id'];

        $update_array  = array(
                'v_member_crm_id'=>$responseCrmID,
                );

        $this->admin_model->update_entry($user_id,$update_array,"tbl_members");

        if(isset($user_step1['contact_id']) && !empty($user_step1['contact_id'])){

	        $contact_id = $user_step1['contact_id'];
        	//UPDATE CONTACT DATA
			$zcrmRecordContactIns = ZCRMRecord::getInstance("Contacts", $contact_id);

			$zcrmRecordContactIns->setFieldValue("Last_Name", $user_step1['lastname']);
			$zcrmRecordContactIns->setFieldValue("Email",$user_step1['email']);
			$zcrmRecordContactIns->setFieldValue("First_Name",$user_step1['firstname']);
			$zcrmRecordContactIns->setFieldValue("Date_of_Birth",date('Y-m-d', strtotime($user_step1['dob'])));
			$zcrmRecordContactIns->setFieldValue("Gender",$user_step1['gender']);

			$zcrmRecordContactIns->setFieldValue("Home_Country",$user_step1['home_country']);
			$zcrmRecordContactIns->setFieldValue("Home_City",$user_step1['home_city']);
			$zcrmRecordContactIns->setFieldValue("Mobile",$user_step1['tele_country_code'].'-'.$user_step1['telephone']); 
			$zcrmRecordContactIns->setFieldValue("Mobile_No",$user_step1['tele_country_code'].'-'.$user_step1['telephone']); 

			$zcrmRecordContactIns->setFieldValue("Country_of_Residence",$user_step1['residence_country']);
			$zcrmRecordContactIns->setFieldValue("City_Of_Residence'",$user_step1['residence_city']);
			$zcrmRecordContactIns->setFieldValue("Mobile_No_2",$user_step1['tele_2_country_code'].'-'.$data['telephone_2']);

			$zcrmRecordContactIns->setFieldValue("Educational_Info",isset($educational_info)?urldecode($educational_info):'');
			$zcrmRecordContactIns->setFieldValue("AAH",$AAH);
			$zcrmRecordContactIns->setFieldValue("Passions_and_interests",$data['passions_interests']);
			$zcrmRecordContactIns->setFieldValue("Experience_Info",isset($exp_info)?urldecode($exp_info):'');
			$zcrmRecordContactIns->setFieldValue("Label",'Member');
			
			$apiResponse=$zcrmRecordContactIns->update();
		}

    }

    public function add_mailchimp($email,$firstname,$lastname){

    	// mailchimp api call to subscribe start

		$merge_vars = array(
                'EMAIL' => $email,
                'FNAME' => $firstname,
                'LNAME' => $lastname,
            );

		mailChimpSubscribe($merge_vars);
		
		// mailchimp api call to subscribe end	

        // if($api->listSubscribe($list_id, $EmailTo, $merge_vars, 'html', false) === true) {

        // }else{

        // }
    }

    public function updateContactLabel($contact_id){

    	$zcrmRecordIns = ZCRMRecord::getInstance("Contacts", $contact_id);
		$zcrmRecordIns->setFieldValue("Label", "Candidate");
		$apiResponse=$zcrmRecordIns->update();
                
    }


    public function getZohoCRMContactsById($contact_id){

    	$apiResponse=ZCRMModule::getInstance('Contacts')->getRecord($contact_id); 
		$record=$apiResponse->getData();
		
		$memberdata['memberdata']=$record->getData();
		$memberdata['contact_id']=$record->getEntityId();

		return $memberdata;
  
    }
   
	public function add_zoho_crm_plan($user_id){

		$member_data = $this->tomouh_model->getUserByUserId($user_id);

		$payment_data = '';

		$user_step1 = $this->session->userdata('user');

		$user_step2 = $this->session->userdata('user_step2');

		if(isset($user_step2['university']) && !empty($user_step2['university'])){

            $educ_data_length = sizeof($user_step2['university']);

            $education_info = array();

            for($i=0;$i<$educ_data_length;$i++){

                if(isset($user_step2['university'][$i]) && !empty($user_step2['university'][$i])){

                $education_info[] = 'University : '.$user_step2['university'][$i].', Degree : '.$user_step2['degree'][$i].', Major : '.$user_step2['major'][$i].', Year : '.$user_step2['graduation_year'][$i];

                }
            }
        }

        if(isset($user_step2['company']) && !empty($user_step2['company'])){

            $company_data_length = sizeof($user_step2['company']);

            $experience_info = array();

            for($i=0;$i<$company_data_length;$i++){

                if(isset($user_step2['company'][$i]) && !empty($user_step2['company'][$i])){

                $experience_info[] = 'Company : '.$user_step2['company'][$i].', Job Title : '.$user_step2['job_title'][$i].', Industry : '.$user_step2['industry'][$i].', Period : '.$user_step2['company_from'][$i].'-'.$user_step2['company_to'][$i];

                }
            }
        }

        if(isset($user_step2['award_details']) && !empty($user_step2['award_details'])){

            $award_data_length = sizeof($user_step2['award_details']);

            $award_detail = '';

            for($i=0;$i<$award_data_length;$i++){

                if(isset($user_step2['award_details'][$i]) && !empty($user_step2['award_details'][$i])){

                 $award_detail .= $user_step2['award_details'][$i].',';

                }
            }
        }

        if(isset($award_detail)){

            $AAH = rtrim($award_detail,',');

        }else{
            $AAH = '';
        }

        $educational_info = implode(' %0A ',$education_info);

        $exp_info = implode(' %0A ', $experience_info);

		$cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');
		// insert into member
		
		//to get the instance of the module
		$moduleMemberIns=ZCRMRestClient::getInstance()->getModuleInstance("Members");

		$recordsMember=array();
		//To get ZCRMRecord instance
		$recordMember=ZCRMRecord::getInstance("Members",null); 
		//This function use to set FieldApiName and value similar to all other FieldApis and Custom field
		$recordMember->setFieldValue("Last_Name",$user_step1['lastname']);
		$recordMember->setFieldValue("Email",$user_step1['email']);
		$recordMember->setFieldValue("Name",$user_step1['firstname']);
		$recordMember->setFieldValue("Date_Of_Birth",date('Y-m-d', strtotime($user_step1['dob'])));
		$recordMember->setFieldValue("Gender",$user_step1['gender']);
		$recordMember->setFieldValue("Expired_On","");
		$recordMember->setFieldValue("Home_Country",$user_step1['home_country']);
		$recordMember->setFieldValue("Home_City",$user_step1['home_city']);
		$recordMember->setFieldValue("Mobile_No",$user_step1['tele_country_code'].'-'.$user_step1['telephone']); 
		$recordMember->setFieldValue("Country_of_Residence",$user_step1['residence_country']);
		$recordMember->setFieldValue("City_Of_Residence",$user_step1['residence_city']);
		$recordMember->setFieldValue("Mobile_No_2",$user_step1['tele_2_country_code'].'-'.$user_step1['telephone_2']);
		$recordMember->setFieldValue("Educational_Info_1",isset($educational_info)? urldecode($educational_info):''); 
		$recordMember->setFieldValue("Experience_Info_1",isset($exp_info)? urldecode($exp_info):'');
		$recordMember->setFieldValue("AAH",$AAH);
		$recordMember->setFieldValue("Passions_and_interests",$user_step2['passions_interests']);
		$recordMember->setFieldValue("Subscription_Info","");
		$recordMember->setFieldValue("Status","Active");
		$recordMember->setFieldValue("Plan","Free");
		$recordMember->setFieldValue("Label","Member");

		array_push($recordsMember, $recordMember);//pushing the record to the array 
		$responseIn=$moduleMemberIns->createRecords($recordsMember);
		$responseIn = $responseIn->getEntityResponses();
		$responseIn = $responseIn[0]->getDetails();
		$responseCrmID = $responseIn['id'];  
	
		$update_array  = array(
				'v_member_crm_id'=>$responseCrmID,
				);

		$this->admin_model->update_entry($user_id,$update_array,"tbl_members");

		if(isset($user_step1['contact_id']) && !empty($user_step1['contact_id'])){

			$contact_id = $user_step1['contact_id'];

			$zcrmRecordIns = ZCRMRecord::getInstance("Contacts", $contact_id);
			$zcrmRecordIns->setFieldValue("Label", "Payment Processing");
			$apiResponse   =$zcrmRecordIns->update();
	
		}
	}

	public function getCountryFromName(){

		$response = array();

		if(isset($_POST['country']) && !empty($_POST['country'])){

			$country = $_POST['country'];

			$country_data = $this->tomouh_model->getCountryCode($country);

			$response['phonecode'] = $country_data['phonecode'];

		}

		echo json_encode($response);
		exit();
	}

	public function signup_success($slug="signup_success"){

			$data['meta_title'] = "Signup Success Page";
			$data['meta_keyword'] = "Signup Success Page";
			$data['meta_description'] = "Signup Success Page";

			$this->load->view('signup_success',$data);
	}

	public function addZohoContactMember( $personal_data , $educational_data, $insert_id ){

		// echo "<pre>";
		// print_r($personal_data);
		// print_r($educational_data); 
		// exit;
		
		if(isset($educational_data['university']) && !empty($educational_data['university'])){

            $educ_data_length = sizeof($educational_data['university']);

            $education_info = array();

            for($i=0;$i<$educ_data_length;$i++){

                if(isset($educational_data['university'][$i]) && !empty($educational_data['university'][$i])){

                $education_info[] = 'University : '.$educational_data['university'][$i].', Degree : '.$educational_data['degree'][$i].', Major : '.$educational_data['major'][$i].', Year : '.$educational_data['graduation_year'][$i];

                }
            }
        }

        if(isset($educational_data['company']) && !empty($educational_data['company'])){

            $company_data_length = sizeof($educational_data['company']);

            $experience_info = array();

            for($i=0;$i<$company_data_length;$i++){

                if(isset($educational_data['company'][$i]) && !empty($educational_data['company'][$i])){

                $experience_info[] = 'Company : '.$educational_data['company'][$i].', Job Title : '.$educational_data['job_title'][$i].', Industry : '.$educational_data['industry'][$i].', Period : '.$educational_data['company_from'][$i].'-'.$educational_data['company_to'][$i];

                }
            }
        }

        if(isset($educational_data['award_details']) && !empty($educational_data['award_details'])){

            $award_data_length = sizeof($educational_data['award_details']);

            $award_detail = '';

            for($i=0;$i<$award_data_length;$i++){

                if(isset($educational_data['award_details'][$i]) && !empty($educational_data['award_details'][$i])){

                 $award_detail .= $educational_data['award_details'][$i].',';

                }
            }
        }

        if(isset($award_detail)){

            $AAH = rtrim($award_detail,',');

        }else{
            $AAH = '';
        }

        $educational_info = implode(' %0A ',$education_info);

        $exp_info = implode(' %0A ', $experience_info);
       
        //insert contact in zoho start

        //to get the instance of the module

		$moduleContactIns=ZCRMRestClient::getInstance()->getModuleInstance("Contacts");
		$recordsContact=array();
		
		//To get ZCRMRecord instance
		$recordContact=ZCRMRecord::getInstance("Contacts",null);

		//This function use to set FieldApiName and value similar to all other FieldApis and Custom field
		$recordContact->setFieldValue("Last_Name",isset($personal_data['lastname']) ? $personal_data['lastname'] : ''); 
		$recordContact->setFieldValue("Email",isset($personal_data['email']) ? $personal_data['email'] : '');
		$recordContact->setFieldValue("First_Name", isset($personal_data['firstname']) ? $personal_data['firstname'] : '');
		$recordContact->setFieldValue("Date_of_Birth",date('Y-m-d', strtotime($personal_data['dob'])));
		$recordContact->setFieldValue("Gender",$personal_data['gender']); 

		// $recordContact->setFieldValue("Expired_On","10/10/2020");
		$recordContact->setFieldValue("Home_Country",isset($personal_data['home_country']) ? $personal_data['home_country'] : '');
		$recordContact->setFieldValue("Home_City",isset($personal_data['home_city']) ? $personal_data['home_city'] : '');
		$recordContact->setFieldValue("Mobile",isset($personal_data['telephone']) && !empty($personal_data['telephone']) ? $personal_data['tele_country_code'].'-'.$personal_data['telephone'] : ''); 
		$recordContact->setFieldValue("Mobile_No",isset($personal_data['telephone']) && !empty($personal_data['telephone']) ? $personal_data['tele_country_code'].'-'.$personal_data['telephone'] : ''); 

		$recordContact->setFieldValue("Country_of_Residence",isset($personal_data['residence_country']) ? $personal_data['residence_country'] : '');
		$recordContact->setFieldValue("City_Of_Residence",isset($personal_data['residence_city']) ? $personal_data['residence_city'] : '');
		$recordContact->setFieldValue("Mobile_No_2",isset($personal_data['telephone_2']) && !empty($personal_data['telephone_2']) ? $personal_data['tele_2_country_code'].'-'.$personal_data['telephone_2'] : '');
		$recordContact->setFieldValue("Educational_Info",isset($educational_info)?urldecode($educational_info):''); 

		$recordContact->setFieldValue("Experience_Info",isset($exp_info)?urldecode($exp_info):'');
		$recordContact->setFieldValue("AAH",$AAH);
		$recordContact->setFieldValue("Passions_and_interests",isset($educational_data['passions_interests']) ? $educational_data['passions_interests'] : '');
		// $recordContact->setFieldValue("Subscription_Info","test");

		$recordContact->setFieldValue("Lead_Source",isset($personal_data['lead_source']) ? $personal_data['lead_source'] : '');
		$recordContact->setFieldValue("Label","Candidate");

		array_push($recordsContact, $recordContact);//pushing the record to the array 

		$responseIn=$moduleContactIns->createRecords($recordsContact);
		$responseIn = $responseIn->getEntityResponses();
		$responseIn = $responseIn[0]->getDetails();
		$responseCrmID = $responseIn['id']; 

		$update_array  = array(
				            'v_contact_crm_id'=>$responseCrmID,
				            );

		$this->admin_model->update_entry($insert_id,$update_array,"tbl_before_member");

        // insert contact in zoho end

	}

	public function delete_account(){

		$user_id = $this->session->userdata("logged_user");

		if( isset($user_id) && !empty($user_id) ){

			$this->delete_crm_data( $user_id );

			$usr_data = $this->tomouh_model->getUserByUserId($user_id);

			if(isset( $usr_data['v_email'] ) && !empty( $usr_data['v_email'] )){

				$this->admin_model->delete_member_temp_data( $usr_data['v_email'] , 'tbl_members_temp');

				$this->admin_model->delete_member_before_data( $usr_data['v_email'] , 'tbl_before_member');
			}

			$delete_result = $this->tomouh_model->deleteUserAccount($user_id);
			
			if( $delete_result ){

				$this->session->unset_userdata('logged_user');

				if(isset($_COOKIE['tomouh_logged_data']) && !empty($_COOKIE['tomouh_logged_data'])){
				  setcookie('tomouh_logged_data', '', time() - 31536000, '/');
				}

				redirect(base_url().'login?succ=1&msg=delete_account');
				exit;
			}
		}

		redirect(base_url().'login');
		exit;
	}

	public function request_website_data(){

		$user_id = $this->session->userdata("logged_user");

		if( isset($user_id) && !empty($user_id) ){
			
			$user_data = $this->tomouh_model->getUserByUserId($user_id);

	 	   	if( !empty($user_data) ){

			$email_content = '<p>Hello '.$user_data["v_firstname"]. ' ' .$user_data["v_lastname"].',</p>
						   <p>
We have recently received your request. Please see the information below that is on the Tomouh\'s website.</p>';

			$email_content .= '<table cellpadding="10" align="center">
						<tr>
							<th align="left">Name</th>
							<td>'.$user_data["v_firstname"]. ' ' .$user_data["v_lastname"].'</td>
						</tr>
						<tr>
							<th align="left">Email</th>
							<td>'.$user_data["v_email"].'</td>
						</tr>
						<tr>
							<th align="left">Date Of Birth</th>
							<td>'.$user_data["d_dob"].'</td>
						</tr>
						<tr>
							<th align="left">Gender</th>
							<td>'.$user_data["e_gender"].'</td>
						</tr>
						<tr>
							<th align="left">Home Country</th>
							<td>'.$user_data["v_home_country"].'</td>
						</tr>
						<tr>
							<th align="left">Home City</th>
							<td>'.$user_data["v_home_city"].'</td>
						</tr>
						<tr>
							<th align="left">Residence Country</th>
							<td>'.$user_data["v_residence_country"].'</td>
						</tr>
						<tr>
							<th align="left">Residence City</th>
							<td>'.$user_data["v_residence_city"].'</td>
						</tr>
						<tr>
							<th align="left">Telephone</th>
							<td>'.$user_data["v_telephone"].'</td>
						</tr>
						<tr>
							<th align="left">Telephone 2</th>
							<td>'.$user_data["v_telephone_2"].'</td>
						</tr>
						<tr>
							<th align="left">Twitter Link</th>
							<td>'.$user_data["v_twitter_link"].'</td>
						</tr>
						<tr>
							<th align="left">LinkedIn Link</th>
							<td>'.$user_data["v_linkedin_link"].'</td>
						</tr>
						<tr>
							<th align="left">Instagram Link</th>
							<td>'.$user_data["v_instagram_link"].'</td>
						</tr>
						<tr>
							<th align="left">Passion And Interest</th>
							<td>'.$user_data["t_passion_interest"].'</td>
						</tr>
					</table>';

			$edu_data = $this->tomouh_model->getEducationalData($select_user['user_id']);
			
			if(!empty($edu_data)){							

				$email_content .= '<table cellpadding="10" style="text-align: center;width:100%;" border="1px;" align="left">
							<tr>
								<th colspan="4">
									Educational Data
								</th>
							</tr>
							<tr>
								<th>University</th>
								<th>Degree</th>
								<th>Major</th>
								<th>Graduation Year</th>
							</tr>';

				foreach( $edu_data as $edu ){

					$email_content.=  '<tr>
											<td>'.$edu['v_university'].'</td>
											<td>'.$edu['v_degree'].'</td>
											<td>'.$edu['v_major'].'</td>
											<td>'.$edu['i_passing_year'].'</td>
										</tr>';	
				}

				$email_content .= '</table>';
			}

			$pro_data = $this->tomouh_model->getProfessionalData($select_user['user_id']);
			
			if(!empty($pro_data)){						

				$email_content .= '<table cellpadding="10" style="margin-top:10px; text-align: center;width:100%;" border="1px;" align="left">

							<tr>
								<th colspan="5">
									Professional Data
								</th>
							</tr>
							<tr>
								<th>Company</th>
								<th>Job Title</th>
								<th>Industry</th>
								<th>From</th>
								<th>To</th>
							</tr>';

				foreach( $pro_data as $pro ){

					$email_content.=  '<tr>
											<td>'.$pro['v_company'].'</td>
											<td>'.$pro['v_job_title'].'</td>
											<td>'.$pro['v_industry'].'</td>
											<td>'.$pro['i_company_from'].'</td>
											<td>'.$pro['i_company_to'].'</td>
										</tr>';	
				}

				$email_content .= '</table>';
			}

			$award_data = $this->tomouh_model->getAchievementalData($select_user['user_id']);

				if(!empty($award_data)){

					$email_content .= '<table cellpadding="10" style="margin-top:10px;width:100%;text-align:center;" border="1px;" align="left">
										<tr>
											<th>
												Achievements, Awards and Honors
											</th>
										</tr>';								

					foreach( $award_data as $awa ){

						$email_content .= '<tr>
												<td>'.$awa['v_achievement'].'</td>
										   </tr>';

					}

					$email_content .= "</table>";
				}

			$email_to = $user_data["v_email"];
			$email_from ='';
			$template = $this->tomouh_model->getEmailTemplate(9);
			$email_subject = $template['v_subject'];
				
			$content = isset($template['l_body']) ? $template['l_body'] : '';

			
			$content = str_replace("REQUESTED_DATA", $email_content, $content);
			
			
			$sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content , $attachments = array() );
						
	                }
					
			/*
			$entry_id = $this->tomouh_model->check_request_data($user_id);

			if($entry_id){

				$request_data = array(

					'e_sended'    => '0',
					'd_added'     => date('Y-m-d H:i:s'),

					); 

				$this->admin_model->update_entry($entry_id,$request_data, 'tbl_request_user_data');		

			}else{

				$user_data = $this->tomouh_model->getUserByUserId($user_id);			

				$request_data = array(

					'user_id'     => $user_id,
					'v_email'     => isset($user_data['v_email']) ? $user_data['v_email'] : '',
					'v_firstname' => isset($user_data['v_firstname']) ? $user_data['v_firstname'] : '',
					'v_lastname'  => isset($user_data['v_lastname']) ? $user_data['v_lastname'] : '',
					'd_added'     => date('Y-m-d H:i:s'),

					); 

				$this->admin_model->add_entry($request_data,"tbl_request_user_data");

			}
			
			*/

			redirect(base_url().'account?succ=1&msg=request_data');
			exit;
		}

		redirect(base_url().'login');
		exit;

	}

	public function update_crm_detail_step1( $data, $user_id ){
	
		$user_data = $this->tomouh_model->getUserByUserId($user_id);

		if(isset($user_data['v_member_crm_id']) && !empty($user_data['v_member_crm_id'])){

			$member_id = $user_data['v_member_crm_id'];
			// UPDATE MEMBER DATA
			$zcrmRecordMemberIns = ZCRMRecord::getInstance("Members", $member_id);
			
			$zcrmRecordMemberIns->setFieldValue("Last_Name", $data['lastname']);
			$zcrmRecordMemberIns->setFieldValue("Email",$data['email']);
			$zcrmRecordMemberIns->setFieldValue("Name",$data['firstname']);
			$zcrmRecordMemberIns->setFieldValue("Date_Of_Birth",date('Y-m-d', strtotime($data['dob'])));
			$zcrmRecordMemberIns->setFieldValue("Gender",$data['gender']);

			$zcrmRecordMemberIns->setFieldValue("Home_Country",$data['home_country']);
			$zcrmRecordMemberIns->setFieldValue("Home_City",$data['home_city']);
			$zcrmRecordMemberIns->setFieldValue("Mobile_No",$data['tele_country_code'].'-'.$data['telephone']); 

			$zcrmRecordMemberIns->setFieldValue("Country_of_Residence",$data['residence_country']);
			$zcrmRecordMemberIns->setFieldValue("City_Of_Residence'",$data['residence_city']);
			$zcrmRecordMemberIns->setFieldValue("Mobile_No_2",$user_step1['tele_2_country_code'].'-'.$data['telephone_2']);
			
			$apiResponse=$zcrmRecordMemberIns->update();


		}

		if(isset($user_data['v_crm_contact_id']) && !empty($user_data['v_crm_contact_id'])){

			$contact_id = $user_data['v_crm_contact_id'];

			//UPDATE CONTACT DATA
			$zcrmRecordContactIns = ZCRMRecord::getInstance("Contacts", $contact_id);
			
			$zcrmRecordContactIns->setFieldValue("Last_Name", $data['lastname']);
			$zcrmRecordContactIns->setFieldValue("Email",$data['email']);
			$zcrmRecordContactIns->setFieldValue("First_Name",$data['firstname']);
			$zcrmRecordContactIns->setFieldValue("Date_of_Birth",date('Y-m-d', strtotime($data['dob'])));
			$zcrmRecordContactIns->setFieldValue("Gender",$data['gender']);

			$zcrmRecordContactIns->setFieldValue("Home_Country",$data['home_country']);
			$zcrmRecordContactIns->setFieldValue("Home_City",$data['home_city']);
			$zcrmRecordContactIns->setFieldValue("Mobile",$data['tele_country_code'].'-'.$data['telephone']); 
			$zcrmRecordContactIns->setFieldValue("Mobile_No",$data['tele_country_code'].'-'.$data['telephone']); 

			$zcrmRecordContactIns->setFieldValue("Country_of_Residence",$data['residence_country']);
			$zcrmRecordContactIns->setFieldValue("City_Of_Residence'",$data['residence_city']);
			$zcrmRecordContactIns->setFieldValue("Mobile_No_2",$data['tele_2_country_code'].'-'.$data['telephone_2']);
			
			$apiResponse=$zcrmRecordContactIns->update();
			// echo "<pre>";print_r($apiResponse); exit;

		}	
	
	}


	public function update_crm_detail_step2( $data, $user_id ){

		$user_data = $this->tomouh_model->getUserByUserId($user_id);

        if(isset($data['university']) && !empty($data['university'])){

            $educ_data_length = sizeof($data['university']);

            $education_info = array();

            for($i=0;$i<$educ_data_length;$i++){

                if(isset($data['university'][$i]) && !empty($data['university'][$i])){

                $education_info[] = 'University : '.$data['university'][$i].', Degree : '.$data['degree'][$i].', Major : '.$data['major'][$i].', Year : '.$data['graduation_year'][$i];

                }
            }
        }

        if(isset($data['company']) && !empty($data['company'])){

            $company_data_length = sizeof($data['company']);

            $experience_info = array();

            for($i=0;$i<$company_data_length;$i++){

                if(isset($data['company'][$i]) && !empty($data['company'][$i])){

                $experience_info[] = 'Company : '.$data['company'][$i].', Job Title : '.$data['job_title'][$i].', Industry : '.$data['industry'][$i].', Period : '.$data['company_from'][$i].'-'.$data['company_to'][$i];

                }
            }
        }

        if(isset($data['award_details']) && !empty($data['award_details'])){

            $award_data_length = sizeof($data['award_details']);

            $award_detail = '';

            for($i=0;$i<$award_data_length;$i++){

                if(isset($data['award_details'][$i]) && !empty($data['award_details'][$i])){

                 $award_detail .= $data['award_details'][$i].',';

                }
            }
        }

        if(isset($award_detail)){

            $AAH = rtrim($award_detail,',');

        }else{
            $AAH = '';
        }

        $educational_info = implode(' %0A ',$education_info);

        $exp_info = implode(' %0A ', $experience_info);

        $cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');

        if(isset($user_data['v_member_crm_id']) && !empty($user_data['v_member_crm_id'])){ 
        	
        	$member_id = $user_data['v_member_crm_id'];
			// UPDATE MEMBER DATA
			$zcrmRecordMemberIns = ZCRMRecord::getInstance("Members", $member_id);

			$zcrmRecordMemberIns->setFieldValue("Educational_Info_1",isset($educational_info)?urldecode($educational_info):'');
			$zcrmRecordMemberIns->setFieldValue("AAH",$AAH);
			$zcrmRecordMemberIns->setFieldValue("Passions_and_interests",$data['passions_interests']);
			$zcrmRecordMemberIns->setFieldValue("Experience_Info_1",isset($exp_info)?urldecode($exp_info):'');
			
			$apiResponse=$zcrmRecordMemberIns->update();
        }

        if(isset($user_data['v_crm_contact_id']) && !empty($user_data['v_crm_contact_id'])){ 
        	
        	$contact_id = $user_data['v_crm_contact_id'];
			
			//UPDATE CONTACT DATA
			$zcrmRecordContactIns = ZCRMRecord::getInstance("Contacts", $contact_id);

			$zcrmRecordContactIns->setFieldValue("Educational_Info",isset($educational_info)?urldecode($educational_info):'');
			$zcrmRecordContactIns->setFieldValue("AAH",$AAH);
			$zcrmRecordContactIns->setFieldValue("Passions_and_interests",$data['passions_interests']);
			$zcrmRecordContactIns->setFieldValue("Experience_Info",isset($exp_info)?urldecode($exp_info):'');
			
			$apiResponse=$zcrmRecordContactIns->update();
        }

	}

	public function delete_crm_data( $user_id ){


		$user_data = $this->tomouh_model->getUserByUserId($user_id);

		$cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');

    	if(isset($user_data['v_member_crm_id']) &&  !empty($user_data['v_member_crm_id'])){

    		$this->admin_model->delete_crm_member($user_data['v_member_crm_id'],"tbl_crm_members_data");

	    	$zcrmRecordIns = ZCRMRecord::getInstance("Members", $user_data['v_member_crm_id']); 
			$apiResponse=$zcrmRecordIns->delete();
		}

		if(isset($user_data['v_crm_contact_id']) &&  !empty($user_data['v_crm_contact_id'])){

    		$this->admin_model->delete_crm_contact($user_data['v_crm_contact_id'],"tbl_crm_contact_data");

	    	$zcrmRecordIns = ZCRMRecord::getInstance("Contacts", $user_data['v_crm_contact_id']); 
			$apiResponse=$zcrmRecordIns->delete();
		}
	}

	public function file_get_contents_curl($url) {

	    $ch = curl_init();

	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_URL, $url);

	    $data = curl_exec($ch);
	    curl_close($ch);

	    return $data;
	}

	public function update_zoho_crm($user_id, $sub_id){

        $results = $this->tomouh_model->getUserByUserId($user_id);

        $payment_data =  $this->tomouh_model->getAllSubscriptionByUserId($user_id);

        $payment_info= [];

        if($payment_data){

        	foreach($payment_data as $data){

        		$payment_info[] = 'Payment Type : paypal, Subscription : '.$data['v_subscription_id'].', Expiration Date : '.date("m/d/Y", strtotime(date("Y-m-d", strtotime($data['d_subscription_date'])) . " + 1 year"));

        	}
        }

        $payment_info[] = 'Payment Type : paypal, Subscription Id : '.$sub_id.', Expiration Date : '.date('m/d/Y',strtotime('+1 year'));  

        $pay_info = implode(' %0A ',$payment_info);

        $member_crm_id = $results['v_member_crm_id'];

        // UPDATE MEMBER DATA
		$zcrmRecordMemberIns = ZCRMRecord::getInstance("Members", $member_crm_id);

		$zcrmRecordMemberIns->setFieldValue("Plan",'Paid');
		$zcrmRecordMemberIns->setFieldValue("Expired_On'",date('Y-m-d',strtotime('+1 year')));
		$zcrmRecordMemberIns->setFieldValue("Subscription_Info",$pay_info);
		
		$apiResponse=$zcrmRecordMemberIns->update();

    }
}