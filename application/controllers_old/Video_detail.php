<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Video_detail extends CI_Controller
{
	public function __construct(){
		parent::__construct();

		$this->load->model('tomouh_model');
		$this->load->model('messages_model');
		$this->load->library('session');
		$this->load->library('form_validation');
	}
	public function index($id=''){

		$data = array();

		if($id){

			$video  = $this->tomouh_model->getvideo($id);
			$data['video'] = $video;
			$data['meta_title'] = ($video['v_meta_title']) ? $video['v_meta_title'] : $video['v_name'];
			$data['meta_keyword'] = $video['v_meta_keyword'];
			$data['meta_description'] = $video['l_meta_description'];
		}

		$this->load->view('video_detail',$data);
	}
}