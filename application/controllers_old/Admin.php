<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require_once('./zoho/vendor/autoload.php');
require_once('application/stripe/config.php');
require_once ('application/phpexcel/PHPExcel.php');
require_once('./application/helpers/general_helper.php');
require_once('./zoho/vendor/autoload.php');
require_once('./zoho_v2/vendor/autoload.php');

use CristianPontes\ZohoCRMClient\ZohoCRMClient;

class Admin extends CI_Controller
{
	public function __construct(){
		parent:: __construct();
	
		$this->load->model('admin/admin_model');
		$this->load->model('tomouh_model');
		$this->load->model('messages_model');
		$this->load->model('paging_model');
		$this->load->model('general_model');
		$this->load->library('form_validation');

		ZCRMRestClient::initialize();
	}

	public function index(){
		// echo "here";
		// exit;
		$data["page_title"] ="Login - Tomouh";

		if($this->form_validation->run('login_details') == TRUE){

			$v_username = $this->input->post('v_name');
			$v_password = $this->input->post('v_password');
			$is_auth = $this->admin_model->authenticate($v_username, $v_password);
			if(!empty($is_auth)){
				$data['admin']=array(
						'u_id'=>$is_auth['id'],
						'i_super'=>$is_auth['i_super'],
						'v_email'=>$is_auth['v_email'],
						'v_username'=>$is_auth['v_username'],
					);

				$this->session->set_userdata($data);
				redirect(base_url().'admin/dashboard/');
				exit;
			}else{
				redirect('admin/?succ=0&msg=invalid');
				exit;
			}
		}
		$this->load->view('admin/index',$data);
	}

	public function dashboard(){

		$user_data = $this->session->userdata["admin"];

		if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}

		$data["page_title"] ="Dashboard";
		$data["page"] =	"Dashboard";

		$data['total_member'] = $this->admin_model->getTotalMembers();
		$data['active_member'] = $this->admin_model->getTotalActiveMembers();
		$data['inactive_member'] = $this->admin_model->getTotalInactiveMembers();
		$data['total_payment'] = $this->admin_model->getTotalPayment();

		$this->load->view('admin/dashboard',$data);
	}

	public function logout(){
		$this->session->unset_userdata('admin');
		redirect(base_url().'admin/?succ=1&msg=logout');
		exit;
	}

	public function settings($update = ""){

		$user_data = $this->session->userdata["admin"];

		if(!isset($this->session->userdata["admin"]) || $user_data['i_super'] != 1){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}

	   	$data["page_title"] ="General Settings";
		$data["page"] =	"Admin";
		$siteKeyData = array();
		$tbl = "tbl_sitesetting";

		$query 	 = $this->db->query("SELECT l_value,v_name FROM tbl_sitesetting");
		$query 	 = $query->result_array();

		foreach ($query as $key => $value) {
			$data[$value["v_name"]] = $value["l_value"];
		}

		if(isset($update) && $update != ''){

			$post = array();
	    	$insert_data=array();

				$que = $this->db->query("SELECT * FROM tbl_sitesetting");
				$res = $que->result_array();

				$secret_key = '';
				$public_key = '';
				$amount = 0;

				// if(!empty($res)){

				// 	foreach($res as $re){

				// 		if($re['v_name'] == 'SECRET_KEY'){

				// 			$secret_key = $re['l_value'];
				// 		}elseif($re['v_name'] == 'PUBLIC_KEY'){

				// 			$public_key = $re['l_value'];
				// 		}elseif($re['v_name'] == 'CHARGES_AMOUNT'){

				// 			$amount = $re['l_value'];
				// 		}
				// 	}

				// 	if($_POST['SECRET_KEY'] != $secret_key || $_POST['PUBLIC_KEY'] != $public_key || $_POST['CHARGES_AMOUNT'] != $amount){

				// 		\Stripe\Stripe::setApiKey($_POST['SECRET_KEY']);
				// 		try{

				// 		   $plan = \Stripe\Plan::create([
				// 			    'currency' => 'usd',
				// 			    'interval' => 'year',
				// 			    'name' => 'tomouh subscription plan',
				// 			    'amount' => $_POST['CHARGES_AMOUNT'],
				// 			]);

				// 			}catch(Exception $e){

				// 				$error = $e->getMessage();
				// 				redirect(base_url().'payment?succ=0&msg='.$error);
				// 				exit;
				// 			}

				// 			$plan_array = array(

				// 				'v_plan_id'=>$plan->id,
				// 				'v_plan_name'=>$plan->name,
				// 				'i_amount'=>$plan->amount,
				// 				'v_interval'=>$plan->interval,
				// 				'l_plan_data'=>$plan,
				// 				'd_created'=>date("Y-m-d H:i:s"),
				// 			);

				// 			$this->db->query("DELETE * FROM tbl_plan");

				// 			$this->admin_model->add_entry($plan_array,"tbl_plan");
				// 	}

				// }else{

				// 	\Stripe\Stripe::setApiKey($_POST['SECRET_KEY']);
				// 		try{

				// 		   $plan = \Stripe\Plan::create([
				// 			    'currency' => 'usd',
				// 			    'interval' => 'month',
				// 			    'name' => 'tomouh subscription plan',
				// 			    'amount' => $_POST['CHARGES_AMOUNT'],
				// 			]);

				// 			}catch(Exception $e){

				// 				$error = $e->getMessage();
				// 				redirect(base_url().'payment?succ=0&msg='.$error);
				// 				exit;
				// 			}

				// 			$plan_array = array(

				// 				'v_plan_id'=>$plan->id,
				// 				'v_plan_name'=>$plan->name,
				// 				'i_amount'=>$plan->amount,
				// 				'v_interval'=>$plan->interval,
				// 				'l_plan_data'=>$plan,
				// 				'd_created'=>date("Y-m-d H:i:s"),
				// 			);

				// 			$this->db->query("DELETE FROM tbl_plan");

				// 			$this->admin_model->add_entry($plan_array,"tbl_plan");
				// }


	        foreach ($_POST as $key => $value) {
				if($key == 'MEMBER_IMAGE' || $key == 'SITE_LOGO'){
					$value = $this->general_model->seoText($value);
				}
				
	        	$query = $this->db->query("SELECT l_value,id FROM tbl_sitesetting WHERE v_name ='".$key."'");
				$results = $query->result_array();



	            if(count($results)){
	            	$siteKey = $results[0]["l_value"];
					$id = $results[0]["id"];
	                $update=array(
	                    'l_value'=>$value
	                );
	                $updateSiteSetting = $this->admin_model->update_entry($id,$update,$tbl);
	            }else{
	                $insert_data=array(
	                    'v_name'=>$key,
	                    'l_value'=>$value,
	                );
	                $addSiteSetting = $this->admin_model->add_entry($insert_data,$tbl);
	            }
	        }

			redirect(base_url().'admin/settings/?succ=1&msg=edit');
			exit;
		}

		$this->load->view('admin/settings',$data);
	}

	public function manage_admin($script="list", $id="")
	{
		$user_data = $this->session->userdata["admin"];

		if(!isset($this->session->userdata["admin"]) || $user_data['i_super'] != 1 ){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
		}

		$data["page_title"] =	"Manage Admin";
		$data["page"] =	"Admin";
		$data["script"] = $script;
		$tbl = "tbl_admin";

		if($script == "list"){
			$wh="";

			$data["form"]="frm_manage_admin";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);
			$rows = $this->admin_model->getAdminList($arg);
			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];
		}
		elseif ($script == 'add'){

			if ($_SERVER['REQUEST_METHOD']=='POST') {

				$postdata["i_super"] =0;

				if($this->input->post("i_super")){
					$postdata["i_super"] = $this->input->post("i_super");
				}
				$postdata["v_username"] = $this->input->post("v_username");
				$postdata["v_email"] = $this->input->post("v_email");
				$postdata["v_password"] = md5($this->input->post("v_password"));
				$postdata['d_added']= date("Y-m-d H:i:s");
				$postdata['d_modified']=date("Y-m-d H:i:s");
				$postdata["e_status"] = $this->input->post("e_status");


				$username = $this->general_model->get_Admin('v_username',$postdata["v_username"]);

				$username_email = $this->general_model->get_Admin('v_email',$postdata["v_email"]);

				if(count($username)){

					redirect(base_url().'admin/manage_admin/add/?succ=0&msg=nameexists');
				}
				if (count($username_email)) {

					redirect(base_url().'admin/manage_admin/add/?succ=0&msg=emailexists');
				}
				if ($this->form_validation->run('manage_admin') == TRUE){

					$addAdmin = $this->admin_model->add_entry($postdata,$tbl);

					redirect(base_url().'admin/manage_admin/?succ=1&msg=addadmin');
					exit;
				}else{
					$data =array_merge($data, $postdata);
				}

			}else{
				$data["v_name"]="";
				$data["v_email"]="";
				$data["v_password"]="";
				$data["e_status"]="";
			}
		}
		elseif($script == "edit")
		{
			$this->form_validation->set_rules('v_username', 'Username', 'required');
			$this->form_validation->set_rules('v_email', 'Email', 'required');

			if($_SERVER['REQUEST_METHOD']=='POST'){

				$postdata["i_super"] =0;

				if($this->input->post("i_super")){
					$postdata["i_super"] = $this->input->post("i_super");
				}

				$postdata["v_username"] = $this->input->post("v_username");
				$postdata["v_email"] = $this->input->post("v_email");
				$password =  $this->input->post("v_password");

				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_modified']=date("Y-m-d H:i:s");
				//echo "<pre>";print_r($postdata);exit;
				$username = $this->general_model->get_Admin_Data('v_username',$postdata["v_username"],$id);
				$username_email = $this->general_model->get_Admin_Data('v_email',$postdata["v_email"],$id);


				if(count($username)){

					redirect(base_url().'admin/manage_admin/edit/'.$id.'?succ=0&msg=nameexists');

				}
				if (count($username_email)) {

					redirect(base_url().'admin/manage_admin/edit/'.$id.'?succ=0&msg=emailexists');

				}

				if($this->input->post("v_password") && $this->input->post("v_password") !="")
					$postdata["v_password"] = md5($this->input->post("v_password"));

				if ($this->form_validation->run() == TRUE){

					$updateAdmin = $this->admin_model->update_entry($id,$postdata,$tbl);

					redirect(base_url().'admin/manage_admin/?succ=1&msg=editadmin');
					exit;
				}else{
					$data =array_merge($data, $postdata);
				}

			}elseif($id !=""){

				$selectAdmin = $this->admin_model->select_entry($tbl,$id);
				$data =array_merge($data, $selectAdmin);
			}
		}
		elseif($script == "active"){
			if($id){
				$postdata["e_status"] ="active";
				$updateAdmin = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/manage_admin/?succ=1&msg=multiact');
				exit;
			}
		}
		elseif($script == "inactive"){
			if($id){
				$postdata["e_status"] ="inactive";
				$updateAdmin = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/manage_admin/?succ=1&msg=multiinact');
				exit;
			}

		}
		elseif($script == "delete"){
			if($id){

				$deleteadmin = $this->admin_model->delete_entry($id,$tbl);
				redirect(base_url().'admin/manage_admin/?succ=1&msg=multidel');
			}
		}

		$this->load->view('admin/manage_admin', $data);
	}


	public function fileupload()
	{
        if (!empty($_FILES)) {

	        $tempFile = $_FILES['file']['tmp_name'];
	        $fileName = $this->general_model->seoText($_FILES['file']['name']);
	        $targetPath = getcwd() . '/assets/frontend/images/';
	        $targetFile = $targetPath . $fileName;
	        move_uploaded_file($tempFile, $targetFile);
        	
        }
    }

    public function fileuploadteam()
	{
        if (!empty($_FILES)) {

	        /*$tempFile = $_FILES['file']['tmp_name'];
	        $fileName = $_FILES['file']['name'];
	        $targetPath = getcwd() . '/assets/frontend/images/';
	        $this->cropImage($fileName,$tempFile,360,343,$targetPath);*/
			
			if (!empty($_FILES)) {
				$width=360;
				$height=345;
				$scale=1;
				
				$tempFile = $_FILES['file']['tmp_name'];
				$fileName = $_FILES['file']['name'];
				$fileName = $fileName;
				$targetPath = getcwd() . '/assets/frontend/images/';
				$targetFile = $targetPath . $fileName;
				
				$imgWidth = $this->getWidth($tempFile);
				$imgHeight = $this->getHeight($tempFile);
				
				move_uploaded_file($tempFile, $targetFile);
       			
				//$this->resizeImage($targetFile,$width,$height,$scale);
				if($imgWidth > $width && $imgHeight > $height){
					//$this->resizeThumbnailImage($targetFile, $tempFile, $width, $height, 0, 0, $scale);
				}
			}
		
			
        }
    }

    public function fileuploadbook()
	{
        /*if (!empty($_FILES)) {

	        $tempFile = $_FILES['file']['tmp_name'];
	        $fileName = $_FILES['file']['name'];
	        $targetPath = getcwd() . '/assets/frontend/images/';
	        $this->cropImage($fileName,$tempFile,100,100,$targetPath,205,153);
        }*/
		
		if (!empty($_FILES)) {
				$width=155;
				$height=205;
				$scale=1;
				
				$tempFile = $_FILES['file']['tmp_name'];
				$fileName = $_FILES['file']['name'];
				$fileName = $fileName;
				$targetPath = getcwd() . '/assets/frontend/images/';
				$targetFile = $targetPath . $fileName;
				$imgWidth = $this->getWidth($tempFile);
				$imgHeight = $this->getHeight($tempFile);
				
				if(move_uploaded_file($tempFile, $targetFile)){
					echo "File uploaded";
				}
       			//$this->resizeImage($targetFile,$width,$height,$scale);
				
				if($imgWidth > $width && $imgHeight > $height){
					//$this->resizeThumbnailImage($targetFile, $tempFile, $width, $height, 0, 0, $scale);
				}
			}
    }
	
	public function getHeight($image) {
		$size = getimagesize($image);
		$height = $size[1];
		return $height;
	}
	//You do not need to alter these functions
	public  function getWidth($image) {
		$size = getimagesize($image);
		$width = $size[0];
		return $width;
	}
    public function removelogo($filename)
	{
		$targetPath = getcwd() . '/assets/frontend/images/';
        $filename = $targetPath.$filename;
       	unlink($filename);

       	$query = $this->db->query("UPDATE tbl_sitesetting SET l_value ='' WHERE v_name ='SITE_LOGO'");
		exit;
    }

    public function removeimage($slug,$filename,$id)
	{
		$targetPath = getcwd() . '/assets/images/';
        $filename = $targetPath.$filename;
       	unlink($filename);

       	$table = 'tbl_'.$slug;
       	$query = $this->db->query("UPDATE " .$table. " SET v_image ='' WHERE id =".$id." ");
		exit;
    }

    public function removephoto($slug,$id)
	{
		$query_del = $this->admin_model->select_entry('tbl_'.$slug,$id);
		if( isset($query_del['v_image']) && $query_del['v_image'] ) {

			$fileName = $query_del['v_image'];
			$targetPath = getcwd() . '/assets/frontend/images/';
	        $fileNamePath = $targetPath.$fileName;
	       	unlink($fileNamePath);
		}
       	$tbl = 'tbl_'.$slug;
       	$query = $this->admin_model->delete_entry($id,$tbl);
		exit;
    }

    public function removephotoupdate($slug,$id)
	{
		$query_del = $this->admin_model->select_entry('tbl_'.$slug,$id);
		if( isset($query_del['v_image']) && $query_del['v_image'] ) {

			$fileName = $query_del['v_image'];
			$targetPath = getcwd() . '/assets/frontend/images/';
	        $fileNamePath = $targetPath.$fileName;
	       	unlink($fileNamePath);
		}
       	$tbl = 'tbl_'.$slug;
        $postdata['v_image']="";
		$this->admin_model->update_entry($id,$postdata,$tbl);
       	echo 1;
		exit;
    }

    public function pages($script="list", $id=""){

    	$user_data = $this->session->userdata["admin"];

		if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}

   	 	$data["page_title"] ="Pages";
		$data["page"] =	"Pages";

		$data["script"] = $script;
		$tbl = "tbl_pages";

		if($script == "list"){
			$wh="";

			$data["form"]="frm_pages";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);
			$rows = $this->admin_model->getPageList($arg);
			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];
		}
		elseif ($script == 'add'){

			if ($_SERVER['REQUEST_METHOD']=='POST') {


				$v_slug = $this->general_model->seoText($this->input->post("v_title"));
				$v_slug = $this->general_model->is_unique('tbl_pages','v_slug',$v_slug);

				$postdata["v_title"] = $this->input->post("v_title");
				$postdata["v_slug"] = $v_slug;
				$postdata["l_description"] = $this->input->post("l_description");
				$postdata["v_meta_title"] = $this->input->post("v_meta_title");
				$postdata["v_meta_keyword"] = $this->input->post("v_meta_keyword");
				$postdata["l_meta_description"] = $this->input->post("l_meta_description");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_added']= date("Y-m-d H:i:s");
				$postdata['d_modified']=date("Y-m-d H:i:s");
				$postdata["e_status"] = $this->input->post("e_status");

				$pagetitle = $this->general_model->get_Page('v_title',$postdata["v_title"]);

				if(count($pagetitle)){

					redirect(base_url().'admin/pages/add/?succ=0&msg=pagetitleexists');
				}
				if ($this->form_validation->run('pages') == TRUE){

					$addPages = $this->admin_model->add_entry($postdata,$tbl);

					redirect(base_url().'admin/pages/?succ=1&msg=addpage');
					exit;
				}else{
					$data =array_merge($data, $postdata);
				}

			}else{
				$data["v_title"]="";
				$data["l_description"]="";
				$data["v_meta_title"]="";
				$data["v_meta_keyword"]="";
				$data["l_meta_description"]="";
				$data["e_status"]="";
			}
		}
		elseif($script == "edit")
		{
			if($_SERVER['REQUEST_METHOD']=='POST'){

				$postdata["v_title"] = $this->input->post("v_title");
				$postdata["l_description"] = $this->input->post("l_description");
				$postdata["v_meta_title"] = $this->input->post("v_meta_title");
				$postdata["v_meta_keyword"] = $this->input->post("v_meta_keyword");
				$postdata["l_meta_description"] = $this->input->post("l_meta_description");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_modified']=date("Y-m-d H:i:s");
				$postdata["e_status"] = $this->input->post("e_status");

				$pagetitle = $this->general_model->get_Page_Data('v_title',$postdata["v_title"],$id);

				if(count($pagetitle)){

					redirect(base_url().'admin/pages/edit/'.$id.'?succ=0&msg=pagetitleexists');
				}

				$updatePages = $this->admin_model->update_entry($id,$postdata,$tbl);

				$meta_fields =  $this->input->post("meta_fields");

				if(!empty($meta_fields)){
						foreach($meta_fields as $mKey => $meta_field){
							$update_meta = $this->admin_model->update_meta($mKey, $meta_field, $id);
						}
					}

					if(!empty($_FILES['meta_fields']['name'])){
						foreach($_FILES['meta_fields']['name'] as $mKey => $meta_field){
							if($_FILES['meta_fields']['name'][$mKey] != ''){
								$image_name = time().$_FILES['meta_fields']['name'][$mKey];
								$dest = getcwd() . '/assets/images/';
								$file_tmp = $_FILES['meta_fields']['tmp_name'][$mKey];
								$is_uploaded = move_uploaded_file($file_tmp,$dest.$image_name);
								$update_meta = $this->admin_model->update_meta($mKey, $image_name, $id);
							}
						}
					}

				redirect(base_url().'admin/pages/?succ=1&msg=editpage');
				exit;


			}elseif($id !=""){
				$data['meta_fields'] =  $this->admin_model->getMetaFieldsWithItems('page', $id);
				$selectPages = $this->admin_model->select_entry($tbl,$id);
				$data =array_merge($data, $selectPages);

				// echo "<pre>";
				// print_r($data);exit();
			}
		}
		elseif($script == "active"){
			if($id){
				$postdata["e_status"] ="active";
				$updatePages = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/pages/?succ=1&msg=multiact');
				exit;
			}
		}
		elseif($script == "inactive"){
			if($id){
				$postdata["e_status"] ="inactive";
				$updatePages = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/pages/?succ=1&msg=multiinact');
				exit;
			}

		}
		elseif($script == "delete"){
			if($id){
				$deletepages = $this->admin_model->delete_entry($id,$tbl);
				redirect(base_url().'admin/pages/?succ=1&msg=multidel');
			}
		}


	   	$this->load->view('admin/pages',$data);
    }

    public function meta_fields($script="list", $id=""){

    	$user_data = $this->session->userdata["admin"];

		if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}

   	 	$data["page_title"] ="Manage Meta Fields";
		$data["page"] 	=	"Meta Fields";

		$data["script"] = $script;
		$tbl 			= "tbl_meta_fields";

		$query 	    	= $this->db->query("SELECT v_title,id FROM tbl_pages");
		$items 			= $query->result();
		$data["items"]  = $items;


		if($script == "list"){

			$wh="";

			$data["form"]="frm_meta_fields";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);
			$rows = $this->admin_model->getMetaList($arg);
			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];
		}
		elseif ($script == 'add'){

			if ($_SERVER['REQUEST_METHOD']=='POST') {



				$postdata["v_title"] = $this->input->post("v_title");
				$postdata["l_description"] = $this->input->post("l_description");
				$postdata["i_order"] = $this->input->post("i_order");
				$postdata["i_item_id"] = $this->input->post("i_item_id");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata["v_type"] = 'page';
				$postdata['d_added']= date("Y-m-d H:i:s");
				$postdata['d_modified']=date("Y-m-d H:i:s");
				$postdata["e_status"] = $this->input->post("e_status");


				// if ($this->form_validation->run('meta_field_itemsields') == TRUE){

				// 	$addMeta = $this->admin_model->add_entry($postdata,$tbl);
				// 	redirect(base_url().'admin/meta_fields/?succ=1&msg=addmeta');
				// 	exit;
				// }else{
				// 	$data =array_merge($data, $postdata);
				// }

				$addMeta = $this->admin_model->add_entry($postdata,$tbl);

				redirect(base_url().'admin/meta_fields/?succ=1&msg=addmeta');
				exit;

			}else{
				$data["v_title"] 		= '';
				$data["l_description"] 	= '';
				$data["i_order"] 		= '';
				$data["i_item_id"] 		= '';
				$data["e_status"] 		= '';
				$data["e_status"] 		= '';
			}
		}
		elseif($script == "edit")
		{

			if($_SERVER['REQUEST_METHOD']=='POST'){

				$postdata["v_title"] = $this->input->post("v_title");
				$postdata["l_description"] = $this->input->post("l_description");
				$postdata["i_order"] = $this->input->post("i_order");
				$postdata["i_item_id"] = $this->input->post("i_item_id");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata["v_type"] = 'page';
				$postdata['d_modified']=date("Y-m-d H:i:s");
				$postdata["e_status"] = $this->input->post("e_status");

				if ($this->form_validation->run('meta_fields') == TRUE){

					$updateMeta = $this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/meta_fields/?succ=1&msg=editmeta');
					exit;
				}
			}elseif($id !=""){

				$selectMeta = $this->admin_model->select_entry($tbl,$id);
				$data =array_merge($data, $selectMeta);
			}
		}
		elseif($script == "active"){
			if($id){
				$postdata["e_status"] ="active";
				$updateMeta = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/meta_fields/?succ=1&msg=multiact');
				exit;
			}
		}
		elseif($script == "inactive"){
			if($id){
				$postdata["e_status"] ="inactive";
				$updateMeta = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/meta_fields/?succ=1&msg=multiinact');
				exit;
			}

		}
		elseif($script == "delete"){
			if($id){
				$deleteMeta = $this->admin_model->delete_entry($id,$tbl);
				redirect(base_url().'admin/meta_fields/?succ=1&msg=multidel');
			}
		}elseif($script == "view"){

			$wh="";

			$data["form"]="frm_meta_fields";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);
			print_r($arg);exit();
			$rows = $this->admin_model->getMetaItems($arg,$id);

			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];
		}

	   	$this->load->view('admin/meta_fields',$data);
    }

   	public function meta_field_items($script="list", $id=""){

    	$user_data = $this->session->userdata["admin"];

		if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}

   	 	$data["page_title"] ="Manage Meta Fields";
		$data["page"] 	=	"Meta Fields";

		$data["script"] = $script;
		$tbl 			= "tbl_meta_items";

		$query 	    	= $this->db->query("SELECT v_title,id FROM tbl_pages");
		$items 			= $query->result();
		$data["items"]  = $items;

		//$id = $this->input->post("i_meta_id");

		if($script == "list"){

			$wh="";

			$data["form"]="frm_meta_fields";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);

			$rows = $this->admin_model->getMeta($arg,$id);

			$data['rows'] = $rows['rows'];
			$data['meta_id'] = $id;
			$data['nototal'] = $rows['nototal'];
			//echo "<pre>";print_r($data['rows']);exit;
		}
		elseif ($script == 'add'){

			if ($_SERVER['REQUEST_METHOD']=='POST') {
				$id = $this->input->post("i_meta_id");
				$postdata["v_title"] = $this->input->post("v_title");
				$postdata["i_meta_id"] = $this->input->post("i_meta_id");
				$postdata["v_key"] = $this->input->post("v_key");
				$postdata["v_type"] = $this->input->post("v_type");
				$postdata["i_order"] = $this->input->post("i_order");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_added']= date("Y-m-d H:i:s");
				$postdata['d_modified']=date("Y-m-d H:i:s");

				if ($this->input->post()){

					$addMeta = $this->admin_model->add_meta_item($postdata,$tbl);
					redirect(base_url().'admin/meta_field_items/view/'.$id.'/?succ=1&msg=addmeta');
					exit;
				}else{
					$data =array_merge($data, $postdata);
				}

			}else{
				$data["v_title"] 		= '';
				$data["v_key"] 			= '';
				$data["i_order"] 		= '';
				$data["v_type"] 		= '';
				$data["e_status"] 		= '';
				$data["e_status"] 		= '';
			}
		}
		elseif($script == "edit")
		{

			if($_SERVER['REQUEST_METHOD']=='POST'){
				 $meta_id = $this->input->post("i_meta_id");

				 $id = $this->input->post("main_id");
				$postdata["v_title"] = $this->input->post("v_title");
				$postdata["i_meta_id"] = $this->input->post("i_meta_id");
				$postdata["v_key"] = $this->input->post("v_key");
				$postdata["v_type"] = $this->input->post("v_type");
				$postdata["i_order"] = $this->input->post("i_order");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_modified']=date("Y-m-d H:i:s");

				$updateMetaItems = $this->admin_model->update_entry($id,$postdata,$tbl);

				redirect(base_url().'admin/meta_field_items/view/'.$meta_id.'/?succ=1&msg=editmeta');
				exit;


			}elseif($id !=""){
				$meta_id = $_GET['meta_id'];
				$selectMeta = $this->admin_model->select_entry($tbl,$meta_id);
				$data =array_merge($data, $selectMeta);

				$wh="";

				$data["form"]="frm_meta_fields";

				if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
					$data["limit"] = $this->input->get('pageno');
				}else{
					$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
				}

				if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
					$data["limitstart"] = $this->input->get('limitstart');
				}else{
					$data["limitstart"] = 0;
				}

				if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
					$data["keyword"] =  $this->input->get('keyword');
				}else{
					$data["keyword"] =  '';
				}

				$arg = array(
						'limit' => $data["limit"],
						'limitstart' =>$data["limitstart"],
						'keyword' =>$data["keyword"],
						'form' => $data["form"],
					);

				$rows = $this->admin_model->getMetaItems($arg,$id);

				$data['rows'] = $rows['rows'];
				$data['meta_id'] = $id;
				$data['nototal'] = $rows['nototal'];
			}
		}
		elseif($script == "delete"){
			$meta_id = $_GET['meta_id'];
			if($meta_id){

				$deleteadmin = $this->admin_model->delete_entry($meta_id,$tbl);
				redirect(base_url().'admin/meta_field_items/view/'.$id.'/?succ=1&msg=multidel');
			}
		}elseif($script == "view"){

			$wh="";

			$data["form"]="frm_meta_fields";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);

			$rows = $this->admin_model->getMetaItems($arg,$id);

			$data['rows'] = $rows['rows'];
			$data['meta_id'] = $id;
			$data['nototal'] = $rows['nototal'];
		}
	   	$this->load->view('admin/meta_field_items',$data);
    }


    public function manage_news($script="list", $id=""){

    	$user_data = $this->session->userdata["admin"];

		if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}

   	 	$data["page_title"] ="News";
		$data["page"] =	"News";

		$data["script"] = $script;
		$tbl = "tbl_news";

		if($script == "list"){
			$wh="";

			$data["form"]="frm_pages";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);
			$rows = $this->admin_model->getNewsList($arg);
			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];
			//echo "<pre>";print_r($data['rows']);exit;
		}
		elseif ($script == 'add'){

			if ($_SERVER['REQUEST_METHOD']=='POST') {

				if (!empty($_FILES)) {
			        $tempFile = $_FILES['v_image']['tmp_name'];
			        $fileName = $_FILES['v_image']['name'];
			        $targetPath = getcwd() . '/assets/images/';
			        // $fileName = time().'-'.$fileName;
			        $targetFile = $targetPath . $fileName ;
			        move_uploaded_file($tempFile, $targetFile);
			        $fname=$this->general_model->seoText($fileName);
					$new_value = time().$fname;
					rename($targetPath.$fileName, $targetPath.$new_value);
		        }

				$postdata["v_title"] = $this->input->post("v_title");
				$postdata["v_slug"] = $this->general_model->seoText($this->input->post("v_title"));
				$postdata["v_slug"] = $this->general_model->is_unique('tbl_news','v_slug',$postdata["v_slug"]);
				// $postdata["v_slug"] = $v_slug;
				$postdata["l_description"] = $this->input->post("l_description");
				$postdata["v_meta_title"] = $this->input->post("v_meta_title");
				$postdata["v_meta_keyword"] = $this->input->post("v_meta_keyword");
				$postdata["l_meta_description"] = $this->input->post("l_meta_description");
				$postdata["v_image"] = isset($new_value) ? $new_value : '';
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_added']= $this->input->post("d_added");
				$postdata['d_modified']=date("Y-m-d H:i:s");

				if ($this->form_validation->run('news') == TRUE){

					$addNews = $this->admin_model->add_entry($postdata,$tbl);

					redirect(base_url().'admin/manage_news/?succ=1&msg=addnews');
					exit;
				}else{
					$data =array_merge($data, $postdata);
				}

			}else{
				$data["v_title"]="";
				$data["l_description"]="";
				$data["v_meta_title"]="";
				$data["v_meta_keyword"]="";
				$data["l_meta_description"]="";
				$data["e_status"]="";
			}
		}
		elseif($script == "edit")
		{

			if($_SERVER['REQUEST_METHOD']=='POST'){

				if (!empty($_FILES)) {
					$tempFile = $_FILES['v_image']['tmp_name'];
			        $fileName = $_FILES['v_image']['name'];
			        $targetPath = getcwd() . '/assets/images/';
			        // $fileName = time().'-'.$fileName;
			        $targetFile = $targetPath . $fileName ;
			        move_uploaded_file($tempFile, $targetFile);
			        $fname=$this->general_model->seoText($fileName);
					$new_value = time().$fname;
					rename($targetPath.$fileName, $targetPath.$new_value);
					$postdata["v_image"] = $new_value;
		        }
		        if($fileName == ''){
		        	$fileName = $this->input->post("oriimg");
		        }
				$postdata["v_title"] = $this->input->post("v_title");
				$postdata["l_description"] = $this->input->post("l_description");
				$postdata["v_meta_title"] = $this->input->post("v_meta_title");
				$postdata["v_meta_keyword"] = $this->input->post("v_meta_keyword");
				$postdata["l_meta_description"] = $this->input->post("l_meta_description");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_added']= $this->input->post("d_added");
				$postdata['d_modified']=date("Y-m-d H:i:s");

				if ($this->form_validation->run('news') == TRUE){

					$updateNews = $this->admin_model->update_entry($id,$postdata,$tbl);

					redirect(base_url().'admin/manage_news/?succ=1&msg=editnews');
					exit;
				}
			}elseif($id !=""){

				$seleceNews = $this->admin_model->select_entry($tbl,$id);
				$data =array_merge($data, $seleceNews);
			}
		}
		elseif($script == "active"){
			if($id){
				$postdata["e_status"] ="active";
				$updateNews = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/manage_news/?succ=1&msg=multiact');
				exit;
			}
		}
		elseif($script == "inactive"){
			if($id){
				$postdata["e_status"] ="inactive";
				$updateNews = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/manage_news/?succ=1&msg=multiinact');
				exit;
			}

		}
		elseif($script == "delete"){
			if($id){
				$deleteNews = $this->admin_model->delete_entry($id,$tbl);
				redirect(base_url().'admin/manage_news/?succ=1&msg=multidel');
			}
		}

	   	$this->load->view('admin/manage_news',$data);
    }

    public function manage_faqs($script="list", $id=""){

    	$user_data = $this->session->userdata["admin"];

		if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}

   	 	$data["page_title"] ="Q&A";
		$data["page"] =	"Q&A";

		$data["script"] = $script;
		$tbl = "tbl_faqs";

		if($script == "list"){
			$wh="";

			$data["form"]="frm_pages";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);
			$rows = $this->admin_model->getFaqsList($arg);
			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];
		}
		elseif ($script == 'add'){

			if ($_SERVER['REQUEST_METHOD']=='POST') {

				$postdata["v_question"] = $this->input->post("v_question");
				$postdata["l_answer"] = $this->input->post("l_answer");
				$postdata["i_order"] = $this->input->post("i_order");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_added']= date("Y-m-d H:i:s");
				$postdata['d_modified']=date("Y-m-d H:i:s");

				if ($this->form_validation->run('faqs') == TRUE){

					$addFaqs = $this->admin_model->add_entry($postdata,$tbl);

					redirect(base_url().'admin/manage_faqs/?succ=1&msg=add');
					exit;
				}else{
					$data =array_merge($data, $postdata);
				}

			}else{
				$data["v_question"]="";
				$data["l_answer"]="";
				$data["e_status"]="";
			}
		}
		elseif($script == "edit")
		{

			if($_SERVER['REQUEST_METHOD']=='POST'){

				$postdata["v_question"] = $this->input->post("v_question");
				$postdata["i_order"] = $this->input->post("i_order");
				$postdata["l_answer"] = $this->input->post("l_answer");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_modified']=date("Y-m-d H:i:s");

				if ($this->form_validation->run('faqs') == TRUE){

					$updateFaqs = $this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/manage_faqs/?succ=1&msg=edit');
					exit;
				}
			}elseif($id !=""){

				$selectFaqs = $this->admin_model->select_entry($tbl,$id);
				$data =array_merge($data, $selectFaqs);
			}
		}
		elseif($script == "active"){
			if($id){
				$postdata["e_status"] ="active";
				$updateFaqs = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/manage_faqs/?succ=1&msg=multiact');
				exit;
			}
		}
		elseif($script == "inactive"){
			if($id){
				$postdata["e_status"] ="inactive";
				$updateFaqs = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/manage_faqs/?succ=1&msg=multiinact');
				exit;
			}

		}
		elseif($script == "delete"){
			if($id){
				$deleteFaqs = $this->admin_model->delete_entry($id,$tbl);
				redirect(base_url().'admin/manage_faqs/?succ=1&msg=multidel');
			}
		}


	   	$this->load->view('admin/manage_faqs',$data);
    }

    public function email_template($script="list", $id=""){

    	$user_data = $this->session->userdata["admin"];

		if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}

   	 	$data["page_title"] ="Manage Email Templates";
		$data["page"] =	"Email Templates";

		$data["script"] = $script;
		$tbl = "tbl_email_templates";

		if($script == "list"){
			$wh="";

			$data["form"]="frm_pages";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);
			$rows = $this->admin_model->getEmailTemplateList($arg);
			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];
		}
		elseif ($script == 'add'){

			if ($_SERVER['REQUEST_METHOD']=='POST') {

				$postdata["v_title"] = $this->input->post("v_title");
				$postdata["v_subject"] = $this->input->post("v_subject");
				$postdata["v_reply_to"] = $this->input->post("v_reply_to");
				$postdata["l_body"] = $this->input->post("l_body");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_added']= date("Y-m-d H:i:s");
				$postdata['d_modified']=date("Y-m-d H:i:s");

				if ($this->form_validation->run('email_template') == TRUE){

					$addEmail = $this->admin_model->add_entry($postdata,$tbl);

					redirect(base_url().'admin/email_template/?succ=1&msg=add');
					exit;
				}else{
					$data =array_merge($data, $postdata);
				}

			}else{
				$data["v_title"]="";
				$data["v_subject"]="";
				$data["v_reply_to"] ="";
				$data["l_body"] ="";
				$data["e_status"]="";
			}
		}
		elseif($script == "edit")
		{

			if($_SERVER['REQUEST_METHOD']=='POST'){

				$postdata["v_title"] = $this->input->post("v_title");
				$postdata["v_subject"] = $this->input->post("v_subject");
				$postdata["v_reply_to"] = $this->input->post("v_reply_to");
				$postdata["l_body"] = $this->input->post("l_body");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_modified']=date("Y-m-d H:i:s");

				if($this->form_validation->run("email_template") == TRUE){

					$updateEmail = $this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/email_template/?succ=1&msg=edit');
					exit;
				}
			}elseif($id !=""){

				$selectEmail = $this->admin_model->select_entry($tbl,$id);
				$data =array_merge($data, $selectEmail);
				//echo "<pre>";print_r($data);exit;
			}
		}
		elseif($script == "active"){
			if($id){
				$postdata["e_status"] ="active";
				$updateEmail = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/email_template/?succ=1&msg=multiact');
				exit;
			}
		}
		elseif($script == "inactive"){
			if($id){
				$postdata["e_status"] ="inactive";
				$updateEmail = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/email_template/?succ=1&msg=multiinact');
				exit;
			}

		}
		elseif($script == "delete"){
			if($id){
				$deleteEmail = $this->admin_model->delete_entry($id,$tbl);
				redirect(base_url().'admin/email_template/?succ=1&msg=multidel');
			}
		}


	   	$this->load->view('admin/manage_email_template',$data);
    }

    public function manage_testimonials($script="list", $id=""){

    	$user_data = $this->session->userdata["admin"];

		if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}

   	 	$data["page_title"] ="Testimonials";
		$data["page"] =	"Testimonials";

		$data["script"] = $script;
		$tbl = "tbl_testimonials";

		if($script == "list"){
			$wh="";

			$data["form"]="frm_pages";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);
			$rows = $this->admin_model->getTestimonialsList($arg);
			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];
		}
		elseif ($script == 'add'){

			if ($_SERVER['REQUEST_METHOD']=='POST') {

				if (!empty($_FILES['v_image']) && $_FILES['v_image']['name'] !='') {
					$tempFile = $_FILES['v_image']['tmp_name'];
			        $fileName = $_FILES['v_image']['name'];
			        $targetPath = getcwd() . '/assets/images/';
			        $fname=$this->general_model->seoText($fileName);
					$new_value = time().$fname;
					$targetFile = $targetPath . $new_value ;
			        move_uploaded_file($tempFile, $targetFile);
			        $postdata["v_image"] = $new_value;
		        }

				$postdata["v_name"] = $this->input->post("v_name");
				$postdata["l_text"] = $this->input->post("l_text");
				$postdata["v_image"] = isset($new_value) ? $new_value : '';
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_added']= date("Y-m-d H:i:s");
				$postdata['d_modified']=date("Y-m-d H:i:s");

				if ($this->form_validation->run('testimonials') == TRUE){

					$addTestimonials = $this->admin_model->add_entry($postdata,$tbl);

					redirect(base_url().'admin/manage_testimonials/?succ=1&msg=add');
					exit;
				}else{
					$data =array_merge($data, $postdata);
				}

			}else{
				$data["v_name"]="";
				$data["l_text"]="";
				$data["e_status"]="";
			}
		}
		elseif($script == "edit")
		{

			if($_SERVER['REQUEST_METHOD']=='POST'){

				if (!empty($_FILES['v_image']) && $_FILES['v_image']['name'] !='') {
					$tempFile = $_FILES['v_image']['tmp_name'];
			        $fileName = $_FILES['v_image']['name'];
			        $targetPath = getcwd() . '/assets/images/';
			        // $fileName = time().'-'.$fileName;
			        $fname=$this->general_model->seoText($fileName);
					$new_value = time().$fname;
					$targetFile = $targetPath . $new_value ;
			        move_uploaded_file($tempFile, $targetFile);
			        $postdata["v_image"] = $new_value;
		        }
		        
				$postdata["v_name"] = $this->input->post("v_name");
				$postdata["l_text"] = $this->input->post("l_text");				
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_modified']=date("Y-m-d H:i:s");
				
				//echo "<pre>"; print_r($_FILES);
				//echo "<pre>"; print_r($postdata);
				//exit;
				// if ($this->form_validation->run('testimonials') == TRUE){

					$update = $this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/manage_testimonials/?succ=1&msg=edit');
					exit;
				// }
			}elseif($id !=""){

				$selectTestimonials = $this->admin_model->select_entry($tbl,$id);
				$data =array_merge($data, $selectTestimonials);
			}
		}
		elseif($script == "active"){
			if($id){
				$postdata["e_status"] ="active";
				$updateTestimonials = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/manage_testimonials/?succ=1&msg=multiact');
				exit;
			}
		}
		elseif($script == "inactive"){
			if($id){
				$postdata["e_status"] ="inactive";
				$updateTestimonials = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/manage_testimonials/?succ=1&msg=multiinact');
				exit;
			}

		}
		elseif($script == "delete"){
			if($id){
				$deleteTestimonials = $this->admin_model->delete_entry($id,$tbl);
				redirect(base_url().'admin/manage_testimonials/?succ=1&msg=multidel');
			}
		}
	   	$this->load->view('admin/manage_testimonials',$data);
    }

    // SK >>

    public function photos($script="list", $id=""){

    	if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}

    	$user_data = $this->session->userdata["admin"];

   	 	$data["page_title"] ="Photos";
		$data["page"] =	"Photos";

		$data["script"] = $script;

		$tbl = "tbl_photos";

		if($script == "list"){
			$wh="";

			$data["form"]="frm_photos";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);
			$rows = $this->admin_model->getPhotoList($arg);
			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];
		}
		elseif ($script == 'add'){

			if ($_SERVER['REQUEST_METHOD']=='POST') {

				if ($this->form_validation->run('photos') == TRUE){

					$postdata["v_slug"] = $this->general_model->seoText($this->input->post("v_name"));
					$postdata["v_slug"] = $this->general_model->is_unique('tbl_photos','v_slug',$postdata["v_slug"]);
					$postdata["i_parent"] = $this->input->post("i_parent");
					$postdata["v_name"] = $this->input->post("v_name");
					$postdata["l_description"] = $this->input->post("l_description");
					$postdata["v_meta_title"] = $this->input->post("v_meta_title");
					$postdata["v_meta_keyword"] = $this->input->post("v_meta_keyword");
					$postdata["l_meta_description"] = $this->input->post("l_meta_description");
					$postdata["e_status"] = $this->input->post("e_status");
					$postdata['d_added']= date("Y-m-d H:i:s");
					$postdata['d_modified']=date("Y-m-d H:i:s");

					$last_id = $this->admin_model->add_entry($postdata,$tbl);

					$_insert = array();
					$v_images = explode("::", $this->input->post("v_image"));

					if(!empty($v_images)){
						foreach($v_images as $v_image){
							if ($v_image != '') {
								$dest = getcwd().'/assets/frontend/images/';
								$filename=$this->general_model->seoText($v_image);
								$new_value = time().$filename;
								rename($dest.$filename, $dest.$new_value);
								$_insert[] = array(
									'i_photos_id' => $last_id,
									'v_image' => $new_value
								);
							}
						}
					}

					if( isset($_insert) && !empty($_insert) ) {
						$this->admin_model->add_multi_entry('tbl_photosimage',$_insert);
					}

					redirect(base_url().'admin/photos/?succ=1&msg=add');
					exit;
				}
				else{
					$postdata["v_slug"] = $this->general_model->seoText($this->input->post("v_name"));
					$postdata["v_slug"] = $this->general_model->is_unique('tbl_photos','v_slug',$postdata["v_slug"]);
					$postdata["i_parent"] = $this->input->post("i_parent");
					$postdata["v_name"] = $this->input->post("v_name");
					$postdata["l_description"] = $this->input->post("l_description");
					$postdata["e_status"] = $this->input->post("e_status");
					$postdata['d_added']= date("Y-m-d H:i:s");
					$postdata['d_modified']=date("Y-m-d H:i:s");
					$data =array_merge($data, $postdata);
				}
			}else{
				$data["v_name"]="";
				$data["e_status"]="";
			}
		}
		elseif($script == "edit") {

			if($_SERVER['REQUEST_METHOD']=='POST'){
				// echo "<pre>";
				// print_r($_REQUEST);
				// die();
				$postdata["v_slug"] = $this->general_model->seoText($this->input->post("v_name"));
				$postdata["v_slug"] = $this->general_model->is_unique('tbl_photos','v_slug',$postdata["v_slug"]);
				$postdata["i_parent"] = $this->input->post("i_parent");
				$postdata["v_name"] = $this->input->post("v_name");
				$postdata["l_description"] = $this->input->post("l_description");
				$postdata["v_meta_title"] = $this->input->post("v_meta_title");
				$postdata["v_meta_keyword"] = $this->input->post("v_meta_keyword");
				$postdata["l_meta_description"] = $this->input->post("l_meta_description");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_added']= date("Y-m-d H:i:s");
				$postdata['d_modified']=date("Y-m-d H:i:s");

				$last_id = $id;
				$v_images = explode("::", $this->input->post("v_image"));

				if(!empty($v_images)){
					foreach($v_images as $v_image){
						if ($v_image != '') {
							$dest = getcwd().'/assets/frontend/images/';
							$filename=$this->general_model->seoText($v_image);
							$new_value = time().$filename;	
							rename($dest.$filename, $dest.$new_value);
							$_insert[] = [
								'i_photos_id' => $last_id,
								'v_image' => $new_value
							];
						}
					}
				}

				if( isset($_insert) && !empty($_insert) ) {
					$this->admin_model->add_multi_entry('tbl_photosimage',$_insert);
				}
				$postdata["v_name"] =$this->input->post("v_name");
				$postdata["v_meta_title"] = $this->input->post("v_meta_title");
				$postdata["v_meta_keyword"] = $this->input->post("v_meta_keyword");
				$postdata["l_meta_description"] = $this->input->post("l_meta_description");
				$postdata["v_slug"] = $this->input->post("v_slug");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_modified']=date("Y-m-d H:i:s");
				$this->admin_model->update_photos($this->input->post("v_slug"),$postdata,$tbl);

				redirect(base_url().'admin/photos/?succ=1&msg=edit');
				exit;
			}elseif($id !=""){

				$data['rows'] = $this->admin_model->select_photo($tbl,$id);

				// $data =array_merge($data, $select);
				// echo "<pre>";
				// print_r($data['rows'][0]['v_category']);
				// exit;
			}
		}

		elseif($script == "active"){
			if($id){
				$tbl = 'tbl_photos';
				$postdata["e_status"] ="active";
				$updatePhoto = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/photos/?succ=1&msg=multiact');
				exit;
			}
		}
		elseif($script == "inactive"){
			if($id){
				$tbl = 'tbl_photos';
				$postdata["e_status"] ="inactive";
				$updatePhoto = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/photos/?succ=1&msg=multiinact');
				exit;
			}

		}
		elseif($script == "delete"){
			if($id){
				$tbl = 'tbl_photos';
				$deletePhoto = $this->admin_model->delete_photo($id,$tbl);
				$deleteimage = $this->admin_model->delete_image($id,'tbl_photosimage');
				redirect(base_url().'admin/photos/?succ=1&msg=multidel');
			}
		}
	   	$this->load->view('admin/photos',$data);
    }

    public function videos($script="list", $id=""){

    	$user_data = $this->session->userdata["admin"];

		if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}

   	 	$data["page_title"] ="Videos";
		$data["page"] =	"Videos";

		$data["script"] = $script;

		$tbl = "tbl_videos";

		if($script == "list"){
			$wh="";

			$data["form"]="frm_videos";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);
			$rows = $this->admin_model->getVideoList($arg);
			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];
		}

		elseif ($script == 'add'){

			if ($_SERVER['REQUEST_METHOD']=='POST') {

				$postdata["v_name"] = $this->input->post("v_name");
				$postdata["v_meta_title"] = $this->input->post("v_meta_title");
				$postdata["v_meta_keyword"] = $this->input->post("v_meta_keyword");
				$postdata["l_meta_description"] = $this->input->post("l_meta_description");
				$postdata["l_description"] =$this->input->post("l_description");
				$postdata["v_url"] = $this->input->post("v_url");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_added']= date("Y-m-d H:i:s");
				$postdata['d_modified']=date("Y-m-d H:i:s");

				if ($this->form_validation->run('videos') == TRUE){

					$addVideos = $this->admin_model->add_entry($postdata,$tbl);

					redirect(base_url().'admin/videos/?succ=1&msg=add');
					exit;
				}else{
					$data =array_merge($data, $postdata);
				}

			}else{
				$data["v_name"]="";
				$date["l_description"]="";
				$data["v_url"]="";
				$data["e_status"]="";
			}
		}

		elseif ($script == 'edit'){

			if($_SERVER['REQUEST_METHOD']=='POST'){

				$postdata["v_name"] = $this->input->post("v_name");
				$postdata["v_meta_title"] = $this->input->post("v_meta_title");
				$postdata["v_meta_keyword"] = $this->input->post("v_meta_keyword");
				$postdata["l_meta_description"] = $this->input->post("l_meta_description");
				$postdata["l_description"] =$this->input->post("l_description");
				$postdata["v_url"] = $this->input->post("v_url");
				$postdata["e_status"] = $this->input->post("e_status");
				$postdata['d_modified']=date("Y-m-d H:i:s");

				if ($this->form_validation->run('videos') == TRUE){

					$updateVideos = $this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/videos/?succ=1&msg=edit');
					exit;
				}
			}elseif($id !=""){

				$selectVideos = $this->admin_model->select_entry($tbl,$id);
				$data =array_merge($data, $selectVideos);
				//echo "<pre>";print_r($data);exit;
			}
		}

		elseif($script == "active"){
			if($id){
				$tbl = 'tbl_videos';
				$postdata["e_status"] ="active";
				$updateVideo = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/videos/?succ=1&msg=multiact');
				exit;
			}
		}
		elseif($script == "inactive"){
			if($id){
				$tbl = 'tbl_videos';
				$postdata["e_status"] ="inactive";
				$updateVideo = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/videos/?succ=1&msg=multiinact');
				exit;
			}

		}
		elseif($script == "delete"){
			if($id){
				$tbl = 'tbl_videos';
				$deleteVideo = $this->admin_model->delete_entry($id,$tbl);
				redirect(base_url().'admin/videos/?succ=1&msg=multidel');
			}
		}
    	$this->load->view('admin/videos',$data);
    }

    public function team($script="list", $id=""){

    	$user_data = $this->session->userdata["admin"];

		if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}
	   	$data["page_title"] ="Team";
		$data["page"] =	"Team";

		$data["script"] = $script;

		$tbl = "tbl_team";

		if($script == "list"){
			$wh="";

			$data["form"]="frm_team";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			if(($this->input->get('members_type')) && $this->input->get('members_type')!=''){
				$data["members_type"] =  $this->input->get('members_type');
			}else{
				$data["members_type"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'members_type' =>$data["members_type"],
					'form' => $data["form"],
				);
			$rows = $this->admin_model->getTeamList($arg);
			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];

			$member_types = [
				'' => '-',
				'Team' => 'Team',
				'Honorary' => 'Honorary Members',
				'Membership' => 'Membership Committee',
			];

			$data['selected_member_type'] = $this->input->get('members_type');
			$data['member_types'] = $member_types;
		}

		if($script == "add"){

			if ($_SERVER['REQUEST_METHOD']=='POST') {

					$v_image = $this->input->post("v_image");

					if ($v_image != '') {
						$dest = getcwd().'/assets/frontend/images/';
						$filename=$this->general_model->seoText($v_image);
						$new_value = time().$filename;
						rename($dest.$v_image, $dest.$new_value);
						$postdata["v_image"] = $new_value;
					}

					$postdata["e_type"] = $this->input->post("e_type");
					$postdata["v_name"] =$this->input->post("v_name");
					$postdata["v_position"] =$this->input->post("v_position");
					$postdata["l_description"] =$this->input->post("l_description");
					$postdata["i_order"] =$this->input->post("i_order");
					$postdata["e_status"] = $this->input->post("e_status");
					$postdata['d_added']= date("Y-m-d H:i:s");
					$postdata['d_modified']=date("Y-m-d H:i:s");

					$this->admin_model->add_entry($postdata,$tbl);

					redirect(base_url().'admin/team/?succ=1&msg=add');
					exit;

					$data =array_merge($data, $postdata);
			}else{
				$data["v_name"]="";
				$data["v_position"]="";
				$data["l_description"]="";
				$data["e_status"]="";
			}
		}

		elseif ($script == 'edit'){

			if ($_SERVER['REQUEST_METHOD']=='POST') {

				$v_image = $this->input->post("v_image");

					if($v_image !=''){

						$dest = getcwd().'/assets/frontend/images/';
						$filename=$this->general_model->seoText($v_image);
						$new_value = time().$filename;
						rename($dest.$v_image, $dest.$new_value);
						$postdata["v_image"] = $new_value;
					}

					$postdata["e_type"] = $this->input->post("e_type");
					$postdata["v_name"] =$this->input->post("v_name");
					$postdata["v_position"] =$this->input->post("v_position");
					$postdata["l_description"] =$this->input->post("l_description");
					$postdata["i_order"] =$this->input->post("i_order");
					$postdata["e_status"] = $this->input->post("e_status");
					$postdata['d_modified']=date("Y-m-d H:i:s");

					$this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/team/?succ=1&msg=edit');
					exit;

			}elseif($id !=""){

				$selectTeam = $this->admin_model->select_entry($tbl,$id);
				$data =array_merge($data, $selectTeam);
			}
		}

		elseif($script == "active"){
			if($id){
				$tbl = 'tbl_team';
				$postdata["e_status"] ="active";
				$updateTeam = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/team/?succ=1&msg=multiact');
				exit;
			}
		}
		elseif($script == "inactive"){
			if($id){
				$tbl = 'tbl_team';
				$postdata["e_status"] ="inactive";
				$updateTeam = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/team/?succ=1&msg=multiinact');
				exit;
			}

		}
		elseif($script == "delete"){
			if($id){
				$tbl = 'tbl_team';
				$deleteTeam = $this->admin_model->delete_entry($id,$tbl);
				redirect(base_url().'admin/team/?succ=1&msg=multidel');
			}
		}
	   	$this->load->view('admin/team',$data);
    }

    public function books($script="list", $id=""){

    	$user_data = $this->session->userdata["admin"];

		if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}

	   	$data["page_title"] ="Books";
		$data["page"] =	"Books";

		$data["script"] = $script;

		$tbl = "tbl_books";

		if($script == "list"){

			$wh="";

			$data["form"]="frm_books";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);
			$rows = $this->admin_model->getBooksList($arg);
			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];
		}

		if($script == "add"){

			if ($_SERVER['REQUEST_METHOD']=='POST') {


				if ($this->form_validation->run('books') == TRUE){
					$v_image = $this->input->post("v_image");

					if ($v_image != '') {
						$dest = getcwd().'/assets/frontend/images/';
						$filename=$this->general_model->seoText($v_image);
						$new_value = time().$filename;
						rename($dest.$v_image, $dest.$new_value);
						$postdata["v_image"] = $new_value;
					}

					$postdata["v_title"] =$this->input->post("v_title");
					$postdata["l_description"] =$this->input->post("l_description");
					$postdata["e_status"] = $this->input->post("e_status");
					$postdata['d_added']= date("Y-m-d H:i:s");
					$postdata['d_modified']=date("Y-m-d H:i:s");
					$this->admin_model->add_entry($postdata,$tbl);

					redirect(base_url().'admin/books/?succ=1&msg=add');
					exit;
				}else{
					$postdata["v_title"] =$this->input->post("v_title");
					$postdata["l_description"] =$this->input->post("l_description");
					$postdata["e_status"] = $this->input->post("e_status");
					$postdata['d_added']= date("Y-m-d H:i:s");
					$postdata['d_modified']=date("Y-m-d H:i:s");
					$data =array_merge($data, $postdata);
				}
			}else{
				$data["v_title"]="";
				$data["l_description"]="";
				$data["e_status"]="";
			}
		}

		if($script == "edit"){

			if ($_SERVER['REQUEST_METHOD']=='POST') {

					$v_image = $this->input->post("v_image");

					if ($v_image != '') {
						$dest = getcwd().'/assets/frontend/images/';
						$filename=$this->general_model->seoText($v_image);
						$new_value = time().$filename;
						rename($dest.$v_image, $dest.$new_value);
						$postdata["v_image"] = $new_value;
					}

					$postdata["v_title"] =$this->input->post("v_title");
					$postdata["l_description"] =$this->input->post("l_description");
					$postdata["e_status"] = $this->input->post("e_status");
					$postdata['d_modified']=date("Y-m-d H:i:s");
					$this->admin_model->update_entry($id,$postdata,$tbl);

				redirect(base_url().'admin/books/?succ=1&msg=edit');
				exit;
			}elseif($id !=""){

				$selectBooks = $this->admin_model->select_entry($tbl,$id);
				$data =array_merge($data, $selectBooks);
			}
		}

		elseif($script == "active"){
			if($id){
				$tbl = 'tbl_books';
				$postdata["e_status"] ="active";
				$updateBooks = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/books/?succ=1&msg=multiact');
				exit;
			}
		}

		elseif($script == "inactive"){
			if($id){
				$tbl = 'tbl_books';
				$postdata["e_status"] ="inactive";
				$updateBooks = $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/books/?succ=1&msg=multiinact');
				exit;
			}
		}

		elseif($script == "delete"){
			if($id){
				$tbl = 'tbl_books';
				$deleteBooks = $this->admin_model->delete_entry($id,$tbl);
				redirect(base_url().'admin/books/?succ=1&msg=multidel');
			}
		}
    	$this->load->view('admin/books',$data);
    }

    public function home_slider($script="list", $id=""){

    	if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}
	   	$user_data = $this->session->userdata["admin"];

	   	$data["page_title"] ="Slider";
		$data["page"] =	"Slider";

		$data["script"] = $script;

		$tbl = "tbl_slider";

		if($script == "list"){

			$wh="";

			$data["form"]="frm_slider";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);
			$rows = $this->admin_model->getSliderList($arg);
			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];
		}

		if($script == "add"){

			if ($_SERVER['REQUEST_METHOD']=='POST') {

				$v_image = $this->input->post("v_image");
				if ($this->form_validation->run('home_slider') == TRUE){
					if( $v_image !=''){
						$dest = getcwd().'/assets/frontend/images/';
						$filename=$this->general_model->seoText($v_image);
						$new_value = time().$filename;
						rename($dest.$v_image, $dest.$new_value);
						$postdata["v_image"] = $new_value;
					}

					$postdata["l_main_text"] =$this->input->post("l_main_text");
					$postdata["v_text"] =$this->input->post("v_text");
					$postdata["e_status"] = $this->input->post("e_status");
					$postdata['d_added']= date("Y-m-d H:i:s");
					$postdata['d_modified']=date("Y-m-d H:i:s");
					$this->admin_model->add_entry($postdata,$tbl);

					redirect(base_url().'admin/home_slider/?succ=1&msg=add');
					exit;

				}else{

					$data =array_merge($data, $postdata);
				}
			}else{
				$data["l_main_text"]="";
				$data["v_text"]="";
				$data["e_status"]="";
			}
		}

		if($script == "edit"){

			if ($_SERVER['REQUEST_METHOD']=='POST') {

				$v_image = $this->input->post("v_image");

				if ($this->form_validation->run('home_slider') == TRUE){
					if( $v_image !=''){
						$dest = getcwd().'/assets/frontend/images/';
						$filename=$this->general_model->seoText($v_image);
						$new_value = time().$filename;
						rename($dest.$v_image, $dest.$new_value);
						$postdata["v_image"] = $new_value;
					}
					$postdata["l_main_text"] =$this->input->post("l_main_text");
					$postdata["v_text"] =$this->input->post("v_text");
					$postdata["e_status"] = $this->input->post("e_status");
					$postdata['d_modified']=date("Y-m-d H:i:s");
					$this->admin_model->update_entry($id,$postdata,$tbl);
				}
				redirect(base_url().'admin/home_slider/?succ=1&msg=edit');
				exit;
			}elseif($id !=""){

				$selectSlider = $this->admin_model->select_entry($tbl,$id);
				$data =array_merge($data, $selectSlider);
			}
		}

		elseif($script == "active"){
			if($id){
				$tbl = 'tbl_slider';
				$postdata["e_status"] ="active";
				$updateSlider= $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/home_slider/?succ=1&msg=multiact');
				exit;
			}
		}

		elseif($script == "inactive"){
			if($id){
				$tbl = 'tbl_slider';
				$postdata["e_status"] ="inactive";
				$updateSlider= $this->admin_model->update_entry($id,$postdata,$tbl);
				redirect(base_url().'admin/home_slider/?succ=1&msg=multiinact');
				exit;
			}
		}

		elseif($script == "delete"){
			if($id){
				$tbl = 'tbl_slider';
				$deletSlider = $this->admin_model->delete_entry($id,$tbl);
				
				redirect(base_url().'admin/home_slider/?succ=1&msg=multidel');


			}
		}
    	$this->load->view('admin/home_slider',$data);

    }
	public function delete_all_members(){
		
		$this->db->truncate('tbl_achievements_data');
		$this->db->truncate('tbl_crm_members_data');
		$this->db->truncate('tbl_educational_data');
		$this->db->truncate('tbl_members');
		$this->db->truncate('tbl_members_temp');
		$this->db->truncate('tbl_members_updates');
		$this->db->truncate('tbl_professional_data');
		
		redirect(base_url().'admin/members/?succ=1&msg=multidel');
		exit;
	}
    public function members($script="list", $id=""){

    	if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}
	   	$user_data = $this->session->userdata["admin"];

	   	$data['industry_data'] = $this->tomouh_model->getIndustryActiveData();

	   	$data["page_title"] ="Member";
		$data["page"] =	"Member";

		$data["script"] = $script;

		$tbl = "tbl_members";

		if($script == "list"){

			$wh="";

			$data["form"]="frm_members";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			if(($this->input->get('status')) && $this->input->get('status')!=''){
				$data["status"] =  $this->input->get('status');
			}else{
				$data["status"] =  '';
			}

			if(($this->input->get('export_members')) && $this->input->get('export_members')!=''){
				$this->getExportListOfMember();
			}else{
				$data["export_members"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'status'=>$data['status'],
					'form' => $data["form"],
				);
			$rows = $this->admin_model->getMemberList($arg);
			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];
		}

		if($script == "add"){


			if ($_SERVER['REQUEST_METHOD']=='POST') {

				$address = $this->input->post("v_residence_city").','.$this->input->post("v_residence_country");

				$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');

				$geo = json_decode($geo, true);

				if (isset($geo['status']) && ($geo['status'] == 'OK')) {

					$latitude = $geo['results'][0]['geometry']['location']['lat'];
					$longitude = $geo['results'][0]['geometry']['location']['lng'];
				}

				if($this->input->post("referral") == 0){

					$referral_email = '';
				}else{

					$referral_email = $this->input->post("referral_email");
				}

				//Mailing Part----------------------

				$user = $this->input->post("v_email");

				$email_to = $user;
				$email_from = "";

				$template = $this->tomouh_model->getEmailTemplate(2);
				$email_subject = $template['v_subject'];
				$content = $template['l_body'];

				$activation_link = '<a href="'.base_url().'login/activation/'.md5($this->input->post('v_email')).'" target="_blank">link here</a>';
				// print_r($activation_link); exit;
				$content = str_replace("link here", $activation_link, $content);

				// print_r($content);exit();
				$sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content , $attachments = array() );

				$email = $this->input->post('v_email');
				$query = $this->db->query("SELECT * FROM tbl_members");

				$results = $query->result_array();

		    	foreach($results as $result){

		      		$saved_email = $result['v_email'];
		      		if($email == $saved_email){

		      			redirect(base_url().'admin/members/add/?succ=0&msg=sameemail');
						exit;
		      		}
		    	}

				$insert_data["v_firstname"] = $this->input->post("v_firstname");
				$insert_data["v_lastname"] = $this->input->post("v_lastname");
				$insert_data["d_dob"] = $this->input->post("d_dob");
				$insert_data["e_gender"] = $this->input->post("e_gender");
				$insert_data["v_email"] = $this->input->post("v_email");
				$insert_data["v_password"] = md5($this->input->post("v_password"));
				$insert_data["v_home_country"] = $this->input->post("v_home_country");
				$insert_data["v_home_city"] = $this->input->post("v_home_city");
				$insert_data["v_residence_country"] = $this->input->post("v_residence_country");
				$insert_data["v_residence_city"] = $this->input->post("v_residence_city");
				$insert_data["v_telephone"] = $this->input->post("v_telephone");
				$insert_data["v_telephone_2"] = $this->input->post("v_telephone_2");
				$insert_data["v_twitter_link"] = $this->input->post("v_twitter_link");
				$insert_data["e_referral"] = $this->input->post("referral");
				$insert_data["v_referral_email"] = $referral_email;
				$insert_data["v_linkedin_link"] = $this->input->post("v_linkedin_link");
				$insert_data["t_passion_interest"] = $this->input->post("passions_interests");
				// $insert_data["t_passion_interest"]  = isset($passions_interests)?$passions_interests:'';
				if($this->input->post("e_member_of_month")){
					$insert_data['e_member_of_month']= $this->input->post("e_member_of_month");
				}else{
					$insert_data['e_member_of_month']= 0;
 				}
				$insert_data['v_month_text']= $this->input->post("v_month_text");
				
				$insert_data['e_status'] = 'inactive';
				$insert_data['v_longitude']= $longitude;
				$insert_data['v_latitude']= $latitude;
				$insert_data['e_type'] = 'non_member';

				$tbl = "tbl_members";
				$add_user_data = $this->admin_model->add_entry($insert_data,$tbl);

				$main_occupation_data = $this->input->post('occupation');
				$int_main_occupation = intval(preg_replace('/[^0-9]+/', '', $main_occupation_data), 10);

				$educ_data_length = sizeof($this->input->post("v_university"));

				for($i=0;$i<$educ_data_length;$i++){

					if($int_main_occupation <= 10){

						if(($i+1) == $int_main_occupation){
							$main_occupation = 1;
						}else{
							$main_occupation = 0;
						}
					}else{
						$main_occupation = 0;
					}
					$insert_array = array(

						'user_id'=>$add_user_data,
						'v_university' =>$this->input->post("v_university")[$i],
						'v_degree'=>$this->input->post("v_degree")[$i],
						'v_major'=>$this->input->post("v_major")[$i],
						'i_passing_year'=>$this->input->post("graduation_year")[$i],
						'i_main_occupation'=>$main_occupation
					);
					$tbl="tbl_educational_data";

					$this->admin_model->add_entry($insert_array,$tbl);
				}

				$company_data_length = sizeof($this->input->post("v_company"));

				for($i=0;$i<$company_data_length;$i++){

					if($int_main_occupation >= 10){

						if(($i+11) == $int_main_occupation){
							$main_occupation = 1;
						}else{
							$main_occupation = 0;
						}
					}else{
						$main_occupation = 0;
					}
					$insert_array = array(

						'user_id'=>$add_user_data,
						'v_company' =>$this->input->post("v_company")[$i],
						'v_job_title'=>$this->input->post("v_job_title")[$i],
						'v_industry'=>$this->input->post("v_industry")[$i],
						'i_company_from'=>$this->input->post("company_from")[$i],
						'i_company_to'=>$this->input->post("company_to")[$i],
						'i_main_occupation'=>$main_occupation
					);
					$tbl="tbl_professional_data";
					$this->admin_model->add_entry($insert_array,$tbl);
				}
				$award_data_length = sizeof($this->input->post("award_details"));

				for($i=0;$i<$award_data_length;$i++){

					$insert_array = array(

						'user_id'=>$add_user_data,
						'v_achievement' =>$this->input->post("award_details")[$i],
					);
					$tbl="tbl_achievements_data";
					$this->admin_model->add_entry($insert_array,$tbl);
				}
				redirect(base_url().'admin/members/?succ=1&msg=add');
				exit;
			}
		}

		if($script == "edit"){

			$selectMembers = $this->admin_model->select_members($tbl,$id);
		
			if($_SERVER['REQUEST_METHOD']=='POST'){

				$data = $this->input->post();
		
				if($selectMembers['members_data']['v_email'] != $data['v_email']){

					mailChimpChangeEmail($data['v_email'],$selectMembers['members_data']['v_email']);
				}

				$data['edu_row_id'] = isset($data['edu_row_id']) ? $data['edu_row_id'] : array('0');

				$edu_data  = isset($selectMembers['educational_data']) ? $selectMembers['educational_data'] : '';
				
				if(isset($data['edu_row_id'])){

					if(count($edu_data)){

						foreach($edu_data as $row){

							if(!in_array($row->id, $data['edu_row_id'])){

								$this->admin_model->delete_entry($row->id,"tbl_educational_data");
							}

						}
					}
				}
				
				$data['pro_row_id'] = isset($data['pro_row_id']) ? $data['pro_row_id'] : array('0');

				$prof_data = isset($selectMembers['professional_data']) ? $selectMembers['professional_data'] : '';

				if(isset($data['pro_row_id'])){

					if(count($prof_data)){

						foreach($prof_data as $row){

							if(!in_array($row->id, $data['pro_row_id'])){

								$this->admin_model->delete_entry($row->id,"tbl_professional_data");
							}
						}
					}
				}

				$data['achi_row_id'] = isset($data['achi_row_id']) ? $data['achi_row_id'] : array('0');

				$achi_data = isset($selectMembers['achievement_data']) ? $selectMembers['achievement_data'] : '';

				if(isset($data['achi_row_id'])){

					if(count($achi_data)){
					
						foreach($achi_data as $row){

							if(!in_array($row->id, $data['achi_row_id'])){

								$this->admin_model->delete_entry($row->id,"tbl_achievements_data");
							}
						}
					}
				}

				if($this->input->post("referral") == 0){

					$referral_email = '';
				}else{

					$referral_email = $this->input->post("referral_email");
				}

				$address = $this->input->post("v_residence_city").','.$this->input->post("v_residence_country");

				$google_api_key = $this->tomouh_model->getSetting('GOOGLE_MAP_KEY');
				
				$geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key='.$google_api_key);

				$geo = json_decode($geo, true);

				if (isset($geo['status']) && ($geo['status'] == 'OK')) {

					$latitude = $geo['results'][0]['geometry']['location']['lat'];
					$longitude = $geo['results'][0]['geometry']['location']['lng'];

				}else{

					$latitude = '';
					$longitude = '';
				}

				$insert_data["v_firstname"]         = $this->input->post("v_firstname");
				$insert_data["v_lastname"]          = $this->input->post("v_lastname");
				$insert_data["d_dob"]               = $this->input->post("d_dob");
				$insert_data["e_gender"]            = $this->input->post("e_gender");
				$insert_data["v_email"]             = $this->input->post("v_email");
				$insert_data["v_home_country"]      = $this->input->post("v_home_country");
				$insert_data["v_home_city"]         = $this->input->post("v_home_city");
				$insert_data["v_residence_country"] = $this->input->post("v_residence_country");
				$insert_data["v_residence_city"]    = $this->input->post("v_residence_city");
				$insert_data["v_telephone"]         = $this->input->post("v_telephone");
				$insert_data["v_telephone_2"]       = $this->input->post("v_telephone_2");
				$insert_data["v_twitter_link"]      = $this->input->post("v_twitter_link");
				$insert_data["v_linkedin_link"]     = $this->input->post("v_linkedin_link");
				$insert_data["v_instagram_link"]    = $this->input->post("v_instagram_link");
				$insert_data["e_referral"]          = $this->input->post("referral");
				$insert_data["v_referral_email"]    = $referral_email;
				$insert_data["t_passion_interest"]  = $this->input->post("passions_interests");
				$insert_data["v_main_occupation"]   = $this->input->post("main_occu");

				if($this->input->post("v_password")){
					$insert_data["v_password"] = md5($this->input->post("v_password"));
				}

				if($this->input->post("e_member_of_month")){
					$insert_data['e_member_of_month']= $this->input->post("e_member_of_month");
					if($this->input->post("e_member_of_month") == 1){
						$this->tomouh_model->set_members_of_month();
					}
				}else{
					$insert_data['e_member_of_month']= 0;
 				}
				$insert_data['v_month_text']= $this->input->post("v_month_text");
				
 				$insert_data["e_status"] = $this->input->post("e_status");
 				$insert_data['v_longitude']=$longitude;
				$insert_data['v_latitude']=$latitude;
				//echo "<pre>"; print_r($insert_data); exit;
				
				// if ($this->form_validation->run('members') == TRUE){
				$tbl = "tbl_members";
				$updateMembers = $this->admin_model->update_members($id,$insert_data,$tbl);
				
				
				$user_id = $updateMembers;
				
				$educ_data_length = sizeof($data['university']);

				for($i=0;$i<$educ_data_length;$i++){

				  if(isset($data['university'][$i]) && !empty($data['university'][$i])){

					if(!empty($data['edu_row_id'][$i])){

						$update_array = array(

						'v_university'      => $data['university'][$i],
						'v_degree'          => $data['degree'][$i],
						'v_major'           => $data['major'][$i],
						'i_passing_year'    => $data['graduation_year'][$i],
						'i_main_occupation' => 0
					);

					$tbl="tbl_educational_data";
					$this->admin_model->update_entry($data['edu_row_id'][$i],$update_array,$tbl);

					}else{

						$insert_array = array(

						'user_id'           => $user_id,
						'v_university'      => $data['university'][$i],
						'v_degree'          => $data['degree'][$i],
						'v_major'           => $data['major'][$i],
						'i_passing_year'    => $data['graduation_year'][$i],
						'i_main_occupation' => 0
					);
					$tbl="tbl_educational_data";
					$this->admin_model->add_entry($insert_array,$tbl);
					}
				  }
				}
				
				$prof_data_length = sizeof($data['company']);

				for($i=0;$i<$prof_data_length;$i++){
				 if(isset($data['company'][$i]) && !empty($data['company'][$i])){

					if(!empty($data['pro_row_id'][$i])){

						$update_array = array(

						'v_company'         => $data['company'][$i],
						'v_job_title'       => $data['job_title'][$i],
						'v_industry'        => $data['industry'][$i],
						'i_company_from'    => $data['company_from'][$i],
						'i_company_to'      => $data['company_to'][$i],
						'i_main_occupation' => 0
					);

					$tbl="tbl_professional_data";
					$this->admin_model->update_entry($data['pro_row_id'][$i],$update_array,$tbl);

					}else{

						$insert_array = array(

						'user_id'           => $user_id,
						'v_company'         => $data['company'][$i],
						'v_job_title'       => $data['job_title'][$i],
						'v_industry'        => $data['industry'][$i],
						'i_company_from'    => $data['company_from'][$i],
						'i_company_to'      => $data['company_to'][$i],
						'i_main_occupation' => 0
					);
					$tbl="tbl_professional_data";
					$this->admin_model->add_entry($insert_array,$tbl);
					}
				  }
				}

				$achie_data_length = sizeof($data['award_details']);

				for($i=0;$i<$achie_data_length;$i++){

				  if(isset($data['award_details'][$i]) && !empty($data['award_details'][$i])){

					if(!empty($data['achi_row_id'][$i])){

						$update_array = array(
						'v_achievement' =>$data['award_details'][$i],
					);

					$tbl="tbl_achievements_data";
					$this->admin_model->update_entry($data['achi_row_id'][$i],$update_array,$tbl);

					}else{

						$insert_array = array(

						'user_id'=>$user_id,
						'v_achievement' =>$data['award_details'][$i],
					);
					$tbl="tbl_achievements_data";
					$this->admin_model->add_entry($insert_array,$tbl);

					}
				 }

				}
				
				$this->update_zoho_crm_admin( $data, $id); 
				
				redirect(base_url().'admin/members/?succ=1&msg=edit');
				exit;
				
				// }
			}elseif($id !=""){

				$sub_id = $selectMembers['members_data']['v_subscription_id'];

				$data['payment_data'] =  $this->tomouh_model->getAllSubscriptionBySubId($sub_id);

				$data =array_merge($data, $selectMembers);

			}
		}

    	elseif($script == "active"){
			if($id){
				$tbl = 'tbl_members';
				$postdata["e_status"] ="active";
				$updateMembers= $this->admin_model->update_entry($id,$postdata,$tbl);
				$data['status']="Active";
				$this->update_zoho_status_crm_admin( $data, $id);

				$selectMembers = $this->admin_model->select_members($tbl,$id);
				
				$merge_vars['EMAIL'] = $selectMembers['members_data']['v_email'];
				$merge_vars['FNAME'] = $selectMembers['members_data']['v_firstname'];
				$merge_vars['LNAME'] = $selectMembers['members_data']['v_lastname'];
				
				mailChimpSubscribe($merge_vars);
				
				redirect(base_url().'admin/members/?succ=1&msg=multiact');
				exit;
			}
		}

		elseif($script == "inactive"){
			if($id){
				
				$tbl = 'tbl_members';
				$postdata["e_status"] ="inactive";
				$updateMembers= $this->admin_model->update_entry($id,$postdata,$tbl);
				
				$data['status']="Cancelled";
				$this->update_zoho_status_crm_admin( $data, $id);
			
				$selectMembers = $this->admin_model->select_members($tbl,$id);
				
				$merge_vars['EMAIL'] = $selectMembers['members_data']['v_email'];
				$merge_vars['FNAME'] = $selectMembers['members_data']['v_firstname'];
				$merge_vars['LNAME'] = $selectMembers['members_data']['v_lastname'];

				mailChimpUnsubscribe($merge_vars);

				// $res = mailChimpUnsubscribe($merge_vars);
				
				redirect(base_url().'admin/members/?succ=1&msg=multiinact');
				exit;
			}
		}

		elseif($script == "delete"){
			if($id){
				$tbl = 'tbl_members';

				$selectMembers = $this->admin_model->select_members($tbl,$id);
				// echo "<pre>";
				// print_r($selectMembers['members_data']['v_email']);
				// exit;

				mailChimpDelete($selectMembers['members_data']['v_email']);

				$this->delete_crm_records_admin($id);

				$usr_data = $this->tomouh_model->getUserByUserId($user_id);

				if(isset( $usr_data['v_email'] ) && !empty( $usr_data['v_email'] )){

					$this->admin_model->delete_member_temp_data( $usr_data['v_email'] , 'tbl_members_temp');

					$this->admin_model->delete_member_before_data( $usr_data['v_email'] , 'tbl_before_member');
				}

				$tbl = 'tbl_members';
				$deletMembers = $this->admin_model->delete_entry($id,$tbl);

				

				$this->session->unset_userdata('logged_user');

				if(isset($_COOKIE['tomouh_logged_data']) && !empty($_COOKIE['tomouh_logged_data'])){
				  setcookie('tomouh_logged_data', '', time() - 31536000, '/');
				}
				
				redirect(base_url().'admin/members/?succ=1&msg=multidel');
			}
		}

		$data['countries'] = $this->country();
    	$this->load->view('admin/members',$data);
    }

    public function country(){

		$country_data =$this->db->query("SELECT * FROM `tbl_country` WHERE e_status ='active'");

		$country = $country_data->result();

		return $country;
	}

	public function manage_payment($script="list", $id=""){

		$user_data = $this->session->userdata["admin"];

		if(!isset($this->session->userdata["admin"])){

			redirect(base_url().'admin/?succ=0&msg=logfirst');
			exit;
	   	}

   	 	$data["page_title"] ="Payment";
		$data["page"] =	"Payment";

		$data["script"] = $script;
		$tbl = "tbl_subscription_data";

		if($script == "list"){

			$wh="";

			$data["form"]="frm_payment";

			if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
				$data["limit"] = $this->input->get('pageno');
			}else{
				$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
			}

			if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
				$data["limitstart"] = $this->input->get('limitstart');
			}else{
				$data["limitstart"] = 0;
			}

			if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
				$data["keyword"] =  $this->input->get('keyword');
			}else{
				$data["keyword"] =  '';
			}

			$arg = array(
					'limit' => $data["limit"],
					'limitstart' =>$data["limitstart"],
					'keyword' =>$data["keyword"],
					'form' => $data["form"],
				);
			$rows = $this->admin_model->getPaymentList($arg);
			$data['rows'] = $rows['rows'];
			$data['nototal'] = $rows['nototal'];

			// echo "<pre>";
			// print_r($data);exit();
		}

		elseif($script == "edit"){

			if($_SERVER['REQUEST_METHOD']=='POST'){

				$postdata["v_firstname"] = $this->input->post("v_firstname");
				$postdata["v_lastname"] =$this->input->post("v_lastname");

				$tbl = "tbl_members";
				$updatePayment = $this->admin_model->update_members($id,$postdata,$tbl);

				$subscription_update["v_subscription_id"] = $this->input->post("v_subscription_id");
				$subscription_update["d_subscription_date"] = $this->input->post("d_subscription_date");
				$subscription_update["d_subscription_exp_date"] = $this->input->post("d_subscription_exp_date");

				$tbl = "tbl_subscription_data";
				$updatePayment = $this->admin_model->update_entry($id,$subscription_update,$tbl);
				redirect(base_url().'admin/manage_payment/?succ=1&msg=edit');
				exit;

			}elseif($id !=""){

				$tbl = "tbl_members";
				$selectPayment = $this->admin_model->select_payment($tbl,$id);
				$data =array_merge($data, $selectPayment);
				// echo "<pre>";print_r($data);exit;
			}
		}
		elseif($script == "delete"){
			if($id){
				$deletePayment = $this->admin_model->delete_entry($id,$tbl);
				redirect(base_url().'admin/manage_payment/?succ=1&msg=multidel');
			}
		}
		$this->load->view('admin/manage_payment',$data);
	}
    // SK >>

    public function delete_meta_value(){

		$meta_key = $this->input->post("meta_key");
		$post_id = $this->input->post("post_id");
		$this->db->where('i_post_id', $post_id);
		$this->db->where('v_key', $meta_key);
		$this->db->delete('tbl_meta_values');
		echo 1;
		exit;
	}

	public function manage_menu($script="list", $id=""){

			if(!isset($this->session->userdata["admin"]) || empty($this->session->userdata["admin"])){
				redirect($this->config->config['base_url'].'/admin/?succ=0&msg=logfirst');
				exit;
			}
			$data["page_title"] =	"Manage Menu";
			$data["page"] =	"Menu";
			$data["script"] = $script;
			if($script == "list"){
				$wh="";
				$data["form"]="frm";
				if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
					$data["limit"] = $this->input->get('pageno');
				}else{
					$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
				}
				if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
					$data["limitstart"] = $this->input->get('limitstart');
				}else{
					$data["limitstart"] = 0;
				}
				if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
					$data["keyword"] =  $this->input->get('keyword');
					$wh = " AND ( ( v_title like '%".$data["keyword"]."%' ) )";
				}

					$ssql = "SELECT * FROM  tbl_menus WHERE 1".$wh ;
					$data["sortby"] = ($this->input->get('sb')=='') ? 'id' : $this->input->get('sb');
					$data["sorttype"] = ($this->input->get('st')=='0') ? 'ASC' : 'DESC';

				$query = $this->db->query($ssql);
				$data["nototal"] = $query->num_rows();

				$sqltepm = $ssql." ORDER BY ".$data["sortby"]." ".$data["sorttype"]." LIMIT ".$data["limitstart"].",".$data["limit"];
				$query = $this->db->query($sqltepm);
				$this->paging_model->vmPageNav($data["nototal"], $data["limitstart"], $data["limit"], $data["form"] ,"black");
				$data["rows"] = $query->result_array();

			}elseif($script == "add"){

				$data["e_status"]="";

				if($_SERVER['REQUEST_METHOD']=='POST'){
					$postdata["v_title"] =  $this->input->post("v_title");
					$postdata["i_parent_id"] = $this->input->post("i_parent_id");
					$postdata["v_link"] = $this->input->post("v_link");
					$postdata["i_order"] = $this->input->post("i_order");
					$postdata["e_status"] = $this->input->post("e_status");
					$postdata["d_added"] = date("Y-m-d H:i:s");
					$postdata["d_modified"] = date("Y-m-d H:i:s");

					if (1){

						$this->db->insert('tbl_menus', $postdata);
						$insert_id = $this->db->insert_id();

						redirect($this->config->config['base_url'].'admin/manage_menu/?succ=1&msg=add');
						exit;
					}
				}
					$ssql_query = "SELECT * FROM tbl_menus WHERE 1 AND i_parent_id = 0";
					 $query = $this->db->query($ssql_query);
					 $data["manues"] = $query->result_array();

			}elseif($script == "edit"){
				if($_SERVER['REQUEST_METHOD']=='POST'){

					$postdata["v_title"] =  $this->input->post("v_title");
					$postdata["i_parent_id"] = $this->input->post("i_parent_id");
					$postdata["v_link"] = $this->input->post("v_link");
					$postdata["e_status"] = $this->input->post("e_status");
					$postdata["i_order"] = $this->input->post("i_order");
					$postdata["d_modified"] = date("Y-m-d H:i:s");

						if (1){
						$this->db->where('id', $id);
						$this->db->update('tbl_menus', $postdata);

						redirect($this->config->config['base_url'].'/admin/manage_menu/?succ=1&msg=edit');
						exit;
					}

				}elseif($id !=""){

					$this->db->select('*');
					$this->db->from('tbl_menus');
					$this->db->where('id', $id);
					$row = $this->db->get()->result_array();
					$data =array_merge($data, $row[0]);

					$ssql_query = "SELECT * FROM  tbl_menus WHERE 1" ;
					 $query = $this->db->query($ssql_query);
					 $data["manues"] = $query->result_array();

				}
			}elseif($script == "active"){
				if($id){
					$postdata["e_status"] ="active";
					$this->db->where('id', $id);
					$this->db->update('tbl_menus', $postdata);
					redirect($this->config->config['base_url'].'admin/manage_menu/?succ=1&msg=multiact');
					exit;
				}
			}elseif($script == "inactive"){
				if($id){
					$postdata["e_status"] ="inactive";
					$this->db->where('id', $id);
					$this->db->update('tbl_menus', $postdata);
					redirect($this->config->config['base_url'].'admin/manage_menu/?succ=1&msg=multiinact');
					exit;
				}

			}elseif($script == "delete"){
				if($id){
					$this->db->where('id', $id);
					$this->db->delete('tbl_menus');

					redirect($this->config->config['base_url'].'admin/manage_menu/?succ=1&msg=multidel');
				}
			}
			$this->load->view('admin/manage_menu', $data);
		}

		public function import_members(){
			if(!isset($this->session->userdata["admin"]) || empty($this->session->userdata["admin"])){
				redirect($this->config->config['base_url'].'/admin/?succ=0&msg=logfirst');
				exit;
			}
			$data["page_title"] =	"Import Members";

			if($_SERVER['REQUEST_METHOD']=='POST'){

				if (!empty($_FILES)) {
					//print_r($_FILES);
					$tempFile = $_FILES['member_file']['tmp_name'];
					$file = fopen($tempFile, "r");
					$rowCounter=0;

					if (($handle = fopen($tempFile, "r")) !== FALSE) {
					  while (($member_data = fgetcsv($handle)) !== FALSE) {
						if($rowCounter > 0){
							//echo "<pre>"; print_r($member_data);
							//exit;
							$this->import_member_data($member_data);
					  	}else{
							//echo "<pre>"; print_r($member_data);
						}
						$rowCounter++;
					  }
					  fclose($handle);
					}
				}

				redirect($this->config->config['base_url'].'admin/import_members/?succ=1&msg=imported');
				exit;
			}


			$this->load->view('admin/import_members', $data);
		}

		public function import_member_data($data){
			
			
			//echo "<pre>"; print_r($data); exit;
			$insert_data["v_firstname"] = isset($data[1])?$data[1]:'';
			$insert_data["v_lastname"] = isset($data[2])?$data[2]:'';
			$insert_data["v_email"] = isset($data[4])?$data[4]:'';
			$insert_data["e_gender"] = isset($data[5])?$data[5]:'';
			$insert_data["v_password"] = md5(rand());
			$insert_data["v_telephone"] = isset($data[7])?$data[7]:'';
			$insert_data["v_telephone_2"] = isset($data[8])?$data[8]:'';
			$insert_data["v_residence_city"] = isset($data[9])?$data[9]:'';
			$insert_data["v_residence_country"] = isset($data[10])?$data[10]:'';
			$insert_data["v_home_city"] = isset($data[11])?$data[11]:'';
			$insert_data["v_home_country"] = isset($data[12])?$data[12]:'';
			$insert_data["d_dob"] = isset($data[15])?$data[15]:'';
			$insert_data["t_passion_interest"] = isset($data[77])?$data[77]:'';
			$insert_data["d_subscription_exp_date"] = isset($data[79])?$data[79]:'';
			$insert_data["e_type"] = 'member';
			$insert_data["v_label"] = 'Member';
			$insert_data['e_status'] = 'active';
			
			if($insert_data["d_dob"] != ''){
				$insert_data["d_dob"] = date("Y-m-d", strtotime($insert_data["d_dob"]));
			}
			
			if($insert_data["d_subscription_exp_date"] != ''){
				$insert_data["d_subscription_exp_date"] = date("Y-m-d", strtotime($insert_data["d_subscription_exp_date"]));
			}
			
			
			
			if($insert_data["v_residence_country"] != 'Saudi Arabia'){
				//echo "<pre>"; print_r($insert_data); exit;
				$address = $insert_data['v_residence_city'].' '.$insert_data['v_residence_country'];
				
				$google_api_key = $this->tomouh_model->getSetting('GOOGLE_MAP_KEY');

				//echo 'https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key='.$google_api_key;
				$geo = $this->file_get_contents_curl('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key='.$google_api_key);

				$geo = json_decode($geo, true);
				//echo "<pre>"; print_r($geo); exit;
				if (isset($geo['status']) && ($geo['status'] == 'OK')) {

				  $latitude = $geo['results'][0]['geometry']['location']['lat'];
				  $longitude = $geo['results'][0]['geometry']['location']['lng'];

				}else{

					$latitude = '';
					$longitude = '';
				}
			
				$insert_data["v_longitude"] = $longitude;
				$insert_data["v_latitude"] = $latitude;
			
			}
			
			
			$tbl = "tbl_members";
			$add_user_data = $this->admin_model->add_entry($insert_data,$tbl);

			$education_info = array();

			$colStart = 17;
			$colEnds = 40;

			if(isset($data[17])){

			while($colStart <= $colEnds){

				if(trim($data[$colStart]) != '' || trim($data[$colStart+1]) != '' || trim($data[$colStart+2]) != '' || trim($data[$colStart+3]) != ''){
					$insert_array = array(
						'user_id'=>$add_user_data,
						'v_university' =>$data[$colStart],
						'v_degree'=>$data[$colStart+1],
						'v_major'=>$data[$colStart+2],
						'i_passing_year'=>$data[$colStart+3],
					);
					$tbl="tbl_educational_data";
					$this->admin_model->add_entry($insert_array,$tbl);

					$education_info[] = 'University : '.$insert_array['v_university'].', Degree : '.$insert_array['v_degree'].', Major : '.$insert_array['v_major'].', Year : '.$insert_array['i_passing_year'];

				}
				$colStart = $colStart+4;
			}

			}

			$experience_info = array();
			$colStart = 41;
			$colEnds = 70;

			if(isset($data[41])){

			while($colStart <= $colEnds){

				if(trim($data[$colStart]) != '' || trim($data[$colStart+1]) != '' || trim($data[$colStart+2]) != '' || trim($data[$colStart+3]) != '' || trim($data[$colStart+4]) != ''){
				$insert_array = array(

					'user_id'=>$add_user_data,
					'v_company' =>$data[$colStart],
					'v_job_title'=>$data[$colStart+1],
					'v_industry'=>$data[$colStart+2],
					'i_company_from'=>$data[$colStart+3],
					'i_company_to'=>$data[$colStart+4],
				);

				$tbl="tbl_professional_data";
				$this->admin_model->add_entry($insert_array,$tbl);

				$experience_info[] = 'Company : '.$insert_array['v_company'].', Job Title : '.$insert_array['v_job_title'].', Industry : '.$insert_array['v_industry'].', Period : '.$insert_array['i_company_from'].'-'.$insert_array['i_company_to'];

				}
				$colStart = $colStart+5;
			}
			}


			$colStart = 71;
			$colEnds = 76;

			$AAH='';
			if(isset($data[71])){
			while($colStart <= $colEnds){
				if(trim($data[$colStart]) != ''){
					$insert_array = array(

						'user_id'=>$add_user_data,
						'v_achievement' =>$data[$colStart],
					);
					$tbl="tbl_achievements_data";
					$this->admin_model->add_entry($insert_array,$tbl);

					$AAH .= $insert_array['v_achievement'].',';
				}
				$colStart = $colStart+1;
			}

			}

			if($add_user_data){

				$educational_info = implode(' %0A ',$education_info);

        		$exp_info = implode(' %0A ', $experience_info);

				$AAH = rtrim($AAH,',');

				// insert into member
		
				//to get the instance of the module
				$moduleMemberIns=ZCRMRestClient::getInstance()->getModuleInstance("Members");

				$recordsMember=array();
				//To get ZCRMRecord instance
				$recordMember=ZCRMRecord::getInstance("Members",null); 
				//This function use to set FieldApiName and value similar to all other FieldApis and Custom field
				$recordMember->setFieldValue("Last_Name",$insert_data['lastname']);
				$recordMember->setFieldValue("Email",$insert_data['email']);
				$recordMember->setFieldValue("Name",$insert_data['firstname']);
				$recordMember->setFieldValue("Date_Of_Birth",date('Y-m-d', strtotime($insert_data['dob'])));
				$recordMember->setFieldValue("Gender",$insert_data['gender']);
				$recordMember->setFieldValue("Expired_On",isset($insert_data['d_subscription_exp_date'])?date('m/d/Y',strtotime($insert_data['d_subscription_exp_date'])) : '');
				$recordMember->setFieldValue("Home_Country",$insert_data['home_country']);
				$recordMember->setFieldValue("Home_City",$insert_data['home_city']);
				$recordMember->setFieldValue("Mobile_No",$insert_data['tele_country_code'].'-'.$insert_data['telephone']); 
				$recordMember->setFieldValue("Country_of_Residence",$insert_data['residence_country']);
				$recordMember->setFieldValue("City_Of_Residence'",$insert_data['residence_city']);
				$recordMember->setFieldValue("Mobile_No_2",$insert_data['tele_2_country_code'].'-'.$insert_data['telephone_2']);
				$recordMember->setFieldValue("Educational_Info_1",isset($educational_info)? urldecode($educational_info):''); 
				$recordMember->setFieldValue("Experience_Info_1",isset($exp_info)? urldecode($exp_info):'');
				$recordMember->setFieldValue("AAH",$AAH);
				$recordMember->setFieldValue("Passions_and_interests",$user_step2['passions_interests']);
				$recordMember->setFieldValue("Subscription_Info",$payment_data_str);
				$recordMember->setFieldValue("Status'","Active");
				$recordMember->setFieldValue("Plan","Paid");
				$recordMember->setFieldValue("Label","Member");

				array_push($recordsMember, $recordMember);//pushing the record to the array 
				$responseIn=$moduleMemberIns->createRecords($recordsMember);
				$responseIn = $responseIn->getEntityResponses();
				$responseIn = $responseIn[0]->getDetails();
				$responseCrmID = $responseIn['id'];

				$update_array  = array(
						'v_member_crm_id'=>$responseCrmID,
						);

				$this->admin_model->update_entry($add_user_data,$update_array,"tbl_members");

			}

		}

		public function members_update($script="list", $id=""){

			$user_data = $this->session->userdata["admin"];

			if(!isset($this->session->userdata["admin"])){

				redirect(base_url().'admin/?succ=0&msg=logfirst');
				exit;
		   	}

	   	 	$data["page_title"] ="Manage Members Update";
			$data["page"] 	=	"Members Update";

			$data["script"] = $script;
			$tbl 			= "tbl_members_updates";

			$query 	    	= $this->db->query("SELECT v_firstname,v_lastname,id FROM tbl_members");
			$items 			= $query->result();
			$data["members"]  = $items;

			if($script == "list"){

				$wh="";

				$data["form"]="frm_members_updates";

				if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
					$data["limit"] = $this->input->get('pageno');
				}else{
					$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
				}

				if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
					$data["limitstart"] = $this->input->get('limitstart');
				}else{
					$data["limitstart"] = 0;
				}

				if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
					$data["keyword"] =  $this->input->get('keyword');
				}else{
					$data["keyword"] =  '';
				}

				$arg = array(
						'limit' => $data["limit"],
						'limitstart' =>$data["limitstart"],
						'keyword' =>$data["keyword"],
						'form' => $data["form"],
					);
				$rows = $this->admin_model->getMembersUpdateList($arg);
				$data['rows'] = $rows['rows'];
				$data['nototal'] = $rows['nototal'];
			}
			elseif ($script == 'add'){

				if ($_SERVER['REQUEST_METHOD']=='POST') {

					$v_user_name_id = $this->input->post("v_user_name");

					$query = $this->db->query("SELECT v_firstname,v_lastname FROM tbl_members WHERE id=".$v_user_name_id);
					$members_data = $query->result();

					$postdata["user_id"] = $this->input->post("v_user_name");
					$postdata["v_user_name"] = $members_data[0]->v_firstname.' '.$members_data[0]->v_lastname;
					$postdata["l_description"] = $this->input->post("l_description");
					$postdata["i_order"] = $this->input->post("i_order");
					$postdata['d_added']= date("Y-m-d H:i:s");
					$postdata["e_status"] = $this->input->post("e_status");

					if (1){

						$addPages = $this->admin_model->add_entry($postdata,$tbl);

						redirect(base_url().'admin/members_update/?succ=1&msg=addpage');
						exit;
					}else{
						$data =array_merge($data, $postdata);
					}

				}
			}
			elseif($script == "edit")
			{
				if($_SERVER['REQUEST_METHOD']=='POST'){

					$v_user_name_id = $this->input->post("v_user_name");

					$query = $this->db->query("SELECT v_firstname,v_lastname FROM tbl_members WHERE id=".$v_user_name_id);
					$members_data = $query->result();

					$postdata["user_id"] = $this->input->post("v_user_name");
					$postdata["v_user_name"] = $members_data[0]->v_firstname.' '.$members_data[0]->v_lastname;
					$postdata["l_description"] = $this->input->post("l_description");
					$postdata["i_order"] = $this->input->post("i_order");
					$postdata["e_status"] = $this->input->post("e_status");

					if (1){

						$updateMeta = $this->admin_model->update_entry($id,$postdata,$tbl);
						redirect(base_url().'admin/members_update/?succ=1&msg=editmemberupdate');
						exit;
					}
				}elseif($id !=""){

					$selectMeta = $this->admin_model->select_entry($tbl,$id);
					$data =array_merge($data, $selectMeta);
				}
			}
			elseif($script == "active"){
				if($id){
					$postdata["e_status"] ="active";
					$updateMeta = $this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/members_update/?succ=1&msg=multiact');
					exit;
				}
			}
			elseif($script == "inactive"){
				if($id){
					$postdata["e_status"] ="inactive";
					$updateMeta = $this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/members_update/?succ=1&msg=multiinact');
					exit;
				}

			}
			elseif($script == "delete"){
				if($id){
					$deleteMeta = $this->admin_model->delete_entry($id,$tbl);
					redirect(base_url().'admin/members_update/?succ=1&msg=multidel');
				}
			}

		   	$this->load->view('admin/members_update',$data);
	    }
		
		function resizeImage($image,$width,$height,$scale) {
				list($imagewidth, $imageheight, $imageType) = getimagesize($image);
				$imageType = image_type_to_mime_type($imageType);
				$newImageWidth = ceil($width * $scale);
				$newImageHeight = ceil($height * $scale);
				$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
				switch($imageType) {
					case "image/gif":
						$source=imagecreatefromgif($image); 
						break;
					case "image/pjpeg":
					case "image/jpeg":
					case "image/jpg":
						$source=imagecreatefromjpeg($image); 
						break;
					case "image/png":
					case "image/x-png":
						$source=imagecreatefrompng($image); 
						break;
				}
				imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);
				
				switch($imageType) {
					case "image/gif":
						imagegif($newImage,$image); 
						break;
					case "image/pjpeg":
					case "image/jpeg":
					case "image/jpg":
						imagejpeg($newImage,$image,90); 
						break;
					case "image/png":
					case "image/x-png":
						imagepng($newImage,$image);  
						break;
				}
				
				chmod($image, 0777);
				return $image;
		}
		function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale){
			list($imagewidth, $imageheight, $imageType) = getimagesize($image);
			$imageType = image_type_to_mime_type($imageType);
			
			$newImageWidth = ceil($width * $scale);
			$newImageHeight = ceil($height * $scale);
			$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
			switch($imageType) {
				case "image/gif":
					$source=imagecreatefromgif($image); 
					break;
				case "image/pjpeg":
				case "image/jpeg":
				case "image/jpg":
					$source=imagecreatefromjpeg($image); 
					break;
				case "image/png":
				case "image/x-png":
					$source=imagecreatefrompng($image); 
					break;
			}
			imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
			switch($imageType) {
				case "image/gif":
					imagegif($newImage,$thumb_image_name); 
					break;
				case "image/pjpeg":
				case "image/jpeg":
				case "image/jpg":
					imagejpeg($newImage,$thumb_image_name,90); 
					break;
				case "image/png":
				case "image/x-png":
					imagepng($newImage,$thumb_image_name);  
					break;
			}
			chmod($thumb_image_name, 0777);
			return $thumb_image_name;
		}
	    public function cropImage($sourcePath, $tempPath, $thumbWidthSize, $thumbheightSize, $destination = null){

	    	  $parts = explode('.', $sourcePath);
			  $ext = $parts[count($parts) - 1];

			  if ($ext == 'jpg' || $ext == 'jpeg') {
			    $format = 'jpg';
			  } elseif($ext == 'png') {
			    $format = 'png';
			  } else{
			  	$format = 'gif';
			  }

			  if ($format == 'jpg') {
			    $sourceImage = imagecreatefromjpeg($tempPath);
			  }
			  if ($format == 'png') {
			    $sourceImage = imagecreatefrompng($tempPath);
			  }
			  if($format == 'gif'){
			  	$sourceImage = imagecreatefromgif($tempPath);
			  }

			  list($srcWidth, $srcHeight) = getimagesize($tempPath);

			  // calculating the part of the image to use for thumbnail
			  // if ($srcWidth > $srcHeight) {
			  //   $y = 0;
			  //   $x = ($srcWidth - $srcHeight) / 2;
			  //   $smallestSide = $srcHeight;
			  // } else {
			  //   $x = 0;
			  //   $y = ($srcHeight - $srcWidth) / 2;
			  //   $smallestSide = $srcWidth;
			  // }
			  $x=$thumbWidthSize;
			  $y=$thumbheightSize;
			  $smallestSide = $thumbWidthSize;

			  $destinationImage = imagecreatetruecolor($srcWidth, $srcHeight);
			  imagecopyresampled($destinationImage, $sourceImage, 0, 0, $x, $y, $srcWidth, $srcHeight, $smallestSide, $smallestSide);

			  if ($destination == null) {
			    header('Content-Type: image/jpeg');
			    if ($format == 'jpg') {
			      imagejpeg($destinationImage, null, 100);
			    }
			    if ($format == 'png') {
			      imagejpeg($destinationImage);
			    }
			    if($format == 'gif'){
				  imagegif($destinationImage);
			    }
			    if ($destination = null) {
			    }
			  } else {
			    if ($format == 'jpg') {
			      imagejpeg($destinationImage, $destination.$sourcePath, 100);
			    }
			    if ($format == 'png') {
			      imagepng($destinationImage, $destination.$sourcePath);
			    }
			    if($format == 'gif'){
			      imagegif($destinationImage, $destination.$sourcePath);
			    }
			  }

	    }

	    public function getExportListOfMember(){

	    	$objPHPExcel = new PHPExcel();

	    	$objPHPExcel->setActiveSheetIndex(0);

	    	$member_data = $this->tomouh_model->getAllMembers();

	    	$excel_data = [];

	    	$excel_data[] = array(

	    			0=>'Member Number',
	    			1=>'FirstName',
	    			2=>'LastName',
	    			3=>'Full Name',
	    			4=>'Email',
	    			5=>'Gender',
	    			6=>'Member Type',
	    			7=>'Mobile 1',
	    			8=>'Mobile 2',
	    			9=>'City of Residence',
	    			10=>'Country of Residence',
	    			11=>'Home City',
	    			12=>'Home Country',
	    			13=>'GCC or Not',
	    			14=>'Date Joined',
	    			15=>'Date of Birth',
	    			16=>'Last Industry',
	    			17=>'University 1',
	    			18=>'Degree 1',
	    			19=>'Major 1',
	    			20=>'Graduation Year 1',
	    			21=>'University 2',
	    			22=>'Degree 2',
	    			23=>'Major 2',
	    			24=>'Graduation Year 2',
	    			25=>'University 3',
	    			26=>'Degree 3',
	    			27=>'Major 3',
	    			28=>'Graduation Year 3',
	    			29=>'University 4',
	    			30=>'Degree 4',
	    			31=>'Major 4',
	    			32=>'Graduation Year 4',
	    			33=>'University 5',
	    			34=>'Degree 5',
	    			35=>'Major 5',
	    			36=>'Graduation Year 5',
	    			37=>'University 6',
	    			38=>'Degree 6',
	    			39=>'Major 6',
	    			40=>'Graduation Year 6',
	    			41=>'Company 1',
	    			42=>'Job Title 1',
	    			43=>'Industry 1',
	    			44=>'From Year 1',
	    			45=>'To Year 1',
	    			46=>'Company 2',
	    			47=>'Job Title 2',
	    			48=>'Industry 2',
	    			49=>'From Year 2',
	    			50=>'To Year 2',
	    			51=>'Company 3',
	    			52=>'Job Title 3',
	    			53=>'Industry 3',
	    			54=>'From Year 3',
	    			55=>'To Year 3',
	    			56=>'Company 4',
	    			57=>'Job Title 4',
	    			58=>'Industry 4',
	    			59=>'From Year 4',
	    			60=>'To Year 4',
	    			61=>'Company 5',
	    			62=>'Job Title 5',
	    			63=>'Industry 5',
	    			64=>'From Year 5',
	    			65=>'To Year 5',
	    			66=>'Company 6',
	    			67=>'Job Title 6',
	    			68=>'Industry 6',
	    			69=>'From Year 6',
	    			70=>'To Year 6',
	    			71=>'Awards, Achievements & Honours 1',
	    			72=>'Awards, Achievements & Honours 2',
	    			73=>'Awards, Achievements & Honours 3',
	    			74=>'Awards, Achievements & Honours 4',
	    			75=>'Awards, Achievements & Honours 5',
	    			76=>'Awards, Achievements & Honours 6',
	    			77=>'Passions & Interests',
	    			78=>'Age',
	    			79=>'Column1',
	    			80=>'Column2',
	    			81=>'Column3',
	    			82=>'Column4',
	    			83=>'Column5',
	    			84=>'Column6',
	    			85=>'Column7',
	    			86=>'Column8',
					87=>'Label',
					88=>'Status',
	    		);


	    	foreach($member_data as $member){

	    	//print_r($member['id']); die();

	    	$member_data2 = $this->tomouh_model->getAllMembers2($member['id']);
	    	$member_data3 = $this->tomouh_model->getAllMembers3($member['id']);
	    	$member_data4 = $this->tomouh_model->getAllMembers4($member['id']);
	    	$member_data5 = $this->tomouh_model->getAllMembers5($member['id']);

	    	$from = new DateTime($member['d_dob']);
			$to   = new DateTime('today');
			$age  = $from->diff($to)->y;
	    		
				$member['v_telephone'] = str_replace(array('+', '-'), '', $member['v_telephone']);
				$member['v_telephone_2'] = str_replace(array('+', '-'), '', $member['v_telephone_2']);
				$member_data5[0]['d_subscription_date'] = date("m/d/Y", strtotime($member_data5[0]['d_subscription_date']));
				$member['d_dob'] = date("m/d/Y", strtotime($member['d_dob']));
				
	    		$excel_data[] = array(

					'Member Number'=>$member['v_member_crm_id'],
	    			'FirstName'=>$member['v_firstname'],
	    			'LastName' =>$member['v_lastname'],
	    			'FullName' =>$member['v_firstname']." ".$member['v_lastname'],
	    			'Email' =>$member['v_email'],
	    			'Gender'=>$member['e_gender'],
	    			'Member Type'=>$member['e_type'],
	    			'Mobile 1'=>$member['v_telephone'],
	    			'Mobile 2'=>$member['v_telephone_2'],
	    			'City of Residence'=>$member['v_residence_city'],
	    			'Country of Residence'=>$member['v_residence_country'],
	    			'Home City'=>$member['v_home_city'],
	    			'Home Country' =>$member['v_home_country'],
	    			'GCC or Not'   =>"",
	    			'Date Joined'  =>$member_data5[0]['d_subscription_date'],
	    			'Date Of Birth'=>$member['d_dob'],

	    			'Last Industry'=>$member_data3[0]['v_industry'],

	    			'University 1'=>$member_data2[0]['v_university'],         
	    			'Degree 1'    =>$member_data2[0]['v_degree'],           
	    			'Major 1'     =>$member_data2[0]['v_major'],             
	    			'Graduation Year 1'=>$member_data2[0]['i_passing_year'],

	    			'University 2'=>$member_data2[1]['v_university'],  
	    			'Degree 2'    =>$member_data2[1]['v_degree'],      
	    			'Major 2'     =>$member_data2[1]['v_major'],       
	    			'Graduation Year 2'=>$member_data2[1]['i_passing_year'],

	    			'University 3'=>$member_data2[2]['v_university'],  
	    			'Degree 3'    =>$member_data2[2]['v_degree'],      
	    			'Major 3'     =>$member_data2[2]['v_major'],       
	    			'Graduation Year 3'=>$member_data2[2]['i_passing_year'],

	    			'University 4'=>$member_data2[3]['v_university'],  
	    			'Degree 4'    =>$member_data2[3]['v_degree'],      
	    			'Major 4'     =>$member_data2[3]['v_major'],       
	    			'Graduation Year 4'=>$member_data2[3]['i_passing_year'],

	    			'University 5'=>$member_data2[4]['v_university'],  
	    			'Degree 5'    =>$member_data2[4]['v_degree'],      
	    			'Major 5'     =>$member_data2[4]['v_major'],       
	    			'Graduation Year 5'=>$member_data2[4]['i_passing_year'],

	    			'University 6'=>$member_data2[5]['v_university'],  
	    			'Degree 6'    =>$member_data2[5]['v_degree'],      
	    			'Major 6'     =>$member_data2[5]['v_major'],       
	    			'Graduation Year 6'=>$member_data2[5]['i_passing_year'],

	    			'Company 1'  =>$member_data3[0]['v_company'],       
	    			'Job Title 1'=>$member_data3[0]['v_job_title'],       
	    			'Industry 1' =>$member_data3[0]['v_industry'],          
	    			'From Year 1'=>$member_data3[0]['i_company_from'],
	    			'To Year 1'  =>$member_data3[0]['i_company_to'],          

	    			'Company 2'  =>$member_data3[1]['v_company'],     
	    			'Job Title 2'=>$member_data3[1]['v_job_title'],   
	    			'Industry 2' =>$member_data3[1]['v_industry'],    
	    			'From Year 2'=>$member_data3[1]['i_company_from'],
	    			'To Year 2'  =>$member_data3[1]['i_company_to'],  

	    			'Company 3'  =>$member_data3[2]['v_company'],     
	    			'Job Title 3'=>$member_data3[2]['v_job_title'],   
	    			'Industry 3' =>$member_data3[2]['v_industry'],    
	    			'From Year 3'=>$member_data3[2]['i_company_from'],
	    			'To Year 3'  =>$member_data3[2]['i_company_to'],  

	    			'Company 4'  =>$member_data3[3]['v_company'],     
	    			'Job Title 4'=>$member_data3[3]['v_job_title'],   
	    			'Industry 4' =>$member_data3[3]['v_industry'],    
	    			'From Year 4'=>$member_data3[3]['i_company_from'],
	    			'To Year 4'  =>$member_data3[3]['i_company_to'],  

	    			'Company 5'  =>$member_data3[4]['v_company'],     
	    			'Job Title 5'=>$member_data3[4]['v_job_title'],   
	    			'Industry 5' =>$member_data3[4]['v_industry'],    
	    			'From Year 5'=>$member_data3[4]['i_company_from'],
	    			'To Year 5'  =>$member_data3[4]['i_company_to'],  

	    			'Company 6'  =>$member_data3[5]['v_company'],     
	    			'Job Title 6'=>$member_data3[5]['v_job_title'],   
	    			'Industry 6' =>$member_data3[5]['v_industry'],    
	    			'From Year 6'=>$member_data3[5]['i_company_from'],
	    			'To Year 6'  =>$member_data3[5]['i_company_to'],  

	    			'Awards, Achievements & Honours 1'=>$member_data4[0]['v_achievement'],
	    			'Awards, Achievements & Honours 2'=>$member_data4[1]['v_achievement'],
	    			'Awards, Achievements & Honours 3'=>$member_data4[2]['v_achievement'],
	    			'Awards, Achievements & Honours 4'=>$member_data4[3]['v_achievement'],
	    			'Awards, Achievements & Honours 5'=>$member_data4[4]['v_achievement'],
	    			'Awards, Achievements & Honours 6'=>$member_data4[5]['v_achievement'],

	    			'Passions & Interests'=>$member['t_passion_interest'],
	    			'Age'=>$age,

	    			'Column1'=>"",
	    			'Column2'=>"",
	    			'Column3'=>"",
	    			'Column4'=>"",
	    			'Column5'=>"",
	    			'Column6'=>"",
	    			'Column7'=>"",
	    			'Column8'=>"",
					'Label' => $member['v_label'],
					'Status' => $member['e_status'],

	    		);

	    	}

	    	$excel_data_col_0 = $excel_data[0];
	    	$excel_data_col_1 = $excel_data[1];

	    	$objPHPExcel->setActiveSheetIndex(0);

	        $char = 'A';
	        foreach( $excel_data_col_0 as $colKey => $colValue ){

	            $colWidth = strlen( $colValue ) + 2;

	            if( strlen( $excel_data_col_1[$colValue] ) > $colWidth ){
	                $colWidth = strlen( $excel_data_col_1[$colValue] ) + 2;
	            }

	            $objPHPExcel->getActiveSheet()->getColumnDimension( $char )->setWidth( $colWidth );

	            $char++;
	        }

	        $rowCount = 1;
	        foreach( $excel_data as $rowValue ){
	            $startChar = "A";
	            foreach( $rowValue as $colKey => $colValue ){

	                $cellNo = $startChar.$rowCount;

	                $colValue = strip_tags( $colValue );

	                $objPHPExcel->getActiveSheet()->SetCellValue( $cellNo, $colValue );

	                $startChar++;
	            }
	            $rowCount++;
	        }

	        header('Content-Type: application/vnd.ms-excel');
	        header('Content-Disposition: attachment;filename="member_list_'.date('Y-m-d').'.xlsx"');
	        header('Cache-Control: max-age=0');
	        header('Cache-Control: max-age=1');
	        $objWriter = new PHPExcel_Writer_Excel2007( $objPHPExcel );
	        $objWriter->save('php://output');
	        exit;
	    }

	     public function manage_country($script="list", $id=""){

	    	$user_data = $this->session->userdata["admin"];

			if(!isset($this->session->userdata["admin"])){

				redirect(base_url().'admin/?succ=0&msg=logfirst');
				exit;
		   	}
		   	$data["page_title"] ="Country";
			$data["page"] =	"Country";

			$data["script"] = $script;

			$tbl = "tbl_country";

			if($script == "list"){
				$wh="";

				$data["form"]="frm_country";

				if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
					$data["limit"] = $this->input->get('pageno');
				}else{
					$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
				}

				if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
					$data["limitstart"] = $this->input->get('limitstart');
				}else{
					$data["limitstart"] = 0;
				}

				if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
					$data["keyword"] =  $this->input->get('keyword');
				}else{
					$data["keyword"] =  '';
				}

				$arg = array(
						'limit' => $data["limit"],
						'limitstart' =>$data["limitstart"],
						'keyword' =>$data["keyword"],
						'form' => $data["form"],
					);
				$rows = $this->admin_model->getCountryList($arg);
				$data['rows'] = $rows['rows'];
				$data['nototal'] = $rows['nototal'];
			}

			if($script == "add"){

				if ($_SERVER['REQUEST_METHOD']=='POST') {

						$postdata["country_code"] = $this->input->post("country_code");
						$postdata["country_name"] =$this->input->post("country_name");
						$postdata["phonecode"] =$this->input->post("phonecode");
						$postdata["e_status"] = $this->input->post("e_status");

						$this->admin_model->add_entry($postdata,$tbl);

						redirect(base_url().'admin/manage_country/?succ=1&msg=add');
						exit;

						$data =array_merge($data, $postdata);
				}else{
					$data["country_code"]="";
					$data["country_name"]="";
					$data["phonecode"]="";
					$data["e_status"]="";
				}
			}

			elseif ($script == 'edit'){

				if ($_SERVER['REQUEST_METHOD']=='POST') {

					$postdata["country_code"] = $this->input->post("country_code");
					$postdata["country_name"] =$this->input->post("country_name");
					$postdata["phonecode"] =$this->input->post("phonecode");
					$postdata["e_status"] = $this->input->post("e_status");

					$this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/manage_country/?succ=1&msg=edit');
					exit;

				}elseif($id !=""){

					$selectTeam = $this->admin_model->select_entry($tbl,$id);
					$data =array_merge($data, $selectTeam);
				}
			}

			elseif($script == "active"){
				if($id){
					$tbl = 'tbl_country';
					$postdata["e_status"] ="active";
					$updateTeam = $this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/manage_country/?succ=1&msg=multiact');
					exit;
				}
			}
			elseif($script == "inactive"){
				if($id){
					$tbl = 'tbl_country';
					$postdata["e_status"] ="inactive";
					$updateTeam = $this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/manage_country/?succ=1&msg=multiinact');
					exit;
				}

			}
			elseif($script == "delete"){
				if($id){
					$tbl = 'tbl_country';
					$deleteTeam = $this->admin_model->delete_entry($id,$tbl);
					redirect(base_url().'admin/manage_country/?succ=1&msg=multidel');
				}
			}
		   	$this->load->view('admin/manage_country',$data);
	    }

	    public function manage_industry($script="list", $id=""){

	    	$user_data = $this->session->userdata["admin"];

			if(!isset($this->session->userdata["admin"])){

				redirect(base_url().'admin/?succ=0&msg=logfirst');
				exit;
		   	}
		   	$data["page_title"] ="Industry";
			$data["page"] =	"Industry";

			$data["script"] = $script;

			$tbl = "tbl_industry";

			if($script == "list"){
				$wh="";

				$data["form"]="frm_industry";

				if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
					$data["limit"] = $this->input->get('pageno');
				}else{
					$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
				}

				if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
					$data["limitstart"] = $this->input->get('limitstart');
				}else{
					$data["limitstart"] = 0;
				}

				if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
					$data["keyword"] =  $this->input->get('keyword');
				}else{
					$data["keyword"] =  '';
				}

				$arg = array(
						'limit' => $data["limit"],
						'limitstart' =>$data["limitstart"],
						'keyword' =>$data["keyword"],
						'form' => $data["form"],
					);
				$rows = $this->admin_model->getIndustryList($arg);
				$data['rows'] = $rows['rows'];
				$data['nototal'] = $rows['nototal'];
			}

			if($script == "add"){

				if ($_SERVER['REQUEST_METHOD']=='POST') {

						$postdata["v_name"] = $this->input->post("v_name");
						$postdata["i_order"] =$this->input->post("i_order");
						$postdata["e_status"] = $this->input->post("e_status");
						$postdata["d_added"] = date("Y-m-d H:i:s");

						$this->admin_model->add_entry($postdata,$tbl);

						redirect(base_url().'admin/manage_industry/?succ=1&msg=add');
						exit;

						$data =array_merge($data, $postdata);
				}else{
					$data["v_name"]="";
					$data["i_order"]="";
					$data["e_status"]="";
					$postdata["d_added"] = date("Y-m-d H:i:s");
				}
			}

			elseif ($script == 'edit'){

				if ($_SERVER['REQUEST_METHOD']=='POST') {

					$postdata["v_name"] = $this->input->post("v_name");
					$postdata["i_order"] =$this->input->post("i_order");
					$postdata["e_status"] =$this->input->post("e_status");

					$this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/manage_industry/?succ=1&msg=edit');
					exit;

				}elseif($id !=""){

					$selectTeam = $this->admin_model->select_entry($tbl,$id);
					$data =array_merge($data, $selectTeam);
				}
			}

			elseif($script == "active"){
				if($id){
					$tbl = 'tbl_industry';
					$postdata["e_status"] ="active";
					$updateTeam = $this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/manage_industry/?succ=1&msg=multiact');
					exit;
				}
			}
			elseif($script == "inactive"){
				if($id){
					$tbl = 'tbl_industry';
					$postdata["e_status"] ="inactive";
					$updateTeam = $this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/manage_industry/?succ=1&msg=multiinact');
					exit;
				}

			}
			elseif($script == "delete"){
				if($id){
					$tbl = 'tbl_industry';
					$deleteTeam = $this->admin_model->delete_entry($id,$tbl);
					redirect(base_url().'admin/manage_industry/?succ=1&msg=multidel');
				}
			}
		   	$this->load->view('admin/manage_industry',$data);
	    }

	    public function manage_initiatives($script="list", $id=""){

	    	$user_data = $this->session->userdata["admin"];

			if(!isset($this->session->userdata["admin"])){

				redirect(base_url().'admin/?succ=0&msg=logfirst');
				exit;
		   	}
		   	$data["page_title"] ="Initiatives";
			$data["page"] =	"Initiatives";

			$data["script"] = $script;

			$data['font_awesome_icon'] = $this->tomouh_model->jt_get_font_icons();

			// echo "<pre>";
			// print_r($data['font_awesome_icon']);exit(); 

			$tbl = "tbl_initiatives";

			if($script == "list"){
				$wh="";

				$data["form"]="frm_initiatives";

				if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
					$data["limit"] = $this->input->get('pageno');
				}else{
					$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
				}

				if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
					$data["limitstart"] = $this->input->get('limitstart');
				}else{
					$data["limitstart"] = 0;
				}

				if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
					$data["keyword"] =  $this->input->get('keyword');
				}else{
					$data["keyword"] =  '';
				}

				$arg = array(
						'limit' => $data["limit"],
						'limitstart' =>$data["limitstart"],
						'keyword' =>$data["keyword"],
						'form' => $data["form"],
					);
				$rows = $this->admin_model->getInitiativesList($arg);
				$data['rows'] = $rows['rows'];
				$data['nototal'] = $rows['nototal'];
			}

			if($script == "add"){

				if ($_SERVER['REQUEST_METHOD']=='POST') {

						if (!empty($_FILES) && isset($_FILES['v_image']['name']) && $_FILES['v_image']['name'] != '' ) {
					        $tempFile = $_FILES['v_image']['tmp_name'];
					        $fileName = $_FILES['v_image']['name'];
					        $targetPath = getcwd() . '/assets/images/';
					        // $fileName = time().'-'.$fileName;
							$targetFile = $targetPath . $fileName ;
					        move_uploaded_file($tempFile, $targetFile);
					        $fname=$this->general_model->seoText($fileName);
							$new_value = time().$fname;
							rename($targetPath.$fileName, $targetPath.$new_value);
				        }

						$postdata["v_name"] = $this->input->post("v_name");
						$postdata["l_text"] = $this->input->post("l_text");
						$postdata["v_image"] = isset($new_value) ? $new_value : '';
						$postdata["v_icon"] = $this->input->post("v_icon");
						$postdata["v_email"] = $this->input->post("v_email");
						$postdata["i_order"] = $this->input->post("i_order");
						$postdata["e_status"] = $this->input->post("e_status");
						$postdata['d_added']= date("Y-m-d H:i:s");

						$this->admin_model->add_entry($postdata,$tbl);

						redirect(base_url().'admin/manage_initiatives/?succ=1&msg=add');
						exit;

						$data =array_merge($data, $postdata);
				}else{
					$postdata["v_name"]   = "";
					$postdata["l_text"]   = "";
					$postdata["v_image"]  = "";
					$postdata["v_email"]  = "";
					$postdata["v_icon"]   = "";
					$postdata["i_order"]  = "";
					$postdata["e_status"] = "";
					$postdata["d_added"]  = date("Y-m-d H:i:s");
				}
			}

			elseif ($script == 'edit'){

				if ($_SERVER['REQUEST_METHOD']=='POST') {

					if (!empty($_FILES) && isset($_FILES['v_image']['name']) && $_FILES['v_image']['name'] != '' ) {
					        $tempFile = $_FILES['v_image']['tmp_name'];
					        $fileName = $_FILES['v_image']['name'];
					        $targetPath = getcwd() . '/assets/images/';
					        // $fileName = time().'-'.$fileName;
							$targetFile = $targetPath . $fileName ;
					        move_uploaded_file($tempFile, $targetFile);
					        $fname=$this->general_model->seoText($fileName);
							$new_value = time().$fname;
							rename($targetPath.$fileName, $targetPath.$new_value);
					        $postdata["v_image"] = isset($new_value) ? $new_value : '';
				        }

						$postdata["v_name"]   = $this->input->post("v_name");
						$postdata["l_text"]   = $this->input->post("l_text");
						$postdata["v_icon"]   = $this->input->post("v_icon");
						$postdata["v_email"]  = $this->input->post("v_email");
						$postdata["i_order"]  = $this->input->post("i_order");
						$postdata["e_status"] = $this->input->post("e_status");
						$postdata['d_added']  = date("Y-m-d H:i:s");

					$this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/manage_initiatives/?succ=1&msg=edit');
					exit;

				}elseif($id !=""){

					$selectTeam = $this->admin_model->select_entry($tbl,$id);
					$data =array_merge($data, $selectTeam);
				}
			}

			elseif($script == "active"){
				if($id){
					$tbl = 'tbl_initiatives';
					$postdata["e_status"] ="active";
					$updateTeam = $this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/manage_initiatives/?succ=1&msg=multiact');
					exit;
				}
			}
			elseif($script == "inactive"){
				if($id){
					$tbl = 'tbl_initiatives';
					$postdata["e_status"] ="inactive";
					$updateTeam = $this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/manage_initiatives/?succ=1&msg=multiinact');
					exit;
				}

			}
			elseif($script == "delete"){
				if($id){
					$tbl = 'tbl_initiatives';
					$deleteTeam = $this->admin_model->delete_entry($id,$tbl);
					redirect(base_url().'admin/manage_initiatives/?succ=1&msg=multidel');
				}
			}
		   	$this->load->view('admin/manage_initiatives',$data);
	    }

	    public function manage_user_request($script="list", $id=""){

	    	$user_data = $this->session->userdata["admin"];

			if(!isset($this->session->userdata["admin"])){

				redirect(base_url().'admin/?succ=0&msg=logfirst');
				exit;
		   	}
		   	$data["page_title"] ="User Request Data";
			$data["page"] =	"User Request Data";

			$query 	    	= $this->db->query("SELECT v_firstname,v_lastname,id FROM tbl_members");
			$items 			= $query->result();
			$data["members"]  = $items;

			$data["script"] = $script;

			$tbl = "tbl_request_user_data";

			if($script == "list"){
				$wh="";

				$data["form"]="frm_user_request";

				if ($this->input->get('pageno') && $this->input->get('pageno') !=''){
					$data["limit"] = $this->input->get('pageno');
				}else{
					$data["limit"] = $this->admin_model->getSetting('RECORD_PER_PAGE');
				}

				if ($this->input->get('limitstart') && $this->input->get('limitstart') !=''){
					$data["limitstart"] = $this->input->get('limitstart');
				}else{
					$data["limitstart"] = 0;
				}

				if(($this->input->get('keyword')) && $this->input->get('keyword')!=''){
					$data["keyword"] =  $this->input->get('keyword');
				}else{
					$data["keyword"] =  '';
				}

				$arg = array(
						'limit' => $data["limit"],
						'limitstart' =>$data["limitstart"],
						'keyword' =>$data["keyword"],
						'form' => $data["form"],
					);
				$rows = $this->admin_model->getRequestUser($arg);
				$data['rows'] = $rows['rows'];
				$data['nototal'] = $rows['nototal'];

			} elseif($script == "add"){

				if ($_SERVER['REQUEST_METHOD']=='POST') {

						$postdata["user_id"] = $this->input->post("i_user_id");

						$query = $this->db->query("SELECT v_firstname,v_lastname FROM tbl_members WHERE id=".$this->input->post("i_user_id"));
						$members_data = $query->result();

						if( isset($members_data[0]) && !empty($members_data[0]) ){

							$postdata["v_firstname"] = $members_data[0]->v_firstname;
							$postdata["v_lastname"]  = $members_data[0]->v_lastname;

						}else{

							$postdata["v_firstname"] = '';
							$postdata["v_lastname"]  = '';

						}

						$postdata["v_email"]  = $this->input->post("v_email");
						$postdata["e_sended"] = $this->input->post("e_sended") == 'yes' ? '1' : '0';
						$postdata["d_added"] =  date('Y-m-d H:i:s');

						$this->admin_model->add_entry($postdata,$tbl);

						redirect(base_url().'admin/manage_user_request/?succ=1&msg=add');
						exit;

						$data =array_merge($data, $postdata);
				}else{
					$postdata["user_id"] = "";
					$postdata["v_firstname"] = "";
					$postdata["v_lastname"] = "";
					$postdata["v_email"] = "";
					$postdata["e_sended"] = "";	
					$postdata["d_added"] = date("Y-m-d H:i:s");
				}
			} elseif ($script == 'edit'){

				if ($_SERVER['REQUEST_METHOD']=='POST') {

						$postdata["user_id"] = $this->input->post("i_user_id");

						$query = $this->db->query("SELECT v_firstname,v_lastname FROM tbl_members WHERE id=".$this->input->post("i_user_id"));
						$members_data = $query->result();

						if( isset($members_data[0]) && !empty($members_data[0]) ){

							$postdata["v_firstname"] = $members_data[0]->v_firstname;
							$postdata["v_lastname"]  = $members_data[0]->v_lastname;

						}else{

							$postdata["v_firstname"] = '';
							$postdata["v_lastname"]  = '';

						}

						$postdata["v_email"]  = $this->input->post("v_email");
						$postdata["e_sended"] = $this->input->post("e_sended") == 'yes' ? '1' : '0';						

					$this->admin_model->update_entry($id,$postdata,$tbl);
					redirect(base_url().'admin/manage_user_request/?succ=1&msg=edit');
					exit;

				}elseif($id !=""){

					$selectTeam = $this->admin_model->select_entry($tbl,$id);
					$data =array_merge($data, $selectTeam);
				}
			}

			elseif($script == "delete"){
				if($id){
					$tbl = 'tbl_request_user_data';
					$deleteTeam = $this->admin_model->delete_entry($id,$tbl);
					redirect(base_url().'admin/manage_user_request/?succ=1&msg=multidel');
				}
			}

			$this->load->view('admin/manage_user_request',$data);	
	    }

	    public function send_requested_data($id=""){


	    	if( isset($id) && !empty($id) ){

	    		$select_user = $this->admin_model->select_entry("tbl_request_user_data",$id);

	    		if( !empty($select_user) ){

	 	   			$user_data = $this->tomouh_model->getUserByUserId($select_user['user_id']);

	 	   			if( !empty($user_data) ){

	 	   				$email_content = '<p>Hello '.$user_data['v_firstname'].',</p>
								  		  <p>We are sending you data that we are currently used in our website. Please check details below.</p>';

						$email_content .= '<table cellpadding="10" align="center">
									<tr>
										<th align="left">Name</th>
										<td>'.$user_data["v_firstname"]. ' ' .$user_data["v_lastname"].'</td>
									</tr>
									<tr>
										<th align="left">Email</th>
										<td>'.$user_data["v_email"].'</td>
									</tr>
									<tr>
										<th align="left">Date Of Birth</th>
										<td>'.$user_data["d_dob"].'</td>
									</tr>
									<tr>
										<th align="left">Gender</th>
										<td>'.$user_data["e_gender"].'</td>
									</tr>
									<tr>
										<th align="left">Home Country</th>
										<td>'.$user_data["v_home_country"].'</td>
									</tr>
									<tr>
										<th align="left">Home City</th>
										<td>'.$user_data["v_home_city"].'</td>
									</tr>
									<tr>
										<th align="left">Residence Country</th>
										<td>'.$user_data["v_residence_country"].'</td>
									</tr>
									<tr>
										<th align="left">Residence City</th>
										<td>'.$user_data["v_residence_city"].'</td>
									</tr>
									<tr>
										<th align="left">Telephone</th>
										<td>'.$user_data["v_telephone"].'</td>
									</tr>
									<tr>
										<th align="left">Telephone 2</th>
										<td>'.$user_data["v_telephone_2"].'</td>
									</tr>
									<tr>
										<th align="left">Twitter Link</th>
										<td>'.$user_data["v_twitter_link"].'</td>
									</tr>
									<tr>
										<th align="left">LinkedIn Link</th>
										<td>'.$user_data["v_linkedin_link"].'</td>
									</tr>
									<tr>
										<th align="left">Instagram Link</th>
										<td>'.$user_data["v_instagram_link"].'</td>
									</tr>
									<tr>
										<th align="left">Passion And Interest</th>
										<td>'.$user_data["t_passion_interest"].'</td>
									</tr>
								</table>';

						$edu_data = $this->tomouh_model->getEducationalData($select_user['user_id']);
						
						if(!empty($edu_data)){							

							$email_content .= '<table cellpadding="10" style="text-align: center;width:100%;" border="1px;" align="left">
										<tr>
											<th colspan="4">
												Educational Data
											</th>
										</tr>
										<tr>
											<th>University</th>
											<th>Degree</th>
											<th>Major</th>
											<th>Graduation Year</th>
										</tr>';

							foreach( $edu_data as $edu ){

								$email_content.=  '<tr>
														<td>'.$edu['v_university'].'</td>
														<td>'.$edu['v_degree'].'</td>
														<td>'.$edu['v_major'].'</td>
														<td>'.$edu['i_passing_year'].'</td>
													</tr>';	
							}

							$email_content .= '</table>';
						}

						$pro_data = $this->tomouh_model->getProfessionalData($select_user['user_id']);
						
						if(!empty($pro_data)){						

							$email_content .= '<table cellpadding="10" style="margin-top:10px; text-align: center;width:100%;" border="1px;" align="left">

										<tr>
											<th colspan="5">
												Professional Data
											</th>
										</tr>
										<tr>
											<th>Company</th>
											<th>Job Title</th>
											<th>Industry</th>
											<th>From</th>
											<th>To</th>
										</tr>';

							foreach( $pro_data as $pro ){

								$email_content.=  '<tr>
														<td>'.$pro['v_company'].'</td>
														<td>'.$pro['v_job_title'].'</td>
														<td>'.$pro['v_industry'].'</td>
														<td>'.$pro['i_company_from'].'</td>
														<td>'.$pro['i_company_to'].'</td>
													</tr>';	
							}

							$email_content .= '</table>';
						}

						$award_data = $this->tomouh_model->getAchievementalData($select_user['user_id']);

							if(!empty($award_data)){

								$email_content .= '<table cellpadding="10" style="margin-top:10px;width:100%;text-align:center;" border="1px;" align="left">
													<tr>
														<th>
															Achievements, Awards and Honors
														</th>
													</tr>';								

								foreach( $award_data as $awa ){

									$email_content .= '<tr>
															<td>'.$awa['v_achievement'].'</td>
													   </tr>';

								}

								$email_content .= "</table>";
							}

						$email_to = $user_data["v_email"];
						$email_from ='';
						$email_subject = 'Your\'s data which was used by Tomouh';
						$template = $this->tomouh_model->getEmailTemplate(9);

						$content = isset($template['l_body']) ? $template['l_body'] : '';

						
						$content = str_replace("REQUESTED_DATA", $email_content, $content);
						
						
	                    $sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content , $attachments = array() );
						
	                    if($sent){

			                $update=array(
			                    'e_sended'=>'1',
			                );
			                $updateSiteSetting = $this->admin_model->update_entry($id,$update,"tbl_request_user_data");

							redirect(base_url().'admin/manage_user_request/?succ=1&msg=sent_email');
							exit();
						}else{
							redirect(base_url().'admin/manage_user_request/?succ=0&msg=not_sent_email');
							exit();
						}
	 	   			}
	    		}
	    	}

	    	redirect(base_url().'admin/manage_user_request/?succ=0&msg=not_sent_email');
			exit();
	    }


	    public function update_zoho_crm_admin( $data, $user_id ){
	    	// echo "<pre>";print_r($data); exit;
	    	$user_data = $this->tomouh_model->getUserByUserId($user_id);

	    	if(isset($data['university']) && !empty($data['university'])){

            $educ_data_length = sizeof($data['university']);

            $education_info = array();

            for($i=0;$i<$educ_data_length;$i++){

                if(isset($data['university'][$i]) && !empty($data['university'][$i])){

                $education_info[] = 'University : '.$data['university'][$i].', Degree : '.$data['degree'][$i].', Major : '.$data['major'][$i].', Year : '.$data['graduation_year'][$i];

	                }
	            }
	        }

	        if(isset($data['company']) && !empty($data['company'])){

	            $company_data_length = sizeof($data['company']);

	            $experience_info = array();

	            for($i=0;$i<$company_data_length;$i++){

	                if(isset($data['company'][$i]) && !empty($data['company'][$i])){

	                $experience_info[] = 'Company : '.$data['company'][$i].', Job Title : '.$data['job_title'][$i].', Industry : '.$data['industry'][$i].', Period : '.$data['company_from'][$i].'-'.$data['company_to'][$i];

	                }
	            }
	        }

	        if(isset($data['award_details']) && !empty($data['award_details'])){

	            $award_data_length = sizeof($data['award_details']);

	            $award_detail = '';

	            for($i=0;$i<$award_data_length;$i++){

	                if(isset($data['award_details'][$i]) && !empty($data['award_details'][$i])){

	                 $award_detail .= $data['award_details'][$i].',';

	                }
	            }
	        }

	        if(isset($award_detail)){

	            $AAH = rtrim($award_detail,',');

	        }else{
	            $AAH = '';
	        }
			
			
	        $educational_info = implode(' %0A ',$education_info);

	        $exp_info = implode(' %0A ', $experience_info);

	        
			if(isset($user_data['v_crm_contact_id']) && !empty($user_data['v_crm_contact_id'])){

				$contact_id = $user_data['v_crm_contact_id'];
				
				//UPDATE CONTACT DATA
				$zcrmRecordContactIns = ZCRMRecord::getInstance("Contacts", $contact_id);
				

				$zcrmRecordContactIns->setFieldValue("Last_Name", $data['v_lastname']);
				$zcrmRecordContactIns->setFieldValue("Email",$data['v_email']);
				$zcrmRecordContactIns->setFieldValue("First_Name",$data['v_firstname']);
				$zcrmRecordContactIns->setFieldValue("Date_of_Birth",date('Y-m-d', strtotime($data['d_dob'])));
				$zcrmRecordContactIns->setFieldValue("Gender",$data['gender']);

				$zcrmRecordContactIns->setFieldValue("Home_Country",$data['v_home_country']);
				$zcrmRecordContactIns->setFieldValue("Home_City",$data['v_home_city']);
				$zcrmRecordContactIns->setFieldValue("Mobile",$data['v_telephone']); 
				$zcrmRecordContactIns->setFieldValue("Mobile_No",$data['v_telephone']); 

				$zcrmRecordContactIns->setFieldValue("Country_of_Residence",$data['v_residence_country']);
				$zcrmRecordContactIns->setFieldValue("City_Of_Residence'",$data['v_residence_city']);
				$zcrmRecordContactIns->setFieldValue("Mobile_No_2",$data['v_telephone_2']);
				$zcrmRecordContactIns->setFieldValue("Educational_Info",isset($educational_info)?urldecode($educational_info):''); 
				$zcrmRecordContactIns->setFieldValue("Experience_Info",isset($exp_info)?urldecode($exp_info):'');
				$zcrmRecordContactIns->setFieldValue("AAH",$AAH);
				$zcrmRecordContactIns->setFieldValue("Passions_and_interests",$data['passions_interests']);
				
				$apiResponse=$zcrmRecordContactIns->update();

				// echo "<pre>"; 
				// print_r($data);
				// print_r($apiResponse); exit;

			}	
			if(isset($user_data['v_member_crm_id']) && !empty($user_data['v_member_crm_id'])){

				$member_id = $user_data['v_member_crm_id'];
				// UPDATE MEMBER DATA
				$zcrmRecordMemberIns = ZCRMRecord::getInstance("Members", $member_id);

				$zcrmRecordMemberIns->setFieldValue("Last_Name", $data['v_lastname']);
				$zcrmRecordMemberIns->setFieldValue("Email",$data['v_email']);
				$zcrmRecordMemberIns->setFieldValue("Name",$data['v_firstname']);
				$zcrmRecordMemberIns->setFieldValue("Date_Of_Birth",date('Y-m-d', strtotime($data['d_dob'])));
				$zcrmRecordMemberIns->setFieldValue("Gender",$data['e_gender']);

				$zcrmRecordMemberIns->setFieldValue("Home_Country",$data['v_home_country']);
				$zcrmRecordMemberIns->setFieldValue("Home_City",$data['v_home_city']);
				$zcrmRecordMemberIns->setFieldValue("Mobile_No",$data['v_telephone']); 

				$zcrmRecordMemberIns->setFieldValue("Country_of_Residence",$data['v_residence_country']);
				$zcrmRecordMemberIns->setFieldValue("City_Of_Residence'",$data['v_residence_city']);
				$zcrmRecordMemberIns->setFieldValue("Mobile_No_2",$data['v_telephone_2']);

				$zcrmRecordMemberIns->setFieldValue("Educational_Info_1",isset($educational_info)?urldecode($educational_info):''); 
				$zcrmRecordMemberIns->setFieldValue("Experience_Info_1",isset($exp_info)?urldecode($exp_info):'');
				$zcrmRecordMemberIns->setFieldValue("AAH",$AAH);
				$zcrmRecordMemberIns->setFieldValue("Passions_and_interests",$data['passions_interests']);
				
				$apiResponse=$zcrmRecordMemberIns->update();

			}



	    }
		
		public function update_zoho_status_crm_admin( $data, $user_id ){

	    	$user_data = $this->tomouh_model->getUserByUserId($user_id);
		
	    	if(isset($user_data['v_member_crm_id']) && $user_data['v_member_crm_id'] != ''){

		    	$zcrmRecordIns = ZCRMRecord::getInstance("Members", $user_data['v_member_crm_id']);
				$zcrmRecordIns->setFieldValue("Status", $data['status']);
				$apiResponse=$zcrmRecordIns->update();

	    	}

	    }

	    public function delete_crm_records_admin( $user_id ){ 

	    	$user_data = $this->tomouh_model->getUserByUserId($user_id);

	    	if(isset($user_data['v_member_crm_id']) &&  !empty($user_data['v_member_crm_id'])){

	    		$this->admin_model->delete_crm_member($user_data['v_member_crm_id'],"tbl_crm_members_data");

		    	$zcrmRecordIns = ZCRMRecord::getInstance("Members", $user_data['v_member_crm_id']); 
				$apiResponse=$zcrmRecordIns->delete();
			}

			if(isset($user_data['v_crm_contact_id']) &&  !empty($user_data['v_crm_contact_id'])){

	    		$this->admin_model->delete_crm_contact($user_data['v_crm_contact_id'],"tbl_crm_contact_data");

		    	$zcrmRecordIns = ZCRMRecord::getInstance("Contacts", $user_data['v_crm_contact_id']); 
				$apiResponse=$zcrmRecordIns->delete();
			}

	    }
		
		public function file_get_contents_curl($url) {

	    $ch = curl_init();

	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_URL, $url);

	    $data = curl_exec($ch);
	    curl_close($ch);

	    return $data;
	}
}
?>