<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./zoho/vendor/autoload.php');
use CristianPontes\ZohoCRMClient\ZohoCRMClient;

require_once('./zoho_v2/vendor/autoload.php');

class GetData extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('tomouh_model');
		$this->load->model('general_model');
		$this->load->model('messages_model');
		$this->load->model('admin/admin_model');
		$this->load->model('paging_model');
		$this->load->library('form_validation'); 
		$this->load->library('session');
		ZCRMRestClient::initialize();
	}
	public function index(){
		/*
		$cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');
		$client = new ZohoCRMClient('Contacts', $cms_token,"eu");
		$records = $client->getRecords()
				    ->since(date_create(date('Y-m-d 00:00:00', strtotime('-1 days'))))
				    ->request(); */
		$moduleIns=ZCRMRestClient::getInstance()->getModuleInstance("Members");  //To get module instance
		$modified_since["If-Modified-Since"]=date('c', strtotime('-1 days'));//time in 8601 format
        $response=$moduleIns->getRecords(null, null, null, 1,200,$modified_since); //To get module records
	
		$records=$response->getData();		    

					    // echo "<pre>";
					    // print_r($records);
					    // exit();

		}
	}
	?>