<?php include("inc/header_top.php");
include("inc/header.php");
 ?>
<div class="after-login">
    <style type="text/css">

      .error_box{display: inline-block;width: 100%;padding-top: 20px;padding-left: 10%;padding-right: 10%;}
      .error_box .icon{float: left;padding-right: 10px;}
      input.parsley-error {
        border: 1px solid red;
      }

      .parsley-error-list {
          color: red;
      }
    </style>
        
              
<div>
  <div class="account_page" style="margin-top: 100px;">
              <?php 
              if(!($this->form_validation->error_array())){
                if(isset($_GET['msg']) && $_GET['msg'] !=''){ ?>
                <div id="response" class="error_box">
               <?php   if($_GET['succ']==1){
                    echo $this->messages_model->getSuccessMsg($_GET['msg']);
                  }
                  else if($_GET['succ']==0){
                    echo $this->messages_model->getErrorMsg($_GET['msg']);
                  } ?>
                </div>
            <?php  }
              }?>
              <?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>
    <section class="event_cal">
      <div class="sec_banner">
        <div class="container">
          <div class="page_head">
            <h1 class="red-text text-center font36">Reset Password</h1>
          </div>
          <div class="border-box">
            <form action="" method="post" class="change_password" parsley-validate>
              <div class="row">
                <div class="col-xs-12">
                  <p class="form-head ">Reset Password Your Password:</p>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12" >
                  <div class="form-group">
                    <label>New Password</label>
                    <input type="password" name="new_password" class="form-control" placeholder="password" parsley-required="true" parsley-minlength="6" parsley-maxlength="12">
                  </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" name="confirm_password" class="form-control" placeholder="password" parsley-required="true" parsley-minlength="6" parsley-maxlength="12">
                  </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                  <button class="btn red-btn" type="submit" style="margin-top: 11%;margin-left: 10px;">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</div>
<?php include('inc/footer.php'); ?>