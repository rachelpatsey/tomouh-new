<?php

$url = $_SERVER['REQUEST_URI'];
$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$data= array();
$query = $this->db->query("SELECT * FROM tbl_sitesetting");
    $results = $query->result_array();

    foreach($results as $result){

      $data[strtolower($result['v_name'])] = $result['l_value'];

    }
?>
<link href="<?php echo base_url(); ?>assets/client/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/client/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/client/css/font-awesome.min.css">


<script src="<?php echo base_url(); ?>assets/client/js/jquery.js"></script> 
<script src="<?php echo base_url(); ?>assets/client/js/custom.js"></script>
<script src="<?php echo base_url(); ?>assets/client/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/client/js/jquery-mobile.js"></script>


<div class="main-header">
  <nav class="navbar navbar-fixed-top hidden-xs hidden-sm <?php if($actual_link !== $data['site_url']){ ?> inner_header <?php } ?>" id="menu">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>

        <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $data['site_logo']; ?>" alt="" class="img-responsive"></a> </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
            <li <?php if($actual_link == $data['site_url']){ ?> class="active" <?php } ?>><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="dropdown <?php if(strpos($url, 'about_us') !== false || strpos($url, 'team') !== false || strpos($url, 'honorary_member') !== false || strpos($url, 'why_join') !== false){ ?> active <?php } ?>"> <a class="dropdown-toggle"  data-toggle="dropdown" href="#">About Us</a>
              <ul class="dropdown-menu">
                <li class=""><a href="<?php echo base_url(); ?>about_us">About Tomouh</a></li>
                <li class=""><a href="<?php echo base_url(); ?>team">Team</a></li>
                <li><a href="<?php echo base_url(); ?>honorary_member">Honorary Members</a></li>
                <li><a href="<?php echo base_url(); ?>why_join">Why Join Tomouh?</a></li>
              </ul>
            </li>
          
          <!--<li><a href="why_join.html">Why Join</a></li>-->
            <li class="dropdown <?php if (strpos($url, 'membership_process') !== false || strpos($url, 'membership_committee') !== false || strpos($url, 'faq') !== false) { ?> active <?php } ?>">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">Membership</a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>membership_process">Membership Process</a></li>
                <li><a href="<?php echo base_url(); ?>membership_committee">Membership Committee</a></li>
                <li><a href="<?php echo base_url(); ?>signup">Apply to Tomouh</a></li>
                <li><a href="<?php echo base_url(); ?>faq">FAQs</a></li>
              </ul>
            </li>
            <li class="dropdown <?php if(strpos($url, 'media') !== false || strpos($url, 'blog') !== false) {?> active <?php } ?>">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">The Latest</a>
              <ul class="dropdown-menu">
                <li class=""><a href="<?php echo base_url(); ?>media">Media</a></li>
                <li><a href="<?php echo base_url(); ?>blog">Blog</a></li>
              </ul>
            </li>
            <li class="<?php if(strpos($url, 'event') !== false){ ?> active <?php } ?>"><a href="<?php echo base_url(); ?>event">Events</a></li>
            <li class="<?php if(strpos($url, 'contact_us') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>contact_us">Contact</a></li>
            <li class="login_signup"><a href="<?php echo base_url(); ?>signup" class="signup">Apply</a><a href="<?php echo base_url(); ?>login" class="login">Login</a></li>
          </ul>
      </div>
    </div>
  </nav>
<div class="mobile_responsive_menu hidden-lg hidden-md">
    <div class="mobile_logo" id="menu1"> <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $data['site_logo']; ?>" alt="" class="img-responsive"></a> </div>
    <header>
      <div class="wsmenucontainer clearfix">
        <div class="overlapblackbg"></div>
        <div class="wrapper clearfix"> <a id="wsnavtoggle" class="animated-arrow"><span></span></a> </div>
        <nav class="wsmenu clearfix">
          <ul class="mobile-sub wsmenu-list clearfix">
            <li class="active"><a href="<?php echo base_url(); ?>">Home<span class="wsmenu-click"><i class="fa fa-close" onClick="hideMenu()"></i></span></a></li>
            <li class=""><span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span><a href="#">About Us<span class="arrow"></span></a>
              <ul class="wsmenu-submenu">
                <li class=""><a href="<?php echo base_url(); ?>about_us">About Tomouh</a></li>
                <li><a href="<?php echo base_url(); ?>team">Team</a></li>
                <li class=""><a href="<?php echo base_url(); ?>honorary_member">Honorary Members</a></li>
                <li><a href="<?php echo base_url(); ?>why_join">Why Join Tomouh?</a></li>
              </ul>
            </li>
          <!--  <li><a href="why_join.html">Why Join</a></li>-->
            <li class=""><span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span><a href="#">Membership<span class="arrow"></span></a>
              <ul class="wsmenu-submenu">
               <li><a href="<?php echo base_url(); ?>membership_process">Membership Process</a></li>
                <li><a href="<?php echo base_url(); ?>membership_committee">Membership Committee</a></li>
                <li><a href="<?php echo base_url(); ?>signup">Apply to Tomouh</a></li>
                <li class=""><a href="<?php echo base_url(); ?>faq">FAQs</a></li>
              </ul>
            </li>
            <li class=""><span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span><a href="#">The Latest<span class="arrow"></span></a>
              <ul class="wsmenu-submenu">
               <li class=""><a href="<?php echo base_url(); ?>media">Media</a></li>
                <li><a href="<?php echo base_url(); ?>blog">Blog</a></li>
              </ul>
            </li>
            <li><a href="<?php echo base_url(); ?>event">Events</a></li>
            <li class=""><a href="<?php echo base_url(); ?>contact_us">Contact</a></li>
            <li class="login_signup"><a href="<?php echo base_url(); ?>signup" class="signup">Apply</a></li>
            <li class="login_signup"><a href="<?php echo base_url(); ?>login" class="signup">Login</a></li>
          </ul>
        </nav>
      </div>
    </header>
  </div>
</div>