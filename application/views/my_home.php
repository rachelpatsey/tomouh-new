<?php
include("inc/header_top.php");
include("inc/header.php");
?>
<script type="text/javascript">
  $(document).ready(function(){

    // Select and loop the container element of the elements you want to equalise
    $('.home_user_page').each(function(){  
      
      // Cache the highest
      var highestBox = 0;
      
      // Select and loop the elements you want to equalise
      $('.same_height', this).each(function(){
        
        // If this box is higher than the cached highest then store it
        if($(this).height() > highestBox) {
          highestBox = $(this).height(); 
        }
      
      });  
            
      // Set the height of all those children to whichever was highest 
      $('.same_height',this).height(highestBox);
                    
    }); 

});
</script>
<div class="after-login">
<div class="inner_wrapper">
  <div class="inner_header_div">
    <div class="container">
      <div class="row">
        <div class="col-sm-7 col-md-8 col-xs-12">
          <div class="left-div"> <a href="<?php echo base_url(); ?>my_home" class="active">My Home</a> <a href="<?php echo base_url(); ?>members">Members</a> <a href="<?php echo base_url(); ?>event">Events</a> <a href="<?php echo base_url(); ?>initiative">Initiatives</a> </div>
        </div>
        <div class="col-sm-5 col-md-4  col-xs-12">
          <div class="right-div"> <a href="<?php echo base_url(); ?>account">Account</a> <a href="<?php echo base_url(); ?>profile">Profile</a> </div>
        </div>
      </div>
    </div>
  </div>
  <div class="home_user_page">
    <div class="container">
      <div class="section1">
        <div class="page_head">
          <h1 class="red-text text-left font36"><?php if(isset($meta['my_home_title'])){  echo $meta['my_home_title']; } ?></h1>
        </div>
        <div class="gray-border-box">
          <?php if(isset($main_description)){  echo $main_description; } ?>
        </div>
      </div>
      <div class="section2">
        <div class="row">
          <div class="col-sm-6 col-xs-12 home_member_update">
            <div class="page_head">
              <h1 class="red-text text-left font36">Members Updates</h1>
            </div>
            <!-- <div class="gray-border-box">
              <div class="content-part">
                <?php
                  if(isset($members_update)){
                  for($i=0;$i<4;$i++){
                ?>
                <div class="member_row">
                  <p class="title"><?php echo $members_update[$i]['v_user_name']; ?></p>
                  <p class="desc"><?php echo $members_update[$i]['l_description']; ?></p>
                  <p class="date"><?php  $date = $members_update[$i]['d_added'];
                        echo date("jS F Y", strtotime($date)); ?></p>
                </div>
                <?php
                  }
                }
                ?>
              </div>
            </div> -->
            <div class="gray-border-box same_height">
              <div class="row gray-border-box_content">
                <div class="col-xs-12 col-sm-12">
                  <div id="myCarousel2" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel" data-interval="false"> 
                    <span href="#myCarousel2" data-slide="prev2" class="btn-vertical-slider glyphicon glyphicon-circle-arrow-up "></span> <br />
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                      <div class="item active">
                        <?php
                          if(isset($members_update)){
                          for($i=0;$i<4;$i++){
                        ?>
                        <div class="row new_member_row">
                          <div class="col-xs-12 col-sm-12">
                            <div class="member_row">
                              <p class="title"><?php echo $members_update[$i]['v_user_name']; ?></p>
                              <p class="desc"><?php echo $members_update[$i]['l_description']; ?></p>
                              <p class="desc"><?php  $date = $members_update[$i]['d_added'];
                              echo date("jS F Y", strtotime($date)); ?></p>
                            </div>
                          </div>
                        </div>
                        <?php
                          }
                        }
                        ?>
                        <!--/row-fluid-->
                      </div>
                      <!--/item-->
                        <?php
                          $length = sizeof($members_update);
                          if($length > 4){
                        ?>
                        <div class="item">
                        <?php
                          for($i=4;$i<8;$i++){
                        ?>
                        <div class="row new_member_row">
                          <div class="col-xs-12 col-sm-12">
                            <div class="member_row">
                              <p class="title"><?php echo $members_update[$i]['v_user_name']; ?></p>
                              <p class="desc"><?php echo $members_update[$i]['l_description']; ?></p>
                              <p class="desc"><?php  $date = $members_update[$i]['d_added'];
                              echo date("jS F Y", strtotime($date)); ?></p>
                            </div>
                          </div>
                        </div>

                         <?php
                          }
                          ?>
                          </div>
                       <?Php }
                        ?>
                        <!--/row-fluid-->

                      <!--/item-->
                    </div>
                    <span href="#myCarousel2" data-slide="next2" class="btn-vertical-slider glyphicon glyphicon-circle-arrow-down"
                        ></span> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-xs-12 home_new_member home_new_member2">
            <div class="page_head">
              <h1 class="red-text text-left font36">New Members</h1>
            </div>
            <div class="gray-border-box same_height">
              <div class="row gray-border-box_content">
                <div class="col-xs-12 col-sm-12">
                  <div id="myCarousel1" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel" data-interval="false"> 
                    <span href="#myCarousel1" data-slide="prev" class="btn-vertical-slider glyphicon glyphicon-circle-arrow-up "></span> <br />
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                      <div class="item active">
                        <?php
                          // echo "<pre>"; print_r($members);
                        if(isset($members)){
                          for($i=0;$i<4;$i++){
                        ?>
                        <div class="row new_member_row">
                          <div class="col-xs-12 col-sm-12">
                            <div class="member_row">
                              <p class="title"><?php echo $members[$i]['v_firstname']; ?> <?php echo $members[$i]['v_lastname']; ?></p>
                              <p class="desc"><?php if(isset($members[$i]['v_main_occupation']) && !empty($members[$i]['v_main_occupation'])){ echo $members[$i]['v_main_occupation']; } ?></p>
                              <p class="desc"><?php if(isset($members[$i]['v_residence_city']) && !empty($members[$i]['v_residence_city'])){ echo $members[$i]['v_residence_city'].", "; } ?><?php if(isset($members[$i]['v_residence_country']) && !empty($members[$i]['v_residence_country'])){ echo $members[$i]['v_residence_country']; }?></p>
                            </div>
                          </div>
                        </div>
                        <?php
                          }
                        }
                        ?>
                        <!--/row-fluid-->
                      </div>
                      <!--/item-->
                        <?php
                          $length = sizeof($members);
                          if($length > 4){
                        ?>
                        <div class="item">
                        <?php
                          for($i=4;$i<8;$i++){
                        ?>
                        <div class="row new_member_row">
                          <div class="col-xs-12 col-sm-12">
                            <div class="member_row">
                              <p class="title"><?php echo $members[$i]['v_firstname']; ?> <?php echo $members[$i]['v_lastname']; ?></p>
                              <p class="desc"><?php if(isset($members[$i]['v_main_occupation']) && !empty($members[$i]['v_main_occupation'])){ echo $members[$i]['v_main_occupation']; } ?></p>
                              <p class="desc"><?php if(isset($members[$i]['v_residence_city']) && !empty($members[$i]['v_residence_city'])){ echo $members[$i]['v_residence_city'].", "; } ?><?php if(isset($members[$i]['v_residence_country']) && !empty($members[$i]['v_residence_country'])){ echo $members[$i]['v_residence_country']; }?></p>
                            </div>
                          </div>
                        </div>

                         <?php
                          }
                          ?>
                        </div>
                       <?Php }
                        ?>
                         <?php
                          $length = sizeof($members);
                          if($length > 8){
                        ?>
                        <div class="item">
                        <?php
                          for($i=8;$i<12;$i++){
                        ?>
                        <div class="row new_member_row">
                          <div class="col-xs-12 col-sm-12">
                            <div class="member_row">
                              <p class="title"><?php echo $members[$i]['v_firstname']; ?> <?php echo $members[$i]['v_lastname']; ?></p>
                              <p class="desc"><?php if(isset($members[$i]['v_main_occupation']) && !empty($members[$i]['v_main_occupation'])){ echo $members[$i]['v_main_occupation']; } ?></p>
                              <p class="desc"><?php if(isset($members[$i]['v_residence_city']) && !empty($members[$i]['v_residence_city'])){ echo $members[$i]['v_residence_city'].", "; } ?><?php if(isset($members[$i]['v_residence_country']) && !empty($members[$i]['v_residence_country'])){ echo $members[$i]['v_residence_country']; }?></p>
                            </div>
                          </div>
                        </div>

                         <?php
                          }
                          ?>
                        </div>
                       <?Php }
                        ?>
                         <?php
                          $length = sizeof($members);
                          if($length > 12){
                        ?>
                        <div class="item">
                        <?php
                          for($i=12;$i<16;$i++){
                        ?>
                        <div class="row new_member_row">
                          <div class="col-xs-12 col-sm-12">
                            <div class="member_row">
                              <p class="title"><?php echo $members[$i]['v_firstname']; ?> <?php echo $members[$i]['v_lastname']; ?></p>
                              <p class="desc"><?php if(isset($members[$i]['v_main_occupation']) && !empty($members[$i]['v_main_occupation'])){ echo $members[$i]['v_main_occupation']; } ?></p>
                              <p class="desc"><?php if(isset($members[$i]['v_residence_city']) && !empty($members[$i]['v_residence_city'])){ echo $members[$i]['v_residence_city'].", "; } ?><?php if(isset($members[$i]['v_residence_country']) && !empty($members[$i]['v_residence_country'])){ echo $members[$i]['v_residence_country']; }?></p>
                            </div>
                          </div>
                        </div>

                         <?php
                          }
                          ?>
                        </div>
                       <?Php }
                        ?>
                        <!--/row-fluid-->

                      <!--/item-->
                    </div>
                    <span href="#myCarousel1" data-slide="next" class="btn-vertical-slider glyphicon glyphicon-circle-arrow-down"
                        ></span> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section3">
      <section class="bottom_slider">
        <div class="container">
          <div class="head">
            <h1 class="red-text text-center font36">Recommended Books</h1>
          </div>
          <div id="owl-demo" class="owl-carousel owl-theme">
            <?php
              if(isset($books)){
                $length = sizeof($books);
                for($i=0;$i<$length;$i++){
            ?>
            <div class="item">
              <div class="white-box">
                <div class="img-block center-block">
                  <div class="img-center"> <img src="<?php echo base_url(); ?>assets/frontend/images/<?php if (isset($books[$i]['v_image'])){ echo $books[$i]['v_image']; } ?>" class="img-responsive" alt=""> </div>
                </div>
                <div class="border"></div>
                <div class="red-text"><?php if (isset($books[$i]['v_title'])){ echo $books[$i]['v_title']; } ?></div>
                <p class="black-text"><?php if (isset($books[$i]['l_description'])){ echo $books[$i]['l_description']; } ?></p>
              </div>
            </div>
            <?php
              }
            }
            ?>
          </div>
          <div class="owl-prev" id="prev"> </div>
          <div class="owl-next" id="next"> </div>
        </div>
      </section>
    </div>
  </div>
</div>
<?php
include("inc/footer.php");
?>