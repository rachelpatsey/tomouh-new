<?php include("inc/header_top.php"); ?>
<?php include("inc/header.php"); ?>
<div class="inner_wrapper">
  <div class="faq_page">
    <div class="sec_banner">
      <div class="container">
        <div class="page_head">
          <h1 class="red-text text-center font36"><?php if(isset($title)){ echo $title; } ?></h1>
        </div>
        <div class="que_ans">
          <ul class="faq">
            <?php 
             foreach($faqs as $faq){
            ?>

            <li class="q">
              <div class="img"></div>
              <p class="question"> <?php if(isset($faq['v_question'])){ echo $faq['v_question']; }?></p>
            </li>
            <li class="a" style="color: #343434 !important;">
              <div class="answer"><?php if(isset($faq['l_answer'])){ echo $faq['l_answer']; }?></div>
            </li>
            <?php 
              }
            ?>
    
          </ul>
        </div>
      </div>
    </div>
  </div>

</div>
<?php include('inc/footer.php'); ?>