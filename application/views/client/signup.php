<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Tomouh</title>

<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url(); ?>assets/client/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>assets/client/css/webapplinks.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/client/css/owl.theme.default.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/client/css/owl.carousel.css">
<link href="<?php echo base_url(); ?>assets/client/css/bootstrap-select.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>assets/client/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/client/css/font-awesome.min.css">
</head>

<body class="offwhite-bg">
<div class="main-header">
  <nav class="navbar navbar-fixed-top hidden-xs hidden-sm inner_header" id="menu">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="navbar-brand" href="index.html"><img src="<?php echo base_url(); ?>assets/client/images/logo@2x.png" alt="" class="img-responsive"></a> </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li class=""><a href="index.html">Home</a></li>
          <li class="dropdown"><a class="dropdown-toggle"  data-toggle="dropdown" href="#">About Us</a>
            <ul class="dropdown-menu">
              <li class=""><a href="about_us.html">About Tomouh</a></li>
              <li class=""><a href="team.html">Team</a></li>
              <li><a href="honory_member.html">Honorary Members</a></li>
            </ul>
          </li>
          
          <!--<li><a href="why_join.html">Why Join</a></li>-->
          <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Membership</a>
            <ul class="dropdown-menu">
              <li><a href="membership_process.html">Membership Process</a></li>
              <li><a href="team_committee.html">Membership Committee</a></li>
              <li><a href="signup.html">Apply to Tomouh</a></li>
              <li><a href="faq.html">FAQs</a></li>
            </ul>
          </li>
          <li class="dropdown "><a class="dropdown-toggle" data-toggle="dropdown" href="#">The Latest</a>
            <ul class="dropdown-menu">
             <li class=""><a href="media.html">Media</a></li>
              <li><a href="blog.html">Blog</a></li>
            </ul>
          </li>
          <li class=""><a href="event.html">Events</a></li>
          <li class=""><a href="contact_us.html">Contact</a></li>
          <li class="login_signup"><a href="signup" class="signup">Sign Up</a><a href="login.html" class="login">Login</a></li>
        </ul>
      </div>
    </div>
  </nav>
<div class="mobile_responsive_menu hidden-lg hidden-md">
    <div class="mobile_logo inner_header" id="menu1"> <a href="index.html"><img src="<?php echo base_url(); ?>assets/client/images/logo@2x.png" alt="" class="img-responsive"></a> </div>
    <header>
      <div class="wsmenucontainer clearfix">
        <div class="overlapblackbg"></div>
        <div class="wrapper clearfix"> <a id="wsnavtoggle" class="animated-arrow"><span></span></a> </div>
        <nav class="wsmenu clearfix">
          <ul class="mobile-sub wsmenu-list clearfix">
            <li class=""><a href="index.html">Home<span class="wsmenu-click"><i class="fa fa-close" onClick="hideMenu()"></i></span></a></li>
            <li class=""><span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span><a href="#">About Us<span class="arrow"></span></a>
              <ul class="wsmenu-submenu">
                <li class=""><a href="about_us.html">About Tomouh</a></li>
                <li><a href="team.html">Team</a></li>
                <li class=""><a href="honory_member.html">Honorary Members</a></li>
              </ul>
            </li>
          <!--  <li><a href="why_join.html">Why Join</a></li>-->
            <li class=""><span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span><a href="#">Membership<span class="arrow"></span></a>
              <ul class="wsmenu-submenu">
               <li><a href="membership_process.html">Membership Process</a></li>
                <li><a href="team_committee.html">Membership Committee</a></li>
                <li class="active"><a href="signup.html">Apply to Tomouh</a></li>
                <li class=""><a href="faq.html">FAQs</a></li>
              </ul>
            </li>
            <li class=""><span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span><a href="#">The Latest<span class="arrow"></span></a>
              <ul class="wsmenu-submenu">
               <li class=""><a href="media.html">Media</a></li>
                <li><a href="blog.html">Blog</a></li>
              </ul>
            </li>
            <li><a href="event.html">Events</a></li>
            <li class=""><a href="contact_us.html">Contact</a></li>
            <li class="login_signup"><a href="signup.html" class="signup">Sign Up</a></li>
            <li class="login_signup"><a href="login.html" class="signup">Login</a></li>
          </ul>
        </nav>
      </div>
    </header>
  </div>
</div>
<style type="text/css">
  .error_box{display: inline-block; width: 100%; background: #ccc; padding-top: 100px;}
  .error_box .icon{float: left;padding-right: 10px;}
</style>
<div id="response" class="error_box">
              <?php 
              if(!($this->form_validation->error_array())){
                if(isset($_GET['msg']) && $_GET['msg'] !=''){
                  if($_GET['succ']==1){
                    echo $this->messages_model->getSuccessMsg($_GET['msg']);
                  }
                  else if($_GET['succ']==0){
                    echo $this->messages_model->getErrorMsg($_GET['msg']);
                  }
                }
              }?>
                          <?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>
            </div>
<div class="inner_wrapper">
<div class="container">
  <div class="signup_page">
    <div class="form">
      <h2 class="signup-head red-text spectral-font font36 text-center"><strong>Register</strong></h2>
      <div class="signup_headline">
        <ul class="nav nav-tabs nav-justified">
          <li class="active"><a><img src="<?php echo base_url(); ?>assets/client/images/active-radio.png" class="img-responsive" alt="">PERSONAL</a></li>
          <li><a><img src="<?php echo base_url(); ?>assets/client/images/radio-default.png" class="img-responsive" alt="">EDUCATION & Professional</a></li>
        </ul>
      </div>
      <form name="signup_form_1" class="signup_form" method="post" enctype="multipart/form-data">
        <div class="gray-border-box">
          <div class="row">
            <div class="col-xs-12">
              <p class="form-head">Personal Information</p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-3">
              <div class="form-group">
                <label>First Name*</label>
                <input type="text" name="firstname" id="firstnme" class="form-control" required/>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label>Last Name*</label>
                <input type="text" name="lastname" id="lastname" class="form-control" required>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label>Date Of Birth*</label>
                <input type="text" name="dob" id="dob" class="form-control" required>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label>Gender</label>
                <div class="select">
                  <select class="selectpicker" name="gender" id="gender">
                    <option>Male</option>
                    <option>Female</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="gray-border-box">
          <div class="row">
            <div class="col-xs-12">
              <p class="form-head">Account</p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-3">
              <div class="form-group">
                <label>Email</label>
                <input type="text" name="email" id="email" class="form-control" required>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group pwd">
                <label>Password</label>
                <input type="text" name="password" id="password" class="form-control fa fa-eye" required>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group pwd">
                <label>Confrim Password</label>
                <input type="text" name="confirm_password" id="confirm_password" class="form-control" required>
              </div>
            </div>
          </div>
        </div>
        <div class="gray-border-box">
          <div class="row">
            <div class="col-xs-12">
              <p class="form-head">Contact Information</p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Home Country</label>
                <div class="select">
                  <select class="selectpicker" name="home_country">
                    <option>Select Country</option>
                    <option>India</option>
                    <option>Canada</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Home City</label>
                <input type="text" name="home_city" id="home_city" class="form-control">
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Country of Residence</label>
                <div class="select">
                  <select class="selectpicker" name="residence_country" id="residence_country">
                    <option>Select Country</option>
                    <option>India</option>
                    <option>Canada</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>City of Residence</label>
                <input type="text" name="residence_city" id="residence_city" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Mobile Number</label>
                <input type="text" name="telephone" id="telephone" class="form-control">
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Mobile Number 2</label>
                <input type="text" name="telephone_2" id="telephone_2" class="form-control">
              </div>
            </div>
          </div>
        </div>
        <div class="gray-border-box">
          <div class="row">
            <div class="col-xs-12">
              <p class="form-head">Social Account</p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Twitter Link</label>
                <input type="text" name="twitter_link" id="twitter_link" class="form-control">
                <p class="gray-text font12">Hints: twitter.com/johndoe</p>
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Linkedin Link</label>
                <input type="text" name="linkedin_link" id="linkedin_link" class="form-control">
                <p class="gray-text font12">Hints: uk.linkedin.com/in/johndoe</p>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12">
              <div class="form-group">
                <label>Were you referred to by a member?</label>
                <div class="row">
                  <div class="col-sm-4 col-md-3 col-xs-6">
                    <button class="btn gray-border-btn">Yes</button>
                  </div>
                  <div class="col-sm-4  col-md-3 col-xs-6">
                    <button class="btn gray-border-btn">No</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="gray-border-box">
          <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
              <p class="form-head">Verification</p>
            </div>
            <div class="col-sm-8 col-md-9 col-xs-12"> <img src="<?php echo base_url(); ?>assets/client/images/captcha.png" class="img-responsive" alt=""> </div>
          </div>
        </div>
        <div class="form-group"> 
          <!-- <a href="signup_step2.html" id="signup_1" class="btn gray-btn">Continue</a> --> 
            <input type="submit" name="signup_1" id="signup_1" class="btn gray-btn" value="Continue">
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<div class="footer">
    <footer>
        <div class="container">
            <div class="first-part">
               <div class="col-sm-4 col-xs-12  col-lg-3">
                   <div class="first-col">
                         <div class="footer-logo"><img src="<?php echo base_url(); ?>assets/client/images/graylogo@2x.png" class="img-responsive" alt=""></div>
                        <ul>
                            <li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                        </ul>
                        
                    </div>
                    <div class="border-right hidden-xs hidden-sm"></div>
                </div>
                <div class="col-sm-4  col-lg-5  col-xs-12">
                    <div class="sec-col">
                        <ul>
                           <li class=""><a href="about_us.html">About Tomouh</a></li>
                          <li><a href="team.html">Team</a></li>
                          <li><a href="membership_process.html">Membership</a></li>
                          <li class=""><a href="media.html">Media</a></li> 
                          <li class=""><a href="blog.html">Blog</a></li>
                          <li><a href="event.html">Events</a></li>
                        </ul>
                    </div>
                  <div class="border-right hidden-xs hidden-sm"></div>
                </div>
                <div class="col-sm-4  col-lg-4  col-xs-12">
                    <div class="third-col">
                        <ul>
                            <li><a href="contact_us.html">Contact Us</a></li>
                            <li><a href="mailto:info@tomouh.net">info@tomouh.net</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
            <div class="sec-part">
                <p>Tomouh Limited 2017. All Rights Reserved | Terms and Conditions </p>
            </div>
        
    </footer>
</div>
<!-- jQuery --> 
<script src="<?php echo base_url(); ?>assets/client/js/jquery.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="<?php echo base_url(); ?>assets/client/js/bootstrap.min.js"></script> 
<!--slider js--> 

<script src="<?php echo base_url(); ?>assets/client/js/jquery-mobile.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/client/js/webapplinks.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/client/js/bootstrap-select.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/client/js/custom.js"></script>
</body>
</html>
