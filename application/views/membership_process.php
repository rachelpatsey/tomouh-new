<?php include("inc/header_top.php"); ?>
<?php include("inc/header.php"); ?>
<style type="text/css">
  .membership_process_page .membership_process .process-parts .left-img, .membership_process_page .membership_process .process-parts .right-img{
    top: -17px;
  }

</style>

<div class="inner_wrapper">
  <div class="membership_process_page">
    <div class="sec_banner">
      <div class="container">
        <div class="page_head">
          <h1 class="red-text text-center font36"><?php if(isset($title)){ echo $title; } ?></h1>
        </div>
        <div class="membership_content">
          <?php if(isset($main_description)){ echo $main_description; } ?>
          <div class="page_head">
            <h1 class="red-text text-center inner_head"><?php if(isset($meta['sub_heading'])){ echo $meta['sub_heading']; } ?></h1>
          </div>
          <div class="membership_process">
            <div class="row">
              <div class="area_process">
                <div class="process-parts col-sm-8 no-padding">
                  <div class="col-sm-6 process1"> <img src="<?php echo base_url(); ?>assets/images/<?php if(isset($meta['process_image_1'])){ echo $meta['process_image_1']; } ?>" class="img-responsive process_img" alt="">
                    <p class="process_text"><?php if(isset($meta['process_image_1_text'])){ echo $meta['process_image_1_text']; } ?></p>
                  </div>
                  <div class="col-sm-6 process2"> <img src="<?php echo base_url(); ?>assets/images/<?php if(isset($meta['process_image_2'])){ echo $meta['process_image_2']; } ?>" class="img-responsive process_img" alt="">
                    <p class="process_text"><?php if(isset($meta['process_image_2_text'])){ echo $meta['process_image_2_text']; } ?></p>
                  </div>
                  <div class="col-xs-12 col-sm-12">
                    <div class="process_note">
                      <p class="text-center"><?php if(isset($meta['process_note'])){ echo $meta['process_note']; }?></p>
                      <img src="<?php echo base_url(); ?>assets/images/<?php if(isset($meta['process_arrow_image'])){ echo $meta['process_arrow_image']; } ?>" class="left-img img-responsive" alt=""> <img src="<?php echo base_url(); ?>assets/images/<?php if(isset($meta['process_arrow_image'])){ echo $meta['process_arrow_image']; } ?>" class="right-img img-responsive" alt=""> 
                    </div>
                  </div>
              </div>
                <div class="col-sm-4 process3"> <img src="<?php echo base_url(); ?>assets/images/<?php if(isset($meta['process_image_3'])){ echo $meta['process_image_3']; } ?>" class="img-responsive process_img" alt="">
                  <p class="process_text"><?php if(isset($meta['process_image_3_text'])){ echo $meta['process_image_3_text']; } ?></p>
                </div>
            </div>
          </div>
          <div class="applicant_note">
            <?php if(isset($meta['applicant_note'])){ echo $meta['applicant_note']; } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('inc/footer.php'); ?>