<?php 
include("inc/header_top.php");
include("inc/header.php");  
?>
<div class="after-login">
<div class="inner_wrapper">
  <div class="inner_header_div">
    <div class="container">
      <div class="row">
        <div class="col-sm-7 col-md-8 col-xs-12">
          <div class="left-div"> <a href="<?php echo base_url(); ?>my_home" class="">My Home</a> <a href="<?php echo base_url(); ?>members">Members</a> <a href="<?php echo base_url(); ?>event">Events</a> <a href="<?php echo base_url(); ?>initiative"  class="active">Initiatives</a> </div>
        </div>
        <div class="col-sm-5 col-md-4  col-xs-12">
          <div class="right-div"> <a href="<?php echo base_url(); ?>account" >Account</a> <a href="<?php echo base_url(); ?>profile">Profile</a> </div>
        </div>
      </div>
    </div>
  </div>
  <div class="initiative_page">
    <div class="container">
      <div class="row">
              <?php 
                for($i=0;$i<sizeof($initiative_data);$i++){

                  // echo "<pre>";
                  // print_r($initiative_data);exit();
               ?>
          <div class="col-sm-4">
              <div class="initiative_block">
                  <?php if( isset($initiative_data[$i]['v_icon']) && !empty($initiative_data[$i]['v_icon'])){ ?>

                      <i class="fa fa-5x <?php echo isset($initiative_data[$i]['v_icon']) && $initiative_data[$i]['v_icon'] ? $initiative_data[$i]['v_icon'] : ''; ?> text-danger" style="margin-top: 6px;"></i>

                  <?php } else { ?>

                  <?php if(!empty($initiative_data[$i]['v_image'])){ ?>

                  <img src="<?php echo base_url(); ?>assets/images/<?php if(isset($initiative_data[$i]['v_image'])) echo $initiative_data[$i]['v_image']; ?>" class="img-responsive" alt="">
                  <?php } ?>

                  <?php } ?>
                  <!-- <?php if(!empty($initiative_data[$i]['v_image'])){ ?>
                  <img src="<?php echo base_url(); ?>assets/images/<?php if(isset($initiative_data[$i]['v_image'])) echo $initiative_data[$i]['v_image']; ?>" class="img-responsive" alt="">
                  <?php }else{ ?>
                    <i class="fa fa-4x <?php echo isset($initiative_data[$i]['v_icon']) && $initiative_data[$i]['v_icon'] ? $initiative_data[$i]['v_icon'] : ''; ?> text-danger" ></i>
                  <?php } ?>  -->

                  <p class="initiative_name"><?php if(isset($initiative_data[$i]['v_name'])) echo $initiative_data[$i]['v_name']; ?></p>
                  <div class="border-bottom"></div>
                  <p class="initiative_desc"><?php if(isset($initiative_data[$i]['l_text'])) echo $initiative_data[$i]['l_text']; ?></p>
                  <a href="mailto:<?php if(isset($initiative_data[$i]['v_email'])) echo $initiative_data[$i]['v_email']; ?>" class="email-btn"><?php if(isset($initiative_data[$i]['v_email'])) echo $initiative_data[$i]['v_email']; ?></a>
              </div>
          </div>
          <?php 
          }
        ?>
      </div>
    </div>
  </div>
</div>
</div>
<?php include('inc/footer.php'); ?>