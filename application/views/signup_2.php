<?php 
include("inc/header_top.php");
include("inc/header.php"); 
?>
<?php 
    $current_year = date("Y");
?>
<script src='https://cdnjs.cloudflare.com/ajax/libs/parsley.js/1.2.2/parsley.min.js'></script>
<style type="text/css">
  .add_detele{
    cursor: pointer;
    position: absolute;
    right: 15px;
    top: 1px;
    font-size: 20px;
    color: red;
  }
  .error_box{display: inline-block;width: 100%;padding-top: 20px;padding-left: 10%;padding-right: 10%;}
  .error_box .icon{float: left;padding-right: 10px;}
  input.parsley-error {
    border: 1px solid red;
  }
</style>
<body class="offwhite-bg">
<div class="inner_wrapper">
<div class="container">
  <div class="signup_page">
    <div class="form">
      <h2 class="signup-head red-text spectral-font font36 text-center"><strong>Register</strong></h2>
      <div class="signup_headline">
        <ul class="nav nav-tabs nav-justified">
          <li><a><img src="<?php echo base_url(); ?>assets/client/images/active-radio.png" class="img-responsive" alt="">PERSONAL</a></li>
          <li class="active"><a><img src="<?php echo base_url(); ?>assets/client/images/active-radio.png" class="img-responsive" alt="">EDUCATION & Professional</a></li>
        </ul>
      </div>
      <form method="post" name="signup_form_2" enctype="multipart/form-data" parsley-validate>
        <div class="signup_form">
          <div class="gray-border-box">
            <div class="row">
              <div class="col-xs-12">
                <p class="form-head">Educational Information</p>
              </div>
            </div>
          <!-- <div class="row" id ="educational_details_more"></div> -->
        <?php 
          if(isset($user_step2['university'][0]) && !empty($user_step2['university'][0])){
          $total_edu = sizeof($user_step2['university']); ?>
          <input type="hidden" id="educational_details_total" value="<?php echo($total_edu - 1); ?>">
          <input type="hidden" id="educational_total" value="<?php echo ($total_edu-1); ?>">
          <?php for($i=0;$i<$total_edu;$i++){
          if(isset($user_step2['university'][$i]) && !empty($user_step2['university'][$i])){
        ?>  <div class="control-group after-add-more educational_block" id="educational_details<?php if($i != 0){ echo $i; }?>">
              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>University</label>
                    <input type="text" name="university[]" onBlur="changeMainOccupation('educational_details<?php if($i != 0){ echo $i; }?>')" id="university" value="<?php echo $user_step2['university'][$i]; ?>" class="form-control" <?php if($i == 0){?> required parsley-required="true" <?php } ?> >
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Degree</label>
                    <input type="text" name="degree[]" id="degree" onBlur="changeMainOccupation('educational_details<?php if($i != 0){ echo $i; }?>')" value="<?php echo $user_step2['degree'][$i]; ?>" class="form-control" <?php if($i == 0){?> required parsley-required="true" <?php } ?> >
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Major</label>
                    <input type="text" name="major[]" id="major" onBlur="changeMainOccupation('educational_details<?php if($i != 0){ echo $i; }?>')" value="<?php echo $user_step2['major'][$i]; ?>" class="form-control" <?php if($i == 0){?> required parsley-required="true" <?php } ?>>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group" id="grad_year">
                    <label>Graduation Year</label>
                    <div class="select">
                      <select class="selectpicker" name="graduation_year[]" id="graduation_year">
                        <option value="present">Present</option>
                        <?php for($k=$current_year;$k>=1950;$k--){ ?>
                        <option value="<?php echo $k; ?>" <?php if($user_step2['graduation_year'][$i] == $k){ ?> selected <?php } ?>><?php echo $k; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <?php if($i != 0){ ?>
                  <div class="form-group remove_button_edu">
                    <i class="fa fa-times add_detele" onClick="removeEducationalDetails('educational_details<?php if($i != 0){ echo $i; }?>')" aria-hidden="true"></i>
                  </div>
                <?php } ?>
                </div>
              </div>
            </div>
            <?php
                } 
              } 
            }else{ ?>
            <input type="hidden" id="educational_details_total" value="0">
            <input type="hidden" id="educational_total" value="0">
            <div class="control-group after-add-more educational_block" id="educational_details">
              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>University</label>
                    <input type="text" name="university[]" parsley-required="true" required onBlur="changeMainOccupation('educational_details')" id="university" class="form-control">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Degree</label>
                    <input type="text" name="degree[]" parsley-required="true" required onBlur="changeMainOccupation('educational_details')" id="degree" class="form-control">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Major</label>
                    <input type="text" name="major[]" id="major" parsley-required="true" required onBlur="changeMainOccupation('educational_details')" class="form-control">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group" id="grad_year">
                    <label>Graduation Year</label>
                    <div class="select">
                      <select class="selectpicker" name="graduation_year[]" id="graduation_year">
                        <option value="present">Present</option>
                        <?php for($k=$current_year;$k>=1950;$k--){ ?>
                        <option value="<?php echo $k; ?>"><?php echo $k; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group remove_button_edu" style="display: none;">
                    <i class="fa fa-times add_detele" onClick="removeEducationalDetails('educational_details')" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
            <div class="row" id ="educational_details_more"></div>
            <div class="row" id="edu_add_more">
              <div class="col-sm-3 pull-right">
                <button class="btn dark-gray-btn add-more" type="button" onClick="addAnotherEducationalDetails()">Add More</button>
              </div>
            </div>
          </div>
          <div class="gray-border-box">
            <div class="row">
              <div class="col-xs-12">
                <p class="form-head">Professional Experience</p>
              </div>
            </div>
        <?php 
          if(isset($user_step2['company'][0]) && !empty($user_step2['company'][0])){
          $total_pro = sizeof($user_step2['company']); ?>
          <input type="hidden" id="occupation_details_total" value="<?php echo ($total_pro-1); ?>">
          <input type="hidden" id="occupation_total" value="<?php echo ($total_pro-1); ?>">
        <?php for($i=0;$i<$total_pro;$i++){
          if(isset($user_step2['company'][$i]) && !empty($user_step2['company'][$i])){
        ?>
            <div class="control-group after-add-more occupation_block" id="occupation_details<?php if($i != 0){ echo $i; }?>">
              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Company</label>
                    <input type="text" name="company[]" onBlur="changeMainOccupation('occupation_details<?php if($i != 0){ echo $i; }?>')" id="company" value="<?php echo $user_step2['company'][$i]; ?>" class="form-control" <?php if($i == 0){?> required parsley-required="true" <?php } ?>>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Job Title</label>
                    <input type="text" name="job_title[]" id="job_title" onBlur="changeMainOccupation('occupation_details<?php if($i != 0){ echo $i; }?>')" value="<?php echo $user_step2['job_title'][$i]; ?>" class="form-control" <?php if($i == 0){?> required parsley-required="true" <?php } ?>>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group" id="industry_to">
                    <label>Industry</label>
                    <div class="select">
                        <select class="selectpicker" name="industry[]" id="industry">
                          <?php foreach($industry_data as $industry){ ?>
                          <option value="<?php echo $industry['v_name']; ?>" <?php if($user_step2['industry'][$i] == $industry['v_name']){ ?> selected <?php } ?>><?php echo $industry['v_name']; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="row">
                    <div class="col-xs-6">
                      <div class="form-group" id="cpny_from">
                        <label>From</label>
                        <div class="select">
                          <select class="selectpicker" name="company_from[]" id="company_from">
                            <?php for($k=$current_year;$k>=1950;$k--){ ?>
                            <option value="<?php echo $k; ?>" <?php if($user_step2['company_from'][$i] == $k){ ?> selected <?php } ?>><?php echo $k; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-6">
                      <div class="form-group" id="cpny_to">
                        <label>To</label>
                        <div class="select">
                          <select class="selectpicker" name="company_to[]" id="company_to">
                            <option value="present">Present</option>
                            <?php for($k=$current_year;$k>=1950;$k--){ ?>
                            <option value="<?php echo $k; ?>" <?php if($user_step2['company_to'][$i] == $k){ ?> selected <?php } ?>><?php echo $k; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <?php if($i != 0){ ?>
                    <div class="form-group remove_button_pro">
                        <i class="fa fa-times add_detele" onClick="removeProfessionalDetails('occupation_details<?php if($i != 0){ echo $i; }?>')" aria-hidden="true"></i>
                    </div>
                    <?php } ?>    
                  </div>
                </div>
              </div>
            </div>
            <?php
                } 
              } 
            }else{ ?>
            <input type="hidden" id="occupation_details_total" value="0">
            <input type="hidden" id="occupation_total" value="0">
            <div class="control-group after-add-more occupation_block" id="occupation_details">
              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Company</label>
                    <input type="text" name="company[]" parsley-required="true" required onBlur="changeMainOccupation('occupation_details')" id="company" class="form-control">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label>Job Title</label>
                    <input type="text" name="job_title[]" parsley-required="true" required onBlur="changeMainOccupation('occupation_details')" id="job_title" class="form-control">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group" id="industry_to">
                    <label>Industry</label>
                     <div class="select">
                        <select class="selectpicker" name="industry[]" id="industry">
                          <?php foreach($industry_data as $industry){ ?>
                          <option value="<?php echo $industry['v_name']; ?>"><?php echo $industry['v_name']; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="row">
                    <div class="col-xs-6">
                      <div class="form-group" id="cpny_from">
                        <label>From</label>
                        <div class="select">
                          <select class="selectpicker" name="company_from[]" id="company_from">
                            <?php for($k=$current_year;$k>=1950;$k--){ ?>
                            <option value="<?php echo $k; ?>"><?php echo $k; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-6">
                      <div class="form-group" id="cpny_to">
                        <label>To</label>
                        <div class="select">
                          <select class="selectpicker" name="company_to[]" id="company_to">
                            <option value="present">Present</option>
                            <?php for($k=$current_year;$k>=1950;$k--){ ?>
                            <option value="<?php echo $k; ?>"><?php echo $k; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group remove_button_pro" style="display: none;">
                        <i class="fa fa-times add_detele" onClick="removeProfessionalDetails('occupation_details')" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
            <div class="row" id ="occupation_details_more"></div>
            <div class="row" id="occu_add_more">
              <div class="col-sm-3 pull-right">
                <button class="btn dark-gray-btn add-more" type="button" onClick="addAnotherOccupationDetails()">Add More</button>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6 col-xs-12">
              <div class="gray-border-box">
                <div class="row">
                  <div class="col-xs-12">
                    <p class="form-head">Achievements, Awards, & Honors</p>
                  </div>
                </div>
        <?php
          if( isset($user_step2['award_details']) ){ 

          if( sizeof($user_step2['award_details']) >= 2 ){
            $total_award = sizeof($user_step2['award_details']);   
          }else{

            $total_award = 2;
          }

          for($i=0;$i<$total_award;$i++){
          // if(isset($user_step2['award_details'][$i]) && !empty($user_step2['award_details'][$i])){
        ?>
                  <div class="row" id="award_details">
                    <input type="hidden" id="award_details_total" value="<?php echo ($total_award-1); ?>">
                    <div class="col-xs-12" id="award<?php echo $i; ?>">
                      <div class="form-group">
                        <input type="text" name="award_details[]" value="<?php echo isset($user_step2['award_details'][$i]) ? $user_step2['award_details'][$i] : ''; ?>" class="form-control" <?php if($i == 0) echo "required";?>>
                      </div>
                      <?php if($i != 0){?>
                      <div class="form-group remove_button_award">
                        <i class="fa fa-times add_detele" style="right: 25px !important;top: 9px !important;" onClick="removeAwardDetails('award<?php echo $i; ?>')" aria-hidden="true"></i>
                      </div>
                      <?php }?>
                    </div>
                  </div>
        <?php 
              // }
            }
          }else{  
        ?>        
                  <div class="row" id="award_details">
                    <input type="hidden" id="award_details_total" value="2">
                    <div class="col-xs-12" id="award1">
                      <div class="form-group">
                        <input type="text" name="award_details[]" class="form-control" required>
                      </div>
                      
                    </div>
                    <div class="col-xs-12" id="award2">
                      <div class="form-group">
                        <input type="text" name="award_details[]" class="form-control">
                      </div>
                      <div class="form-group remove_button_award">
                        <i class="fa fa-times add_detele" style="right: 25px !important;top: 9px !important;" onClick="removeAwardDetails('award2')" aria-hidden="true"></i>
                      </div>
                    </div>
                  </div>
          <?php } ?>
                  <div class="row" id ="award_details_more"></div>
                  <div class="row">
                    <div class="col-xs-12 pull-right">
                      <button class="btn dark-gray-btn add-more" type="button" onClick="addAwards()">Add More</button>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
              <div class="gray-border-box">
                <div class="row">
                  <div class="col-xs-12">
                    <p class="form-head">Passions and interests</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <div class="form-group">
                      <textarea class="form-control" name="passions_interests" id="passions_interests"><?php if(isset($user_step2['passions_interests'])){ echo $user_step2['passions_interests']; } ?></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6 col-xs-12">
              <div class="gray-border-box">
                <div class="row">
                  <div class="col-xs-12">
                    <p class="form-head">Main Occupation</p>
                    <p class="font14 text-justify" style="margin-bottom: 10px;">Your main occupation will be based on your current educational or professional information. This field will be considered as your main record and will be highlighted in search result. However,you can change it later from your Profile Page once you have registered successfully.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <div class="select">
                      <select class="selectpicker" name="main_occu" id="main_occu" required>
                        <option value="">Select Main Occupation</option>
                        <?php  if(isset($user_step2['university'][0]) && !empty($user_step2['university'][0])){
                          $total_edu = sizeof($user_step2['university']);
                          $i = 0; 
                          for($i=0;$i<$total_edu;$i++){
                          if(isset($user_step2['university'][$i]) && !empty($user_step2['university'][$i])){
                        ?>
                          <option value="<?php echo $user_step2['university'][$i].' - '.$user_step2['degree'][$i].' - '.$user_step2['major'][$i]; ?>" class= "educational_details<?php if($i != 0){ echo $i; }?>" <?php if(isset($user_step2['main_occu']) && $user_step2['main_occu'] == $user_step2['university'][$i].' - '.$user_step2['degree'][$i].' - '.$user_step2['major'][$i]){ ?> selected <?php } ?>><?php echo $user_step2['university'][$i].' - '.$user_step2['degree'][$i].' - '.$user_step2['major'][$i]; ?></option>
                        <?php
                            } 
                          }
                        }
                        ?>

                        <?php  if(isset($user_step2['company'][0]) && !empty($user_step2['company'][0])){
                          $total_occu = sizeof($user_step2['company']);
                          $i = 0; 
                          for($i=0;$i<$total_occu;$i++){
                          if(isset($user_step2['company'][$i]) && !empty($user_step2['company'][$i])){
                        ?>
                          <option value="<?php echo $user_step2['company'][$i].' - '.$user_step2['job_title'][$i]; ?>" class= "occupation_details<?php if($i != 0){ echo $i; }?>" <?php if(isset($user_step2['main_occu']) && $user_step2['main_occu'] == $user_step2['company'][$i].' - '.$user_step2['job_title'][$i] ){?> selected <?php } ?> ><?php echo $user_step2['company'][$i].' - '.$user_step2['job_title'][$i]; ?></option>
                        <?php
                            } 
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-sm-6 col-xs-12">
              <div class="gray-border-box">
                <div class="row">
                  <div class="col-xs-12">
                    <p class="form-head">Terms & Conditions</p>
                    <p class="font14 text-justify" style="margin-bottom: 10px;">
                    	<input type="checkbox" name="check_agree" id="check_agree" value="1">
                        <label for="check_agree">
                        I accept the <a href="<?php echo base_url(); ?>terms-and-conditions/" target="_blank">terms & conditions</a>
                    	</label>
                    </p>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
          <div class="form-group">
                <input type="submit" name="signup_2" id="btn_signup_2" value="Submit" class="btn gray-btn" disabled="disabled" />        
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
  	$(document).ready(function(e) {
        $("#check_agree").change(function(){
			if($(this).is(':checked')){
				$("#btn_signup_2").removeAttr('disabled');
			}else{
				$("#btn_signup_2").attr('disabled', 'disabled');
			}
		});
    });
      function addAnotherEducationalDetails(){

          var clnstr = '<div class="row">'+
                      '<div class="col-sm-3">'+
                        '<div class="form-group">'+
                          '<label>University</label>'+
                          '<input type="text" name="university[]" onBlur="changeMainOccupation(\'educational_details\')" id="university" class="form-control">'+
                        '</div>'+
                      '</div>'+
                      '<div class="col-sm-3">'+
                        '<div class="form-group">'+
                          '<label>Degree</label>'+
                          '<input type="text" name="degree[]" onBlur="changeMainOccupation(\'educational_details\')" id="degree" class="form-control">'+
                        '</div>'+
                      '</div>'+
                      '<div class="col-sm-3">'+
                        '<div class="form-group">'+
                          '<label>Major</label>'+
                          '<input type="text" name="major[]" id="major" onBlur="changeMainOccupation(\'educational_details\')" class="form-control">'+
                        '</div>'+
                      '</div>'+
                      '<div class="col-sm-3">'+
                        '<div class="form-group" id="grad_year">'+
                          '<label>Graduation Year</label>'+
                          '<div class="select">'+
                            '<select class="selectpicker" name="graduation_year[]" id="graduation_year">'+
                              '<option value="present">Present</option>'+
                              '<?php for($k=$current_year;$k>=1950;$k--){ ?>'+
                              '<option value="<?php echo $k; ?>"><?php echo $k; ?></option>'+
                              '<?php } ?>'+
                            '</select>'+
                          '</div>'+
                        '</div>'+
                        '<div class="form-group remove_button_edu">'+
                          '<i class="fa fa-times add_detele" style="cursor: pointer;"  onclick="removeEducationalDetails(\'educational_details\')" aria-hidden="true"></i>'+
                        '</div>'+
                      '</div>'+
                    '</div>';

          var parentval = $("#educational_details_total").val();
          var edu_parent_total = $("#educational_total").val(); 

          if(parseInt($("#educational_total").val()) >= 5){
            $('#edu_add_more').css('display','none');
            return false;
          } 

          var parentvalue = parseInt(parentval)+1;
          $("#educational_details_total").val(parentvalue);
          $("#educational_total").val(parseInt(edu_parent_total)+1);

          var main_occup = "main_occupation"+(parentvalue+1);
          var main_occ = clnstr.replace(/main_occupation1/g,main_occup);

          clnstr.replace(/main_occupation1/g,main_occup);

          

          var parentid = "educational_details"+parentvalue;

          var ab = '<div class="control-group after-add-more educational_block" id="educational_details" style="padding-left: 15px;margin-left: 15px;margin-right: 15px;">'+main_occ+'</div>';   
          ab = ab.replace(/educational_details/g, parentid);

          $('#educational_details_more').append(ab);

          $('.selectpicker').selectpicker({});
       
      }

      function addAnotherOccupationDetails(){

          var clnstr = '<div class="row">'+
                '<div class="col-sm-3">'+
                  '<div class="form-group">'+
                    '<label>Company</label>'+
                    '<input type="text" name="company[]" onBlur="changeMainOccupation(\'occupation_details\')" id="company" class="form-control">'+
                  '</div>'+
                '</div>'+
                '<div class="col-sm-3">'+
                  '<div class="form-group">'+
                    '<label>Job Title</label>'+
                    '<input type="text" name="job_title[]" onBlur="changeMainOccupation(\'occupation_details\')" id="job_title" class="form-control">'+
                  '</div>'+
                '</div>'+
                '<div class="col-sm-3">'+
                  '<div class="form-group" id="industry_to">'+
                    '<label>Industry</label>'+
                     '<div class="select">'+
                        '<select class="selectpicker" name="industry[]" id="industry">'+
                          '<?php foreach($industry_data as $industry){ ?>'+
                          '<option value="<?php echo $industry['v_name']; ?>"><?php echo $industry['v_name']; ?></option>'+
                          '<?php } ?>'+
                        '</select>'+
                      '</div>'+
                  '</div>'+
                '</div>'+
                '<div class="col-sm-3">'+
                  '<div class="row">'+
                    '<div class="col-xs-6">'+
                      '<div class="form-group" id="cpny_from">'+
                        '<label>From</label>'+
                        '<div class="select">'+
                          '<select class="selectpicker" name="company_from[]" id="company_from">'+
                            '<?php for($k=$current_year;$k>=1950;$k--){ ?>'+
                            '<option value="<?php echo $k; ?>"><?php echo $k; ?></option>'+
                            '<?php } ?>'+
                          '</select>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '<div class="col-xs-6">'+
                      '<div class="form-group" id="cpny_to">'+
                        '<label>To</label>'+
                        '<div class="select">'+
                          '<select class="selectpicker" name="company_to[]" id="company_to">'+
                            '<option value="present">Present</option>'+
                            '<?php for($k=$current_year;$k>=1950;$k--){ ?>'+
                            '<option value="<?php echo $k; ?>"><?php echo $k; ?></option>'+
                            '<?php } ?>'+
                          '</select>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '<div class="form-group remove_button_pro">'+
                        '<i class="fa fa-times add_detele" onClick="removeProfessionalDetails(\'occupation_details\')" aria-hidden="true"></i>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>';

          var parentval = $("#occupation_details_total").val();  
          var occ_parent_val = $('#occupation_total').val();

           if(parseInt($("#occupation_total").val()) >= 5){
            $('#occu_add_more').css('display','none');
            return false;
          }  
          
          var parentvalue = parseInt(parentval)+1;
          $("#occupation_details_total").val(parentvalue);
          $("#occupation_total").val(parseInt(occ_parent_val)+1);
          var main_occup = "main_occupation"+(parentvalue+parseInt(11));
          var main_occ = clnstr.replace(/main_occupation11/g,main_occup)   
          var parentid = "occupation_details"+parentvalue;
          var ab = '<div class="control-group after-add-more occupation_block" id="occupation_details" style="padding-left: 15px;margin-left: 15px;margin-right: 15px;">'+main_occ+'</div>';   
          ab = ab.replace(/occupation_details/g, parentid);

          $('#occupation_details_more').append(ab);

          $('.selectpicker').selectpicker({});
      }

      function addAwards(){

                    var parentval = $('#award_details_total').val();
                    var parentvalue = parseInt(parentval)+1;

                    var str = "<div class='col-xs-12' id='award"+parentvalue+"''>"+
                             "<div class='form-group'>"+
                             "<input type='text' name='award_details[]' id='award_details"+parentvalue+"' class='form-control' >"+
                             "</div>"+
                             "</div>";  

                    var remove_str =  '<div class="form-group remove_button_award">'+
                        '<i class="fa fa-times add_detele" style="right: 25px !important;top: 9px !important;" onclick="removeAwardDetails(\'award'+parentvalue+'\')" aria-hidden="true"></i>'+
                      '</div>';       

                    $('#award_details_total').val(parentvalue);
         
                    $('#award_details_more').append(str);

                    $('#award'+parentvalue).append(remove_str);
      }

      function removeEducationalDetails(data){
        var id = '#'+data;
        var edu_total_details = $('#educational_total').val();
        if(edu_total_details < 6){
          $('#edu_add_more').css('display','block');
        }
        var details_value = parseInt(edu_total_details)-1;
        var uni_val = $(id).closest(id).find("#university").val();
        $("#main_occu option[class='"+data+"']").remove();
        $('#main_occu').selectpicker('refresh');
        $('#educational_total').val(details_value);
        $('#'+data).remove();
      }
      function removeProfessionalDetails(data){
        var id = '#'+data;
        var occu_total_details = $('#occupation_total').val();
        if(occu_total_details < 6){
          $('#occu_add_more').css('display','block');
        }
        var occu_details_value = parseInt(occu_total_details)-1;
        var pro_val = $(id).closest(id).find("#company").val();
        $("#main_occu option[class='"+data+"']").remove();
        $('#main_occu').selectpicker('refresh');
        $('#occupation_total').val(occu_details_value);
        $('#'+data).remove();
      }
      function removeAwardDetails(data){
        $('#'+data).remove();
      }

      function changeMainOccupation(inObj){
        var id = '#'+inObj;
        var uni_val = $(id).closest(id).find("#university").val();
        var major_val = $(id).closest(id).find("#major").val();
        var degree_val = $(id).closest(id).find("#degree").val();
        var comp_val = $(id).closest(id).find("#company").val();
        var job_title_val = $(id).closest(id).find("#job_title").val();
        
        if( typeof(uni_val) !== 'undefined' ){
          $("#main_occu option[class='"+inObj+"']").remove();
          var html_str= $('#main_occu').html();
          if ( major_val != '' && degree_val != '' ) {  
            html_str +='<option value="'+uni_val+' - '+degree_val+' - '+major_val+'" class="'+inObj+'">'+uni_val+' - '+degree_val+' - '+major_val+'</option>';
          } else if( major_val == '' && degree_val != '' ) {
            html_str +='<option value="'+uni_val+' - '+degree_val+'" class="'+inObj+'">'+uni_val+' - '+degree_val+'</option>';
          } else if( degree_val == '' && major_val != '' ) {
            html_str +='<option value="'+uni_val+' - '+major_val+'" class="'+inObj+'">'+uni_val+' - '+major_val+'</option>';
          } else {
            html_str +='<option value="'+uni_val+'" class="'+inObj+'">'+uni_val+'</option>';
          }
          $('#main_occu').html(html_str);
          $('#main_occu').selectpicker('refresh');
        }

        if( typeof(comp_val) !== 'undefined' ){
          $("#main_occu option[class='"+inObj+"']").remove();
          var html_str= $('#main_occu').html();
          if( job_title_val != '' ){  
            html_str +='<option value="'+comp_val+' - '+job_title_val+'" class="'+inObj+'">'+comp_val+' - '+job_title_val+'</option>';
          }else{
            html_str +='<option value="'+comp_val+'" class="'+inObj+'">'+comp_val+'</option>';
          }
          $('#main_occu').html(html_str);
          $('#main_occu').selectpicker('refresh');
        }
      }

</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/client/js/bootstrap-select.min.js"></script>
<?php 
include("inc/footer.php"); 
?>