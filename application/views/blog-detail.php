<?php include("inc/header_top.php"); ?>
<?php include("inc/header.php"); ?>
<div class="inner_wrapper">
  <div class="blog_page">
    <div class="sec_banner">
      <div class="container">
        <div class="page_head">
          <h1 class="red-text text-center font36"><?php if(isset($v_title)){ echo $v_title; }?></h1>
        </div>
        <div class="row">
        <div class="col-md-12">
            <div class="blog">
              <div class="blog_img" style="background-image: url('<?php echo base_url(); ?>assets/images/<?php if(isset($v_image)){ echo $v_image; }?>');"></div>
              <p class="blog_name spectral-font"><?php if(isset($v_title)){ echo $v_title; }?></p>
              <p class="blog_date gray-text font14"><?php if(isset($d_added)){ 
                        $date = $d_added;
                        echo date("jS F, Y", strtotime($date));
                      }     ?>                     
              </p>
              <div class="blog_desc"><?php if(isset($l_description)){ echo $l_description; }?></div>
            </div>
          	</div>
      	   </div>
  	      </div>
	    </div>
	  </div>
 </div>
 <?php include("inc/footer.php"); ?>