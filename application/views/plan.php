<?php 
include("inc/header_top.php");
include("inc/header.php"); 
?>
<style type="text/css">
  .error_box{display: inline-block;width: 100%;padding-top: 20px;padding-left: 10%;padding-right: 10%;}
  .error_box .icon{float: left;padding-right: 10px;}
  input.parsley-error {
    border: 1px solid red;
  }
  .type{
    clear: left;
  }

  .parsley-error-list {
      color: red;
  }
  .border-box {
  border: 1px solid #ddd;
  max-width: 100%;
  padding: 80px 40px;
  border-radius: 4px;
  background: #fff;
  margin: 0 auto;
  float: none;
  font-size: 24px;
  text-align: center;
}
.border-box .labelfree{margin-right: 15px;}
 .border-box label{font-weight: bold;}
</style>
<script src='https://cdnjs.cloudflare.com/ajax/libs/parsley.js/1.2.2/parsley.min.js'></script>
<div class="account_page" style="margin-top: 100px;" id="payment_option">
  <section class="event_cal">
      <div class="sec_banner">
        <div class="container">
          <div class="page_head">
            <h1 class="red-text text-center font36">Plan Option</h1>
          </div>
          <div class="clearfix"></div>
          <div class="border-box">
            <form action="" method="post">  
              <div class="row">
                <div class="col-xs-12">
                  <input type="radio" name="plan_option" value="free" class="form-group" required="required">
                  <label class="labelfree">Free Plan</label>
                
                  <input type="radio" name="plan_option" value="paid" class="form-group" required="required">
                  <label>Paid Plan</label>
                 <div class="clearfix"></div>
                  <input type="submit" name="selected_plan" value="Submit" class="form-group btn red-btn" style="margin-top: 35px;">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
</div>
<?php
include("inc/footer.php");  
?>