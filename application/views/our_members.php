<?php
    $query_params =array();
    $url = $meta['home_page_video'];
    $query_str = parse_url($url, PHP_URL_QUERY);
    parse_str($query_str, $query_params);  

    $latitude = json_encode($lan_lat); 

include("inc/header_top.php");
include("inc/header.php"); ?>

<style type="text/css">
   .head h1 {text-transform: none !important;font-family:'Montserrat'!important;}
</style>

</head>

<body>
<div class="inner_wrapper">
 
  <section class="member_section">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="member_updates">
            <div class="head">
              <h1 class="red-text font36">Member Updates</h1>
            </div>
            <div class="content-part">
              <?php
                    for($i=0;$i<5;$i++){
              ?>
              <div class="member_row">
                <p class="title"><?php echo $members_update[$i]['v_user_name']; ?></p>
                <p class="desc"><?php echo $members_update[$i]['l_description']; ?></p>
                <p class="date"><?php  $date = $members_update[$i]['d_added'];
                        echo date("jS F Y", strtotime($date)); ?></p> 
              </div>
              <?php 
                }
              ?>
              <div class="btn_cls">
                <a href="<?php echo base_url().'member_updates'; ?>" class="btn gray-btn">View all</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="member_month">
            <div class="head">
              <h1 class="red-text font36">Member of the Month</h1>
            </div>
            <div class="content-part">
              <?php
                  $currentMonth = date('F');
                  $previous_month = Date('F', strtotime($currentMonth . " last month")); 
                  $current_year = date('Y');
                  if($previous_month == 'December'){
                      $current_year = $current_year - 1;
                  }
				  $MEMBER_IMAGE = $this->tomouh_model->getSetting('MEMBER_IMAGE');
				  $MEMBER_MAIN_OCCUPATION = $this->tomouh_model->getSetting('MEMBER_MAIN_OCCUPATION')
               ?>
              <p class="member_date"><?php echo $member_of_month['v_month_text']; ?></p>
              <div class="member_img"> 
              <?php if($MEMBER_IMAGE != ''){?>
              <img src="<?php echo base_url().'assets/frontend/images/'.$MEMBER_IMAGE;?>" class="img-responsive img-circle" alt="">
              <?php }else{?>
              <img src="<?php echo base_url(); ?>assets/images/no-photo-image.jpg" class="img-responsive img-circle" alt="">
              <?php }?> 
              </div>
              <h1 class="member_name"><?php if(isset($member_of_month['v_firstname'])) echo $member_of_month['v_firstname']; ?> <?php if(isset($member_of_month['v_lastname'])) echo $member_of_month['v_lastname']; ?></h1>
              <p class="member_post">
			  <?php if(isset($MEMBER_MAIN_OCCUPATION))
			  	 echo $MEMBER_MAIN_OCCUPATION.'<br>'; ?>          
			  <?php if(isset($member_of_month['v_job_title'])) 
			  	echo $member_of_month['v_job_title']; ?> 
			  <?php if(isset($member_of_month['v_company'])) 
			  	echo $member_of_month['v_company'].'<br>'; ?>
              <?php if(isset($member_of_month['v_residence_city'])) echo $member_of_month['v_residence_city']; ?><?php if(isset($member_of_month['v_residence_country']) && !empty($member_of_month['v_residence_country'])) echo ", ".$member_of_month['v_residence_country']; ?></p>
                
            </div>
            <?php 
			$MEMBER_OF_MONTH_LINK = $this->tomouh_model->getSetting('MEMBER_OF_MONTH_LINK');
			if($MEMBER_OF_MONTH_LINK != ''){
			?>
            <div class="btn_cls">
              <a href="<?php echo $MEMBER_OF_MONTH_LINK; ?>"><button type="button" class="btn gray-btn">Read more</button></a>
            </div>
            <?php }?>
          </div>
        </div>
      </div>
    </div>
  </section>
 
  <section class="bottom_slider">
    <div class="container">
      <div class="head white-text ">
        <h1>Recommended by Members</h1>
      </div>
      <div id="owl-demo" class="owl-carousel owl-theme">

      <?php
        if(isset($books)){
          $length = sizeof($books);
        for($i=0;$i<$length;$i++){
      ?>
        <div class="item">
          <div class="white-box">
              <div class="img-block center-block">
                <div class="img-center">
                  <img src="<?php echo base_url(); ?>assets/frontend/images/<?php if (isset($books[$i]['v_image'])){ echo $books[$i]['v_image']; } ?>" class="img-responsive" alt="">
                </div>
              </div>
              <div class="border"></div>
              <div class="red-text"><?php if (isset($books[$i]['v_title'])){ echo $books[$i]['v_title']; } ?></div>
              <p class="black-text"><?php if (isset($books[$i]['l_description'])){ echo $books[$i]['l_description']; } ?></p>
          </div>
        </div>
      <?php 
        }
      }
      ?>
      </div>
      <div class="owl-prev" id="prev">  </div>
      <div class="owl-next" id="next"> </div>
    </div>
    </section>
</div>


</script> 

<script type="text/javascript">

</script>
<script src="<?php echo base_url(); ?>assets/client/js/custom.js"></script>

<?php include('inc/footer.php') ?>