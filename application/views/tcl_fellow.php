<?php
include("inc/header_top.php");
include("inc/header.php");
?>
<style>
.vision.objective{
  width: 100%;
  /* max-width: 1045px; */
  margin: 0px auto;
  margin-bottom:40px;
  /* background-color: #fff; */
  /* box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.39); */
    /* text-align: center; */
    /* margin-bottom: 30px; */
    /* padding-top: 50px; */
    /* margin-top: 50px; */
}
.main_decp{
  display: flex;
  flex-direction:column;
}
.tcf_follow_head{
  margin-top: 25px;
    margin-left: 15px;
}
.blog_desc{
  margin-left: 15px;
  line-height: 20px;
}
.blog_img{
  width: 100%;
    max-width: 475px;
}
.blog_name, .blog_date{
  line-height: 27px !important;
  }
.new_vision .new_vision_left{
  text-align:center;
}
.round-img{
  border-radius:5px;
  width:100% !important;
  max-width:250px !important;
  height:250px !important;
  margin:0px auto;
  position:relative;
  margin-top:60px;
  margin-bottom:60px;
}
.round-img:before{
  content:"";
  position:absolute;
  top:-25px;
  left:-25px;
  right:-25px;
  bottom:-25px;
  border:1px solid #b6b6b6;
  border-radius:5px;
}
.float-none{
  float:none;
}
.new_vision_left{
  margin-bottom:40px;
}
.new_vision_left h1{
  margin-bottom:0px;
}
.new_vision h1{
  margin-bottom:13px;
  font-size:30px;
}
.new_vision .c_title{
  font-size:38px;
  text-align:center;
  color:black;
  font-weight:bold;
}
.new_vision .blog_img{
  background-size:cover;
}
.left-div{
    display: flex;
    justify-content: center;
}
h1{
  font-size:2em;
  font-weight:bold;
}
h2{
  font-size:1.5em;
  font-weight:bold;
}
h3{
  font-size:1.17em;
  font-weight:bold;
}
h4{
  font-weight:bold;
}
h5{
  font-size:0.83em;
  font-weight:bold;
}
h6{
  font-size:0.67em;
  font-weight:bold;
}
strong{
  font-weight: 600 !important;

}
em{
  font-style: italic;
}
/*
.new_vision{
  display:flex;
  /*background-color:white;*//*
}

.new_vision .left{
  width:30%;
  padding-top:25px;
}
.new_vision .right{
  width:80%;
}
.new_vision .tcf_follow_head{
  margin-top:0px;
}
.new_vision .round-img{
  margin-top:0px;
  margin-bottom:45px;
}
.new_vision .right_top_sec{
  margin-bottom:15px;
  margin-left:15px;
}

.tabbing_new{
  margin-bottom:20px;
  margin-top:50px;
}
*/
.custom_tab{
  display:flex;
  justify-content:center;
  border-bottom:none;
  margin-bottom:80px;
}
.custom_tab{
  display:flex;
  justify-content:stretch;
}
.custom_tab li{
  width:100%;
  text-align:center;
}
.custom_tab li a{
  border:1px solid #ddd;
  color:#343434;
   margin-right:0px;
   border-right:none;
   border-radius:0px;
}
.custom_tab li:nth-child(1) a{
  border-top-left-radius:5px;
  border-bottom-left-radius:5px;
}
.custom_tab li:last-child a{
  border-right:1px solid #ddd; 
  border-top-right-radius:5px;
  border-bottom-right-radius:5px;
}
.custom_tab li.active a{
  color:#800000 !important;
  border-bottom:1px solid #ddd !important;
}
.back-none{
  background: none;
  white-space: pre-line;
}
</style>
<div class="inner_wrapper">
  <div class="blog_page">
    <section class="about_us">
      <div class="banner_section">
        <div class="sec_banner">
          <div class="container">      
            <div class="page_head">
              <h1 class="red-text text-center font36" style="text-transform:none"><?php if (isset($title)) {echo $title;} ?></h1>
            </div>
            <p class="content"><?php if (isset($main_description)) {echo $main_description;} ?></p>
          </div>
        </div>
      </div>
    </section>
    <div class="container">
  <h2 style="opacity:0;">Dynamic Tabs</h2>


  <ul class="nav nav-tabs custom_tab">
    <?php 
    if (!empty($years)) {
      foreach ($years as $key => $val) { 
          if (isset($val['d_year'])) {
            if(isset($selectedYear) && $selectedYear == $val['d_year']){
                  $active='active';
            }else {
                  $active='';
            }?>
          <li class="<?php echo $active;?>"><a data-toggle="tab" href="#tab-<?php echo $val['d_year'];?>"><?php echo $val['d_year'];?></a></li>
          <?php }
      }
  }?>
  </ul>

  <div class="tab-content">   
      <?php      
        if (!empty($fellows)) {
          foreach ($fellows as $k => $value) { 
            if(isset($selectedYear) && $selectedYear == $k){
              $tabActive='in active';
            }else {
                  $tabActive='';
            }?>
            <div id="tab-<?php echo $k?>" class="tab-pane fade <?php echo $tabActive;?>">
              <?php
              foreach ($value as $key => $val) { ?>
                  
                <section class="vision objective ff new_vision">
                  <div class="col-sm-12">
                    <div class="row flip-mobile">
                      <div class="col-sm-12 col-xs-12 new_vision_left">
                        <div class="content tcf_follow_head">
                        
                        </div>
                      <!-- <h2 class="c_title"></h2> -->
                      <div class="blog_img round-img" style="background-image: url('<?php echo base_url(); ?>assets/images/<?php if (isset($val['v_image'])) { echo $val['v_image'];             } ?>');height: 350px;background-repeat: no-repeat;"></div>                                                                                                                 <!-- <div class="content"> -->
                         <div class="page_head float-none">
                            <h1 class="red-text font36"><?php if (isset($val['v_title'])) {
                                                          echo $val['v_title'];
                                                        } ?></h1>
                            
                          </div>
                          <p class="blog_name spectral-font">
                            <pre class="back-none">
                            <?php if (isset($val['l_bio'])) {
                              echo $val['l_bio'];
                            } ?>
                            </pre>
                          </p>
                            <p class="blog_date gray-text font14"><?php if (isset($val['d_year'])) {
                              echo $val['d_year'];
                            } ?></p>
                          <!-- </div> -->
                         </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                  <!-- <h1 class="red-text font36" style="margin-left: 15px;margin-bottom: 15px;"></h1> -->
                  <div class="blog_desc">
                    <?php if(isset($val['l_description']))
                    { 
                        echo $val['l_description']; 
                    }?>
                  </div>
                  </div>
              </section>
              <?php
              }?>
            </div>
         <?php
          }
        }?>    
      
  </div>
</div>
      
    
  </div>
</div>
<?php include('inc/footer.php') ?>