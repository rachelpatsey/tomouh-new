<?php 
include("inc/header_top.php");
include("inc/header.php"); 
?>
<style type="text/css">
  strong {font-weight: 600 !important;}
  em {font-style: italic;}
</style>
<div class="inner_wrapper">
  <div class="about_page">
    <!-- <section class="about_us">
      <div class="banner_section">
        <div class="sec_banner">
          <div class="container">
            <div class="page_head">
              <h1 class="red-text text-center font36"><?php if(isset($title)){echo $title;} ?></h1>              
              <p class="content"><?php if(isset($main_description)){echo $main_description;} ?></p>
          </div>
        </div>
      </div>
    </section> -->
    <div class="vision_objective">
      <div class="container">
        <section class="vision">
          <!-- <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-6 col-xs-12 vision_img no-padding">
                <div class="vision_bg"> </div>
              </div>
              <div class="col-sm-6 col-xs-12 vision_content">
                <div class="table_cell">
                  <div class="page_head">
                    <h1 class="red-text font36"><?php //if(isset($meta['vision_title'])){ echo $meta['vision_title']; }?></h1>
                  </div>
                  <p class="content"><?php //if(isset($meta['vision'])){echo $meta['vision']; }?></p>
                </div>
              </div>
            </div>
          </div> -->
          <div class="col-sm-12">
            <div class="row">
              <div class="page_head">
                <h1 class="red-text font36"><?php if(isset($meta['vision_title'])){ echo $meta['vision_title']; }?></h1>
              </div>
              <div class="content">   
                <p><?php if(isset($meta['vision'])){echo $meta['vision']; }?></p>
              </div><br/><br/><br/>
            </div>
          </div>          
        </section>
        <section class="vision objective">
          <!-- <div class="col-sm-12">
            <div class="row flip-mobile">
              <div class="col-sm-6 col-xs-12 vision_content">
                <div class="table_cell">
                  <div class="page_head">
                    <h1 class="red-text font36"><?php //if(isset($meta['objective_title'])){echo $meta['objective_title']; }?></h1>
                  </div>
                  <div class="content">
                    <?php //if(isset($meta['objective'])){echo $meta['objective']; }?>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-xs-12 vision_img no-padding">
                <div class="objective_bg"> </div>
              </div>
            </div>
          </div> -->
          <div class="col-sm-12">
            <div class="row">
              <div class="page_head">
                <h1 class="red-text font36"><?php if(isset($meta['objective_title'])){echo $meta['objective_title']; }?></h1>
              </div>
              <div class="content">   
                <p><?php if(isset($meta['objective'])){echo $meta['objective']; }?></p>
              </div>
            </div>
          </div>
          
        </section>
        <!-- <section class="about_address"> <img class="img-responsive" src="<?php //echo base_url(); ?>assets/client/images/address.png" alt="">
          <p class="red-text"><strong>Registered Address</strong></p>
          <p><?php //if(isset($address)){ echo $address; }?></p>
        </section> -->
      </div>
    </div>
  </div>
</div>
<?php include('inc/footer.php') ?>