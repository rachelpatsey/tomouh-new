<?php
include 'inc/header_top.php';
include 'inc/header.php';
?>
<div style="margin-top:168px;">
</div>
<style type="text/css">
	.loader {
     position: absolute;
     left: 50%;
     top: 50%;
     z-index: 1;
     width: 150px;
     height: 150px;
     margin: -75px 0 0 -75px;
     border: 16px solid #f3f3f3;
     border-radius: 50%;
     border-top: 16px solid #e04646;
     width: 120px;
     height: 120px;
     background-color: rgba(0, 0, 0, 0.5);
     -webkit-animation: spin 2s linear infinite;
     animation: spin 2s linear infinite;
   }
   #loader {
      background-color: rgba(0, 0, 0, 0.5);
     position: fixed;
      top: 0;
      right: 0;
      left: 0;
      bottom: 0;
      z-index: 99999;
   }

   @-webkit-keyframes spin {
     0% { -webkit-transform: rotate(0deg); }
     100% { -webkit-transform: rotate(360deg); }
   }

   @keyframes spin {
     0% { transform: rotate(0deg); }
     100% { transform: rotate(360deg); }
   }
   /* Payment >>> */
.card_label_details {
    display: block;
    color: #4f4f4f;
    font-family: 'MazzardL-Medium';
    font-size: 20px;
    margin-bottom: 8px;
}
.form_row.card_details>div {
    padding: 20px 10px;
    /* box-shadow: 0px 2px 3px 0px rgba(174, 174, 174, 0.16); */
    border-radius: 8px;
    border: 2px solid #e9e9e9;
}
.left_payment_form_inner .card_name input {
    height: 62px;
    width: 100%;
    border: 2px solid #e9e9e9;
    border-radius: 8px;
    background-color: white;
    box-shadow: 0px 2px 3px 0px rgba(174, 174, 174, 0.16);
    padding: 0px 15px;
    font-family: 'MazzardM-Regular';
    font-size: 20px;
    color: #71706f;
}
.left_payment_form_inner  .card_name {
    margin-top: 24px;
}
.left_payment_form_inner .submit_btn {
    cursor: pointer;
    background-color: #03a9f4;
    color: white;
    font-family: 'MazzardL-Medium';
    font-size: 22px;
    border: 2px solid #03a9f4;
    margin-top: 26px;
    transition: 0.5s;
    position: absolute;
    bottom: -88px;
}
.submit_row {
    margin-top: 0px;
    position: absolute;
    width: calc(100% - 30px);
    bottom: 0;
}
.left_payment_form_inner button {
    height: 62px;
    width: 100%;
    border: 2px solid #03a9f4;
    border-radius: 8px;
    background-color: white;
    box-shadow: 0px 2px 3px 0px rgba(174, 174, 174, 0.16);
    padding: 0px 15px;
    font-family: 'MazzardM-Regular';
    font-size: 20px;
    color: #71706f;
}
.panel-body{
    padding-bottom:100px !important;
}
.flex_property_label{
    display:flex;
    /* justify-content: space-between; */
    justify-content: center;
    margin-bottom: 10px;
}
.flex_property_label .card_label_details{
    font-size: 30px;
}
.custom_error_box{
    width: 100%;
    height: 100%;
    max-width: 32%;
    margin: 0 auto;
}
.custom_error_box .icon{
    display: inline-block;
    margin-right: 10px;
}
</style>

<!-- <div id="loader">
   <div class="loader"></div>
</div> -->
<?php
if (isset($stripe_publishable_key)) {
    $stripePubKey = $stripe_publishable_key;
} else {
    $stripePubKey = '';
}
if (isset($charges_amount)) {
    $stripeChargeAmount = $charges_amount;
} else {
    $stripeChargeAmount = 0;
}
if (isset($member_temp_id)) {
    $memTempID = $member_temp_id;
} else {
    $memTempID = 0;
}

if (isset($is_previous_subscription)) {
    $is_previous_subscription = $is_previous_subscription;
} else {
    $is_previous_subscription = '';
}
?>
<div class="panel-body">
        <?php if ($this->session->flashdata('success')) {?>
        <div class="alert alert-success text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
        <?php }?>

        <?php

if (isset($_GET['msg']) && $_GET['msg'] != '') {?>
            <div id="response" class="error_box custom_error_box">
            <?php if ($_GET['succ'] == 1) {
    echo '<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>' . $_GET['msg'] . '</div>';
} else if ($_GET['succ'] == 0) {
    echo '<div class="alert alert-danger alert-white rounded">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>Payment declined. Please use another card or recheck the entered information is correct' . $_GET['msg'] . '</div>';
}?>
            </div>
           <?php
}?>
        <form action="<?php echo base_url(); ?>stripePost" name="frmPayment" id="frmPayment" method="POST">

            <!-- £ -->
            <div class="col-md-3 left_payment_form"></div>
            <div class="col-md-6 left_payment_form">
                <div class="flex_property_label">
                    <label class="card_label_details" style="font-family: 'Montserrat';">TCL Membership Subscription $<?php echo $stripeChargeAmount; ?> P/Year</label>
                </div>
            </div>
            <div class="col-md-4 left_payment_form"></div>
            <div class="col-md-4 left_payment_form">
                <div class="flex_property_label">
                    <label class="card_label_details"></label>
                </div>
                <!-- <div class="flex_property_label">
                    <label class="card_label_details">$</label>
                </div> -->
                <div class="left_payment_form_inner">
                    <!-- <div id="card-errors" role="alert"></div> -->
                    <div class="form_row card_details">
                        <label class="card_label_details" style="font-family: 'Montserrat';">Card details</label>
                        <div id="card-element" style="font-family: 'Montserrat';"></div>
                    </div>
                    <div class="form_row card_name">
                        <label class="card_label_details" style="font-family: 'Montserrat';">Name on the card</label>
                        <input class="card_name_input" type="text" name="cardholder-name" id="cardholder-name" style="font-family: 'Montserrat';"/>
                    </div>
                    <div class="form_row submit_row stripe_paynow_button">
                        <button class="submit_btn" type="submit" name="card-button" id="card-button" style="background-color: #8f2339;border: 1px solid #8f2339;font-family: 'Montserrat';">Pay Now</button>
                    </div>
                </div>
            </div>
            <div class="col-md-4 left_payment_form"></div>
            <input type="hidden" name="item_name" value="Tomouh Subscription Plan">
            <input type="hidden" name="currency_code" value="USD">
            <input type="hidden" name="charges_amount" value="<?php echo $stripeChargeAmount; ?>">
            <input type="hidden" value="<?php echo $memTempID; ?>" name="member_temp_id">
            <input type="hidden" value="<?php echo $is_previous_subscription; ?>" name="is_previous_subscription">
        </form>
    </div>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">

var STRIPE_API_KEY = "<?php echo $stripePubKey; ?>";
    var stripe = Stripe(STRIPE_API_KEY);
    var style = {
        base: {
            border: '1px solid #CCC',
            padding: '10px',
            width: '300px',
            fontFamily: 'Montserrat',
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

    var style = {
        base: {
            fontSize: '16px',
            color: "#32325d",
            border: "1px solid black",
            fontFamily: 'Montserrat',
        }
    };

    var elements = stripe.elements();
    var cardElement = elements.create('card', {style: style});

    cardElement.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    cardElement.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });
    // Handle form submission.
    var form = document.getElementById('frmPayment');
    form.addEventListener('submit', function(event) {
        event.preventDefault();

        stripe.createToken(cardElement).then(function(result) {
            if (result.error) {
            // Inform the user if there was an error.
            var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server.
                stripeTokenHandler(result.token);
            }
        });
    });

    // Submit the form with the token ID.
    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('frmPayment');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);

        // Submit the form
        form.submit();
    }
</script>
<?php include 'inc/footer.php';?>