<?php 
include("inc/header_top.php");
include("inc/header.php"); 
?>
<?php
	$query_params =array();
    $url = $video['v_url'];
    $query_str = parse_url($url, PHP_URL_QUERY);
    parse_str($query_str, $query_params);
?>
<div class="inner_wrapper">
  <div class="media_page">
          <div class="sec_banner">
             <div class="container">
                  <div class="page_head">
                      <h1 class="red-text text-center font36" id="heading"><?php if(isset($video['v_name'])){  echo $video['v_name']; } ?></h1>
                  </div>
                <div class="media_tab">
                   <div class="row">
                    <div class="col-xs-12">
                          <div class="btns center-block pull-right">
                              <a href="<?php echo base_url(); ?>media" class="btn gray-btn">Back</a>
                          </div>
                        </div>
                    </div>
                    <div class="videos_div">
                        <div class="videoWrapper" id="main_video">
                          <iframe src="https://www.youtube.com/embed/<?php echo $query_params['v']; ?>?rel=0&autoplay=1" allowfullscreen="" width="640" height="480" frameborder="0"></iframe>
                        </div>
                       <div class="video-title video_member" id="main_video_title"><?php if(isset($video['v_name'])){  echo $video['v_name']; } ?></div>
                       <p class="video_desc"><?php if(isset($video['l_description'])){  echo ($video['l_description']); } ?></p>
                
                        <div class="video_listing">
                            <div class="row" id="video_list">
                            </div>
                        </div>
                    </div>
                </div>
             </div>
         </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
  
  var vidArray = [];
  getVids();

function getVids(PageToken){
    pid = '<?php echo $query_params['list']; ?>';

    $.get(
        "https://www.googleapis.com/youtube/v3/playlistItems",{
        part : 'snippet', 
        maxResults : 50,
        playlistId : pid,
        pageToken : PageToken,
        key: 'AIzaSyDDMyAQtsAapDa5uTUosDUkTdWLeRz3pYY'
        },
        function(data){
              myPlan(data);
        }        
    ); 
 }

  function myPlan(data){
    
      total = data.pageInfo.totalResults;
      nextPageToken=data.nextPageToken;
	  console.log(data.items);	
      for(i=0;i<data.items.length;i++){
          var vid = data.items[i].snippet.resourceId.videoId;
		  var desc_str = data.items[i].snippet.description;
		  	
          vidArray.push(vid);
                  var str_video = '<div class="col-sm-12 col-xs-6 video_tab" onclick="test(this)" data-vid="'+vid+'" id="'+data.items[i].snippet.title+'" data-desc="'+desc_str+'">'+
                        '<div class="col-sm-3 col-xs-12">'+
                              '<div class="">'+
                                '<img src=https://img.youtube.com/vi/'+ vid +'/hqdefault.jpg style="max-width:100%;"/>'+
                              '</div>'+
                        '</div>'+
                        '<div class="col-sm-9 col-xs-12">'+
                                '<div class="video-title video_member">'+
                                  '<span style="cursor: pointer;">'+data.items[i].snippet.title+'</span>'+
								  '<p>'+desc_str+'</p>'
                                '</div>'+
                        '</div>'+
                      '</div>';

                  $('#video_list').append(str_video);  
                  }         
        }
});
</script>

<script type="text/javascript">
    function test(_this){

      var spanclass = $(_this).attr('data-vid');
      var spanid = $(_this).attr('id');
      var desc = $(_this).attr('data-desc');
      
	   	
        $('#main_video').html('<iframe src="https://www.youtube.com/embed/'+spanclass+'?rel=0&autoplay=1" allowfullscreen="" width="640" height="480" frameborder="0"></iframe>'); 

        $('#main_video_title').html(spanid);

        $('#heading').html(spanid);
		$('.videos_div .video_desc').html(desc);

        $('html, body').animate({ scrollTop: 0 }, 'slow');
    }
</script>
<?php
include("inc/footer.php"); 
?>