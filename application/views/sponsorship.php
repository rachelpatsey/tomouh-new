<?php 
include("inc/header_top.php");
include("inc/header.php"); 
?>
<style>
.center-align {
    text-align: center;
}
.sposor_one_tbl{
    width: 500px;
    line-height: 35px;
    /* text-align: center; */
    font-size: 16px;
    padding: 0 9%;
    margin-top: 20px;

}
.sposor_one_list{
    line-height: 24px;
    font-size: 16px;
    padding: 0 9%;
}
.sposor_one_list h2{
    font-size: 30px;
    padding: 20px 0 20px 0;
}
.sposor_one_list ul{
    list-style: disc;
    padding-left: 30px;
}
.sposor_one_tbl table tr td{
    border: 1px solid black;
    padding: 6px;
}
.container {
    margin-top:40px; 
}
.sposor_one_tbl{
    width:100%;
    max-width:500px;
    padding-right:0px;
}
.sposor_one_tbl table{
    width:100% !important;
}

</style>
<div class="inner_wrapper">
  <div class="about_page">
    <section class="about_us">
      <div class="banner_section">
        <!-- <div class="sec_banner"> -->
        <div class="">
          <div class="container">
            <div class="page_head">
              <h1 class="red-text text-center font36"><?php if(isset($title)){echo $title;} ?></h1>
            </div>
            <p class="content"><?php if(isset($main_description)){echo $main_description;} ?></p>
          </div>
        </div>
      </div>
   
      <div class="container">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-12 col-xs-12 vision_content">
                  <div class="page_head">
                    <h1 class="center-align red-text font36"><?php if(isset($meta['title1'])){ echo $meta['title1']; }?></h1>
                  </div>
                  <p class="content"><?php if(isset($meta['description1'])){echo $meta['description1']; }?></p>
              </div>
            </div>
          </div>
      </div>
      <div class="container">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-12 col-xs-12 vision_content">
                  <div class="page_head">
                    <h1 class="center-align red-text font36"><?php if(isset($meta['title2'])){ echo $meta['title2']; }?></h1>
                  </div>
                  <p class="content"><?php if(isset($meta['description2'])){echo $meta['description2']; }?></p>
              </div>
            </div>
          </div>
      </div>
      <div class="container">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-12 col-xs-12 vision_content">
                  <div class="page_head">
                    <h1 class="center-align red-text font36"><?php if(isset($meta['title3'])){ echo $meta['title3']; }?></h1>
                  </div>
                  <p class="content"><?php if(isset($meta['description3'])){echo $meta['description3']; }?></p>
              </div>
            </div>
          </div>
      </div>
      </section>
  </div>
</div>
<?php include('inc/footer.php') ?>    
