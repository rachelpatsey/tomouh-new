<?php

$url = $_SERVER['REQUEST_URI'];

// $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";



$TWITTER_LINK = $this->tomouh_model->getSetting('TWITTER_LINK');

$YOUTUBE_LINK = $this->tomouh_model->getSetting('YOUTUBE_LINK');

$INSTAGRAM_LINK = $this->tomouh_model->getSetting('INSTAGRAM_LINK');

$LINKEDIN_LINK = $this->tomouh_model->getSetting('LINKEDIN_LINK');

$CONTACT_INFO = $this->tomouh_model->getSetting('CONTACT_INFO');

$FOOTER_TEXT = $this->tomouh_model->getSetting('FOOTER_TEXT');

$SITE_LOGO = $this->tomouh_model->getSetting('SITE_LOGO');

?>


<div class="clearfix"></div>
<div class="footer">

    <footer>

        <div class="container">

            <div class="first-part">
              <div class="col-sm-2 col-xs-12  col-lg-2"></div>
               <div class="col-sm-4 col-xs-12  col-lg-4">

                   <div class="first-col">

                         <div class="footer-logo"><img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $SITE_LOGO; ?>" class="img-responsive" alt=""></div>

                        <ul>

                            <li><a href="<?php echo $INSTAGRAM_LINK; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

                            <li><a href="<?php echo $LINKEDIN_LINK; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>

                            <li><a href="<?php echo $TWITTER_LINK; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                            <li><a href="<?php echo $YOUTUBE_LINK; ?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>

                        </ul>

                        

                    </div>

                    <div class="border-right hidden-xs hidden-sm"></div>

                </div>

                <!-- <div class="col-sm-4  col-lg-5  col-xs-12">

                    <div class="sec-col">

                        <ul>

                          <li class="<?php if(strpos($url, 'about_us') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>about_us">About TCL</a></li>

                          <li class="<?php if(strpos($url, 'team') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>team">Team</a></li>

                          <li class="<?php if(strpos($url, 'membership_process') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>membership_process">Membership</a></li>

                          <li class="<?php if(strpos($url, 'media') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>media">Media</a></li> 

                          <li class="<?php if(strpos($url, 'blog') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>blog">Blog</a></li>

                          <li class="<?php if(strpos($url, 'event') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>event">Events</a></li>

                        </ul>

                    </div>

                  <div class="border-right hidden-xs hidden-sm"></div>

                </div> -->

                <div class="col-sm-4  col-lg-4  col-xs-12">

                    <div class="third-col">

                        <ul>

                            <!-- <li class="<?php if(strpos($url, 'contact_us') !== false) { ?> active <?php } ?>"><a href="<?php echo base_url(); ?>contact_us">Contact Us</a></li> -->

                            <li><a href="mailto:<?php echo $CONTACT_INFO; ?>"><?php echo $CONTACT_INFO; ?></a></li>
                            <li><a href="javascript:;">+44 7464 299644</a></li>
                        </ul>

                    </div>

                </div>
                <div class="col-sm-2 col-xs-12  col-lg-2"></div>
            </div>

        </div>

            <div class="sec-part">

                <p><?php echo $FOOTER_TEXT; ?>  | <a href="<?php echo base_url(); ?>terms-and-conditions/">Terms and Conditions</a> </p>

            </div>

        

    </footer>

</div>
<style>
html {
    position: relative;
    min-height: 100%;
}
body{
  float: left;
  width: 100%;
}

.footer {
    display:none;
    position: absolute;
    left: 0;
    bottom: 0;
    height: auto;
    width: 100%;
}


</style>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/client/js/webapplinks.js"></script> 

<script>

    // media query event handler

    var mql = window.matchMedia("screen and (max-width: 767px)");

    if (mql.matches){ // if media query matches

    //alert("Window is 1199px or wider")

    $(document).ready(function() {

      $("#owl-demo").owlCarousel({

        margin:0,

        dots:false,

        loop :true,

        autoplay:true,

        slideSpeed:500, 

                responsive:{

          0:{

            items:1

          },

                    375:{

            items:2

          },

          600:{

            items:4

          },

          1000:{

            items:4

          }     

        }

        });

      });

    }

    else{

    // do something else

    }



    

</script> 

<script type="text/javascript" src="<?php echo base_url(); ?>assets/client/js/owl.carousel.js"></script> 

<script>

    $(window).scroll(function() {    

var scroll = $(window).scrollTop();

 if (scroll >= 100) {

  $("#menu").addClass("menufixed", 2000);

}  

var scroll = $(window).scrollTop();

 if (scroll <= 100) {

  $("#menu").removeClass("menufixed");

}

});

       $(window).scroll(function() {    

var scroll = $(window).scrollTop();

 if (scroll >= 100) {

  $("#menu1").addClass("menufixed", 2000);

}  

var scroll = $(window).scrollTop();

 if (scroll <= 100) {

  $("#menu1").removeClass("menufixed");

}

});
function footerAlign() {
  $('.footer').css('display', 'block');
  $('.footer').css('height', 'auto');
  var footerHeight = $('.footer').outerHeight();
  $('body').css('padding-bottom', footerHeight);
  $('.footer').css('height', footerHeight);
}
$(document).ready(function(){
  footerAlign();
});

$( window ).resize(function() {
  footerAlign();
});
</script> 

</body>

</html>

