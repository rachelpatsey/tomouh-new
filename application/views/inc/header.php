<?php
$url = $_SERVER['REQUEST_URI'];
$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$site_url = $this->tomouh_model->getSetting('SITE_URL');
$site_logo = $this->tomouh_model->getSetting('SITE_LOGO');
if(isset($_COOKIE['tomouh_logged_data']) && !empty($_COOKIE['tomouh_logged_data'])){

  $logged_data = json_decode($_COOKIE['tomouh_logged_data'],true);

  $this->session->set_userdata($logged_data);
}
// $adword_script = $this->tomouh_model->getSetting('ADWORD_SCRIPT');
// $adword_script = $this->tomouh_model->getSetting('ADWORD_SCRIPT');


?>
<style>
  .carousel-inner{margin-top: 126px;}
  .main-header .navbar{background: #252525;}
  .home_slider .carousel .item {
    width: 100%;
    height: 100vh;
    background-position: center;
  }
  .home_slider .carousel .carousel-inner .item {
    height: auto;
    padding-top: 0;
  }
  .home_slider .carousel .carousel-inner .item img{
  width: 100%;
  height: auto;
  }
  .footer footer .first-col{
    text-align: center;
  }

  .footer footer .first-col ul li:last-child{
    margin-right: 0px;
  }

  .home_slider .carousel .carousel-inner{margin-top: 124px !important;}

  @media screen and (max-width:1280px) {
  .home_slider .carousel .carousel-inner{margin-top: 110px !important;}
  }
  @media screen and (max-width:1199px) {
  .home_slider .carousel .carousel-inner{margin-top: 102px !important;}
  }
  @media screen and (max-width:991px) {
  .home_slider .carousel .carousel-inner{margin-top: 85px !important;}
  }
  .mobile_responsive_menu.hidden-lg.hidden-md .mobile_logo {background: #252525;}  
  }
  .page_head h1 {font-family: 'Spectral SC', serif !important;
      text-transform: uppercase !important;
  }
  .page_head h1 {text-transform: none !important;font-family:'Montserrat'!important;}
    /* dev */
    /* .page_head h1 {text-transform: none !important;font-family:'Baker Signet' !important;} */
    /* .red-text{color:#828282 !important}
    .main-header .navbar {background: #828282 !important;}
    .mobile_responsive_menu.hidden-lg.hidden-md .mobile_logo {background: #828282;}
    .footer{background: #828282 !important;}
    .footer footer .sec-part p {background: #828282 !important;}
    .main-header .navbar {
    background: #fff !important;
    }
    .main-header ul li a,
    .footer footer .sec-part p,
    .footer footer ul li a, .footer footer a{color: #828282;}
    .main-header .navbar ul li a:hover, .main-header .navbar ul li a:focus, .main-header .navbar ul li.active a{
      color: #828282 !important;
    }
    .footer {
      background: #fff !important;
    }
    .footer footer .sec-part p {
      background: #fff !important;
    }
    .home_slider .carousel .carousel-inner .item img {
    width: 100%;
    height: 84vh;
    max-width: 100%;
    object-fit: cover;
    }

    .main-header .navbar ul li a:hover, .main-header .navbar ul li a:focus, .main-header .navbar ul li.active a {
    border-bottom: 4px solid #942d33;
    } */ 
     /* end */
    /* .main-header .navbar ul.dropdown-menu li {
    background-color: #00425e;
    } 
 
  /* Live */
  .home_slider .carousel .carousel-inner .item img {
  width: 100%;
  height: 84vh;
  max-width: 100%;
  object-fit: cover;
  }
  @media(max-width: 767px){
    .home_slider .carousel .carousel-inner .item img{
    height: auto;
    }
  }
 
.main-header .navbar .navbar-brand img {
    /* width: 200px; */
    width: 150px;
    height: 75px;
    object-fit: contain;
}
.footer footer .footer-logo img {
   width: 200px;
    height: 75px;
    object-fit: contain;
    /* margin-left: -18px; */
}
@media only screen and (max-width:991px){
.mobile_logo img {
    width: 150px;
    height: 55px;
    object-fit: contain;
}
@media only screen and (max-width:767px) {
  /* margin-left: auto; */
  .footer footer .footer-logo img {
    /* margin-left: 40px !important; */
  }
}
}

  /* Live */
</style>
<div class="main-header">
  <nav class="navbar navbar-fixed-top hidden-xs hidden-sm <?php if($actual_link !== $site_url){ ?> inner_header <?php } ?>" id="menu">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>

        <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $site_logo; ?>" alt="" class="img-responsive"></a> </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
           <?php $parent_menu = $this->tomouh_model->getParentMenus(); 
           foreach($parent_menu as $menu){
                $child_menu = $this->tomouh_model->getChildMenus($menu['id']);  
           ?>
            <li class="<?php if(isset($child_menu) && !empty($child_menu)){ ?>dropdown<?php } ?> <?php if($menu['v_link'] == $actual_link){ ?> active <?php } ?> <?php if(!empty($child_menu)){ foreach($child_menu as $cm){ if($cm['v_link'] == $actual_link){ ?> active <?php } } } ?>"> 
              <a href="<?php echo $menu['v_link'];?>" <?php if(isset($child_menu) && !empty($child_menu)){ ?> class="dropdown-toggle"  data-toggle="dropdown"  <?php } ?>><?php echo $menu['v_title'];?></a>
                  <?php if(isset($child_menu) && !empty($child_menu)){ ?>
                    <ul class="dropdown-menu">
                      <?php foreach($child_menu as $child){ ?>
                        <?php if($child['v_title'] == 'Apply To Tomouh' && $this->session->has_userdata('logged_user')){?>
                        <?php }else{ ?>    
                        <li><a href="<?php echo $child['v_link'];?>"><?php echo $child['v_title'];?></a></li>
                        <?php } ?>
                      <?php } ?>
                    </ul>
                  <?php } ?>
            </li>  
            <?php } ?>    
           <?php if($this->session->has_userdata('logged_user')){?>
           <li class="login_signup"><a href="<?php echo base_url(); ?>account" class="signup">Account</a><a href="<?php echo base_url(); ?>logout" class="login">Logout</a></li>
           <?php }else{ ?>
            <li class="login_signup"><a href="<?php echo base_url(); ?>signup" class="signup">Apply</a><a href="<?php echo base_url(); ?>login" class="login">Login</a></li>
            <?php } ?>
          </ul>
      </div>
    </div>
  </nav>
<div class="mobile_responsive_menu hidden-lg hidden-md">
    <div class="mobile_logo" id="menu1"> <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $site_logo; ?>" alt="" class="img-responsive"></a> </div>
    <header>
      <div class="wsmenucontainer clearfix">
        <div class="overlapblackbg"></div>
        <div class="wrapper clearfix"> <a id="wsnavtoggle" class="animated-arrow"><span></span></a> </div>
        <nav class="wsmenu clearfix">
          <ul class="mobile-sub wsmenu-list clearfix">
          <?php $parent_menu = $this->tomouh_model->getParentMenus(); 
           foreach($parent_menu as $menu){
                $child_menu = $this->tomouh_model->getChildMenus($menu['id']);  
           ?>
            <li class="<?php if($menu['v_link'] == $actual_link){ ?> active <?php } ?> <?php if(!empty($child_menu)){ foreach($child_menu as $cm){ if($cm['v_link'] == $actual_link){ ?> active <?php } } } ?>"><span class="wsmenu-click"><i class="wsmenu-arrow fa fa-angle-down"></i></span> 
              <a href="<?php echo $menu['v_link'];?>" ><?php echo $menu['v_title'];?><?php if(isset($child_menu) && !empty($child_menu)){ ?><span class="arrow"></span> <?php } ?></a>
                  <?php if(isset($child_menu) && !empty($child_menu)){ ?>
                    <ul class="wsmenu-submenu">
                      <?php foreach($child_menu as $child){ ?>
                        <?php if($child['v_title'] == 'Apply To Tomouh' && $this->session->has_userdata('logged_user')){?>
                        <?php }else{ ?>    
                        <li><a href="<?php echo $child['v_link'];?>"><?php echo $child['v_title'];?></a></li>
                        <?php } ?>
                      <?php } ?>
                    </ul>
                  <?php } ?>
            </li>  
            <?php } ?> 
            <?php if($this->session->has_userdata('logged_user')){?>
              <li class="login_signup"><a href="<?php echo base_url(); ?>account" class="signup">Account</a></li>
              <li class="login_signup"><a href="<?php echo base_url(); ?>logout" class="signup">Logout</a></li>
            <?php }else{ ?>
              <li class="login_signup"><a href="<?php echo base_url(); ?>signup" class="signup">Apply</a></li>
              <li class="login_signup"><a href="<?php echo base_url(); ?>login" class="signup">Login</a></li>
            <?php } ?>        
          </ul>
        </nav>
      </div>
    </header>
  </div>
</div>