<?php
if (isset($google_recaptch_site_key)) {
  $site_key = $google_recaptch_site_key;
} else {
  $site_key = '';
}

if (isset($user_data['v_telephone']) && !empty($user_data['v_telephone'])) {
  if(strstr($user_data['v_telephone'],'-')){
    $tel = explode('-', $user_data['v_telephone']);
    // $tele = isset($tel[1]) ? $tel[1] : $tel[0];
    $tele = isset($tel[1]) ? $tel[1] : '';
    $tele_code = isset($tel[0]) ? $tel[0] : '';
  }elseif(strstr($user_data['v_telephone'],'+')){
    $tele_code = $user_data['v_telephone'];
  }else{
    $tele =  $user_data['v_telephone'];
  }
 
} else {

  $tele = '';
  $tele_code = '';
}
if (isset($user_data['v_telephone_2']) && !empty($user_data['v_telephone_2'])) {

 /*  $tel_2 = explode('-', $user_data['v_telephone_2']);
  // $tele_2 = isset($tel_2[1]) ? $tel_2[1] : $tel_2[0];
  $tele_2 = isset($tel_2[1]) ? $tel_2[1] : '';
  $tele_2_code = isset($tel_2[0]) ? $tel_2[0] : ''; */
  if(strstr($user_data['v_telephone_2'],'-')){
    $tel_2 = explode('-', $user_data['v_telephone_2']);
    // $tele = isset($tel[1]) ? $tel[1] : $tel[0];
    $tele_2 = isset($tel_2[1]) ? $tel_2[1] : '';
    $tele_2_code = isset($tel_2[0]) ? $tel_2[0] : '';
  }elseif(strstr($user_data['v_telephone_2'],'+')){
    $tele_2_code = $user_data['v_telephone_2'];
  }else{
    $tele_2 =  $user_data['v_telephone_2'];
  }


} else {

  $tele_2 = '';
  $tele_2_code = '';
}

$current_year = date("Y");
?>
<?php
include("inc/header_top.php");
include("inc/header.php");
?>

<style type="text/css">
  .add_detele {
    cursor: pointer;
    position: absolute;
    right: 15px;
    top: 1px;
    font-size: 20px;
    color: red;
  }
</style>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/parsley.js/1.2.2/parsley.min.js'></script>

<style type="text/css">
  .error_box {
    display: inline-block;
    width: 100%;
    padding-top: 20px;
    padding-left: 10%;
    padding-right: 10%;
  }

  .error_box .icon {
    float: left;
    padding-right: 10px;
  }

  input.parsley-error {
    border: 1px solid red;
  }

  .parsley-error-list {
    color: red;
  }

  .eye_input {
    position: absolute;
    right: 22px;
    line-height: 37px;
    z-index: 99;
    top: 30px;
    color: #888;
    font-size: 18px;
  }

  .frm-cntl .select2 .select2-selection--single {
    box-shadow: none !important;
    height: 40px !important;
    border-radius: 0px;
    border: 1px solid #ccc !important;

  }

  .frm-cntl .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 40px;
  }

  .frm-cntl .select2-selection__clear {
    display: none;
  }

  .frm-cntl .select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 7px;
  }

  .select2-container--default .select2-results__option--highlighted[aria-selected] {
    background-color: white !important;
    color: black !important;
  }
</style>

<div class="inner_wrapper">
  <?php
  if (!($this->form_validation->error_array())) {
    if (isset($_GET['msg']) && $_GET['msg'] != '') { ?>
      <div id="response" class="error_box">
        <?php if ($_GET['succ'] == 1) {
          echo $this->messages_model->getSuccessMsg($_GET['msg']);
        } else if ($_GET['succ'] == 0) {
          echo $this->messages_model->getErrorMsg($_GET['msg']);
        } ?>
      </div>
    <?php  }
  } ?>
  <?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>'); ?>

  <div class="inner_header_div">
    <div class="container">
      <div class="row">
        <div class="col-sm-7 col-md-8 col-xs-12">
          <div class="left-div"> <a href="<?php echo base_url(); ?>my_home" class="">My Home</a> <a href="<?php echo base_url(); ?>members">Members</a> <a href="<?php echo base_url(); ?>event">Events</a> <a href="<?php echo base_url(); ?>initiative">Initiatives</a> </div>
        </div>
        <div class="col-sm-5 col-md-4  col-xs-12">
          <div class="right-div"> <a href="<?php echo base_url(); ?>account">Account</a> <a href="<?php echo base_url(); ?>profile">Profile</a> </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="signup_page">
      <div class="form">
        <h2 class="signup-head red-text spectral-font font36 text-center"><strong>Edit Profile</strong></h2>
        <div class="signup_headline">
          <ul class="nav nav-tabs nav-justified">
            <?php
            if ($_GET['edu_info'] && $_GET['edu_info'] == 1) {
              $active_cls_per = '';
              $in_cls_per = '';
              $active_cls_edu = 'active';
              $in_cls_edu = 'in';
            } else {
              $active_cls_per = 'active';
              $in_cls_per = 'in';
              $active_cls_edu = '';
              $in_cls_edu = '';
            }

            ?>
            <li class="<?php echo $active_cls_per; ?>"><a data-toggle="tab" href="#personal_info">PERSONAL</a></li>
            <li class="<?php echo $active_cls_edu; ?>"><a data-toggle="tab" href="#edu_info">EDUCATION & Professional</a></li>
          </ul>
        </div>
        <form name="signup_form_1" class="signup_form" method="post" enctype="multipart/form-data" parsley-validate>

          <div class="tab-content">

            <div id="personal_info" class="tab-pane fade <?php echo $active_cls_per . ' ' . $in_cls_per ?>">
              <input type="hidden" name="update_info" value="1">

              <div class="gray-border-box">
                <div class="row">
                  <div class="col-xs-12">
                    <p class="form-head">Personal Information</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-3">
                    <div class="form-group">
                      <label>First Name*</label>
                      <input type="text" name="firstname" id="firstnme" class="form-control" value="<?php if (!empty($user_data['v_firstname'])) {
                                                                                                      echo $user_data['v_firstname'];
                                                                                                    } ?>" parsley-required="true">
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <label>Last Name*</label>
                      <input type="text" name="lastname" id="lastname" class="form-control" value="<?php if (!empty($user_data['v_lastname'])) {
                                                                                                      echo $user_data['v_lastname'];
                                                                                                    } ?>" parsley-required="true">
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <label>Date Of Birth*</label>
                      <div class="input-group date" data-provide="datepicker">
                        <input type="text" name="dob" id="dob" value="<?php if (!empty($user_data['d_dob']) && $user_data['d_dob'] != '0000-00-00') {
                                                                        echo date('m/d/Y', strtotime($user_data['d_dob']));
                                                                      } ?>" class="form-control" parsley-required="true">
                        <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <label>Gender</label>
                      <div class="select">
                        <select class="selectpicker" name="gender" id="gender">
                          <option <?php if (isset($user_data)) {
                                    if (!empty($user_data['e_gender']) && $user_data['e_gender'] == 'Male') { ?> selected <?php }
                                                                                                                          } ?>>Male</option>
                          <option <?php if (isset($user_data)) {
                                    if (!empty($user_data['e_gender']) && $user_data['e_gender'] == 'Female') { ?> selected <?php }
                                                                                                                            } ?>>Female</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="gray-border-box">
                <div class="row">
                  <div class="col-xs-12">
                    <p class="form-head">Account</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-3">
                    <div class="form-group">
                      <label>Email*</label>
                      <input type="text" name="email" id="email" value="<?php if (!empty($user_data['v_email'])) {
                                                                          echo $user_data['v_email'];
                                                                        } ?>" class="form-control" parsley-required="true" parsley-type="email">
                    </div>
                  </div>
                </div>
              </div>
              <div class="gray-border-box">
                <div class="row">
                  <div class="col-xs-12">
                    <p class="form-head">Contact Information</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-3 col-xs-12">
                    <div class="form-group frm-cntl">
                      <label>Home Country</label>
                      <!--  <div class="select"> -->
                      <select class="form-control" name="home_country" id="home_country" required="required">
                        <option value="">Select Country</option>
                        <?php foreach ($countries as $country) { ?>
                          <option <?php if (isset($user_data)) {
                                    if (!empty($user_data['v_home_country']) && $user_data['v_home_country'] == $country['country_name']) { ?> selected <?php }
                                                                                                                                                          } ?>><?php echo $country['country_name']; ?></option>
                        <?php } ?>
                      </select>
                      <!--  </div> -->
                    </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <div class="form-group">
                      <label>Home City</label>
                      <input type="text" name="home_city" id="home_city" class="form-control" value="<?php if (!empty($user_data['v_home_city'])) {
                                                                                                        echo $user_data['v_home_city'];
                                                                                                      } ?>">
                    </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <div class="form-group frm-cntl">
                      <label>Country of Residence</label>
                      <!-- <div class="select"> -->
                      <select class="form-control" name="residence_country" id="residence_country" required="required">
                        <option value="">Select Country</option>
                        <?php foreach ($countries as $country) { ?>
                          <option <?php if (isset($user_data)) {
                                    if (!empty($user_data['v_residence_country']) && $user_data['v_residence_country'] == $country['country_name']) { ?> selected <?php }
                                                                                                                                                                    } ?>><?php echo $country['country_name']; ?></option>
                        <?php } ?>
                      </select>
                      <!-- </div> -->
                    </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <div class="form-group">
                      <label>City of Residence</label>
                      <input type="text" name="residence_city" id="residence_city" class="form-control" value="<?php if (!empty($user_data['v_residence_city'])) {
                                                                                                                  echo $user_data['v_residence_city'];
                                                                                                                } ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-3 col-xs-12">
                    <div class="form-group frm-cntl">
                      <label>Mobile Number</label>
                      <div class="row">
                        <div class="col-sm-12">
                          <div style="width: 30%;float: left;">
                            <select class="form-control" name="tele_country_code" id="tele_country_code" required="required">
                              <option value="">Code</option>
                              <?php foreach ($countries as $country) { ?>
                                <option <?php if ($tele_code == $country['phonecode']) { ?> selected <?php } ?>>+<?php echo $country['phonecode']; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <input type="text" style="width: 68%;float: right;" name="telephone" id="telephone" class="form-control" value="<?php echo $tele; ?>" parsley-type="number" parsley-minlength="6" parsley-maxlength="12">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <div class="form-group frm-cntl">
                      <label>Mobile Number 2</label>
                      <div class="row">
                        <div class="col-sm-12">
                          <div style="width: 30%;float: left;">
                            <select class="form-control" name="tele_2_country_code" id="tele_2_country_code">
                              <option value="">Code</option>
                              <?php foreach ($countries as $country) { ?>
                                <option <?php if ($tele_2_code == $country['phonecode']) { ?> selected <?php } ?>>+<?php echo $country['phonecode']; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <input type="text" style="width: 68%;float: right;" name="telephone_2" id="telephone_2" class="form-control" value="<?php echo $tele_2; ?>" parsley-type="number" parsley-minlength="6" parsley-maxlength="12">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="gray-border-box">
                <div class="row">
                  <div class="col-xs-12">
                    <p class="form-head">Social Account</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-3 col-xs-12">
                    <div class="form-group">
                      <label>Twitter Link</label>
                      <input type="text" name="twitter_link" id="twitter_link" class="form-control" value="<?php if (!empty($user_data['v_twitter_link'])) {
                                                                                                              echo $user_data['v_twitter_link'];
                                                                                                            } ?>">
                      <p class="gray-text font12"><a href="https://twitter.com/" target="_blank"> twitter.com/johndoe</a></p>
                    </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <div class="form-group">
                      <label>Linkedin Link</label>
                      <input type="text" name="linkedin_link" id="linkedin_link" class="form-control" value="<?php if (!empty($user_data['v_linkedin_link'])) {
                                                                                                                echo $user_data['v_linkedin_link'];
                                                                                                              } ?>">
                      <p class="gray-text font12"><a href="https://uk.linkedin.com/" target="_blank"> uk.linkedin.com/in/johndoe</a></p>
                    </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <div class="form-group">
                      <label>Instagram Link</label>
                      <input type="text" name="instagram_link" id="instagram_link" class="form-control" value="<?php if (!empty($user_data['v_instagram_link'])) {
                                                                                                                  echo $user_data['v_instagram_link'];
                                                                                                                } ?>">
                      <p class="gray-text font12"><a href="https://www.instagram.com/" target="_blank"> www.instagram.com/johndoe</a></p>
                    </div>
                  </div>
                  <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label>Were you referred to by a member?</label>
                      <div class="row">
                        <div class="select col-md-3 col-sm-4 col-xs-12">
                          <select class="selectpicker" name="referral" id="referral">
                            <option value="0" <?php if (!empty($user_data['e_referral']) && $user_data['e_referral'] == 0) { ?> selected <?php } ?>>No</option>
                            <option value="1" <?php if (!empty($user_data['e_referral']) && $user_data['e_referral'] == 1) { ?> selected <?php } ?>>Yes</option>
                          </select>
                        </div>
                        <div class="col-md-8" id="referral_block" style="<?php if (!empty($user_data['e_referral']) && $user_data['e_referral'] == 1) { ?>display: block; <?php } else { ?>display: none; <?php } ?>">
                          <input type="email" name="referral_email" id="referral_email" placeholder="Email" class="form-control" value="<?php if (!empty($user_data['v_referral_email'])) {
                                                                                                                                          echo $user_data['v_referral_email'];
                                                                                                                                        } ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="edu_info" class="tab-pane fade <?php echo $active_cls_edu . ' ' . $in_cls_edu ?>">
              <input type="hidden" name="update_info" value="1">

              <div class="signup_form">
                <div class="gray-border-box">
                  <div class="row">
                    <div class="col-xs-12">
                      <p class="form-head">Educational Information</p>
                    </div>
                  </div>
                  <!-- <div class="row" id ="educational_details_more"></div> -->
                  <?php
                  $i = 0;
                  if (!empty($educational_data)) {
                    $total_edu = sizeof($educational_data); ?>
                    <input type="hidden" id="educational_details_total" value="<?php echo ($total_edu - 1); ?>">
                    <input type="hidden" id="educational_total" value="<?php echo ($total_edu - 1); ?>">
                    <?php foreach ($educational_data as $row) { ?>
                      <div class="control-group after-add-more educational_block" id="educational_details<?php if ($i != 0) {
                                                                                                            echo $i;
                                                                                                          } ?>">
                        <input type="hidden" name="edu_row_id[]" value="<?php echo $row['id']; ?>">
                        <div class="row">
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label>University</label>
                              <input type="text" name="university[]" onBlur="changeMainOccupation('educational_details<?php if ($i != 0) {
                                                                                                                        echo $i;
                                                                                                                      } ?>')" id="university" value="<?php echo $row['v_university']; ?>" class="form-control">
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label>Degree</label>
                              <input type="text" name="degree[]" onBlur="changeMainOccupation('educational_details<?php if ($i != 0) {
                                                                                                                    echo $i;
                                                                                                                  } ?>')" id="degree" value="<?php echo $row['v_degree']; ?>" class="form-control">
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label>Major</label>
                              <input type="text" name="major[]" id="major" onBlur="changeMainOccupation('educational_details<?php if ($i != 0) {
                                                                                                                              echo $i;
                                                                                                                            } ?>')" value="<?php echo $row['v_major']; ?>" class="form-control">
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group" id="grad_year">
                              <label>Graduation Year</label>
                              <div class="select">
                                <select class="selectpicker" name="graduation_year[]" id="graduation_year">
                                  <option value="present" <?php if ($row['i_passing_year'] == 'present') { ?> selected <?php } ?>>Present</option>
                                  <?php for ($k = $current_year; $k >= 1950; $k--) { ?>
                                    <option <?php if ($row['i_passing_year'] == $k) { ?> selected <?php } ?> value="<?php echo $k; ?>"><?php echo $k; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                            <div class="form-group remove_button_edu">
                              <i class="fa fa-times add_detele" onclick="removeEducationalDetails('educational_details<?php if ($i != 0) {
                                                                                                                        echo $i;
                                                                                                                      } ?>')" aria-hidden="true"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php
                      $i++;
                    }
                  } else { ?>
                    <input type="hidden" id="educational_details_total" value="0">
                    <input type="hidden" id="educational_total" value="0">
                    <div class="control-group after-add-more educational_block" id="educational_details">
                      <div class="row">
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label>University</label>
                            <input type="text" name="university[]" onBlur="changeMainOccupation('educational_details')" id="university" class="form-control">
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label>Degree</label>
                            <input type="text" name="degree[]" onBlur="changeMainOccupation('educational_details')" id="degree" class="form-control">
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label>Major</label>
                            <input type="text" name="major[]" onBlur="changeMainOccupation('educational_details')" id="major" class="form-control">
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group" id="grad_year">
                            <label>Graduation Year</label>
                            <div class="select">
                              <select class="selectpicker" name="graduation_year[]" id="graduation_year">
                                <option value="present">Present</option>
                                <?php for ($k = $current_year; $k >= 1950; $k--) { ?>
                                  <option value="<?php echo $k; ?>"><?php echo $k; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                          <div class="form-group remove_button_edu">
                            <i class="fa fa-times add_detele" style="cursor: pointer;" onclick="removeEducationalDetails('educational_details')" aria-hidden="true"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php
                  }
                  ?>
                  <div class="row" id="educational_details_more"></div>
                  <div class="row" id="edu_add_more">
                    <div class="col-sm-3 pull-right">
                      <button class="btn dark-gray-btn add-more" type="button" onClick="addAnotherEducationalDetails()">Add More</button>
                    </div>
                  </div>
                </div>
                <div class="gray-border-box">
                  <div class="row">
                    <div class="col-xs-12">
                      <p class="form-head">Professional Experience</p>
                    </div>
                  </div>
                  <!-- <div class="row" id ="occupation_details_more"></div> -->
                  <?php
                  $j = 0;
                  if (!empty($professional_data)) {
                    $total_pro = sizeof($professional_data); ?>
                    <input type="hidden" id="occupation_details_total" value="<?php echo ($total_pro - 1); ?>">
                    <input type="hidden" id="occupation_total" value="<?php echo ($total_pro - 1); ?>">
                    <?php foreach ($professional_data as $row) { ?>
                      <div class="control-group after-add-more occupation_block" id="occupation_details<?php if ($j != 0) {
                                                                                                          echo $j;
                                                                                                        } ?>">
                        <input type="hidden" name="pro_row_id[]" value="<?php echo $row['id']; ?>">
                        <div class="row">
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label>Company</label>
                              <input type="text" name="company[]" onBlur="changeMainOccupation('occupation_details<?php if ($j != 0) {
                                                                                                                    echo $j;
                                                                                                                  } ?>')" id="company" value="<?php echo $row['v_company']; ?>" class="form-control">
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label>Job Title</label>
                              <input type="text" name="job_title[]" onBlur="changeMainOccupation('occupation_details<?php if ($j != 0) {
                                                                                                                      echo $j;
                                                                                                                    } ?>')" id="job_title" value="<?php echo $row['v_job_title']; ?>" class="form-control">
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group" id="industry_to">
                              <label>Industry</label>
                              <div class="select">
                                <select class="selectpicker" name="industry[]" id="industry">
                                  <?php foreach ($industry_data as $industry) { ?>
                                    <option value="<?php echo $industry['v_name']; ?>" <?php if ($row['v_industry'] == $industry['v_name']) { ?> selected <?php } ?>><?php echo $industry['v_name']; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="row">
                              <div class="col-xs-6">
                                <div class="form-group" id="cpny_from">
                                  <label>From</label>
                                  <div class="select">
                                    <select class="selectpicker" name="company_from[]" id="company_from">
                                      <?php for ($k = $current_year; $k >= 1950; $k--) { ?>
                                        <option <?php if ($row['i_company_from'] == $k) { ?> selected <?php } ?> value="<?php echo $k; ?>"><?php echo $k; ?></option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>
                              </div>
                              <div class="col-xs-6">
                                <div class="form-group" id="cpny_to">
                                  <label>To</label>
                                  <div class="select">
                                    <select class="selectpicker" name="company_to[]" id="company_to">
                                      <option <?php if ($row['i_company_to'] == 'current' || $row['i_company_to'] == 'present') { ?> selected <?php } ?> value="present">Present</option>
                                      <?php for ($k = $current_year; $k >= 1950; $k--) { ?>
                                        <option <?php if ($row['i_company_to'] == $k) { ?> selected <?php } ?> value="<?php echo $k; ?>"><?php echo $k; ?></option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group remove_button_pro">
                                <i class="fa fa-times add_detele" onClick="removeProfessionalDetails('occupation_details<?php if ($j != 0) {
                                                                                                                          echo $j;
                                                                                                                        } ?>')" aria-hidden="true"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <?php /* ?>
                    <div class="row">
                      <div class="col-sm-6 custom">
                        <div class="radio">
                          <input id="main_occupation<?php echo $j; ?>" type="radio" name="occupation" <?php if( $row['i_main_occupation'] == 1){?> checked <?php } ?> value="main_occupation<?php echo $j; ?>">
                          <label for="main_occupation<?php echo $j; ?>">Main Occupation</label>
                        </div>
                      </div>
                    </div>
                    <?php */ ?>
                      </div>
                      <?php
                      $j++;
                    }
                  } else {
                    ?>
                    <input type="hidden" id="occupation_details_total" value="0">
                    <input type="hidden" id="occupation_total" value="0">
                    <div class="control-group after-add-more occupation_block" id="occupation_details">
                      <div class="row">
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label>Company</label>
                            <input type="text" name="company[]" onBlur="changeMainOccupation('occupation_details')" id="company" class="form-control">
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label>Job Title</label>
                            <input type="text" name="job_title[]" onBlur="changeMainOccupation('occupation_details')" id="job_title" class="form-control">
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group" id="industry_to">
                            <label>Industry</label>
                            <div class="select">
                              <select class="selectpicker" name="industry[]" id="industry">
                                <?php foreach ($industry_data as $industry) { ?>
                                  <option value="<?php echo $industry['v_name']; ?>"><?php echo $industry['v_name']; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="row">
                            <div class="col-xs-6">
                              <div class="form-group" id="cpny_from">
                                <label>From</label>
                                <div class="select">
                                  <select class="selectpicker" name="company_from[]" id="company_from">
                                    <?php for ($k = $current_year; $k >= 1950; $k--) { ?>
                                      <option value="<?php echo $k; ?>"><?php echo $k; ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-xs-6">
                              <div class="form-group" id="cpny_to">
                                <label>To</label>
                                <div class="select">
                                  <select class="selectpicker" name="company_to[]" id="company_to">
                                    <option value="present">Present</option>
                                    <?php for ($k = $current_year; $k >= 1950; $k--) { ?>
                                      <option value="<?php echo $k; ?>"><?php echo $k; ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="form-group remove_button_pro">
                              <i class="fa fa-times add_detele" onClick="removeProfessionalDetails('occupation_details')" aria-hidden="true"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php /* ?>
                    <div class="row">
                      <div class="col-sm-6 custom">
                        <div class="radio">
                          <input id="main_occupation11" type="radio" name="occupation" value="main_occupation11">
                          <label for="main_occupation11">Main Occupation</label>
                        </div>
                      </div>
                    </div>
                    <?php */ ?>
                    </div>
                  <?php } ?>
                  <div class="row" id="occupation_details_more"></div>
                  <div class="row" id="occu_add_more">
                    <div class="col-sm-3 pull-right">
                      <button class="btn dark-gray-btn add-more" type="button" onClick="addAnotherOccupationDetails()">Add More</button>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6 col-xs-12">
                    <div class="gray-border-box">
                      <div class="row">
                        <div class="col-xs-12">
                          <p class="form-head">Achievements, Awards, & Honors</p>
                        </div>
                      </div>
                      <!-- <div class="row" id ="award_details_more"></div> -->
                      <?php
                      if (!empty($achievement_data)) {
                        $m = 1;
                        $total_achi = sizeof($achievement_data);
                        foreach ($achievement_data as $row) {
                          ?>
                          <input type="hidden" id="award_details_total" value="<?php echo $total_achi; ?>">
                          <div class="row" id="award_details">
                            <div class="col-xs-12" id="award<?php echo $m; ?>">
                              <input type="hidden" name="achi_row_id[]" value="<?php echo $row['id']; ?>">
                              <div class="form-group">
                                <input type="text" name="award_details[]" value="<?php echo $row['v_achievement']; ?>" class="form-control" <?php if ($m ==  1) echo "required"; ?>>
                              </div>
                              <?php if ($m != 1) { ?>
                                <div class="form-group remove_button_award">
                                  <i class="fa fa-times add_detele" style="right: 25px !important;top: 9px !important;" onClick="removeAwardDetails('award<?php echo $m; ?>')" aria-hidden="true"></i>
                                </div>
                              <?php } ?>
                            </div>
                          </div>
                          <?php
                          $m++;
                        }
                      } else {
                        ?>
                        <div class="row" id="award_details">
                          <input type="hidden" id="award_details_total" value="2">
                          <div class="col-xs-12" id="award1">
                            <div class="form-group">
                              <input type="text" name="award_details[]" class="form-control" required>
                            </div>
                          </div>
                          <div class="col-xs-12" id="award2">
                            <div class="form-group">
                              <input type="text" name="award_details[]" class="form-control">
                            </div>
                            <div class="form-group remove_button_award">
                              <i class="fa fa-times add_detele" style="right: 25px !important;top: 9px !important;" onClick="removeAwardDetails('award2')" aria-hidden="true"></i>
                            </div>
                          </div>
                        </div>
                      <?php } ?>
                      <div class="row" id="award_details_more"></div>
                      <div class="row">
                        <div class="col-xs-12 pull-right">
                          <button class="btn dark-gray-btn add-more" type="button" onClick="addAwards()">Add More</button>
                        </div>
                      </div>
                    </div>

                  </div>
                  <div class="col-sm-6 col-xs-12">
                    <div class="gray-border-box">
                      <div class="row">
                        <div class="col-xs-12">
                          <p class="form-head">Passions and interests</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12">
                          <div class="form-group">
                            <textarea class="form-control" name="passions_interests" id="passions_interests"><?php echo $t_passion_interest; ?></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-xs-12">
                    <div class="gray-border-box">
                      <div class="row">
                        <div class="col-xs-12">
                          <p class="form-head">Main Occupation</p>
                          <p class="font14 text-justify" style="margin-bottom: 10px;">Please select your main occupation from the list. You can update it anytime later by logging on to the website.</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12">
                          <div class="select">
                            <select class="selectpicker" name="main_occu" id="main_occu" required>
                              <option value="">Select Main Occupation</option>
                              <?php if (isset($educational_data) && !empty($educational_data)) {
                                $i = 0;
                                foreach ($educational_data as $row) {
                                  ?>
                                  <option value="<?php echo $row['v_university'] . ' - ' . $row['v_degree'] . ' - ' . $row['v_major']; ?>" class="educational_details<?php if ($i != 0) {
                                                                                                                                                                        echo $i;
                                                                                                                                                                      } ?>" <?php if (isset($v_main_occupation) && $v_main_occupation == $row['v_university'] . ' - ' . $row['v_degree'] . ' - ' . $row['v_major']) { ?> selected="selected" <?php } ?>><?php echo $row['v_university'] . ' - ' . $row['v_degree'] . ' - ' . $row['v_major']; ?></option>
                                  <?php
                                  $i++;
                                }
                              }
                              ?>
                              <?php if (isset($professional_data) && !empty($professional_data)) {
                                $j = 0;
                                foreach ($professional_data as $row) {
                                  ?>
                                  <option value="<?php echo $row['v_company'] . ' - ' . $row['v_job_title']; ?>" class="occupation_details<?php if ($j != 0) {
                                                                                                                                            echo $j;
                                                                                                                                          } ?>" <?php if (isset($v_main_occupation) && $v_main_occupation == $row['v_company'] . ' - ' . $row['v_job_title']) { ?> selected="selected" <?php } ?>><?php echo $row['v_company'] . ' - ' . $row['v_job_title']; ?></option>
                                  <?php
                                  $j++;
                                }
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <input type="submit" name="edit_profile" id="edit_profile" class="btn gray-btn" value="Update">
            </div>
          </div>
        </form>



      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$('.selectpicker').selectpicker({});
  
      function addAnotherEducationalDetails(){

          var clnstr = '<div class="row">'+
                      '<div class="col-sm-3">'+
                        '<div class="form-group">'+
                          '<label>University</label>'+
                          '<input type="text" name="university[]" onBlur="changeMainOccupation(\'educational_details\')" id="university" class="form-control">'+
                        '</div>'+
                      '</div>'+
                      '<div class="col-sm-3">'+
                        '<div class="form-group">'+
                          '<label>Degree</label>'+
                          '<input type="text" name="degree[]" onBlur="changeMainOccupation(\'educational_details\')" id="degree" class="form-control">'+
                        '</div>'+
                      '</div>'+
                      '<div class="col-sm-3">'+
                        '<div class="form-group">'+
                          '<label>Major</label>'+
                          '<input type="text" name="major[]" id="major" onBlur="changeMainOccupation(\'educational_details\')" class="form-control">'+
                        '</div>'+
                      '</div>'+
                      '<div class="col-sm-3">'+
                        '<div class="form-group" id="grad_year">'+
                          '<label>Graduation Year</label>'+
                          '<div class="select">'+
                            '<select class="selectpicker" name="graduation_year[]" id="graduation_year">'+
                              '<option value="present">Present</option>'+
                              '<?php for($k=$current_year;$k>=1950;$k--){ ?>'+
                              '<option value="<?php echo $k; ?>"><?php echo $k; ?></option>'+
                              '<?php } ?>'+
                            '</select>'+
                          '</div>'+
                        '</div>'+
                        '<div class="form-group remove_button_edu">'+
                          '<i class="fa fa-times add_detele" style="cursor: pointer;"  onclick="removeEducationalDetails(\'educational_details\')" aria-hidden="true"></i>'+
                        '</div>'+
                      '</div>'+
                    '</div>';                


          var parentval = $("#educational_details_total").val(); 
          var edu_parent_total = $("#educational_total").val();


          if(parseInt($("#educational_total").val()) >= 5){
            $('#edu_add_more').css('display','none');
            return false;
          } 

          var parentvalue = parseInt(parentval)+1;
          $("#educational_details_total").val(parentvalue);
          $("#educational_total").val(parseInt(edu_parent_total)+1);

          var main_occup = "main_occupation"+(parentvalue+1);
          var main_occ = clnstr.replace(/main_occupation1/g,main_occup);

          clnstr.replace(/main_occupation1/g,main_occup);

          

          var parentid = "educational_details"+parentvalue;

          var ab = '<div class="control-group after-add-more educational_block" id="educational_details" style="padding-left: 15px;margin-left: 15px;margin-right: 15px;">'+main_occ+'</div>';   
          ab = ab.replace(/educational_details/g, parentid);

          $('.remove_button_edu').css('display','block');

          $('#educational_details_more').append(ab);

          $('.selectpicker').selectpicker({});
       
      }

      function addAnotherOccupationDetails(){

            var clnstr = '<div class="row">'+
                '<div class="col-sm-3">'+
                  '<div class="form-group">'+
                    '<label>Company</label>'+
                    '<input type="text" name="company[]" onBlur="changeMainOccupation(\'occupation_details\')" id="company" class="form-control">'+
                  '</div>'+
                '</div>'+
                '<div class="col-sm-3">'+
                  '<div class="form-group">'+
                    '<label>Job Title</label>'+
                    '<input type="text" name="job_title[]" onBlur="changeMainOccupation(\'occupation_details\')" id="job_title" class="form-control">'+
                  '</div>'+
                '</div>'+
                '<div class="col-sm-3">'+
                  '<div class="form-group" id="industry_to">'+
                    '<label>Industry</label>'+
                     '<div class="select">'+
                        '<select class="selectpicker" name="industry[]" id="industry">'+
                          '<?php foreach($industry_data as $industry){ ?>'+
                          '<option value="<?php echo $industry['v_name']; ?>"><?php echo $industry['v_name']; ?></option>'+
                          '<?php } ?>'+
                        '</select>'+
                      '</div>'+
                  '</div>'+
                '</div>'+
                '<div class="col-sm-3">'+
                  '<div class="row">'+
                    '<div class="col-xs-6">'+
                      '<div class="form-group" id="cpny_from">'+
                        '<label>From</label>'+
                        '<div class="select">'+
                          '<select class="selectpicker" name="company_from[]" id="company_from">'+
                            '<?php for($k=$current_year;$k>=1950;$k--){ ?>'+
                            '<option value="<?php echo $k; ?>"><?php echo $k; ?></option>'+
                            '<?php } ?>'+
                          '</select>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '<div class="col-xs-6">'+
                      '<div class="form-group" id="cpny_to">'+
                        '<label>To</label>'+
                        '<div class="select">'+
                          '<select class="selectpicker" name="company_to[]" id="company_to">'+
                            '<option value="present">Present</option>'+
                            '<?php for($k=$current_year;$k>=1950;$k--){ ?>'+
                            '<option value="<?php echo $k; ?>"><?php echo $k; ?></option>'+
                            '<?php } ?>'+
                          '</select>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '<div class="form-group remove_button_pro">'+
                        '<i class="fa fa-times add_detele" onClick="removeProfessionalDetails(\'occupation_details\')" aria-hidden="true"></i>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>';

          var parentval = $("#occupation_details_total").val();  

          if(parseInt($("#occupation_total").val()) >= 5){
            $('#occu_add_more').css('display','none');
            return false;
          }  
          
          var parentvalue = parseInt(parentval)+1;
          $("#occupation_details_total").val(parentvalue);
          $("#occupation_total").val(parentvalue);
          var main_occup = "main_occupation"+(parentvalue+parseInt(11));
          var main_occ = clnstr.replace(/main_occupation11/g,main_occup)   
          var parentid = "occupation_details"+parentvalue;
          var ab = '<div class="control-group after-add-more occupation_block" id="occupation_details" style="padding-left: 15px;margin-left: 15px;margin-right: 15px;">'+main_occ+'</div>';   
          ab = ab.replace(/occupation_details/g, parentid);

          $('.remove_button_pro').css('display','block');

          $('#occupation_details_more').append(ab);

          $('.selectpicker').selectpicker({});
      }

      function addAwards(){

                    var parentval = $('#award_details_total').val();
                    var parentvalue = parseInt(parentval)+1;

                    var str = "<div class='col-xs-12' id='award"+parentvalue+"''>"+
                             "<div class='form-group'>"+
                             "<input type='text' name='award_details[]' id='award_details"+parentvalue+"' class='form-control' >"+
                             "</div>"+
                             "</div>";  

                    var remove_str =  '<div class="form-group remove_button_award">'+
                        '<i class="fa fa-times add_detele" style="right: 25px !important;top: 9px !important;" onclick="removeAwardDetails(\'award'+parentvalue+'\')" aria-hidden="true"></i>'+
                      '</div>';       

                    $('#award_details_total').val(parentvalue);

                    $('.remove_button_award').css('display','block');
         
                    $('#award_details_more').append(str);

                    $('#award'+parentvalue).append(remove_str);
      }

      function removeEducationalDetails(data){
        var id = '#'+data;
        var edu_total_details = $('#educational_total').val();
        if(edu_total_details < 6){
          $('#edu_add_more').css('display','block');
        }
        var details_value = parseInt(edu_total_details)-1;
        var uni_val = $(id).closest(id).find("#university").val();
        $("#main_occu option[class='"+data+"']").remove();
        $('#main_occu').selectpicker('refresh');
        $('#educational_total').val(details_value);
        $('#'+data).remove();
      }
      function removeProfessionalDetails(data){
        var id = '#'+data;
        var occu_total_details = $('#occupation_total').val();
        if(occu_total_details < 6){
          $('#occu_add_more').css('display','block');
        }
        var occu_details_value = parseInt(occu_total_details)-1;
        var pro_val = $(id).closest(id).find("#company").val();
        $("#main_occu option[class='"+data+"']").remove();
        $('#main_occu').selectpicker('refresh');  
        $('#occupation_total').val(occu_details_value);
        $('#'+data).remove();
      }
      function removeAwardDetails(data){

        var award_details_total = $('#award_details_total').val();
        var award_details_total_value = parseInt(award_details_total)-1;
        $('#award_details_total').val(award_details_total_value);
        $('#'+data).remove();
      }

      function changeMainOccupation(inObj){
        var id = '#'+inObj;
        var uni_val = $(id).closest(id).find("#university").val();
        var major_val = $(id).closest(id).find("#major").val();
        var degree_val = $(id).closest(id).find("#degree").val();
        var comp_val = $(id).closest(id).find("#company").val();
        var job_title_val = $(id).closest(id).find("#job_title").val();
        
        if( uni_val != '' && typeof(uni_val) !== 'undefined' ){
          $("#main_occu option[class='"+inObj+"']").remove();
          var html_str= $('#main_occu').html();
          if ( major_val != '' && degree_val != '' ) {  
            html_str +='<option value="'+uni_val+' - '+degree_val+' - '+major_val+'" class="'+inObj+'">'+uni_val+' - '+degree_val+' - '+major_val+'</option>';
          } else if( major_val == '' && degree_val != '' ) {
            html_str +='<option value="'+uni_val+' - '+degree_val+'" class="'+inObj+'">'+uni_val+' - '+degree_val+'</option>';
          } else if( degree_val == '' && major_val != '' ) {
            html_str +='<option value="'+uni_val+' - '+major_val+'" class="'+inObj+'">'+uni_val+' - '+major_val+'</option>';
          } else {
            html_str +='<option value="'+uni_val+'" class="'+inObj+'">'+uni_val+'</option>';
          }
          $('#main_occu').html(html_str);
          $('#main_occu').selectpicker('refresh');
        }

        if( comp_val != '' && typeof(comp_val) !== 'undefined' ){
          $("#main_occu option[class='"+inObj+"']").remove();
          var html_str= $('#main_occu').html();
          if( job_title_val != '' ){  
            html_str +='<option value="'+comp_val+' - '+job_title_val+'" class="'+inObj+'">'+comp_val+' - '+job_title_val+'</option>';
          }else{
            html_str +='<option value="'+comp_val+'" class="'+inObj+'">'+comp_val+'</option>';
          }
          $('#main_occu').html(html_str);
          $('#main_occu').selectpicker('refresh');
        }
      }

</script>
<script type="text/javascript">
  $('#referral').change(function() {
    var ref = $('#referral').val();

    if (ref == 1) {
      $('#referral_block').css('display', 'block');
    } else {
      $('#referral_block').css('display', 'none');
    }
  });
</script>
<script type="text/javascript">
  // $('#residence_country').change(function() {

  //   var val = $(this).val();

  //   var ajax_url = '<?php echo base_url(); ?>getCountryFromName';
  //   $.ajax({
  //     url: ajax_url,
  //     type: 'POST',
  //     crossDomain: true,
  //     cache: false,
  //     dataType: 'json',
  //     data: {
  //       "country": val,
  //     },
  //     success: function(data) {

  //       response_data = JSON.parse(JSON.stringify(data));

  //       if (response_data['phonecode']) {

  //         $('#tele_country_code').val('+' + response_data['phonecode']).trigger("change");
  //         $('#tele_2_country_code').val('+' + response_data['phonecode']).trigger("change");
  //       }
  //     }
  //   });
  // });

   $('#residence_country').change(function(){

    var val = $(this).val();

    var ajax_url = '<?php echo base_url(); ?>getCountryFromName';
    $.ajax({
        url:ajax_url,
        type: 'POST',
        crossDomain: true,
        cache: false,
        dataType: 'json',
        data: {
          "country":val,
        },
        success: function(data){

          response_data = JSON.parse(JSON.stringify(data));

          if(response_data['phonecode']){

              // $('#tele_country_code').val('+'+response_data['phonecode']).trigger("change");
              $('#tele_2_country_code').val('+'+response_data['phonecode']).trigger("change");
          }
        }
    });
  });
   $('#home_country').change(function(){

    var val = $(this).val();

    var ajax_url = '<?php echo base_url(); ?>getCountryFromName';
    $.ajax({
        url:ajax_url,
        type: 'POST',
        crossDomain: true,
        cache: false,
        dataType: 'json',
        data: {
          "country":val,
        },
        success: function(data){

          response_data = JSON.parse(JSON.stringify(data));

          if(response_data['phonecode']){

              $('#tele_country_code').val('+'+response_data['phonecode']).trigger("change");
             
          }
        }
    });
  });
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/client/js/bootstrap-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/client/js/custom.js"></script>



<script type="text/javascript">
  // $(function() {
  //   $('#signup_form_1').parsley().on('field:validated', function() {
  //       var ok = $('.parsley-error').length === 0;
  //       $('.bs-callout-info').toggleClass('hidden', !ok);
  //       $('.bs-callout-warning').toggleClass('hidden', ok);
  //     })
  //     .on('form:submit', function() {
  //       return false; // Don't submit form for this demo
  //     });
  // });
</script>

<script>
  function passwordVisible() {
    var x = document.getElementById("password");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }

  function confirmPasswordVisible() {
    var x = document.getElementById("confirm_password");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
</script>
<script type="text/javascript">
  $("#home_country, #residence_country,#tele_country_code,#tele_2_country_code").select2({
    allowClear: true,
    width: "resolve"
  });
</script>
<!-- <script type="text/javascript">
    var onloadCallback = function() {
      grecaptcha.render('recaptcha', {
        'sitekey' : '<?php echo $site_key; ?>'
      });
    };
</script>

<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script> -->
<script>
  $(function() {
    $("#dob").datepicker();
  });
</script>


<?php
include("inc/footer.php");
?>