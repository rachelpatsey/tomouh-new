<?php include("inc/header_top.php"); ?>
<?php include("inc/header.php"); ?>
</head>

<body>
<div class="inner_wrapper">
  <div class="why_join_page">
    <div class="sec_banner">
      <div class="container">
        <div class="page_head">
          <h1 class="red-text text-center font36"><?php if(isset($title)){ echo $title; } ?></h1>
        </div>
        <div class="why_join_content">
          <div class=""><?php if(isset($main_description)){ echo $main_description; } ?></div>
          <div class="content-box">
            <div class="row">
                <?php 
                $length = ceil(sizeof($meta)/3);
                $j=0; 
                for($i=1;$i<=$length;$i++){
                  if($j == 7 || $j > 7){
                      $j=0;
                  }
               ?>
              <div class="col-sm-3 col-xs-6 gradient_box">
               <a href="<?php echo base_url(); ?>why_join_detail/<?php echo $i; ?>">
                <div class="img_box">
                  <div class="gradient_img" style="background-image: url('<?php echo base_url(); ?>assets/images/<?php if(isset($meta['image'.$i])) echo $meta['image'.$i]; ?>');">
                    <div class="gradient_overlay <?php if($j == 0) { ?>blue_gradient <?php }else if($j == 1){ ?> pink_gradient <?php }else if($j == 2){ ?> green_gradient <?php }else if($j == 3){ ?> yellow_gradient<?php }else if($j == 4){ ?> brown_gradient <?php }else if($j == 5){ ?>sky_gradient <?php }else if($j == 6){ ?> black_gradient<?php } ?>">
                        <p class="join_text"><?php if(isset($meta['text'.$i])) echo $meta['text'.$i]; ?></p>
                    </div>
                  </div>
                </div>
               </a>
              </div>
              <?php 
                $j++;
                }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include("inc/footer.php"); ?>