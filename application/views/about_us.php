<?php
include("inc/header_top.php");
include("inc/header.php");
?>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>

<script type="text/javascript">
        jQuery(document).ready(function() {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                horizontalOrder: true,
                gutter: '.gutter-sizer'
            });
        });
    </script>
    <style type="text/css">

  strong {font-weight: 600 !important;}
  em {font-style: italic;}
  .grid-item {
  max-width:30.5%;
  width:100%;
  height: 150px;
  border-radius: 5px;
  margin-bottom:2%;
}.grid-item--height2 { height: 300px; }
.grid {
  max-width:100%;
  width:100%;
  margin-top:16px;
}
.container{
  margin-top: 20px;
  max-width:40% !important;
}
.vision_grid{
  background-color:#003c54;
  color:white;
  display:flex;
  align-items:center;
  justify-content:center;
  font-size:20px;
}
.objective_grid{
  background-color:#8f2339;
  color:white;
  display:flex;
  align-items:center;
  justify-content:center;
  font-size:20px;
}
.logo_tcl{
  background-color:#99969c;
  color:white;
  display:flex;
  align-items:center;
  justify-content:center;
}
/* clearfix */
.grid:after {
  content: '';
  display: block;
  clear: both;
}
.grid-item--height2 img{
  object-fit:cover;
  height:300px;
  max-width:100%;
}
.image_grid{
  position:relative;
}
.image_grid::before{
  background: rgba(0,0,0,0.5);
    position: absolute;
    content: "";
    top: 0px;
    right: 0px;
    left: 0px;
    bottom: 0px;
    width: 100%;
    height: 100%;
}
.grid_content {
  position: absolute;
    top: 0;
    left: 0;
    right: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 95%;
    padding: 0px 1px;
    text-align: center;
    margin-top:10px;
}
.grid_content label{
  margin-top:10px;
}
.grid_content label p span{
  /* font-size: 14px !important; */
  padding-left: 5px;
  padding-right: 5px;
  display: block;
}
.logo_tcl img{
  max-width:100%;
  width:70%;
}
.grid .grid-item:nth-child(6){
  position:relative;
}
.gutter-sizer { width: 2%; }


@media Only Screen and (max-width:1199px){
  .grid-item {
  max-width:31.5%;
  height: 140px;
}
.container{
  max-width:50% !important;
}
.grid-item--height2 { height: 280px; }
.grid-item--height2 img{
  object-fit:cover;
  height:280px;
  max-width:100%;
}
.grid_content label p span{
    font-size: 11px !important;
  }
}
@media Only Screen and (max-width:991px){
.container{
  max-width:63% !important;
}
.grid {
  margin-top:12px;
}
}
@media Only Screen and (max-width:767px){
  .container{
  max-width:84% !important;
}
.grid_content label{
  margin-top:10px;
}
.grid_content label p span{
    font-size: 8px !important;
    padding-left: 1px;
    padding-right: 1px;
    display: block;
    line-height: 8px;
}
  .grid_content label p{margin-bottom: 3px !important;}
}
@media Only Screen and (max-width:575px){
  .container{
  max-width:100% !important;
  }
.grid-item {
  max-width:32%;
  width:100%;
  height: 100px;
}
.grid {
margin-top:10px;
}
.grid-item--height2 { height: 200px; }
.grid-item--height2 img{
  object-fit:cover;
  height:200px;
  max-width:100%;
}
.grid_content label {
  overflow-x: auto;
}
.vision_grid , .objective_grid {
  font-size:15px !important;
}
/*.grid_content .mCustomScrollBox  .mCSB_container label p {
  font-size: 17px !important;
}
.grid_content .mCustomScrollBox  .mCSB_container label p span{
  font-size: 17px !important;
}*/
}
@media Only Screen and (max-width:399px){
  .container{
  max-width:100% !important;
}
.grid_content{
  margin-top:0px;
}
.grid-item {
  max-width:31.6%;
  height: 75px;
}
.grid-item--height2 { height: 150px; }
.grid-item--height2 img{
  object-fit:cover;
  height:150px;
  max-width:100%;
}
.vision_grid , .objective_grid {
  font-size:12px !important;
}
.grid_content label p span{
    font-size: 7px !important;
  }
.grid {
  margin-top:6px;
}
}

</style>


<?php
  $SITE_ABOUT_US_BLOCK2_IMG = $this->tomouh_model->getSetting('SITE_ABOUT_US_BLOCK2_IMG');
  $SITE_ABOUT_US_BLOCK4_IMG = $this->tomouh_model->getSetting('SITE_ABOUT_US_BLOCK4_IMG');
  $SITE_ABOUT_US_BLOCK5_IMG = $this->tomouh_model->getSetting('SITE_ABOUT_US_BLOCK5_IMG');
  $SITE_ABOUT_US_BLOCK6_IMG = $this->tomouh_model->getSetting('SITE_ABOUT_US_BLOCK6_IMG');
?>
<div class="inner_wrapper">
<div class="container">
        <div class="grid">
        <div class="grid-item vision_grid "><?php if(isset($meta['vision_title'])){ echo $meta['vision_title']; }?></div>
        <div class="gutter-sizer"></div>
        <div class="grid-item grid-item--height2 "><img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $SITE_ABOUT_US_BLOCK2_IMG; ?>"></div>
        <div class="grid-item objective_grid "><?php if(isset($meta['objective_title'])){echo $meta['objective_title']; }?></div>
        <div class="grid-item grid-item--height2 image_grid">
          <img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $SITE_ABOUT_US_BLOCK4_IMG; ?>">
          <div class="grid_content" data-mcs-theme="dark"><label><?php if(isset($meta['vision'])){echo $meta['vision']; }?>
          </label></div>
        </div>
        <div class="grid-item logo_tcl">
         <img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $SITE_ABOUT_US_BLOCK5_IMG; ?>">
        </div>
        <div class="grid-item grid-item--height2 image_grid">
          <img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $SITE_ABOUT_US_BLOCK6_IMG; ?>">
          <div class="grid_content" data-mcs-theme="dark" ><label><?php if(isset($meta['objective'])){echo $meta['objective']; }?>
          </label></div>
        </div>
        </div>
        </div>
</div>

<?php include('inc/footer.php') ?>