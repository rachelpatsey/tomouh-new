<?php include("inc/header.php"); ?>



<?php include("inc/header-top.php"); ?>


<!-- <script src="https://cdn.tiny.cloud/1/1bfe84h39maz6kkn3gmos2e6nkp1031rill1zfrfv33w6q4q/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> -->
<script src="https://cdn.tiny.cloud/1/1bfe84h39maz6kkn3gmos2e6nkp1031rill1zfrfv33w6q4q/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<?php include("jsfunctions/jsfunctions.php"); ?>



<div id="cl-wrapper" class="fixed-menu">

<?php include("inc/sidebar.php"); ?>

	

	<div class="container-fluid" id="pcont">

		<div class="page-head">

			<h2><?php echo $page_title;?></h2>

		</div>

		<div class="cl-mcont">

			<div class="row">

				<div class="col-md-12">

					<div class="block-flat">

						<div class="header">

							<h3> 

									

	                        	<?php echo ($script == 'list') ? 'List Of '.' '.ucfirst($page) : ucfirst($script).' '.ucfirst($page);  ?> 

								<?php if($script == 'list'){ ?>

								<a href="<?php echo base_url().'admin/pages/add/';?>" class="fright">

									<button class="btn btn-primary" type="button">Add <?php  echo ' '.ucfirst($page);?></button>

							    </a> 

							    <?php } ?>

	                        </h3>

						</div>

						<div id="response">

							<?php 

							if(!($this->form_validation->error_array())){

								if(isset($_GET['msg']) && $_GET['msg'] !=''){

									if($_GET['succ']==1){

										echo $this->messages_model->getSuccessMsg($_GET['msg']);

									}

									else if($_GET['succ']==0){

										echo $this->messages_model->getErrorMsg($_GET['msg']);

									}

								}

							}?>

                        	<?php echo validation_errors('<div class="alert alert-danger alert-white rounded">

							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>

							<div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>

						</div>

						<?php 

							if(($script == 'add') || ($script == 'edit')){ ?>

							<form role="form" action="" method="post" enctype="multipart/form-data" novalidate="">

							<div class="row">

								<div class="col-md-12">

									<div class="content">

									

                                       		<div class="form-group">

                                                <label>Page Title<span style="color:red;">*</span></label>

                                                <input type="text" placeholder="Title" class="form-control" name="v_title" value="<?php echo $v_title?>" required="required" data-parsley-trigger="keyup">

                                            </div>

                                           <?php $url = base_url().'assets/frontend/css/style.css';?>

                                            <script type="text/javascript">            

									            

                                            /*$(document).ready(function(){

                                            

                                              CKEDITOR.replace( 'l_description',{

                                                height: '400px',

                                                toolbar : 'Basic',

												contentsCss : '<?php echo $url;?>',

												autoParagraph: false

                                              });

                                            });*/

                                                       

									        </script>

                                            <div class="form-group">

                                               <label>Page Description<span style="color:red;">*</span></label>

                                                <textarea placeholder="Description" class="form-control" id="l_description" name="l_description" required="required" data-parsley-trigger="keyup"><?php echo $l_description?></textarea>

                                           	</div>



                                            <div class="form-group">

                                                <label>Meta Title<span style="color:red;">*</span></label>

                                                    <input type="text" placeholder="Meta Title" class="form-control" name="v_meta_title" value="<?php echo $v_meta_title?>" required="required" data-parsley-trigger="keyup">                

                                            </div> 



                                            <div class="form-group">

                                                <label>Meta Keyword<span style="color:red;">*</span></label>

                                                    <input type="text" placeholder="Meta Keyword" class="form-control" name="v_meta_keyword" value="<?php echo $v_meta_keyword?>" required="required" data-parsley-trigger="keyup"> 

                                            </div>



                                            <div class="form-group">

                                                <label>Meta Description<span style="color:red;">*</span></label>

                                                    <textarea rows="4" type="text" name="l_meta_description" placeholder="Meta Description" class="form-control" required="required" data-parsley-trigger="keyup"><?php echo $l_meta_description?></textarea>

                                            </div> 



                                           <div class="form-group">

												<label>Status<span style="color:red;">*</span></label>

												<select class="select2" name="e_status" id="e_status" required>

                                                	<option value="">-</option>

													<?php $this->general_model->getDropdownList(array('active','inactive'),$e_status); ?>

												</select>

											</div>

									</div>

								</div>

							</div>

							

							<?php if(!empty($meta_fields)){

								foreach($meta_fields as $meta_field){	

								?>

								<div class="row">

                                    <div class="col-md-12">

                                        <div class="block-flat">

                                        	<div class="header meta-header">

                                                <h4>

													<span class="expand-icon"><i class="fa fa-angle-down"></i></span>

													<?php echo $meta_field['v_title'];?>

                                                </h4>

                                            </div>

                                            <div class="content">

                                            	<?php if(!empty($meta_field['items'])){

													foreach($meta_field['items'] as $item){

														$meta_value = $this->admin_model->getMetaValue($item['v_key'], $id);

														if($item['v_type'] == 'text'){

														?>

                                                        	<div class="form-group">

                                                                <label><?php echo $item['v_title']?></label>

                                                                <input type="text" placeholder="<?php echo $item['v_title']?>" class="form-control" name="meta_fields[<?php echo $item['v_key'];?>]" value="<?php echo $meta_value?>" >

                                                            </div>

                                                        <?php		

														}elseif($item['v_type'] == 'textarea'){

														?>

                                                        	<div class="form-group">

                                                                <label><?php echo $item['v_title']?></label>

                                                                <textarea placeholder="<?php echo $item['v_title']?>" class="form-control" name="meta_fields[<?php echo $item['v_key'];?>]"><?php echo $meta_value?></textarea>

                                                            </div>

                                                        <?php	

														}elseif($item['v_type'] == 'editor'){

														?>

                                                        	<div class="form-group">

                                                                <label><?php echo $item['v_title']?></label>

                                                                <textarea id="editor-<?php echo $item["id"];?>" placeholder="<?php echo $item['v_title']?>" class="form-control" name="meta_fields[<?php echo $item['v_key'];?>]"><?php echo $meta_value?></textarea>

                                                            </div>

                                                            <script type="text/javascript">
																/*				
																$(document).ready(function(){

																  //initialize the javascript

																  CKEDITOR.replace( 'editor-<?php echo $item["id"];?>',{

																	height: '500px',

																	toolbar : 'Basic',

																  });

																});*/
																				
																$(document).ready(function(){
/* 
																   tinymce.init({
																	selector: '#editor-<?php echo $item["id"];?>',
																	theme: 'modern',
																	plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount  a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help a11ychecker advcode table advlist lists image media anchor hr link autoresize ', 
																	toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | undo redo | styleselect | fontsizeselect | a11ycheck | formatselect bold forecolor backcolor | bullist numlist | link image media anchor | table | code',
																	image_advtab: true,
																	fontsize_formats: "8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 24px 36px 48px 60px 72px 96px",																	
																	content_style: "body {font-size: 14px;}",
																	content_css: [
																		'//www.tinymce.com/css/codepen.min.css'
																	]
																	}); */
																	var useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;

tinymce.init({
  selector: '#editor-<?php echo $item["id"];?>',
  plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount   imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap   quickbars  emoticons advtable',

//   a11ychecker tinycomments mentions linkchecker tinymcespellchecker
/*   tinydrive_token_provider: 'URL_TO_YOUR_TOKEN_PROVIDER',
  tinydrive_dropbox_app_key: 'YOUR_DROPBOX_APP_KEY',
  tinydrive_google_drive_key: 'YOUR_GOOGLE_DRIVE_KEY',
  tinydrive_google_drive_client_id: 'YOUR_GOOGLE_DRIVE_CLIENT_ID', */
  mobile: {
    plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount   textpattern noneditable help formatpainter pageembed charmap  quickbars  emoticons advtable'
  },
  /* linkchecker mentions tinymcespellchecker a11ychecker*/
 /*  menu: {
    tc: {
      title: 'TinyComments',
      items: 'addcomment showcomments deleteallconversations'
    }
  }, */
  menubar: 'file edit view insert format tools table tc help',
  toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
  autosave_ask_before_unload: true,
  autosave_interval: '30s',
  autosave_prefix: '{path}{query}-{id}-',
  autosave_restore_when_empty: false,
  autosave_retention: '2m',
  image_advtab: true,
  link_list: [
    { title: 'My page 1', value: 'https://www.tiny.cloud' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_list: [
    { title: 'My page 1', value: 'https://www.tiny.cloud' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_class_list: [
    { title: 'None', value: '' },
    { title: 'Some class', value: 'class-name' }
  ],
  importcss_append: true,
  templates: [
        { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
    { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
    { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
  ],
  template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
  template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
  height: 600,
  image_caption: true,
  quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
  noneditable_noneditable_class: 'mceNonEditable',
  toolbar_mode: 'sliding',
  spellchecker_whitelist: ['Ephox', 'Moxiecode'],
  tinycomments_mode: 'embedded',
  content_style: '.mymention{ color: gray; }',
  contextmenu: 'link image imagetools table configurepermanentpen',
  a11y_advanced_options: true,
  skin: useDarkMode ? 'oxide-dark' : 'oxide',
  content_css: useDarkMode ? 'dark' : 'default',
  /*
  The following settings require more configuration than shown here.
  For information on configuring the mentions plugin, see:
  https://www.tiny.cloud/docs/plugins/premium/mentions/.
  */
  mentions_selector: '.mymention',
/*   mentions_fetch: mentions_fetch,
  mentions_menu_hover: mentions_menu_hover,
  mentions_menu_complete: mentions_menu_complete,
  mentions_select: mentions_select */
});

																});
															 </script>

                                                        <?php		

														}elseif($item['v_type'] == 'image'){

														?>

                                                        	<div class="form-group">

                                                                <label><?php echo $item['v_title']?></label>

                                                                <input type="file" name="meta_fields[<?php echo $item['v_key'];?>]" />

                                                                <?php if($meta_value !=''){?>

                                                                	<span class="meta-image" id="meta-image-<?php echo $item['id'];?>">

                                                                	<img src="<?php echo base_url().'assets/images/'.$meta_value?>" width="100"/><br>

                                                                	<button type="button" class="btn" onclick="delete_meta('<?php echo $item['v_key'];?>', <?php echo $id;?>, 'meta-image-<?php echo $item['id'];?>')">Delete</button>

                                                                	</span>

																<?php }?>

                                                            </div>

                                                        <?php	

														}

													}

												}?>

                                                

                                            </div>

                                        </div>

                                    </div>

                                </div>  			

								<?php 

								}

							?>



                            <script type="text/javascript">

                            function delete_meta(meta_key, post_id, element_id){

								var input_data = $('#search_data').val();

								var post_data="meta_key="+meta_key+"&post_id="+post_id;

								  

								$.ajax({

								   type: "POST",

								   url: "<?php echo base_url(); ?>admin/delete_meta_value",

								   data: post_data,

								   success: function(data) {

																				   

									$('#'+element_id).remove();

								   }

								});

							}

							$(document).ready(function(){

								$(".meta-header").click(function(){

									if($(this).find("i").hasClass("fa-angle-right")){

										$(this).find("i").removeClass("fa-angle-right");

										$(this).find("i").addClass("fa-angle-down");

									}else{

										$(this).find("i").removeClass("fa-angle-down")

										$(this).find("i").addClass("fa-angle-right");

									}

									$(this).next().toggle();

									$(this).toggleClass("hide-content");

								});

							});

							</script>

                            <style>

                            .expand-icon{width:20px;display:inline-block;}

							.meta-header.hide-content{border:none;}

                            </style>

                            <?php }?>

                            				<div class="form-group">

                                                    <button class="btn btn-primary" type="submit" name="submit_btn" value="<?php echo ($script=='edit')?'Update':'Submit';?>"><?php echo ($script=='edit')?'Update':'Submit';?> </button>

                                                    <a href="<?php echo base_url().'admin/pages/';?>">

                                                    <button class="btn fright" type="button" name="submit_btn">Cancel</button>

                                                    </a> 

                                            </div>                            

                             </form>

						<?php }else{ ?>	



							<div class="row">

								<div class="col-md-12">

									<div class="content">



									<form name="frm_pages" action="<?php echo base_url().'admin/pages/';?>" method="get">

									  	<div class="table-responsive">

											<div class="row">

												<div class="col-sm-12">

													<div class="dataTables_filter" id="datatable_filter">

														<label>

														<?php $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : ''; ?>

															<input type="text" aria-controls="datatable" class="form-control fleft" placeholder="Search" name="keyword" value="<?php echo $keyword;?>" style="width:auto;"/>

															<button type="submit" class="btn btn-primary fleft" style="margin-left:0px;"><span class="fa fa-search"></span></button>

														</label>

													</div>

                                                    <div class="pull-left">

                                                        <div id="datatable_length" class="dataTables_length">

                                                            <label>

                                                                <?php $this->paging_model->writeLimitBox(); ?>

                                                            </label>

                                                        </div>

                                                    </div>

                                                </div>

												<div class="clearfix"></div>

											</div>

										</div>

										<table class="table table-bordered" id="datatable" >

										   	<thead>

										   		<tr>

													<th width="20%">Page Title</th>

                                                    <th>Page Slug</th>

													<th>Date</th>

                                                    <th>Status</th>

													<th></th>

												</tr>

										   	</thead>

										   	<tbody>



										   <?php 

										   	if(!empty($rows)){

										   	foreach ($rows as $key => $val) { ?>

										   		<tr>

													<td><?php echo $val['v_title']; ?></td>

                                                    <td><?php echo $val['v_slug']; ?></td>

													<td><?php echo $val['d_added']; ?></td>

                                                    <td><?php echo $val['e_status']; ?></td>

													<td>

														<div class="btn-group action_btns">

															<a class="btn btn-primary btn-xs" title="Edit" href="<?php echo base_url().'admin/pages/edit/'.$val["id"].'/';?>"><span class="fa fa-edit"></span></a>



															<?php if($val['e_status'] == 'inactive'){?>

															<a class="btn btn-success btn-xs" title="Active" href="<?php echo base_url().'admin/pages/active/'.$val["id"].'/';?>"><span class="fa fa-eye"></span></a>

															<?php }else{?>



															<a class="btn btn-warning btn-xs" title="Inactive" href="<?php echo base_url().'admin/pages/inactive/'.$val["id"].'/';?>"><span class="fa fa-eye-slash"></span></a>

															<?php } ?>



															<a class="btn btn-danger btn-xs" title="Delete" href="javascript:void(0)" class="md-trigger delete-confirmation" onClick="showConfirmBox('<?php echo base_url().'admin/pages/delete/'.$val["id"].'/';?>')" ><span class="fa fa-trash"></span></a>

                                                            

                                                            <a class="btn btn-primary btn-xs" title="View" href="<?php echo base_url().'page/view/'.$val["v_slug"].'/';?>" target="_blank"><span class="fa fa-eye"></span></a>

                                                            

														</div>

													</td>

												</tr>

											<?php } }else{?>



                                                <tr><td colspan="4">No Record found.</td></tr>



                                            <?php }?>

										   </tbody>

										</table>	

										<div class="row">

											<div class="col-sm-12">

												<div class="pull-left"> <?php echo $this->paging_model->getPagesCounter();?> </div>

												<div class="pull-right">

													<div class="dataTables_paginate paging_bs_normal">

														<ul class="pagination">

															<?php $this->paging_model->writePagesLinks(); ?>

														</ul>

													</div>

												</div>

												<div class="clearfix"></div>

											</div>

										</div>

										<input type="hidden" name="a" value="<?php echo @$_REQUEST['a'];?>" />

										<input type="hidden" name="st" value="<?php echo @$_REQUEST['st'];?>" />

										<input type="hidden" name="sb" value="<?php echo @$_REQUEST['sb'];?>" />

									</form>

								</div>

								

								</div>

							</div>

						<?php } ?>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>
<!-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=oc4n1tsbxfuh9n6g3i0lyaf9tvyxadvpf5l029shmnck6bn8"></script> -->
<!-- <script src="https://cdn.tiny.cloud/1/oc4n1tsbxfuh9n6g3i0lyaf9tvyxadvpf5l029shmnck6bn8/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> -->
<!-- <script src="https://cdn.tiny.cloud/1/1bfe84h39maz6kkn3gmos2e6nkp1031rill1zfrfv33w6q4q/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> -->
  <script>




  </script>
<script>
/* tinymce.init({
  selector: '#l_description',
  height: 500,
  theme: 'modern',
  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  content_css: [
    '//www.tinymce.com/css/codepen.min.css'
  ]
 }); */
 var useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;

tinymce.init({
  selector: '#l_description',
  plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount   imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap   quickbars  emoticons advtable',

//   a11ychecker tinycomments mentions linkchecker tinymcespellchecker
/*   tinydrive_token_provider: 'URL_TO_YOUR_TOKEN_PROVIDER',
  tinydrive_dropbox_app_key: 'YOUR_DROPBOX_APP_KEY',
  tinydrive_google_drive_key: 'YOUR_GOOGLE_DRIVE_KEY',
  tinydrive_google_drive_client_id: 'YOUR_GOOGLE_DRIVE_CLIENT_ID', */
  mobile: {
    plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount   textpattern noneditable help formatpainter pageembed charmap  quickbars  emoticons advtable'
  },
  /* linkchecker mentions tinymcespellchecker a11ychecker*/
 /*  menu: {
    tc: {
      title: 'TinyComments',
      items: 'addcomment showcomments deleteallconversations'
    }
  }, */
  menubar: 'file edit view insert format tools table tc help',
  toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
  autosave_ask_before_unload: true,
  autosave_interval: '30s',
  autosave_prefix: '{path}{query}-{id}-',
  autosave_restore_when_empty: false,
  autosave_retention: '2m',
  image_advtab: true,
  link_list: [
    { title: 'My page 1', value: 'https://www.tiny.cloud' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_list: [
    { title: 'My page 1', value: 'https://www.tiny.cloud' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_class_list: [
    { title: 'None', value: '' },
    { title: 'Some class', value: 'class-name' }
  ],
  importcss_append: true,
  templates: [
        { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
    { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
    { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
  ],
  template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
  template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
  height: 600,
  image_caption: true,
  quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
  noneditable_noneditable_class: 'mceNonEditable',
  toolbar_mode: 'sliding',
  spellchecker_whitelist: ['Ephox', 'Moxiecode'],
  tinycomments_mode: 'embedded',
  content_style: '.mymention{ color: gray; }',
  contextmenu: 'link image imagetools table configurepermanentpen',
  a11y_advanced_options: true,
  skin: useDarkMode ? 'oxide-dark' : 'oxide',
  content_css: useDarkMode ? 'dark' : 'default',
  /*
  The following settings require more configuration than shown here.
  For information on configuring the mentions plugin, see:
  https://www.tiny.cloud/docs/plugins/premium/mentions/.
  */
  mentions_selector: '.mymention',
/*   mentions_fetch: mentions_fetch,
  mentions_menu_hover: mentions_menu_hover,
  mentions_menu_complete: mentions_menu_complete,
  mentions_select: mentions_select */
});
</script>