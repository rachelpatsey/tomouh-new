<?php include("inc/header.php"); ?>
<?php include("inc/header-top.php"); ?>
<?php include("jsfunctions/jsfunctions.php"); ?>
<div id="cl-wrapper" class="fixed-menu">
  <?php include("inc/sidebar.php"); ?>
  <div class="container-fluid" id="pcont">
    <div class="page-head">
      <h2><?php echo $page_title;?></h2>
    </div>
    <div class="cl-mcont">
      <div class="row">
        <div class="col-md-12">
          <div class="block-flat">
            <div class="header">
              <h3> <?php echo ($script == 'list') ? 'List Of '.' '.ucfirst($page).' Users' : ucfirst($script).' '.ucfirst($page);  ?>
                <?php if($script == 'list'){ ?>
                <a href="<?php echo base_url().'admin/photos/add/';?>" class="fright">
                <button class="btn btn-primary" type="button">Add
                <?php  echo ' '.ucfirst($page);?>
                </button>
                </a>
                <?php } ?>
              </h3>
            </div>
            <div id="response">
              <?php 
			if(!($this->form_validation->error_array())){
				if(isset($_GET['msg']) && $_GET['msg'] !=''){
					if($_GET['succ']==1){
						echo $this->messages_model->getSuccessMsg($_GET['msg']);
					}
					else if($_GET['succ']==0){
						echo $this->messages_model->getErrorMsg($_GET['msg']);
					}
				}
			}?>
<?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?> </div>
<?php 
			if(($script == 'add') || ($script == 'edit')){ 
			//echo "<pre>"; print_r($rows);
			?>
            <div class="row">
              <div class="col-md-12">
                <div class="content">
                  <form role="form" action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" value="<?php echo $rows[0]['v_slug'];?>" name="v_slug">
                    <div class="form-group required">
                      <label>Parent</label>
                      <select class="select2" name="i_parent">
                        <option value="0">Select Parent</option>
                        <?php 
							$parent_id=0;
							$current_id= $rows[0]['i_photos_id'];
							$selected_id= $rows[0]['i_parent'];
							
                            echo $this->tomouh_model->get_album_dropdown($parent_id, $current_id, $selected_id);
							
							//$albums = $this->tomouh_model->getChildPhotos(0);
                            /*foreach($albums as $album){?>
                        <option value="<?php echo $album['id']?>" <?php if($album['id'] == $rows[0]['i_parent']) echo "selected";?>> <?php echo $album['v_name']?></option>
                        <?php }*/?>
                      </select>
                    </div>
                    <div class="form-group required">
                      <label>Name<span style="color:red;">*</span></label>
                      <input type="text" placeholder="Name" class="form-control" name="v_name" value="<?php echo $rows[0]['v_name']; ?>" required/>
                    </div>
                    <div class="dropzone-previews">
                      <?php 
                        foreach ($rows as $key => $value) { ?>
                      <div class="dz-preview dz-processing dz-success dz-image-preview" id="my_img_<?php echo $value['id']?>">
                        <div class="dz-details"> <img src="<?php echo base_url().'assets/frontend/images/'.$value['v_image'];?>" style="max-width:100px;"> </div>
                        <a href="javascript:" onclick="removePhoto(this.id)" id="<?php echo $value['id']?>" class="dz-remove">Remove file</a> </div>
                      <?php	} ?>
                    </div>
                    <div class="form-group">
                      <label>Images (203⨯46)<span style="color:red;">*</span></label>
                      <div class="cl-mcont" style="">
                        <?php if($SITE_LOGO != ''){?>
                        <div class="dropzone-previews">
                          <div class="dz-preview dz-processing dz-success dz-image-preview">
                            <div class="dz-details"> <img src="<?php echo base_url().'assets/frontend/images/'.$SITE_LOGO;?>" style="max-width:100px;"> </div>
                            <a href="javascript:" id="removeLogo" class="dz-remove">Remove file</a> </div>
                        </div>
                        <?php }?>
                        <input type="hidden" name="v_image" id="v_image" value="">
                        <div action="<?php echo base_url().'admin/fileupload';?>" class="dropzone dz-clickable" id="logo-dropzone" multiple="true">
                          <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Description</label>
                      <textarea rows="4" type="text" name="l_description" placeholder="Description" class="form-control"><?php echo $rows[0]['l_description']; ?></textarea>
                    </div>
                    <div class="form-group">
                      <label>Meta Title</label>
                      <input type="text" placeholder="Meta Title" class="form-control" name="v_meta_title" value="<?php echo $rows[0]['v_meta_title']; ?>" data-parsley-trigger="keyup">
                    </div>
                    <div class="form-group">
                      <label>Meta Keyword</label>
                      <input type="text" placeholder="Meta Keyword" class="form-control" name="v_meta_keyword" value="<?php echo $rows[0]['v_meta_keyword']; ?>" data-parsley-trigger="keyup">
                    </div>
                    <div class="form-group">
                      <label>Meta Description</label>
                      <textarea rows="4" type="text" name="l_meta_description" placeholder="Meta Description" class="form-control" data-parsley-trigger="keyup"><?php echo $rows[0]['l_meta_description']; ?></textarea>
                    </div>
                    <div class="form-group">
                      <label>Status<span style="color:red;">*</span></label>
                      <select class="select2" name="e_status" id="e_status" required>
                        <option value="">-</option>
                        <?php $this->general_model->getDropdownList(array('active','inactive'),$rows[0]['e_status']); ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <button class="btn btn-primary" type="submit" name="submit_btn" value="<?php echo ($script=='edit')?'Update':'Submit';?>"><?php echo ($script=='edit')?'Update':'Submit';?></button>
                      <a href="<?php echo base_url().'admin/photos/';?>">
                      <button class="btn fright" type="button" name="submit_btn">Cancel</button>
                      </a> </div>
                  </form>
                </div>
              </div>
            </div>
            <?php }else{ ?>
            <div class="row">
              <div class="col-md-12">
                <div class="content">
                  <form name="frm_photos" action="<?php echo base_url().'admin/photos/';?>" method="get">
                    <div class="table-responsive">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="dataTables_filter" id="datatable_filter">
                            <label>
                              <?php $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : ''; ?>
                              <input type="text" aria-controls="datatable" class="form-control fleft" placeholder="Search" name="keyword" value="<?php echo $keyword;?>" style="width:auto;"/>
                              <button type="submit" class="btn btn-primary fleft" style="margin-left:0px;"><span class="fa fa-search"></span></button>
                            </label>
                          </div>
                          <div class="pull-left">
                            <div id="datatable_length" class="dataTables_length">
                              <label>
                                <?php $this->paging_model->writeLimitBox(); ?>
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <table class="table table-bordered" id="datatable" >
                      <thead>
                        <tr>
                          <th width="20%">Name</th>
                          <th>Page Slug</th>
                          <th>Status</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
										   	if(!empty($rows)){
										   	foreach ($rows as $key => $val) { ?>
                        <tr>
                          <td><?php echo $val['v_name']; ?></td>
                          <td><?php echo $val['v_slug']; ?></td>
                          <td><?php echo $val['e_status']; ?></td>
                          <td><div class="btn-group action_btns"> <a class="btn btn-primary btn-xs" title="Edit" href="<?php echo base_url().'admin/photos/edit/'.$val["id"].'/';?>"><span class="fa fa-edit"></span></a>
                              <?php if($val['e_status'] == 'inactive'){?>
                              <a class="btn btn-success btn-xs" title="Active" href="<?php echo base_url().'admin/photos/active/'.$val["id"].'/';?>"><span class="fa fa-eye"></span></a>
                              <?php }else{?>
                              <a class="btn btn-warning btn-xs" title="Inactive" href="<?php echo base_url().'admin/photos/inactive/'.$val["id"].'/';?>"><span class="fa fa-eye-slash"></span></a>
                              <?php } ?>
                              <a class="btn btn-danger btn-xs" title="Delete" href="javascript:void(0)" class="md-trigger delete-confirmation" onClick="showConfirmBox('<?php echo base_url().'admin/photos/delete/'.$val["id"].'/';?>')" ><span class="fa fa-trash"></span></a> </div></td>
                        </tr>
                        <?php } }else{?>
                        <tr>
                          <td colspan="4">No Record found.</td>
                        </tr>
                        <?php }?>
                      </tbody>
                    </table>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="pull-left"> <?php echo $this->paging_model->getPagesCounter();?> </div>
                        <div class="pull-right">
                          <div class="dataTables_paginate paging_bs_normal">
                            <ul class="pagination">
                              <?php $this->paging_model->writePagesLinks(); ?>
                            </ul>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <input type="hidden" name="a" value="<?php echo @$_REQUEST['a'];?>" />
                    <input type="hidden" name="st" value="<?php echo @$_REQUEST['st'];?>" />
                    <input type="hidden" name="sb" value="<?php echo @$_REQUEST['sb'];?>" />
                  </form>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	
function removePhoto(id){

	var redirecturl = '<?php echo base_url().'admin/removephoto/photosimage/';?>'+id;

	$.ajax({
		url: redirecturl,
		cache: false,
		success: function(html){
	    	$("#my_img_"+id).hide();
		}
	});
}
</script> 
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#logo-dropzone").addClass("dropzone");    
		new Dropzone("#logo-dropzone", {
			multiple : true,  
			acceptedFiles: ".jpeg,.jpg,.png,.gif",
			maxFilesize : 64
		}).on("complete", function(file) { 
			jQuery("#logo-dropzone .dz-success input").each(function(){
				if(file.name == $(this).val()){
					jQuery(this).attr("name", "SITE_LOGO");
					$('#v_image').val($('#v_image').val()+'::'+file.name);

                 	// $(".dropzone-previews").show();
                }
            });
		});
	});
</script> 