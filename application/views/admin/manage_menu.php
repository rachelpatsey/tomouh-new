<?php
defined('BASEPATH') OR exit('No direct script access allowed');
extract($_POST);
?>
<?php include("inc/header.php");?>
<?php include("inc/header-top.php");?>
<div id="cl-wrapper" class="fixed-menu">
    <?php include('inc/sidebar.php');?>
    <div class="container-fluid" id="pcont">
    <div class="page-head">
        <h2><?php echo $page_title;?></h2>
      </div>
    <div class="cl-mcont">
        <div id="response">
        <?php 
                if(!($this->form_validation->error_array())){
                    if(isset($_GET['msg']) && $_GET['msg'] !=''){
                        if($_GET['succ']==1){
                            echo $this->messages_model->getSuccessMsg($_GET['msg']);
                        }
                        else if($_GET['succ']==0){
                            echo $this->messages_model->getErrorMsg($_GET['msg']);
                        }
                    }
                }?>
        <?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?> </div>
        <div class="row">
        <div class="col-md-12">
            <div class="block-flat">
            <div class="header">
                <h3><?php echo ($script == 'list') ? 'List Of '.' '.ucfirst($page) : ucfirst($script).' '.ucfirst($page);  ?> 
                <?php if($script == 'list'){ ?>
                <a href="<?php echo base_url().'admin/manage_menu/add/';?>" class="fright">
                  <button class="btn btn-primary" type="button">Add <?php  echo ' '.ucfirst($page);?></button>
                  </a> 
                  <?php } ?></h3>
              </div>
            <?php 
			if(($script == 'add') || ($script == 'edit')){?>
            <form role="form" action="" method="post" parsley-validate novalidate>
                <div class="row">
                <div class="col-md-12">
                  <div class="content">
                    <div class="form-group">
                     <label>Name<span style="color:red;">*</span></label>
                      <input type="text" placeholder="Title" class="form-control" name="v_title" value="<?php echo $v_title?>" required="required" data-parsley-trigger="keyup" />
                    </div>
                   <div class="form-group">
                     <label>Link</label>
                      <input type="text" placeholder="Link" class="form-control" name="v_link" value="<?php echo $v_link?>" />
                      </div>
                    <div class="form-group">
                    	<label>Parent Manu</label>
                    	<select name="i_parent_id" class="select2">
                        	<option value="">-</option>
                            <?php foreach($manues as $sKey=>$manu){?>
                            	<option value="<?php echo $manu["id"];?>" <?php if($manu["id"] == $i_parent_id) echo 'selected=selected'?>>
									<?php echo $manu["v_title"];?>
                                </option>
							<?php }?>
                        </select>
                    </div> 
                   <div class="form-group">
                     <label>Order<span style="color:red;">*</span></label>
                      <input type="text" placeholder="Order" class="form-control" name="i_order" value="<?php echo $i_order?>" required="required" data-parsley-trigger="keyup"/>
                      </div>
                      
                    <div class="form-group">
                        <label>Status<span style="color:red;">*</span></label>
                        <select class="select2" name="e_status" id="e_status" required>
                        <option value="">-</option>
                        <?php $this->general_model->getDropdownList(array('active','inactive'),$e_status); ?>
                      </select>
                      </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" name="submit_btn" value="<?php echo ($script=='edit')?'Update':'Submit';?>"><?php echo ($script=='edit')?'Update':'Submit';?></button>
                        <a href="<?php echo $this->config->config['base_url'].'admin/manage_menu';?>">
                      <button class="btn fright" type="button" name="submit_btn">Cancel</button>
                      </a> </div>
                  </div>
                  </div>
              </div>
              </form>
            <?php 
							}else{ 
							?>
            <div class="content">
                <form name="frm" action="<?php echo $this->config->config['base_url'].'/admin/manage_menu/';?>" method="get" enctype="multipart/form-data" >
                <div class="table-responsive">
                    <div class="row">
                    <div class="col-sm-12">
                        <div class="dataTables_filter" id="datatable_filter">
                        <label>
                            <input type="text" aria-controls="datatable" class="form-control fleft" placeholder="Search" name="keyword" value="" style="width:auto;"/>
                            <button type="submit" class="btn btn-primary fleft" style="margin-left:0px;"><span class="fa fa-search"></span></button>
                          </label>
                      </div>
                        <div class="pull-left">
                        <div id="datatable_length" class="dataTables_length">
                            <label>
                            <?php $this->paging_model->writeLimitBox(); ?>
                          </label>
                          </div>
                      </div>
                      </div>
                    <div class="clearfix"></div>
                  </div>
                  </div>
                <table class="table table-bordered" id="datatable" >
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Added On</th>
                         <th>Order</th>
                        <th>Modified On</th>
                        <th>Status</th>
                        <th></th>
                      </tr>
                  </thead>
                    <tbody>
                    <?php 
												if(!empty($rows)){
												foreach($rows as $key=>$row){?>
                    <tr class="<?php echo ($key%2)?'odd':'even';?>">
                        <td><?php echo $row['v_title']?></td>
                        <td><?php echo $row['d_added']?></td>
                        <td><?php echo $row['i_order']?></td>
                        <td><?php echo $row['d_modified']?></td>
                        <td><?php echo $row['e_status']?></td>
                        <td><div class="btn-group">
                            <button class="btn btn-default btn-xs" type="button">Actions</button>
                            <button data-toggle="dropdown" class="btn btn-xs btn-primary dropdown-toggle" type="button"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                            <ul role="menu" class="dropdown-menu pull-right">
                            <li><a href="<?php echo $this->config->config['base_url'].'admin/manage_menu/edit/'.$row["id"].'/';?>">Edit</a></li>
                            <li><a href="<?php echo $this->config->config['base_url'].'admin/manage_menu/active/'.$row["id"].'/';?>">Active</a></li>
                            <li><a href="<?php echo $this->config->config['base_url'].'admin/manage_menu/inactive/'.$row["id"].'/';?>">Inactive</a></li>
                            <li><a href="<?php echo $this->config->config['base_url'].'admin/manage_menu/delete/'.$row["id"].'/';?>" onclick="return confirm('Are you sure to delete this record?');">Delete</a></li>
                          </ul>
                          </div></td>
                      </tr>
                    <?php }
												}else{?>
                    <tr>
                        <td colspan="4">No Record found.</td>
                      </tr>
                    <?php }?>
                  </tbody>
                  </table>
                <div class="row">
                    <div class="col-sm-12">
                    <div class="pull-left"> <?php echo $this->paging_model->getPagesCounter();?> </div>
                    <div class="pull-right">
                        <div class="dataTables_paginate paging_bs_normal">
                        <ul class="pagination">
                            <?php $this->paging_model->writePagesLinks(); ?>
                          </ul>
                      </div>
                      </div>
                    <div class="clearfix"></div>
                  </div>
                  </div>
                <input type="hidden" name="a" value="<?php echo @$_REQUEST['a'];?>" />
                <input type="hidden" name="st" value="<?php echo @$_REQUEST['st'];?>" />
                <input type="hidden" name="sb" value="<?php echo @$_REQUEST['sb'];?>" />
              </form>
              </div>
            <?php 
							}?>
          </div>
          </div>
      </div>
      </div>
  </div>
  </div>
<?php include("inc/footer.php");?>
