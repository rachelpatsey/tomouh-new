<?php include("inc/header.php"); ?>
</head>
<body>

  <!-- Fixed navbar -->
  <?php include("inc/header-top.php"); ?>

<div id="cl-wrapper" class="fixed-menu">
<?php include("inc/sidebar.php"); ?>

<div class="container-fluid" id="pcont">
		<div class="page-head">
			<h2><?php echo $page_title;?></h2>
		</div>
		
		<div class="cl-mcont">
			<div id="response">
				<?php 
				if(!($this->form_validation->error_array())){
					if(isset($_GET['msg']) && $_GET['msg'] !=''){
						if($_GET['succ']==1){
							echo $this->messages_model->getSuccessMsg($_GET['msg']);
						}
						else if($_GET['succ']==0){
							echo $this->messages_model->getErrorMsg($_GET['msg']);
						}
					}
				}?>
	        	<?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				<div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>
			</div>
  		<form role="form" action="<?php echo base_url().'admin/import_members';?>" method="post" novalidate enctype="multipart/form-data">
    		<div class="col-sm-6 col-md-6">
    				<div class="block-flat">
    					<div class="header">
    						<h3><?php echo $page_title;?></h3>
            	</div>
    					<div class="row">
    						<div class="col-md-12">
    							<div class="content">
                      <div class="form-group">
                          <label>Upload File<span style="color:red;">*</span></label>
                          <input type="file" name="member_file" required>	
                        </div>
                      

                      <div class="form-group">
                          <button class="btn btn-primary" type="submit" name="import_btn" value="Update">Import</button>
                      </div>	
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>	
          
  		</form>
	</div>
</div>