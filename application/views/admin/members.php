<?php
$current_year = date("Y");
?>
<?php include("inc/header.php"); ?>

<?php include("inc/header-top.php"); ?>

<?php include("jsfunctions/jsfunctions.php"); ?>
<style type="text/css">
	.select2 {
		width: 100%;
	}

	.select2-results .select2-result-label {
		padding: 7px 7px 9px 5px;
	}
</style>
<div id="cl-wrapper" class="fixed-menu">
	<?php include("inc/sidebar.php"); ?>
	<div class="container-fluid" id="pcont">
		<div class="page-head">
			<h2><?php echo $page_title; ?></h2>
		</div>
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">
					<div class="block-flat">
						<div class="header">
							<h3>
								<?php echo ($script == 'list') ? 'List Of ' . ' ' . ucfirst($page) . 's' : ucfirst($script) . ' ' . ucfirst($page);  ?>
								<!-- <?php if ($script == 'list') { ?>
											<a href="<?php echo base_url() . 'admin/members/add/'; ?>" class="fright">
												<button class="btn btn-primary" type="button">Add <?php echo ' ' . ucfirst($page); ?></button>
							    			</a> 
							    <?php } ?> -->
							</h3>
						</div>
						<div id="response">
							<?php
							if (!($this->form_validation->error_array())) {
								if (isset($_GET['msg']) && $_GET['msg'] != '') {
									if ($_GET['succ'] == 1) {
										echo $this->messages_model->getSuccessMsg($_GET['msg']);
									} else if ($_GET['succ'] == 0) {
										echo $this->messages_model->getErrorMsg($_GET['msg']);
									}
								}
							} ?>
							<?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							<div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>'); ?>
						</div>
						<?php
						if (($script == 'add') || ($script == 'edit')) { ?>

							<div class="row">
								<div class="col-md-12">
									<div class="content">
										<form role="form" action="" method="post" enctype="multipart/form-data">
											<div class="form-group">
												<label>First Name<span style="color:red;">*</span></label>
												<input type="text" placeholder="Firstname" class="form-control" id="v_firstname" name="v_firstname" value="<?php echo isset($members_data['v_firstname']) ? $members_data['v_firstname'] : ''; ?>" required="required" data-parsley-trigger="keyup">
											</div>

											<div class="form-group">
												<label>Last Name<span style="color:red;">*</span></label>
												<input type="text" placeholder="Lastname" class="form-control" name="v_lastname" value="<?php echo isset($members_data['v_lastname']) ? $members_data['v_lastname'] : ''; ?>" required="required" data-parsley-trigger="keyup">
											</div>

											<div class="form-group">
												<label>Date of Birth<span style="color:red;">*</span></label>
												<input placeholder="Date of Birth" data-date-format="yyyy-mm-dd" class="form-control datetime" type="text" name="d_dob" value="<?php echo isset($members_data['d_dob']) ? $members_data['d_dob'] : ''; ?>" required="required" data-parsley-trigger="keyup">
											</div>

											<div class="form-group">
												<label>Gender<span style="color:red;">*</span></label>
												<select class="select2" name="e_gender" id="e_gender" required="">
													<option value="">-</option>
													<?php $this->general_model->getDropdownList(array('Male', 'Female'), $members_data['e_gender']); ?>
												</select>
											</div>

											<div class="form-group">
												<label>Email<span style="color:red;">*</span></label>
												<input type="email" placeholder="Email" class="form-control" name="v_email" value="<?php echo isset($members_data['v_email']) ? $members_data['v_email'] : ''; ?>" required="required" data-parsley-trigger="keyup">
											</div>

											<div class="form-group">
												<label>Password</label>
												<input type="password" placeholder="Password" class="form-control" name="v_password" minlength="6" value="" maxlength="12">
											</div>

											<div class="form-group">
												<label>Home Country</label>
												<select class="select2" name="v_home_country">
													<option value=" ">Select Country</option>
													<?php foreach ($countries as $country) { ?>
														<option <?php if (isset($members_data)) {
																			if (!empty($members_data['v_home_country']) && $members_data['v_home_country'] == $country->country_name) { ?> selected <?php }
																																																																																	} ?>><?php echo $country->country_name; ?></option>
													<?php } ?>
												</select>
											</div>

											<div class="form-group">
												<label>Home City</label>
												<input type="text" placeholder="Home City" class="form-control" name="v_home_city" value="<?php echo isset($members_data['v_home_city']) ? $members_data['v_home_city'] : ''; ?>">
											</div>

											<div class="form-group">
												<label>Country of Residence</label>
												<select class="select2" name="v_residence_country">
													<option value="">Select Country</option>
													<?php foreach ($countries as $country) { ?>
														<option <?php if (isset($members_data)) {
																			if (!empty($members_data['v_residence_country']) && $members_data['v_residence_country'] == $country->country_name) { ?> selected <?php }
																																																																																						} ?>><?php echo $country->country_name; ?></option>
													<?php } ?>
												</select>
											</div>

											<div class="form-group">
												<label>City of Residence</label>
												<input type="text" placeholder="City of Residence" class="form-control" name="v_residence_city" value="<?php echo isset($members_data['v_residence_city']) ? $members_data['v_residence_city'] : ''; ?>">
											</div>

											<div class="form-group">
												<label>Mobile Number</label>
												<input type="text" placeholder="Mobile Number" class="form-control" name="v_telephone" value="<?php echo isset($members_data['v_telephone']) ? $members_data['v_telephone'] : ''; ?>">
											</div>

											<div class="form-group">
												<label>Mobile Number 2</label>
												<input type="text" placeholder="Other Mobile Number" class="form-control" name="v_telephone_2" value="<?php echo isset($members_data['v_telephone_2']) ? $members_data['v_telephone_2'] : ''; ?>">
											</div>
											<div class="form-group">
												<label>Last Profile update</label>
												<?php if(isset($members_data['d_last_profile_update']) && $members_data['d_last_profile_update'] != '0000-00-00' ) 
															$l_p_date= $members_data['d_last_profile_update']; 
												?>
												<input placeholder="Last profile update" data-date-format="yyyy-mm-dd" id="d_last_profile_update" class="form-control" type="text" name="d_last_profile_update" value="<?php echo $l_p_date;?>">
									</div>

											<div class="row">
												<div class="col-sm-3 col-xs-12">
													<div class="form-group">
														<label>Twitter Link</label>
														<input type="text" placeholder="Twitter Link" name="v_twitter_link" id="v_twitter_link" class="form-control" value="<?php echo isset($members_data['v_twitter_link']) ? $members_data['v_twitter_link'] : ''; ?>">
													</div>
												</div>

												<div class="col-sm-3 col-xs-12">
													<div class="form-group">
														<label>Linkedin Link</label>
														<input type="text" placeholder="Linkedin Link" name="v_linkedin_link" id="v_linkedin_link" class="form-control" value="<?php echo isset($members_data['v_linkedin_link']) ? $members_data['v_linkedin_link'] : ''; ?>">
													</div>
												</div>

												<div class="col-sm-3 col-xs-12">
													<div class="form-group">
														<label>Instagram Link</label>
														<input type="text" placeholder="Instagram Link" name="v_instagram_link" id="v_instagram_link" class="form-control" value="<?php echo isset($members_data['v_instagram_link']) ? $members_data['v_instagram_link'] : ''; ?>">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-6 col-xs-12">
													<div class="form-group">
														<label>Were you referred to by a member?</label>
														<div class="row" style="margin-top: 0px;">
															<div class="select col-sm-4 col-xs-12">
																<select class="select2" name="referral" id="referral">
																	<option value="0" <?php if (!empty($members_data['e_referral']) && $members_data['e_referral'] == 0) { ?> selected <?php } ?>>No</option>
																	<option value="1" <?php if (!empty($members_data['e_referral']) && $members_data['e_referral'] == 1) { ?> selected <?php } ?>>Yes</option>
																</select>
															</div>
															<div class="col-md-8" id="referral_block" style="<?php if (!empty($members_data['e_referral']) && $members_data['e_referral'] == 1) { ?>display: block; <?php } else { ?>display: none; <?php } ?>">
																<input type="email" name="referral_email" id="referral_email" value="<?php if (!empty($members_data['v_referral_email'])) {
																																																				echo $members_data['v_referral_email'];
																																																			} ?>" placeholder="Email" class="form-control">
															</div>
														</div>
													</div>
												</div>
											
											</div>
											<p>&nbsp;</p>
											<div class="header">
												<h3>Educational Information</h3>
											</div>

											<?php
											$i = 0;
											if (!empty($educational_data)) {
												$total_edu = sizeof($educational_data); ?>
												<input type="hidden" id="educational_details_total" value="<?php echo ($total_edu - 1); ?>">
												<input type="hidden" id="educational_total" value="<?php echo ($total_edu - 1); ?>">
												<?php foreach ($educational_data as $row) { ?>
													<div class="control-group after-add-more educational_block" id="educational_details<?php if ($i != 0) {
																																																								echo $i;
																																																							} ?>">
														<input type="hidden" name="edu_row_id[]" value="<?php echo $row->id; ?>">
														<div class="row">
															<div class="col-sm-3">
																<div class="form-group">
																	<label>University</label>
																	<input type="text" name="university[]" onBlur="changeMainOccupation('educational_details<?php if ($i != 0) {
																																																														echo $i;
																																																													} ?>')" id="university" value="<?php echo $row->v_university; ?>" class="form-control">
																</div>
															</div>
															<div class="col-sm-3">
																<div class="form-group">
																	<label>Degree</label>
																	<input type="text" name="degree[]" onBlur="changeMainOccupation('educational_details<?php if ($i != 0) {
																																																												echo $i;
																																																											} ?>')" id="degree" value="<?php echo $row->v_degree; ?>" class="form-control">
																</div>
															</div>
															<div class="col-sm-3">
																<div class="form-group">
																	<label>Major</label>
																	<input type="text" name="major[]" id="major" onBlur="changeMainOccupation('educational_details<?php if ($i != 0) {
																																																																	echo $i;
																																																																} ?>')" value="<?php echo $row->v_major; ?>" class="form-control">
																</div>
															</div>
															<div class="col-sm-3">
																<div class="form-group" id="grad_year">
																	<div class="remove_button_edu" style="position: absolute;right: 0;">
																		<i class="fa fa-times add_detele" onclick="removeEducationalDetails('educational_details<?php if ($i != 0) {
																																																															echo $i;
																																																														} ?>')" aria-hidden="true"></i>
																	</div>
																	<label>Graduation Year</label>
																	<div class="select">
																		<select class="select2" name="graduation_year[]" id="graduation_year">
																			<option value="present" <?php if ($row->i_passing_year == 'present') { ?> selected <?php } ?>>Present</option>
																			<?php for ($k = $current_year; $k >= 1950; $k--) { ?>
																				<option <?php if ($row->i_passing_year == $k) { ?> selected <?php } ?> value="<?php echo $k; ?>"><?php echo $k; ?></option>
																			<?php } ?>
																		</select>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<?php
													$i++;
												}
											} else { ?>
												<input type="hidden" id="educational_details_total" value="0">
												<input type="hidden" id="educational_total" value="0">
												<div class="control-group after-add-more educational_block" id="educational_details">
													<div class="row">
														<div class="col-sm-3">
															<div class="form-group">
																<label>University</label>
																<input type="text" name="university[]" onBlur="changeMainOccupation('educational_details')" id="university" class="form-control">
															</div>
														</div>
														<div class="col-sm-3">
															<div class="form-group">
																<label>Degree</label>
																<input type="text" name="degree[]" onBlur="changeMainOccupation('educational_details')" id="degree" class="form-control">
															</div>
														</div>
														<div class="col-sm-3">
															<div class="form-group">
																<label>Major</label>
																<input type="text" name="major[]" onBlur="changeMainOccupation('educational_details')" id="major" class="form-control">
															</div>
														</div>
														<div class="col-sm-3">
															<div class="form-group" id="grad_year">
																<div class="remove_button_edu" style="position: absolute;right: 0;">
																	<i class="fa fa-times add_detele" onclick="removeEducationalDetails('educational_details')" aria-hidden="true"></i>
																</div>
																<label>Graduation Year</label>
																<div class="select">
																	<select class="select2" name="graduation_year[]" id="graduation_year">
																		<option value="present">Present</option>
																		<?php for ($k = $current_year; $k >= 1950; $k--) { ?>
																			<option value="<?php echo $k; ?>"><?php echo $k; ?></option>
																		<?php } ?>
																	</select>
																</div>
															</div>
														</div>
													</div>
												</div>
											<?php
											}
											?>
											<div class="row" id="educational_details_more"></div>
											<div class="row" id="edu_add_more">
												<div class="col-sm-3 pull-right">
													<button class="btn dark-gray-btn add-more" type="button" onclick="addAnotherEducationalDetails()">Add More</button>
												</div>
											</div>
											<p>&nbsp;</p>
											<div class="gray-border-box">
												<div class="header">
													<h3>Professional Experience Information</h3>
												</div>
												<?php
												$j = 0;
												if (!empty($professional_data)) {
													$total_pro = sizeof($professional_data); ?>
													<input type="hidden" id="occupation_details_total" value="<?php echo ($total_pro - 1); ?>">
													<input type="hidden" id="occupation_total" value="<?php echo ($total_pro - 1); ?>">
													<?php foreach ($professional_data as $row) { ?>
														<div class="control-group after-add-more occupation_block" id="occupation_details<?php if ($j != 0) {
																																																								echo $j;
																																																							} ?>">
															<input type="hidden" name="pro_row_id[]" value="<?php echo $row->id; ?>">
															<div class="row">
																<div class="col-sm-3">
																	<div class="form-group">
																		<label>Company</label>
																		<input type="text" name="company[]" onBlur="changeMainOccupation('occupation_details<?php if ($j != 0) {
																																																													echo $j;
																																																												} ?>')" id="company" value="<?php echo $row->v_company; ?>" class="form-control">
																	</div>
																</div>
																<div class="col-sm-3">
																	<div class="form-group">
																		<label>Job Title</label>
																		<input type="text" name="job_title[]" onBlur="changeMainOccupation('occupation_details<?php if ($j != 0) {
																																																														echo $j;
																																																													} ?>')" id="job_title" value="<?php echo $row->v_job_title; ?>" class="form-control">
																	</div>
																</div>
																<div class="col-sm-3">
																	<div class="form-group" id="industry_to">
																		<label>Industry</label>
																		<div class="select">
																			<select class="select2" name="industry[]" id="industry">
																				<?php foreach ($industry_data as $industry) { ?>
																					<option value="<?php echo $industry['v_name']; ?>" <?php if ($row->v_industry == $industry['v_name']) { ?> selected <?php } ?>><?php echo $industry['v_name']; ?></option>
																				<?php } ?>
																			</select>
																		</div>
																	</div>
																</div>
																<div class="col-sm-3">
																	<div class="row" style="margin-top: 0;">
																		<div class="col-xs-6">
																			<div class="form-group" id="cpny_from">
																				<label>From</label>
																				<div class="select">
																					<select class="select2" name="company_from[]" id="company_from">
																						<?php for ($k = $current_year; $k >= 1950; $k--) { ?>
																							<option <?php if ($row->i_company_from == $k) { ?> selected <?php } ?> value="<?php echo $k; ?>"><?php echo $k; ?></option>
																						<?php } ?>
																					</select>
																				</div>
																			</div>
																		</div>
																		<div class="col-xs-6">
																			<div class="form-group" id="cpny_to">
																				<div class="remove_button_pro" style="position: absolute;right: 0;">
																					<i class="fa fa-times add_detele" onClick="removeProfessionalDetails('occupation_details<?php if ($j != 0) {
																																																																		echo $j;
																																																																	} ?>')" aria-hidden="true"></i>
																				</div>
																				<label>To</label>
																				<div class="select">
																					<select class="select2" name="company_to[]" id="company_to">
																						<option <?php if ($row->i_company_to == 'current' || $row->i_company_to == 'present') { ?> selected <?php } ?> value="present">Present</option>
																						<?php for ($k = $current_year; $k >= 1950; $k--) { ?>
																							<option <?php if ($row->i_company_to == $k) { ?> selected <?php } ?> value="<?php echo $k; ?>"><?php echo $k; ?></option>
																						<?php } ?>
																					</select>
																				</div>
																			</div>
																		</div>
																		<div class="clearfix"></div>

																	</div>
																</div>
															</div>
															<?php /* ?>
												<div class="row">
													<div class="col-sm-6 custom">
														<div class="radio">
															<input id="main_occupation<?php echo $j; ?>" type="radio" name="occupation" <?php if( $row['i_main_occupation'] == 1){?> checked <?php } ?> value="main_occupation<?php echo $j; ?>">
															<label for="main_occupation<?php echo $j; ?>">Main Occupation</label>
														</div>
													</div>
												</div>
												<?php */ ?>
														</div>
														<?php
														$j++;
													}
												} else {
													?>
													<input type="hidden" id="occupation_details_total" value="0">
													<input type="hidden" id="occupation_total" value="0">
													<div class="control-group after-add-more occupation_block" id="occupation_details">
														<div class="row">
															<div class="col-sm-3">
																<div class="form-group">
																	<label>Company</label>
																	<input type="text" name="company[]" onBlur="changeMainOccupation('occupation_details')" id="company" class="form-control">
																</div>
															</div>
															<div class="col-sm-3">
																<div class="form-group">
																	<label>Job Title</label>
																	<input type="text" name="job_title[]" onBlur="changeMainOccupation('occupation_details')" id="job_title" class="form-control">
																</div>
															</div>
															<div class="col-sm-3">
																<div class="form-group" id="industry_to">
																	<label>Industry</label>
																	<div class="select">
																		<select class="select2" name="industry[]" id="industry">
																			<?php foreach ($industry_data as $industry) { ?>
																				<option value="<?php echo $industry['v_name']; ?>"><?php echo $industry['v_name']; ?></option>
																			<?php } ?>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-sm-3">
																<div class="row">
																	<div class="col-xs-6">
																		<div class="form-group" id="cpny_from">
																			<label>From</label>
																			<div class="select">
																				<select class="select2" name="company_from[]" id="company_from">
																					<?php for ($k = $current_year; $k >= 1950; $k--) { ?>
																						<option value="<?php echo $k; ?>"><?php echo $k; ?></option>
																					<?php } ?>
																				</select>
																			</div>
																		</div>
																	</div>
																	<div class="col-xs-6">
																		<div class="form-group" id="cpny_to">
																			<div class="remove_button_pro" style="position: absolute;right: 0;">
																				<i class="fa fa-times add_detele" onClick="removeProfessionalDetails('occupation_details')" aria-hidden="true"></i>
																			</div>
																			<label>To</label>
																			<div class="select">
																				<select class="select2" name="company_to[]" id="company_to">
																					<option value="present">Present</option>
																					<?php for ($k = $current_year; $k >= 1950; $k--) { ?>
																						<option value="<?php echo $k; ?>"><?php echo $k; ?></option>
																					<?php } ?>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<?php /* ?>
												<div class="row">
													<div class="col-sm-6 custom">
														<div class="radio">
															<input id="main_occupation11" type="radio" name="occupation" value="main_occupation11">
															<label for="main_occupation11">Main Occupation</label>
														</div>
													</div>
												</div>
												<?php */ ?>
													</div>
												<?php } ?>
												<div class="row" id="occupation_details_more"></div>
												<div class="row" id="occu_add_more">
													<div class="col-sm-3 pull-right">
														<button class="btn dark-gray-btn add-more" type="button" onclick="addAnotherOccupationDetails()">Add More</button>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="header">
													<h3>Achievements, Awards & Honours</h3>
												</div>
												<p>&nbsp;</p>
												<?php
												if (!empty($achievement_data)) {
													$m = 1;
													$total_achi = sizeof($achievement_data);
													foreach ($achievement_data as $row) {
														?>
														<input type="hidden" id="award_details_total" value="<?php echo $total_achi; ?>">
														<div class="row" id="award_details" style="margin-top: 0px !important;">
															<div class="col-xs-12" id="award<?php echo $m; ?>">
																<div class="input-group">
																	<input type="text" name="award_details[]" value="<?php echo $row->v_achievement; ?>" class="form-control">
																	<span class="input-group-btn">
																		<button class="btn btn-secondary" onClick="removeAwardDetails('award<?php echo $m; ?>')" type="button">X</button>
																	</span>
																</div>

																<!-- <div class="form-group remove_button_award">
								                       									<i class="fa fa-times add_detele" style="right: 25px !important;top: 9px !important;"  aria-hidden="true"></i>
								                    									</div> -->
															</div>
														</div>
														<?php
														$m++;
													}
												} else {
													?>
													<div class="row" id="award_details" style="margin-top: 0px !important;">
														<input type="hidden" id="award_details_total" value="2">
														<div class="col-xs-12" id="award1">
															<div class="input-group">
																<input type="text" name="award_details[]" class="form-control">
																<span class="input-group-btn">
																	<button class="btn btn-secondary" onClick="removeAwardDetails('award1')" type="button">X</button>
																</span>
															</div>
														</div>
														<div class="col-xs-12" id="award2">
															<div class="input-group">
																<input type="text" name="award_details[]" class="form-control">
																<span class="input-group-btn">
																	<button class="btn btn-secondary" onClick="removeAwardDetails('award2')" type="button">X</button>
																</span>
															</div>
														</div>
													</div>
												<?php } ?>
												<div class="row" id="award_details_more" style="margin-top: 0px; "></div>
												<div class="row">
													<div class="col-xs-12 pull-right">
														<button class="btn dark-gray-btn add-more" type="button" onclick="addAwards()">Add More</button>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="header">
													<h3>Passions and interests</h3>
												</div>

												<div class="row">
													<div class="col-xs-12">
														<div class="form-group">
															<textarea class="form-control" name="passions_interests" id="passions_interests"><?php echo isset($members_data['t_passion_interest']) ? $members_data['t_passion_interest'] : ''; ?>
  																	</textarea>
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="header">
													<h3>Main Occupations</h3>
												</div>

												<div class="row">
													<div class="col-xs-12">
														<div class="select">
															<select class="select2" name="main_occu" id="main_occu">
																<option value="">Select Main Occupation</option>
																<?php if (isset($educational_data) && !empty($educational_data)) {
																	$i = 0;
																	foreach ($educational_data as $row) {
																		?>
																		<option value="<?php echo $row->v_university . ' - ' . $row->v_degree . ' - ' . $row->v_major; ?>" class="educational_details<?php if ($i != 0) {
																																																																																		echo $i;
																																																																																	} ?>" <?php if (isset($members_data['v_main_occupation']) && $members_data['v_main_occupation'] == $row->v_university . ' - ' . $row->v_degree . ' - ' . $row->v_major) { ?> selected="selected" <?php } ?>><?php echo $row->v_university . ' - ' . $row->v_degree . ' - ' . $row->v_major; ?></option>
																		<?php
																		$i++;
																	}
																}
																?>
																<?php if (isset($professional_data) && !empty($professional_data)) {
																	$j = 0;
																	foreach ($professional_data as $row) {
																		?>
																		<option value="<?php echo $row->v_company . ' - ' . $row->v_job_title; ?>" class="occupation_details<?php if ($j != 0) {
																																																																					echo $j;
																																																																				} ?>" <?php if (isset($members_data['v_main_occupation']) && $members_data['v_main_occupation'] == $row->v_company . ' - ' . $row->v_job_title) { ?> selected="selected" <?php } ?>><?php echo $row->v_company . ' - ' . $row->v_job_title; ?></option>
																		<?php
																		$j++;
																	}
																}
																?>
															</select>
														</div>
													</div>
												</div>
											</div>
									</div>

									<div class="form-group">
										<label>Member of Month</label><br>
										<label class="checkbox-inline">
											<input type="checkbox" name="e_member_of_month" value="1" <?php if ($members_data['e_member_of_month'] == 1) { ?> checked="checked" <?php } ?>>Yes
										</label>
									</div>

									<div class="form-group">
										<label>Month Text</label>
										<input type="text" placeholder="Firstname" class="form-control" id="v_month_text" name="v_month_text" value="<?php echo isset($members_data['v_month_text']) ? $members_data['v_month_text'] : ''; ?>">

										</label>
									</div>

									<div class="form-group">
										<label>Status</label>
										<select class="select2" name="e_status" id="e_status" required="">
											<option value="">-</option>
											<?php $this->general_model->getDropdownList(array('active', 'inactive'), $members_data['e_status']); ?>
										</select>
									</div>

									<?php if (isset($payment_data) && !empty($payment_data)) { ?>
										<div class="form-group">
											<label>Payment Data</label>
											<table style="border: 1px solid;">
												<tr>
													<td>Payment Type</td>
													<td>Subscription Id</td>
													<td>Payment Date</td>
												</tr>
												<?php
												foreach ($payment_data as $pay) {
													?>
													<tr>
														<td><?php echo $pay['e_payment_type']; ?></td>
														<td><?php echo $pay['v_subscription_id']; ?></td>
														<td><?php echo $pay['d_subscription_date']; ?></td>
													</tr>
												<?php
												}
												?>
											</table>
										</div>
									<?php } ?>
									<div class="form-group">
										<button class="btn btn-primary" type="submit" name="submit_btn" value="<?php echo ($script == 'edit') ? 'Update' : 'Submit'; ?>"><?php echo ($script == 'edit') ? 'Update' : 'Submit'; ?></button>
										<a href="<?php echo base_url() . 'admin/members/'; ?>">
											<button class="btn fright" type="button" name="submit_btn">Cancel</button>
										</a>
									</div>
									</form>
								</div>
							</div>
						</div>
					<?php } else { ?>

						<div class="row">
							<div class="col-md-12">
								<div class="content">

									<form name="frm_members" action="<?php echo base_url() . 'admin/members/'; ?>" method="get">
										<div class="table-responsive">
											<div class="row" style="margin-top: 0px !important;">
												<div class="col-sm-2 pull-left" style="margin-right: 4px;">
													<a href="<?php echo base_url() . 'admin/delete_all_members/' ?>" onClick="return confirm('Are you sure to delete all members?')">
														<input type="button" class="form-control btn btn-primary" value="Delete All Members">
													</a>
												</div>

												<div class="col-sm-2 pull-right" style="margin-right: 4px;">
													<input type="submit" name="export_members" id="export_members" class="form-control btn btn-primary" value="Export">
												</div>
												<div class="col-sm-12">
													<div class="dataTables_filter" id="datatable_filter">
														<label>
															<?php $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : ''; ?>
															<input type="text" aria-controls="datatable" class="form-control fleft" placeholder="Search" name="keyword" value="<?php echo $keyword; ?>" style="width:auto;" />
															<button type="submit" class="btn btn-primary fleft" style="margin-left:0px;"><span class="fa fa-search"></span></button>
														</label>
													</div>
													<div class="pull-left">
														<div id="datatable_length" class="dataTables_length">
															<label>
																<?php $this->paging_model->writeLimitBox(); ?>
															</label>
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
										<table class="table table-bordered" id="datatable">
											<thead>
												<tr>
													<th>Name</th>
													<th>Email</th>
													<th>Member of Month</th>
													<th>Plan Type</th>
													<th>Subscription Expiry Date</th>
													<th>Last Profile Updated</th>
													<th>Status</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
						
												<?php
												if (!empty($rows)) {
													foreach ($rows as $key => $val) { ?>
														<tr>
															<td><?php echo $val['v_firstname'] . ' ' . $val['v_lastname']; ?></td>
															<td><?php echo $val['v_email']; ?></td>
															<?php
															$member = "";
															if ($val['e_member_of_month'] == 1) {

																$member = "Yes";
															} else {

																$member = "No";
															}
															?>
															<td><?php echo $member; ?></td>
															<td><?php echo isset($val['e_plan_type']) && $val['e_plan_type'] == 'free' ? 'Free' : 'Paid'; ?></td>
															<td><?php echo $val['d_subscription_exp_date']; ?></td>
															<td><?php echo $val['d_last_profile_update']; ?></td>
															<td><?php echo $val['e_status']; ?></td>
															<td>
																<div class="btn-group action_btns">
																	<a class="btn btn-primary btn-xs" title="Edit" href="<?php echo base_url() . 'admin/members/edit/' . $val["id"] . '/'; ?>"><span class="fa fa-edit"></span></a>

																	<?php if ($val['e_status'] == 'inactive') { ?>
																		<a class="btn btn-success btn-xs" title="Active" href="<?php echo base_url() . 'admin/members/active/' . $val["id"] . '/'; ?>"><span class="fa fa-eye"></span></a>
																	<?php } else { ?>

																		<a class="btn btn-warning btn-xs" title="Inactive" href="<?php echo base_url() . 'admin/members/inactive/' . $val["id"] . '/'; ?>"><span class="fa fa-eye-slash"></span></a>
																	<?php } ?>

																	<a class="btn btn-danger btn-xs" title="Delete" href="javascript:void(0)" class="md-trigger delete-confirmation" onClick="showConfirmBox('<?php echo base_url() . 'admin/members/delete/' . $val["id"] . '/'; ?>')"><span class="fa fa-trash"></span></a>
																</div>
															</td>
														</tr>
													<?php }
												} else { ?>

													<tr>
														<td colspan="8">No Record found.</td>
													</tr>

												<?php } ?>
											</tbody>
										</table>
										<div class="row">
											<div class="col-sm-12">
												<div class="pull-left"> <?php echo $this->paging_model->getPagesCounter(); ?> </div>
												<div class="pull-right">
													<div class="dataTables_paginate paging_bs_normal">
														<ul class="pagination">
															<?php $this->paging_model->writePagesLinks(); ?>
														</ul>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
										<input type="hidden" name="a" value="<?php echo @$_REQUEST['a']; ?>" />
										<input type="hidden" name="st" value="<?php echo @$_REQUEST['st']; ?>" />
										<input type="hidden" name="sb" value="<?php echo @$_REQUEST['sb']; ?>" />
										<input type="hidden" name="status" value="<?php echo isset($_GET['status']) ? $_GET['status'] : ''; ?>" />
									</form>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
	$('#referral').change(function() {
		var ref = $('#referral').val();

		if (ref == 1) {
			$('#referral_block').css('display', 'block');
		} else {
			$('#referral_block').css('display', 'none');
		}
	});
</script>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>

<script type="text/javascript">
  
      function addAnotherEducationalDetails(){

          var clnstr = '<div class="row">'+
                      '<div class="col-sm-3">'+
                        '<div class="form-group">'+
                          '<label>University</label>'+
                          '<input type="text" name="university[]" onBlur="changeMainOccupation(\'educational_details\')" id="university" class="form-control">'+
                        '</div>'+
                      '</div>'+
                      '<div class="col-sm-3">'+
                        '<div class="form-group">'+
                          '<label>Degree</label>'+
                          '<input type="text" name="degree[]" onBlur="changeMainOccupation(\'educational_details\')" id="degree" class="form-control">'+
                        '</div>'+
                      '</div>'+
                      '<div class="col-sm-3">'+
                        '<div class="form-group">'+
                          '<label>Major</label>'+
                          '<input type="text" name="major[]" id="major" onBlur="changeMainOccupation(\'educational_details\')" class="form-control">'+
                        '</div>'+
                      '</div>'+
                      '<div class="col-sm-3">'+
                        '<div class="form-group" id="grad_year">'+
                        '<div class="remove_button_edu" style="position: absolute;right: 0;">'+
                          '<i class="fa fa-times add_detele" style="cursor: pointer;"  onclick="removeEducationalDetails(\'educational_details\')" aria-hidden="true"></i>'+
                        '</div>'+
                          '<label>Graduation Year</label>'+
                          '<div class="select">'+
                            '<select class="select2" name="graduation_year[]" id="graduation_year">'+
                              '<option value="present">Present</option>'+
                              '<?php for($k=$current_year;$k>=1950;$k--){ ?>'+
                              '<option value="<?php echo $k; ?>"><?php echo $k; ?></option>'+
                              '<?php } ?>'+
                            '</select>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                    '</div>';                


          var parentval = $("#educational_details_total").val(); 
          var edu_parent_total = $("#educational_total").val();


          if(parseInt($("#educational_total").val()) >= 5){
            $('#edu_add_more').css('display','none');
            return false;
          } 

          var parentvalue = parseInt(parentval)+1;
          $("#educational_details_total").val(parentvalue);
          $("#educational_total").val(parseInt(edu_parent_total)+1);

          var main_occup = "main_occupation"+(parentvalue+1);
          var main_occ = clnstr.replace(/main_occupation1/g,main_occup);

          clnstr.replace(/main_occupation1/g,main_occup);

          

          var parentid = "educational_details"+parentvalue;

          var ab = '<div class="control-group after-add-more educational_block" id="educational_details" style="padding-left: 15px;margin-left: 15px;margin-right: 15px;">'+main_occ+'</div>';   
          ab = ab.replace(/educational_details/g, parentid);

          $('.remove_button_edu').css('display','block');

          $('#educational_details_more').append(ab);

          $("select").select2("destroy").select2();
       
      }

      function addAnotherOccupationDetails(){

            var clnstr = '<div class="row">'+
                '<div class="col-sm-3">'+
                  '<div class="form-group">'+
                    '<label>Company</label>'+
                    '<input type="text" name="company[]" onBlur="changeMainOccupation(\'occupation_details\')" id="company" class="form-control">'+
                  '</div>'+
                '</div>'+
                '<div class="col-sm-3">'+
                  '<div class="form-group">'+
                    '<label>Job Title</label>'+
                    '<input type="text" name="job_title[]" onBlur="changeMainOccupation(\'occupation_details\')" id="job_title" class="form-control">'+
                  '</div>'+
                '</div>'+
                '<div class="col-sm-3">'+
                  '<div class="form-group" id="industry_to">'+
                    '<label>Industry</label>'+
                     '<div class="select">'+
                        '<select class="select2" name="industry[]" id="industry">'+
                          '<?php foreach($industry_data as $industry){ ?>'+
                          '<option value="<?php echo $industry['v_name']; ?>"><?php echo $industry['v_name']; ?></option>'+
                          '<?php } ?>'+
                        '</select>'+
                      '</div>'+
                  '</div>'+
                '</div>'+
                '<div class="col-sm-3">'+
                    '<div class="col-xs-6">'+
                      '<div class="form-group" id="cpny_from">'+
                        '<label>From</label>'+
                        '<div class="select">'+
                          '<select class="select2" name="company_from[]" id="company_from">'+
                            '<?php for($k=$current_year;$k>=1950;$k--){ ?>'+
                            '<option value="<?php echo $k; ?>"><?php echo $k; ?></option>'+
                            '<?php } ?>'+
                          '</select>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '<div class="col-xs-6">'+
                      '<div class="form-group" id="cpny_to">'+
                      	'<div class="remove_button_pro" style="position: absolute;right: 0;">'+
                        '<i class="fa fa-times add_detele" onClick="removeProfessionalDetails(\'occupation_details\')" aria-hidden="true"></i>'+
                    	'</div>'+
                        '<label>To</label>'+
                        '<div class="select">'+
                          '<select class="select2" name="company_to[]" id="company_to">'+
                            '<option value="present">Present</option>'+
                            '<?php for($k=$current_year;$k>=1950;$k--){ ?>'+
                            '<option value="<?php echo $k; ?>"><?php echo $k; ?></option>'+
                            '<?php } ?>'+
                          '</select>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                '</div>'+
              '</div>';

          var parentval = $("#occupation_details_total").val();  

          if(parseInt($("#occupation_total").val()) >= 5){
            $('#occu_add_more').css('display','none');
            return false;
          }  
          
          var parentvalue = parseInt(parentval)+1;
          $("#occupation_details_total").val(parentvalue);
          $("#occupation_total").val(parentvalue);
          var main_occup = "main_occupation"+(parentvalue+parseInt(11));
          var main_occ = clnstr.replace(/main_occupation11/g,main_occup)   
          var parentid = "occupation_details"+parentvalue;
          var ab = '<div class="control-group after-add-more occupation_block" id="occupation_details" style="padding-left: 15px;margin-left: 15px;margin-right: 15px;">'+main_occ+'</div>';   
          ab = ab.replace(/occupation_details/g, parentid);

          $('.remove_button_pro').css('display','block');

          $('#occupation_details_more').append(ab);

          $("select").select2("destroy").select2();
      }

      function addAwards(){

                    var parentval = $('#award_details_total').val();
                    var parentvalue = parseInt(parentval)+1;

                    var str = '<div class="col-xs-12" id="award'+parentvalue+'">'+
										'<div class="input-group">'+
							      			'<input type="text" name="award_details[]"  value="<?php echo $row->v_achievement; ?>" class="form-control" >'+
							      			'<span class="input-group-btn">'+
							        			'<button class="btn btn-secondary" onClick="removeAwardDetails(\'award'+parentvalue+'\')" type="button">X</button>'+
							      			'</span>'+
							    		'</div>'+
							    	'<div>';
                         
                    // var remove_str =  '<div class="form-group remove_button_award">'+
                    //     '<i class="fa fa-times add_detele" style="right: 25px !important;top: 9px !important;" onclick="removeAwardDetails(\'award'+parentvalue+'\')" aria-hidden="true"></i>'+
                    //   '</div>';       

                    $('#award_details_total').val(parentvalue);

                    $('.remove_button_award').css('display','block');
         
                    $('#award_details_more').append(str);

                    $('#award'+parentvalue).append(remove_str);
      }

      function removeEducationalDetails(data){
	        var id = '#'+data;
	        var edu_total_details = $('#educational_total').val();
	        if(edu_total_details < 6){
	          $('#edu_add_more').css('display','block');
	        }
	        var details_value = parseInt(edu_total_details)-1;
	        var uni_val = $(id).closest(id).find("#university").val();
	        $("#main_occu option[class='"+data+"']").remove();
	        $("#main_occu").select2("destroy").select2();
	        $('#educational_total').val(details_value);
	        $('#'+data).remove();
      }
      function removeProfessionalDetails(data){
	        var id = '#'+data;
	        var occu_total_details = $('#occupation_total').val();
	        if(occu_total_details < 6){
	          $('#occu_add_more').css('display','block');
	        }
	        var occu_details_value = parseInt(occu_total_details)-1;
	        var pro_val = $(id).closest(id).find("#company").val();
	        $("#main_occu option[class='"+data+"']").remove();
	        $("#main_occu").select2("destroy").select2();  
	        $('#occupation_total').val(occu_details_value);
	        $('#'+data).remove();
      }
      function removeAwardDetails(data){

        var award_details_total = $('#award_details_total').val();
        var award_details_total_value = parseInt(award_details_total)-1;
        $('#award_details_total').val(award_details_total_value);
        $('#'+data).remove();
      }

      function changeMainOccupation(inObj){
        var id = '#'+inObj;
        var uni_val = $(id).closest(id).find("#university").val();
        var major_val = $(id).closest(id).find("#major").val();
        var degree_val = $(id).closest(id).find("#degree").val();
        var comp_val = $(id).closest(id).find("#company").val();
        var job_title_val = $(id).closest(id).find("#job_title").val();
        
        if( uni_val != '' && typeof(uni_val) !== 'undefined' ){
          $("#main_occu option[class='"+inObj+"']").remove(); 
          var html_str= $('#main_occu').html();
          if ( major_val != '' && degree_val != '' ) {  
            $("#main_occu").append($("<option></option>").attr("value", uni_val+' - '+degree_val+' - '+major_val ).attr("class", inObj ).text(uni_val+' - '+degree_val+' - '+major_val));
          } else if( major_val == '' && degree_val != '' ) {
          	$("#main_occu").append($("<option></option>").attr("value", uni_val+' - '+degree_val ).attr("class", inObj ).text(uni_val+' - '+degree_val));
          } else if( degree_val == '' && major_val != '' ) {
          	$("#main_occu").append($("<option></option>").attr("value", uni_val+' - '+major_val ).attr("class", inObj ).text(uni_val+' - '+major_val));
          } else {
          	$("#main_occu").append($("<option></option>").attr("value", uni_val ).attr("class", inObj ).text(uni_val));
          }

        }

        if( comp_val != '' && typeof(comp_val) !== 'undefined' ){
          $("#main_occu option[class='"+inObj+"']").remove();
          var html_str= $('#main_occu').html();
          if( job_title_val != '' ){  
            $("#main_occu").append($("<option></option>").attr("value", comp_val+' - '+job_title_val ).attr("class", inObj ).text(comp_val+' - '+job_title_val));
          }else{
            $("#main_occu").append($("<option></option>").attr("value", comp_val ).attr("class", inObj ).text(comp_val));
          }
        }

        $("select").select2("destroy").select2();
      }

</script>
<script>
    $( function() {
      $( "#d_last_profile_update" ).datepicker({"setDate": new Date(),
        "autoclose": true});
    } );
  </script>