<?php include("inc/header.php"); ?>



<?php include("inc/header-top.php"); ?>



<?php include("jsfunctions/jsfunctions.php"); ?>



<div id="cl-wrapper" class="fixed-menu">

<?php include("inc/sidebar.php"); ?>

<?php

	$e_type_arr = [

		'Honorary' => 'Honorary Members',

		'Team' => 'Team',

		'Membership' => 'Membership Committee',

		'CouncilofSeniorAdvisers' => 'Council of Senior Advisers',

		'Board' => 'Board Members',

	];

?>

	

	<div class="container-fluid" id="pcont">

		<div class="page-head">

			<h2><?php echo $page_title;?></h2>

		</div>

		<div class="cl-mcont">

			<div class="row">

				<div class="col-md-12">

					<div class="block-flat">

						<div class="header">

							<h3> 

									

	                        	<?php echo ($script == 'list') ? 'List Of '.' '.ucfirst($page) : ucfirst($script).' '.ucfirst($page);  ?> 

								<?php if($script == 'list'){ ?>

								<a href="<?php echo base_url().'admin/team/add/';?>" class="fright">

									<button class="btn btn-primary" type="button">Add <?php  echo ' '.ucfirst($page);?></button>

							    </a> 

							    <?php } ?>

	                        </h3>

						</div>

						<div id="response">

							<?php 

							if(!($this->form_validation->error_array())){

								if(isset($_GET['msg']) && $_GET['msg'] !=''){

									if($_GET['succ']==1){

										echo $this->messages_model->getSuccessMsg($_GET['msg']);

									}

									else if($_GET['succ']==0){

										echo $this->messages_model->getErrorMsg($_GET['msg']);

									}

								}

							}?>

                        	<?php echo validation_errors('<div class="alert alert-danger alert-white rounded">

							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>

							<div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>

						</div>

						<?php 

							if(($script == 'add') || ($script == 'edit')){ ?>



							<div class="row">

								<div class="col-md-12">

									<div class="content">

										<form role="form" action="" method="post" enctype="multipart/form-data">

											<div class="form-group">

												<label>Type<span style="color:red;">*</span></label>

													<select class="select2" name="e_type" id="e_type" required>

                                                    	<option value="">-</option>

														<?php $this->general_model->getDropdownListKey($e_type_arr,$e_type); ?>

													</select>

											</div>

											<div class="form-group">

												<label>Name<span style="color:red;">*</span></label>

													<input type="text"  placeholder="Name" class="form-control" name="v_name" value="<?php echo $v_name; ?>" required/>

											</div>

											<div class="form-group">

												<label>Position<span style="color:red;">*</span></label>

													<input type="text"  placeholder="Position" class="form-control" name="v_position" value="<?php echo $v_position; ?>" required/>

											</div>

											<?php if($v_image != '' && $script == 'edit'){?>

											<div class="dropzone-previews" id="my_img_<?php echo $id ?>" >

	                                            <div class="dz-preview dz-processing dz-success dz-image-preview">

			                                        <div class="dz-details"> <img src="<?php echo base_url().'assets/frontend/images/'.$v_image;?>" style="max-width:100px;">

			                                        </div>

			                                        <a href="javascript:" onclick="removePhoto(this.id)" id="<?php echo $id ?>" class="dz-remove">Remove file</a> 

			                                    </div>

		                                    </div>

		                                    <?php }?>

		                                    

											<div class="form-group">

                          						<label>Image (360*345)<span style="color:red;">*</span></label>

                      							<div class="cl-mcont" style="">

                      								<input type="hidden" name="v_image" id="v_image" value="">

                      								<div action="<?php echo base_url().'admin/fileuploadteam';?>" class="dropzone dz-clickable" id="image-upload-dropzone" multiple="false">

                      									<div class="dz-default dz-message"><span>Drop files here to upload</span></div>

                     	 							</div>



                     	 						</div>

                      						</div>

											<?php $url = base_url().'assets/frontend/css/style.css';?>

                                            <?php $content = $l_description;?>

                                            <?php /*?><script type="text/javascript">   

	                                            $(document).ready(function(){

	                                            	

	                                              	CKEDITOR.replace( 'l_description',{

		                                                height: '400px',

		                                                toolbar : 'Basic',

														contentsCss : '<?php echo $url;?>',

	                                              	});



	                                            });

									        </script>

                                            <div class="form-group">

                                              	<label>Description<span style="color:red;">*</span></label>

                                                <textarea placeholder="Description" class="form-control" id="l_description" name="l_description" required="required" data-parsley-trigger="keyup"><?php echo $content?></textarea>

                                           	</div><?php */?>
                                            
                                            <div class="form-group">

                                              	<label>Description<span style="color:red;">*</span></label>

                                                <textarea placeholder="Description" class="form-control" id="l_description" name="l_description" required="required" data-parsley-trigger="keyup"><?php echo $content?></textarea>

                                           	</div>

                                           	<div class="form-group">

												<label>Order<span style="color:red;">*</span></label>

													<input type="number"  placeholder="Order" class="form-control" name="i_order" value="<?php echo $i_order; ?>" required/>

											</div>



											<div class="form-group">

												<label>Status<span style="color:red;">*</span></label>

												<select class="select2" name="e_status" id="e_status" required>

                                                	<option value="">-</option>

													<?php $this->general_model->getDropdownList(array('active','inactive'),$e_status); ?>

												</select>

											</div>

											<div class="form-group">

												<button class="btn btn-primary" type="submit" name="submit_btn" value="<?php echo ($script=='edit')?'Update':'Submit';?>"><?php echo ($script=='edit')?'Update':'Submit';?></button>

												<a href="<?php echo base_url().'admin/team/';?>">

													<button class="btn fright" type="button" name="submit_btn">Cancel</button>

												</a> 

											</div>

										</form>

									</div>

								</div>

							</div>

						<?php }else{ ?>



							<div class="row">

								<div class="col-md-12">

									<div class="content">



										<form name="frm_team" id="frm_team" action="<?php echo base_url().'admin/team/';?>" method="get">

										  	<div class="table-responsive">

												<div class="row">

													<div class="col-sm-12">

														<div class="dataTables_filter" id="datatable_filter">

															<label>

																<?php $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : ''; ?>

																<input type="text" aria-controls="datatable" class="form-control fleft" placeholder="Search" name="keyword" value="<?php echo $keyword;?>" style="width:auto;"/>

																<button type="submit" class="btn btn-primary fleft" style="margin-left:0px;"><span class="fa fa-search"></span></button>

															</label>

														</div>

	                                                    <div class="pull-left">

	                                                        <div id="datatable_length" class="dataTables_length">

	                                                            <label>

	                                                                <?php $this->paging_model->writeLimitBox(); ?>

	                                                            </label>

	                                                            <label style="padding-left: 20px;">

	                                                            	Member:

	                                                            	

	                                                            	<select name='members_type' id="members_type"  size='1' aria-controls='datatable' class='select2 wd100' style=''>



	                                                            	<?php

	                                                            		if ( isset($member_types) && !empty($member_types) ) {

	                                                            			foreach ( $member_types as $key => $value) {



	                                                            				$selected = '';

	                                                            				if( isset($selected_member_type) && $key == $selected_member_type ) $selected = 'selected="selected"';

	                                                            				?>

	                                                            					<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $value; ?></option>

	                                                            				<?php

	                                                            			}

	                                                            		}

	                                                            	?>

	                                                            	</select>

	                                                            </label>

	                                                        </div>

	                                                    </div>

	                                                </div>

													<div class="clearfix"></div>

												</div>

											</div>

											<table class="table table-bordered" id="datatable" >

											   	<thead>

											   		<tr>

														<th>Type</th>

	                                                    <th width="15%">Name</th>

	                                                    <th width="10%">Position</th>

	                                                    <th>Image</th>

	                                                    <th width="30%">Description</th>

	                                                    <th>Status</th>

														<th></th>

													</tr>

											   	</thead>

											   	<tbody>



											   <?php 

											   	if(!empty($rows)){

											   	foreach ($rows as $key => $val) { ?>

											   		<tr>

											   			<?php 

											   				$user_image = "";	

															if($val['v_image']!=""){

																$v_image = base_url().'assets/frontend/images/'.$val['v_image'];

															}else{

																$v_image = base_url().'assets/frontend/images/no-image.png';

															}

											   			?>

														<td><?php echo isset($e_type_arr[$val['e_type']]) ? $e_type_arr[$val['e_type']] : $val['e_type']; ?></td>

														<td><?php echo $val['v_name']; ?></td>

														<td><?php echo $val['v_position']; ?></td>

	                                                    <td align="center">

															<a><img src="<?php echo $v_image; ?>" width="100"></a>

														</td>

														<td><?php echo $val['l_description']; ?></td>

	                                                    <td><?php echo $val['e_status']; ?></td>

														<td>

															<div class="btn-group action_btns">

																<a class="btn btn-primary btn-xs" title="Edit" href="<?php echo base_url().'admin/team/edit/'.$val["id"].'/';?>"><span class="fa fa-edit"></span></a>



																<?php if($val['e_status'] == 'inactive'){?>

																<a class="btn btn-success btn-xs" title="Active" href="<?php echo base_url().'admin/team/active/'.$val["id"].'/';?>"><span class="fa fa-eye"></span></a>

																	<?php }else{?>



																<a class="btn btn-warning btn-xs" title="Inactive" href="<?php echo base_url().'admin/team/inactive/'.$val["id"].'/';?>"><span class="fa fa-eye-slash"></span></a>

																	<?php } ?>



																<a class="btn btn-danger btn-xs" title="Delete" href="javascript:void(0)" class="md-trigger delete-confirmation" onClick="showConfirmBox('<?php echo base_url().'admin/team/delete/'.$val["id"].'/';?>')" ><span class="fa fa-trash"></span></a>

															</div>

														</td>

													</tr>

												<?php } }else{?>



	                                                <tr><td colspan="4">No Record found.</td></tr>



	                                            <?php }?>

											   </tbody>

											</table>	

											<div class="row">

												<div class="col-sm-12">

													<div class="pull-left"> <?php echo $this->paging_model->getPagesCounter();?> </div>

													<div class="pull-right">

														<div class="dataTables_paginate paging_bs_normal">

															<ul class="pagination">

																<?php $this->paging_model->writePagesLinks(); ?>

															</ul>

														</div>

													</div>

													<div class="clearfix"></div>

												</div>

											</div>

											<input type="hidden" name="a" value="<?php echo @$_REQUEST['a'];?>" />

											<input type="hidden" name="st" value="<?php echo @$_REQUEST['st'];?>" />

											<input type="hidden" name="sb" value="<?php echo @$_REQUEST['sb'];?>" />

										</form>

									</div>

								</div>

							</div>

						<?php } ?>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<!-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=oc4n1tsbxfuh9n6g3i0lyaf9tvyxadvpf5l029shmnck6bn8"></script> -->
<script src="https://cdn.tiny.cloud/1/oc4n1tsbxfuh9n6g3i0lyaf9tvyxadvpf5l029shmnck6bn8/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
/* tinymce.init({
  selector: '#l_description',
  height: 500,
  theme: 'modern',
  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  content_css: [
    '//www.tinymce.com/css/codepen.min.css'
  ]
 }); */
 var useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;

tinymce.init({
  selector: '#l_description',
  plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount   imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap   quickbars  emoticons advtable',

//   a11ychecker tinycomments mentions linkchecker tinymcespellchecker
/*   tinydrive_token_provider: 'URL_TO_YOUR_TOKEN_PROVIDER',
  tinydrive_dropbox_app_key: 'YOUR_DROPBOX_APP_KEY',
  tinydrive_google_drive_key: 'YOUR_GOOGLE_DRIVE_KEY',
  tinydrive_google_drive_client_id: 'YOUR_GOOGLE_DRIVE_CLIENT_ID', */
  mobile: {
    plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount   textpattern noneditable help formatpainter pageembed charmap  quickbars  emoticons advtable'
  },
  /* linkchecker mentions tinymcespellchecker a11ychecker*/
 /*  menu: {
    tc: {
      title: 'TinyComments',
      items: 'addcomment showcomments deleteallconversations'
    }
  }, */
  menubar: 'file edit view insert format tools table tc help',
  toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
  autosave_ask_before_unload: true,
  autosave_interval: '30s',
  autosave_prefix: '{path}{query}-{id}-',
  autosave_restore_when_empty: false,
  autosave_retention: '2m',
  image_advtab: true,
  link_list: [
    { title: 'My page 1', value: 'https://www.tiny.cloud' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_list: [
    { title: 'My page 1', value: 'https://www.tiny.cloud' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_class_list: [
    { title: 'None', value: '' },
    { title: 'Some class', value: 'class-name' }
  ],
  importcss_append: true,
  templates: [
        { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
    { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
    { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
  ],
  template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
  template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
  height: 600,
  image_caption: true,
  quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
  noneditable_noneditable_class: 'mceNonEditable',
  toolbar_mode: 'sliding',
  spellchecker_whitelist: ['Ephox', 'Moxiecode'],
  tinycomments_mode: 'embedded',
  content_style: '.mymention{ color: gray; }',
  contextmenu: 'link image imagetools table configurepermanentpen',
  a11y_advanced_options: true,
  skin: useDarkMode ? 'oxide-dark' : 'oxide',
  content_css: useDarkMode ? 'dark' : 'default',
  /*
  The following settings require more configuration than shown here.
  For information on configuring the mentions plugin, see:
  https://www.tiny.cloud/docs/plugins/premium/mentions/.
  */
  mentions_selector: '.mymention',
/*   mentions_fetch: mentions_fetch,
  mentions_menu_hover: mentions_menu_hover,
  mentions_menu_complete: mentions_menu_complete,
  mentions_select: mentions_select */
});
</script>

<script type="text/javascript">
function removePhoto(id){
	var redirecturl = '<?php echo base_url().'admin/removephotoupdate/team/';?>'+id;
	$.ajax({
		url: redirecturl,
		cache: false,
		success: function(html){

	    	$("#my_img_"+id).hide();

		}

	});

}

</script>



<script type="text/javascript">

	

	jQuery(document).ready(function(){

		jQuery("#image-upload-dropzone").addClass("dropzone");    

		var myDropzone = new Dropzone("#image-upload-dropzone", {

			multiple : false,  

			maxFiles:1, 

			acceptedFiles: ".jpeg,.jpg,.png,.gif",
			maxFilesize : 64
		}).on("complete", function(file) { 

		

			jQuery("#image-upload-dropzone .dz-success input").each(function(){

				if(file.name == $(this).val()){

					jQuery(this).attr("name", "v_image");

					$('#v_image').val(file.name);



                 	// $(".dropzone-previews").show();

                }

            });

			

			 var $button = $('<a href="#" class="js-open-cropper-modal btn btn-xs btn-success" data-file-name="' + file.name + '" style="margin-top:10px;">Crop & Upload</a>');

            $(file.previewElement).append($button);

		});

		

var dataURItoBlob = function (dataURI) {

    var byteString = atob(dataURI.split(',')[1]);

    var ab = new ArrayBuffer(byteString.length);

    var ia = new Uint8Array(ab);

    for (var i = 0; i < byteString.length; i++) {

        ia[i] = byteString.charCodeAt(i);

    }

    return new Blob([ab], {type: 'image/jpeg'});

};



Dropzone.autoDiscover = false;

var c = 0;

		

$('#image-upload-dropzone').on('click', '.js-open-cropper-modal', function (e) {

    e.preventDefault();

    var fileName = $(this).data('file-name');



    var modalTemplate =

        '<div class="modal fade" tabindex="-1" role="dialog">' +

        '<div class="modal-dialog modal-lg" role="document">' +

        '<div class="modal-content">' +

        '<div class="modal-header">' +

        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +

        '<h4 class="modal-title">Crop</h4>' +

        '</div>' +

        '<div class="modal-body">' +

        '<div class="image-container">' +

        '<img id="img-' + ++c + '" src="<?php echo $this->config->config['base_url'];?>assets/frontend/images/' + fileName + '">' +

        '</div>' +

        '</div>' +

        '<div class="modal-footer">' +

        '<button type="button" class="btn btn-warning rotate-left"><span class="fa fa-rotate-left"></span></button>' +

        '<button type="button" class="btn btn-warning rotate-right"><span class="fa fa-rotate-right"></span></button>' +

        '<button type="button" class="btn btn-warning scale-x" data-value="-1"><span class="fa fa-arrows-h"></span></button>' +

        '<button type="button" class="btn btn-warning scale-y" data-value="-1"><span class="fa fa-arrows-v"></span></button>' +

        '<button type="button" class="btn btn-warning reset"><span class="fa fa-refresh"></span></button>' +

        '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +

        '<button type="button" class="btn btn-primary crop-upload">Crop & upload</button>' +

        '</div>' +

        '</div>' +

        '</div>' +

        '</div>';



    var $cropperModal = $(modalTemplate);



    $cropperModal.modal('show').on("shown.bs.modal", function () {

        var cropper = new Cropper(document.getElementById('img-' + c), {

            autoCropArea: 1,

            movable: true,

            cropBoxResizable: true,

            rotatable: true,

			aspectRatio: 360/345,

			

        });

        var $this = $(this);

        $this

            .on('click', '.crop-upload', function () {

                // get cropped image data

                var blob = cropper.getCroppedCanvas().toDataURL();

                // transform it to Blob object

                var croppedFile = dataURItoBlob(blob);

                croppedFile.name = fileName;



                var files = myDropzone.getAcceptedFiles();

                for (var i = 0; i < files.length; i++) {

                    var file = files[i];

                    if (file.name === fileName) {

                        myDropzone.removeFile(file);

                    }

                }

                myDropzone.addFile(croppedFile);

                $this.modal('hide');

            })

            .on('click', '.rotate-right', function () {

                cropper.rotate(90);

            })

            .on('click', '.rotate-left', function () {

                cropper.rotate(-90);

            })

            .on('click', '.reset', function () {

                cropper.reset();

            })

            .on('click', '.scale-x', function () {

                var $this = $(this);

                cropper.scaleX($this.data('value'));

                $this.data('value', -$this.data('value'));

            })

            .on('click', '.scale-y', function () {

                var $this = $(this);

                cropper.scaleY($this.data('value'));

                $this.data('value', -$this.data('value'));

            });

    });

});

	});

	

	$('#members_type').on('change', function() {

		$('#frm_team').submit();

	});



</script>