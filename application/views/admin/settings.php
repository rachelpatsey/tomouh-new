<?php include("inc/header.php"); ?>
</head>
<body>

  <!-- Fixed navbar -->
  <?php include("inc/header-top.php"); ?>

<div id="cl-wrapper" class="fixed-menu">
<?php include("inc/sidebar.php"); ?>

<div class="container-fluid" id="pcont">
		<div class="page-head">
			<h2><?php echo $page_title;?></h2>
		</div>
		
		<div class="cl-mcont">
			<div id="response">
				<?php 
				if(!($this->form_validation->error_array())){
					if(isset($_GET['msg']) && $_GET['msg'] !=''){
						if($_GET['succ']==1){
							echo $this->messages_model->getSuccessMsg($_GET['msg']);
						}
						else if($_GET['succ']==0){
							echo $this->messages_model->getErrorMsg($_GET['msg']);
						}
					}
				}?>
	        	<?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				<div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>
			</div>
  		<form role="form" action="<?php echo base_url().'admin/settings/update';?>" method="post" novalidate>
    			<div class="col-sm-6 col-md-6">
    				<div class="block-flat">
    					<div class="header">
    						<h3><?php echo $page_title;?></h3>
            	</div>
    					<div class="row">
    						<div class="col-md-12">
    							<div class="content">
                      <div class="form-group">
                          <label>Site Name<span style="color:red;">*</span></label>
                          <input type="text" placeholder="Site Name" class="form-control" name="SITE_NAME" value="<?php echo $SITE_NAME?>" required data-parsley-trigger="keyup">
                        </div>
                      <div class="form-group">
                          <label>Site URl<span style="color:red;">*</span></label>
                          <input type="text" placeholder="Site Url" class="form-control" name="SITE_URL" value="<?php echo $SITE_URL?>" required data-parsley-trigger="keyup">
                        </div>
                      <div class="form-group">
                          <label>Conatct Info<span style="color:red;">*</span></label>
                          <input type="email" placeholder="Conatct Info" class="form-control" name="CONTACT_INFO" value="<?php echo $CONTACT_INFO?>" required data-parsley-trigger="keyup">
                      </div>
                     
                      <div class="form-group">
                          <label>Site Logo (203⨯46)<span style="color:red;">*</span></label>
                          <div class="cl-mcont" style="">
                          	<?php if($SITE_LOGO != ''){?>
                              <div class="dropzone-previews">
                                  <div class="dz-preview dz-processing dz-success dz-image-preview">
                                <div class="dz-details"> <img src="<?php echo base_url().'assets/frontend/images/'.$SITE_LOGO;?>" style="max-width:100px;"> </div>
                                <a href="javascript:" id="removeLogo" class="dz-remove">Remove file</a> </div>
                            	</div>
                            	<?php }?>
                              <div action="<?php echo base_url().'admin/fileupload';?>" class="dropzone dz-clickable" id="logo-dropzone" multiple="false">
                              	<div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                              </div>
                          <script type="text/javascript">
                              jQuery(document).ready(function(){
                                  jQuery("#logo-dropzone").addClass("dropzone");    
                                  new Dropzone("#logo-dropzone", {
                                    multiple : true,  
                                    maxFiles:1, 
                                    acceptedFiles: ".jpeg,.jpg,.png,.gif",
									                  maxFilesize : 64
                                    }).on("complete", function(file) { 
                                      jQuery("#logo-dropzone .dz-success input").each(function(){
                                          if(file.name == $(this).val()){
                                              jQuery(this).attr("name", "SITE_LOGO");
                                             // $(".dropzone-previews").show();
                                          }
                                      });
                                      
                                    });
                              });
                          </script> 
                          </div>
                      </div>

                      <div class="form-group">
                          <label>Address<span style="color:red;">*</span></label>
                          <textarea rows="4" type="text" name="ADDRESS" placeholder="Address" class="form-control" required data-parsley-trigger="keyup"><?php echo $ADDRESS?></textarea>
                      </div>
                      
                      <div class="form-group">
                          <label>Footer Text<span style="color:red;">*</span></label>
                          <input type="text" placeholder="Currency Symbol" class="form-control" name="FOOTER_TEXT" value="<?php echo $FOOTER_TEXT?>" required data-parsley-trigger="keyup">
                      </div>
                      <div class="form-group">
                          <label>Slider Text<span style="color:red;">*</span></label>
                          <input type="text" placeholder="Slider Text" class="form-control" name="SLIDER_TEXT" value="<?php echo $SLIDER_TEXT?>" required data-parsley-trigger="keyup">
                      </div>        
                      <div class="form-group">
                          <label>Event RSVP email to<span style="color:red;">*</span></label>
                          <input type="text" placeholder="Event RSVP email to" class="form-control" name="EVENT_RSVP_EMAIL_TO" value="<?php echo $EVENT_RSVP_EMAIL_TO?>" required data-parsley-trigger="keyup">
                      </div>
                      <div class="form-group">
                        <label>Map total members</label>
                        <label class="radio-inline">
                            <div class="iradio_square-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input type="radio" class="icheck" name="IS_SHOW" value="1" <?php echo $IS_SHOW ? "checked" : ""?> style="position: absolute; opacity: 0;">
                            </div>&nbsp;&nbsp;Activate
                        </label>
                        <label class="radio-inline">
                            <div class="iradio_square-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                                <input type="radio" class="icheck" name="IS_SHOW" value="0" <?php echo $IS_SHOW ? "" : "checked"?> style="position: absolute; opacity: 0;">
                            </div>&nbsp;&nbsp;Deactivate
                        </label>
                      </div>
                      <!-- About us -->
                      <div class="form-group">
                          <label>About us Block 2 image<span style="color:red;">*</span></label>
                          <div class="cl-mcont" style="">
                          	<?php if($SITE_ABOUT_US_BLOCK2_IMG != ''){?>
                              <div class="dropzone-previews about_us_block2_img_dropzone_div">
                                  <div class="dz-preview dz-processing dz-success dz-image-preview">
                                <div class="dz-details"> <img src="<?php echo base_url().'assets/frontend/images/'.$SITE_ABOUT_US_BLOCK2_IMG;?>" style="max-width:100px;"> </div>
                                <a href="javascript:" id="removeAboutUsBlock2Img" class="dz-remove">Remove file</a> </div>
                            	</div>
                            	<?php }?>
                              <div action="<?php echo base_url().'admin/fileupload';?>" class="dropzone dz-clickable" id="about_us_block2_img_dropzone" multiple="false">
                              	<div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                              </div>
                            <script type="text/javascript">
                                jQuery(document).ready(function(){
                                    jQuery("#about_us_block2_img_dropzone").addClass("dropzone");    
                                    new Dropzone("#about_us_block2_img_dropzone", {
                                      multiple : true,  
                                      maxFiles:1, 
                                      acceptedFiles: ".jpeg,.jpg,.png,.gif",
                                      maxFilesize : 64
                                      }).on("complete", function(file) { 
                                        jQuery("#about_us_block2_img_dropzone .dz-success input").each(function(){
                                            if(file.name == $(this).val()){
                                                jQuery(this).attr("name", "SITE_ABOUT_US_BLOCK2_IMG");
                                              // $(".dropzone-previews").show();
                                            }
                                        });
                                        
                                      });
                                });
                            </script> 
                          </div>
                        </div>
                        <div class="form-group">
                            <label>About us Block 4 image<span style="color:red;">*</span></label>
                            <div class="cl-mcont" style="">
                                <?php if($SITE_ABOUT_US_BLOCK4_IMG != ''){?>
                                <div class="dropzone-previews about_us_block4_img_dropzone_div">
                                    <div class="dz-preview dz-processing dz-success dz-image-preview">
                                    <div class="dz-details"> <img src="<?php echo base_url().'assets/frontend/images/'.$SITE_ABOUT_US_BLOCK4_IMG;?>" style="max-width:100px;"> </div>
                                    <a href="javascript:" id="removeAboutUsBlock4Img" class="dz-remove">Remove file</a> </div>
                                    </div>
                                    <?php }?>
                                <div action="<?php echo base_url().'admin/fileupload';?>" class="dropzone dz-clickable" id="about_us_block4_img_dropzone" multiple="false">
                                    <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                </div>
                            <script type="text/javascript">
                                jQuery(document).ready(function(){
                                    jQuery("#about_us_block4_img_dropzone").addClass("dropzone");    
                                    new Dropzone("#about_us_block4_img_dropzone", {
                                        multiple : true,  
                                        maxFiles:1, 
                                        acceptedFiles: ".jpeg,.jpg,.png,.gif",
                                                        maxFilesize : 64
                                        }).on("complete", function(file) { 
                                        jQuery("#about_us_block4_img_dropzone .dz-success input").each(function(){
                                            if(file.name == $(this).val()){
                                                jQuery(this).attr("name", "SITE_ABOUT_US_BLOCK4_IMG");
                                                // $(".dropzone-previews").show();
                                            }
                                        });
                                        
                                        });
                                });
                            </script> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label>About us  Block 5 image<span style="color:red;">*</span></label>
                            <div class="cl-mcont" style="">
                                <?php if($SITE_ABOUT_US_BLOCK5_IMG != ''){?>
                                <div class="dropzone-previews about_us_block5_img_dropzone_div">
                                    <div class="dz-preview dz-processing dz-success dz-image-preview">
                                    <div class="dz-details"> <img src="<?php echo base_url().'assets/frontend/images/'.$SITE_ABOUT_US_BLOCK5_IMG;?>" style="max-width:100px;"> </div>
                                    <a href="javascript:" id="removeAboutUsBlock5Img" class="dz-remove">Remove file</a> </div>
                                    </div>
                                    <?php }?>
                                <div action="<?php echo base_url().'admin/fileupload';?>" class="dropzone dz-clickable" id="about_us_block5_img_dropzone" multiple="false">
                                    <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                </div>
                            <script type="text/javascript">
                                jQuery(document).ready(function(){
                                    jQuery("#about_us_block5_img_dropzone").addClass("dropzone");    
                                    new Dropzone("#about_us_block5_img_dropzone", {
                                        multiple : true,  
                                        maxFiles:1, 
                                        acceptedFiles: ".jpeg,.jpg,.png,.gif",
                                                        maxFilesize : 64
                                        }).on("complete", function(file) { 
                                        jQuery("#about_us_block5_img_dropzone .dz-success input").each(function(){
                                            if(file.name == $(this).val()){
                                                jQuery(this).attr("name", "SITE_ABOUT_US_BLOCK5_IMG");
                                                // $(".dropzone-previews").show();
                                            }
                                        });
                                        
                                        });
                                });
                            </script> 
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label>About us Block 6 image<span style="color:red;">*</span></label>
                            <div class="cl-mcont" style="">
                                <?php if($SITE_ABOUT_US_BLOCK6_IMG != ''){?>
                                <div class="dropzone-previews about_us_block6_img_dropzone_div">
                                    <div class="dz-preview dz-processing dz-success dz-image-preview">
                                    <div class="dz-details"> <img src="<?php echo base_url().'assets/frontend/images/'.$SITE_ABOUT_US_BLOCK6_IMG;?>" style="max-width:100px;"> </div>
                                    <a href="javascript:" id="removeAboutUsBlock6Img" class="dz-remove">Remove file</a> </div>
                                    </div>
                                    <?php }?>
                                <div action="<?php echo base_url().'admin/fileupload';?>" class="dropzone dz-clickable" id="about_us_block6_img_dropzone" multiple="false">
                                    <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                </div>
                            <script type="text/javascript">
                                jQuery(document).ready(function(){
                                    jQuery("#about_us_block6_img_dropzone").addClass("dropzone");    
                                    new Dropzone("#about_us_block6_img_dropzone", {
                                        multiple : true,  
                                        maxFiles:1, 
                                        acceptedFiles: ".jpeg,.jpg,.png,.gif",
                                                        maxFilesize : 64
                                        }).on("complete", function(file) { 
                                        jQuery("#about_us_block6_img_dropzone .dz-success input").each(function(){
                                            if(file.name == $(this).val()){
                                                jQuery(this).attr("name", "SITE_ABOUT_US_BLOCK6_IMG");
                                                // $(".dropzone-previews").show();
                                            }
                                        });
                                        
                                        });
                                });
                            </script> 
                            </div>
                        </div>
                     
                        <!-- About us -->        
                      <div class="form-group">
                          <button class="btn btn-primary" type="submit" name="submit_btn" value="Update">Update</button>
                      </div>	
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>	
                      
          <div class="col-sm-6 col-md-6">
              <div class="block-flat">
                  <div class="header">
                  <h3>Social Media</h3>
                </div>
                <div class="content">
                	
                    <div class="form-group">
                      <label>Instagram<span style="color:red;">*</span></label>
                      <input type="text" placeholder="Instagram Link" class="form-control" name="INSTAGRAM_LINK" value="<?php echo $INSTAGRAM_LINK?>" required data-parsley-trigger="keyup">
                    </div>
                    
                    <div class="form-group">
                        <label>Linkedin<span style="color:red;">*</span></label>
                        <input type="text" placeholder="Linkedin Link" class="form-control" name="LINKEDIN_LINK" value="<?php echo $LINKEDIN_LINK?>" required data-parsley-trigger="keyup">
                    </div>
                   
                   <div class="form-group">
                      <label>Twitter<span style="color:red;">*</span></label>
                      <input type="text" placeholder="Twitter Link" class="form-control" name="TWITTER_LINK" value="<?php echo $TWITTER_LINK?>" required data-parsley-trigger="keyup">
                   </div>
                    <div class="form-group">
                      <label>Youtube<span style="color:red;">*</span></label>
                      <input type="text" placeholder="Youtube Link" class="form-control" name="YOUTUBE_LINK" value="<?php echo $YOUTUBE_LINK?>" required data-parsley-trigger="keyup">
                   </div>
                    
                  <div class="form-group">
                      <button class="btn btn-primary" type="submit" name="submit_btn" value="Update">Update</button>
                  </div>
                </div>
                
              </div>
  		    </div>
          <div class="col-sm-6 col-md-6">
            <div class="block-flat">
                <div class="header">
                <h3>SMTP Settings</h3>
              </div>
                <div class="content">
                <div class="form-group">
                    <label>Mail By</label>
                    <label class="radio-inline">
                    <div class="iradio_square-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                    <input type="radio" class="icheck" name="IS_SMTP" value="1" <?php echo $IS_SMTP ? "checked" : ""?> style="position: absolute; opacity: 0;">
                  </div>
                    &nbsp;&nbsp;SMTP
                    </label>
                    <label class="radio-inline">
                    <div class="iradio_square-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                    <input type="radio" class="icheck" name="IS_SMTP" value="0" <?php echo $IS_SMTP ? "" : "checked"?> style="position: absolute; opacity: 0;">
                    </div>
                        &nbsp;&nbsp;PHP "mail()" function
                        </label>
                    </div>
                  <div class="form-group">
                      <label>From Name<span style="color:red;">*</span></label>
                      <input type="text" placeholder="From Name" class="form-control"  name="SMTP_FROM_NAME" value="<?php echo $SMTP_FROM_NAME?>" required>
                    </div>
                  <div class="form-group">
                      <label>From Email<span style="color:red;">*</span></label>
                      <input type="email" placeholder="From Email" class="form-control"  name="SMTP_FROM_EMAIL" value="<?php echo $SMTP_FROM_EMAIL?>" required parsley-type="email" >
                    </div>
                  <div class="form-group">
                      <label>Reply To Email<span style="color:red;">*</span></label>
                      <input type="email" placeholder="Reply To Email" class="form-control"  name="SMTP_REPLY_TO_EMAIL" value="<?php echo $SMTP_REPLY_TO_EMAIL?>" required parsley-type="email" >
                    </div>
                  <div class="form-group">
                      <label>SMTP Host<span style="color:red;">*</span></label>
                      <input type="text" placeholder="SMTP Host" class="form-control"  name="SMTP_HOST" value="<?php echo $SMTP_HOST?>" required>
                  </div>
                  <div class="form-group">
                      <label>SMTP Username<span style="color:red;">*</span></label>
                      <input type="text" placeholder="SMTP Host" class="form-control"  name="SMTP_USERNAME" value="<?php echo $SMTP_USERNAME?>" required>
                  </div>
                  <div class="form-group">
                      <label>SMTP Password<span style="color:red;">*</span></label>
                      <input type="text" placeholder="SMTP Host" class="form-control"  name="SMTP_PASSWORD" value="<?php echo $SMTP_PASSWORD?>" required>
                  </div>
                  <div class="form-group">
                      <label>SMTP Port<span style="color:red;">*</span></label>
                      <input type="text" placeholder="SMTP Port" class="form-control"  name="SMTP_PORT" value="<?php echo $SMTP_PORT?>" required>
                    </div>
                  
                  <div class="form-group">
                      <button class="btn btn-primary" type="submit" name="submit_btn" value="Update">Update</button>
                  </div>
              </div>
              
            </div>
          </div>

          <div class="col-sm-6 col-md-6">
              <div class="block-flat">
                  <div class="header">
                  <h3>Manage MailChimp Keys</h3>
                </div>
                <div class="content">

                    <div class="form-group">
                      <label>MailChimp List Id<span style="color:red;">*</span></label>
                      <input type="text" placeholder="MailChimp List Id" class="form-control" name="MAILCHIMP_LIST_ID" value="<?php echo $MAILCHIMP_LIST_ID?>" required data-parsley-trigger="keyup">
                    </div>

                    <div class="form-group">
                        <label>MailChimp Api Key<span style="color:red;">*</span></label>
                        <input type="text" placeholder="MailChimp Api Key" class="form-control" name="MAILCHIMP_API_KEY" value="<?php echo $MAILCHIMP_API_KEY?>" required data-parsley-trigger="keyup">
                    </div>
                   
                    <div class="form-group">
                      <button class="btn btn-primary" type="submit" name="submit_btn" value="Update">Update</button>
                    </div>
                </div>
                
              </div>
          </div>

          <div class="col-sm-6 col-md-6">
              <div class="block-flat">
                  <div class="header">
                  <h3>Manage Keys</h3>
                </div>
                <div class="content">

                    <div class="form-group">
                      <label>Zoho CMS Token<span style="color:red;">*</span></label>
                      <input type="text" placeholder="Zoho CMS Token" class="form-control" name="ZOHO_CMS_TOKEN" value="<?php echo $ZOHO_CMS_TOKEN?>" required data-parsley-trigger="keyup">
                    </div>
                    
                    <div class="form-group">
                      <label>Eventbrite Token<span style="color:red;">*</span></label>
                      <input type="text" placeholder="Eventbrite Token" class="form-control" name="EVENTBRIGHT_TOKEN" value="<?php echo $EVENTBRIGHT_TOKEN?>" required data-parsley-trigger="keyup">
                    </div>

                    <div class="form-group">
                        <label>Google Map Key<span style="color:red;">*</span></label>
                        <input type="text" placeholder="Google Map Key" class="form-control" name="GOOGLE_MAP_KEY" value="<?php echo $GOOGLE_MAP_KEY?>" required data-parsley-trigger="keyup">
                    </div>
                   
                    <div class="form-group">
                      <button class="btn btn-primary" type="submit" name="submit_btn" value="Update">Update</button>
                    </div>
                </div>
                
              </div>
          </div>
          <div class="col-sm-6 col-md-6">
              <div class="block-flat">
                  <div class="header">
                  <h3>Member Of Month</h3>
                </div>
                <div class="content">
                	<div class="form-group">
                          <label>Image (200 X 200)<span style="color:red;">*</span></label>
                          <div class="cl-mcont" style="">
                          	<?php if($MEMBER_IMAGE != ''){?>
                              <div class="dropzone-previews">
                                  <div class="dz-preview dz-processing dz-success dz-image-preview">
                                <div class="dz-details"> <img src="<?php echo base_url().'assets/frontend/images/'.$MEMBER_IMAGE;?>" style="max-width:100px;"> </div>
                               <?php /*?> <a href="javascript:" id="removeLogo" class="dz-remove">Remove file</a><?php */?> </div>
                            	</div>
                            	<?php }?>
                              <div action="<?php echo base_url().'admin/fileupload';?>" class="dropzone dz-clickable" id="member-dropzone" multiple="false">
                              	<div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                              </div>
                          <script type="text/javascript">
                              jQuery(document).ready(function(){
                                  jQuery("#member-dropzone").addClass("dropzone");    
                                  new Dropzone("#member-dropzone", {
                                    multiple : true,  
                                    maxFiles:1, 
                                    acceptedFiles: ".jpeg,.jpg,.png,.gif",
									maxFilesize : 64
                                    }).on("complete", function(file) { 
                                      jQuery("#member-dropzone .dz-success input").each(function(){
                                          if(file.name == $(this).val()){
                                              jQuery(this).attr("name", "MEMBER_IMAGE");
                                             // $(".dropzone-previews").show();
                                          }
                                      });
                                      
                                    });
                              });
                          </script> 
                          </div>
                      </div>
                    <div class="form-group">
                          <label> Link<span style="color:red;">*</span></label>
                          <input type="text" placeholder="Member Of Month Link" class="form-control" name="MEMBER_OF_MONTH_LINK" value="<?php echo $MEMBER_OF_MONTH_LINK?>" required data-parsley-trigger="keyup">
                    </div>
                    <div class="form-group">
                          <label>Main Occupation<span style="color:red;">*</span></label>
                          <input type="text" placeholder="Member Main Occupation" class="form-control" name="MEMBER_MAIN_OCCUPATION" value="<?php echo $MEMBER_MAIN_OCCUPATION?>" required data-parsley-trigger="keyup">
                    </div>
                    <div class="form-group">
                          <button class="btn btn-primary" type="submit" name="submit_btn" value="Update">Update</button>
                      </div>  
                </div>
                
              </div>
  		    </div>     
     <!--  <div class="col-sm-6 col-md-6">
              <div class="block-flat">
                  <div class="header">
                  <h3>Manage Stripe Payment</h3>
                </div>
                <div class="content">
                  
                    <div class="form-group">
                      <label>Secret Key<span style="color:red;">*</span></label>
                      <input type="text" placeholder="Secret Key" class="form-control" name="SECRET_KEY" value="<?php echo $SECRET_KEY?>" required="required" data-parsley-trigger="keyup">
                    </div>
                    
                    <div class="form-group">
                        <label>Public Key<span style="color:red;">*</span></label>
                        <input type="text" placeholder="Public Key" class="form-control" name="PUBLIC_KEY" value="<?php echo $PUBLIC_KEY?>" required="required" data-parsley-trigger="keyup">
                    </div>
                   
                   <div class="form-group">
                      <label>Charges Amount<span style="color:red;">*</span></label>
                      <input type="number" placeholder="Charges Amount" class="form-control" name="CHARGES_AMOUNT" value="<?php echo $CHARGES_AMOUNT?>" required="required" data-parsley-trigger="keyup">
                   </div>
                    
                  <div class="form-group">
                      <button class="btn btn-primary" type="submit" name="submit_btn" value="Update">Update</button>
                  </div>
                </div>
                
              </div>
          </div> -->

            <div class="col-sm-6 col-md-6">
              <div class="block-flat">
                  <div class="header">
                  <h3>Manage Google Adword Script</h3>
                </div>
                <div class="content">
                  
                    <div class="form-group">
                      <label>Adword Script<span style="color:red;">*</span></label>
                       <textarea placeholder="Description" class="form-control" name="ADWORD_SCRIPT" required data-parsley-trigger="keyup"><?php echo $ADWORD_SCRIPT?></textarea>
                    </div>
                    
                  <div class="form-group">
                      <button class="btn btn-primary" type="submit" name="submit_btn" value="Update">Update</button>
                  </div>
                </div>
                
              </div>
            </div>

            <div class="col-sm-6 col-md-6">
              <div class="block-flat">
                  <div class="header">
                  <h3>Manage Paypal Payment</h3>
                </div>
                <div class="content">
                    
                    <div class="form-group">
                      <label>Type</label>
                        <label class="radio-inline">
                        <div class="iradio_square-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                          <input type="radio" class="icheck" name="IS_TEST" value="1" <?php echo $IS_TEST ? "checked" : ""?> style="position: absolute; opacity: 0;">
                        </div>
                      &nbsp;&nbsp;Sandbox
                      </label>
                      <label class="radio-inline">
                        <div class="iradio_square-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                          <input type="radio" class="icheck" name="IS_TEST" value="0" <?php echo $IS_TEST ? "" : "checked"?> style="position: absolute; opacity: 0;">
                          </div>
                          &nbsp;&nbsp;Live
                      </label>
                    </div>

                    <div class="form-group">
                        <label>Business Name<span style="color:red;">*</span></label>
                        <input type="text" placeholder="Business Name" class="form-control" name="PAYPAL_BUSINESS_NAME" value="<?php echo $PAYPAL_BUSINESS_NAME?>" required data-parsley-trigger="keyup">
                    </div>

                    <div class="form-group">
                      <label>Paypal Charges Amount<span style="color:red;">*</span></label>
                      <input type="number" placeholder="Paypal Charges Amount" class="form-control" name="PAYPAL_CHARGES_AMOUNT" value="<?php echo $PAYPAL_CHARGES_AMOUNT?>" required data-parsley-trigger="keyup">
                    </div>    

                    <div class="form-group">
                      <button class="btn btn-primary" type="submit" name="submit_btn" value="Update">Update</button>
                    </div>

                </div>                
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="block-flat">
                  <div class="header">
                  <h3>Manage Stripe Payment</h3>
                </div>
                <div class="content">
                    
                    <!-- <div class="form-group">
                      <label>Type</label>
                        <label class="radio-inline">
                        <div class="iradio_square-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                          <input type="radio" class="icheck" name="IS_STRIPE_TEST" value="1" <?php echo $IS_STRIPE_TEST ? "checked" : ""?> style="position: absolute; opacity: 0;">
                        </div>
                      &nbsp;&nbsp;Sandbox
                      </label>
                      <label class="radio-inline">
                        <div class="iradio_square-blue" style="position: relative;" aria-checked="false" aria-disabled="false">
                          <input type="radio" class="icheck" name="IS_STRIPE_TEST" value="0" <?php echo $IS_STRIPE_TEST ? "" : "checked"?> style="position: absolute; opacity: 0;">
                          </div>
                          &nbsp;&nbsp;Live
                      </label>
                    </div> -->

                    <div class="form-group">
                        <label>Publishable key<span style="color:red;">*</span></label>
                        <input type="text" placeholder="Publishable key" class="form-control" name="STRIPE_PUBLISHABLE_KEY" value="<?php echo $STRIPE_PUBLISHABLE_KEY?>" required data-parsley-trigger="keyup">
                    </div>
                    <div class="form-group">
                        <label>Secret key<span style="color:red;">*</span></label>
                        <input type="text" placeholder="Secret key" class="form-control" name="STRIPE_SECRET_KEY" value="<?php echo $STRIPE_SECRET_KEY?>" required data-parsley-trigger="keyup">
                    </div>
                    <div class="form-group">
                      <label>Stripe Product ID<span style="color:red;">*</span></label>
                      <input type="text" placeholder="Stripe Charges Amount" class="form-control" name="STRIPE_PRODUCT_ID" value="<?php echo $STRIPE_PRODUCT_ID?>" required data-parsley-trigger="keyup">
                    </div>
                    <div class="form-group">
                      <label>Stripe Charges Amount<span style="color:red;">*</span></label>
                      <input type="number" placeholder="Stripe Charges Amount" class="form-control" name="STRIPE_CHARGES_AMOUNT" value="<?php echo $STRIPE_CHARGES_AMOUNT?>" required data-parsley-trigger="keyup">
                    </div>

                    <div class="form-group">
                      <button class="btn btn-primary" type="submit" name="submit_btn" value="Update">Update</button>
                    </div>

                </div>                
              </div>
            </div>                  
            <!--<div class="col-sm-12 col-md-12">
              <div class="block-flat">
                  <div class="header">
                  <h3>Manage Menu</h3>
                </div>
                <div class="content">
                  
                    <?php $url = base_url().'assets/frontend/css/style.css';?>
                        <script type="text/javascript">            
                              
                        $(document).ready(function(){        
                                CKEDITOR.replace( 'HEADER_MENU',{
                                          height: '400px',
                                          toolbar : 'Basic',
                                          contentsCss : '<?php echo $url;?>',
                                          autoParagraph: false
                                              });
                                            });                                                       
                        </script>
                        <div class="form-group">
                            <label>Header Menu<span style="color:red;">*</span></label>
                            <textarea placeholder="Description" class="form-control" name="HEADER_MENU" required="required" data-parsley-trigger="keyup"><?php echo $HEADER_MENU?></textarea>
                        </div>
                  <div class="form-group">
                      <button class="btn btn-primary" type="submit" name="submit_btn" value="Update">Update</button>
                  </div>
                </div>
                
              </div>
            </div> -->
  		</form>
	</div>
</div>

 <script type="text/javascript">
 	
$( "#removeLogo" ).on( "click", function() {
  var redirecturl = '<?php echo base_url().'admin/removelogo/'.$SITE_LOGO;?>';

	  $.ajax({
	  url: redirecturl,
	  cache: false,
	  success: function(html){
	    $(".dropzone-previews").hide();
	  }
	});
});

$( "#removeAboutUsBlock2Img" ).on( "click", function() {
  var redirecturl = '<?php echo base_url().'admin/removeAboutUsBlock2Img/'.$SITE_ABOUT_US_BLOCK2_IMG;?>';

	  $.ajax({
	  url: redirecturl,
	  cache: false,
	  success: function(html){
	    $(".about_us_block2_img_dropzone_div").hide();
	  }
	});
});
$( "#removeAboutUsBlock4Img" ).on( "click", function() {
  var redirecturl = '<?php echo base_url().'admin/removeAboutUsBlock4Img/'.$SITE_ABOUT_US_BLOCK4_IMG;?>';

	  $.ajax({
	  url: redirecturl,
	  cache: false,
	  success: function(html){
	    $(".about_us_block4_img_dropzone_div").hide();
	  }
	});
});
$( "#removeAboutUsBlock5Img" ).on( "click", function() {
  var redirecturl = '<?php echo base_url().'admin/removeAboutUsBlock5Img/'.$SITE_ABOUT_US_BLOCK5_IMG;?>';

	  $.ajax({
	  url: redirecturl,
	  cache: false,
	  success: function(html){
	    $(".about_us_block5_img_dropzone_div").hide();
	  }
	});
});
$( "#removeAboutUsBlock6Img" ).on( "click", function() {
  var redirecturl = '<?php echo base_url().'admin/removeAboutUsBlock6Img/'.$SITE_ABOUT_US_BLOCK6_IMG;?>';

	  $.ajax({
	  url: redirecturl,
	  cache: false,
	  success: function(html){
	    $(".about_us_block6_img_dropzone_div").hide();
	  }
	});
});
 </script>


 