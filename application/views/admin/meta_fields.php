<?php include("inc/header.php"); ?>

<?php include("inc/header-top.php"); ?>

<?php include("jsfunctions/jsfunctions.php"); ?>

<div id="cl-wrapper" class="fixed-menu">
<?php include("inc/sidebar.php"); ?>
	
	<div class="container-fluid" id="pcont">
		<div class="page-head">
			<h2><?php echo $page_title;?></h2>
		</div>
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">
					<div class="block-flat">
						<div class="header">
							<h3> 
									
	                        	<?php echo ($script == 'list') ? 'List Of '.' '.ucfirst($page) : ucfirst($script).' '.ucfirst($page);  ?> 
								<?php if($script == 'list'){ ?>
								<a href="<?php echo base_url().'admin/meta_fields/add/';?>" class="fright">
									<button class="btn btn-primary" type="button">Add <?php  echo ' '.ucfirst($page);?></button>
							    </a> 
							    <?php } ?>
	                        </h3>
						</div>
						<div id="response">
							<?php 
							if(!($this->form_validation->error_array())){
								if(isset($_GET['msg']) && $_GET['msg'] !=''){
									if($_GET['succ']==1){
										echo $this->messages_model->getSuccessMsg($_GET['msg']);
									}
									else if($_GET['succ']==0){
										echo $this->messages_model->getErrorMsg($_GET['msg']);
									}
								}
							}?>
                        	<?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							<div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>
						</div>
						<?php 
							if(($script == 'add') || ($script == 'edit')){ ?>

							<div class="row">
								<div class="col-md-12">
									<div class="content">
										<form role="form" action="" method="post" enctype="multipart/form-data" novalidate="">
    										<div class="form-group">
												<label>Select Item<span style="color:red;">*</span></label>
												<select class="select2" name="i_item_id" id="i_item_id">
                                                	<option value="">All Pages</option>
                                                	<?php 
                                                		$sel ='';
														foreach($items as $key => $val){
															 if($i_item_id == $val->id) { $sel = 'selected="selected"'; }
															 else { $sel = ''; }
															$str .= '<option value="'.$val->id.'" '.$sel.' />'.ucwords($val->v_title).'</option>';
														}
														echo $str;
													?>
												</select>
											</div>

                                       		<div class="form-group">
                                                <label>Title<span style="color:red;">*</span></label>
                                                <input type="text" pattern="[a-zA-Z\s]+" placeholder="Title" class="form-control" name="v_title" value="<?php echo $v_title?>" required="required" data-parsley-trigger="keyup">
                                            </div>
                                            
                                            <div class="form-group">
                                               <label>Description</label>
                                                <textarea placeholder="Description" class="form-control" id="l_description" name="l_description"><?php echo $l_description?></textarea>
                                           	</div>

                                            <div class="form-group">
                                                <label>Order</label>
                                                    <input type="text" placeholder="Order" class="form-control" name="i_order" value="<?php echo $i_order?>"> 
                                            </div>

                                          
                                           <div class="form-group">
												<label>Status<span style="color:red;">*</span></label>
												<select class="select2" name="e_status" id="e_status" required>
                                                	<option value="">-</option>
													<?php $this->general_model->getDropdownList(array('active','inactive'),$e_status); ?>
												</select>
											</div>
                                            
                                            <div class="form-group">
                                                    <button class="btn btn-primary" type="submit" name="submit_btn" value="<?php echo ($script=='edit')?'Update':'Submit';?>"><?php echo ($script=='edit')?'Update':'Submit';?> </button>
                                                    <a href="<?php echo base_url().'admin/meta_fields/';?>">
                                                    <button class="btn fright" type="button" name="submit_btn">Cancel</button>
                                                    </a> 
                                            </div>
				                   		</form>
									</div>
								</div>
							</div>

						<?php }elseif($script == 'view'){?>

							<div class="row">
								<div class="col-md-6">
									<div class="content">
                                        <form name="frm_meta_field_items" id="frm_meta_field_items" action="<?php echo base_url().'admin/meta_field_items/add/'.'';?>" method="post" novalidate="">
                                        <input type="hidden" name="i_meta_id" value="<?php echo $meta_id?>">
                                        <div class="form-group">
                                            <label>Meta Title</label>
                                            <input type="text" placeholder="Title" class="form-control" name="v_title" value="" required="" data-parsley-id="4">
                                        </div>
                                        <div class="form-group">
                                            <label>Meta Key</label>
                                            <input type="text" placeholder="Key" class="form-control" name="v_key" value="" required="" data-parsley-id="6">
                                        </div>
                                        <div class="form-group">
                                            <label>Type</label>
                                        	<select class="select2" name="v_type" id="v_type" required>
                                                	<option value="">-</option>
													<?php $this->general_model->getDropdownList(array('text','textarea','image'),$v_type); ?>
												</select>
                                        </div>
                                        <div class="form-group">
                                            <label>Order</label>
                                            <input type="text" class="form-control" name="i_order" value="" data-parsley-id="14">
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                        	<select class="select2" name="e_status" id="e_status" required>
                                                	<option value="">-</option>
													<?php $this->general_model->getDropdownList(array('active','inactive'),$e_status); ?>
												</select>
                                        </div>
                                        <div class="form-group">
											<button class="btn btn-primary" type="submit" name="submit_btn" value="<?php echo ($script=='edit')?'Update':'Submit';?>"><?php echo ($script=='edit')?'Update':'Submit';?></button>
                                        </div>             
                                        
                                    </form></div>
								</div>	
                                <div class="col-md-6">
									<div class="content">
                                    <table class="table table-bordered">
										<tbody>
										<tr>
                                        	<th>Name</th>
                                            <th>Key</th>
                                            <th>Type</th>
                                            <th>Order</th>
                                            <th></th>
                                        </tr>
										
										<?php 
										   	if(!empty($rows)){
										   	foreach ($rows as $key => $val) { ?>
										   		<tr>
													<td><?php echo $val['v_title']; ?></td>
                                                    <td><?php echo $val['v_key']; ?></td>
													<td><?php echo $val['v_type']; ?></td>
                                                    <td><?php echo $val['i_order']; ?></td>
													<td>
														<div class="btn-group">
															<button class="btn btn-default btn-xs" type="button">Actions</button>
															<button data-toggle="dropdown" class="btn btn-xs btn-primary dropdown-toggle" type="button"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>

															<ul role="menu" class="dropdown-menu pull-right">
																<li><a href="<?php echo base_url().'admin/meta_field_items/edit/'.$val["id"].'/';?>">Edit</a></li>
																<li><a href="javascript:void(0)" class="md-trigger delete-confirmation" onClick="showConfirmBox('<?php echo base_url().'admin/meta_field_items/delete/'.$val["id"].'/';?>')" >Delete</a></li>
                                                        	</ul>
														</div>
													</td>
												</tr>
											<?php } }else{?>

                                                <tr><td colspan="4">No Record found.</td></tr>

                                            <?php }?>
	                                    </tbody>
	                                </table>
                                    </div>
								</div>
							</div>

						<?php }else{ ?>	

							<div class="row">
								<div class="col-md-12">
									<div class="content">

									<form name="frm_meta_fields" action="<?php echo base_url().'admin/meta_fields/';?>" method="get">
									  	<div class="table-responsive">
											<div class="row">
												<div class="col-sm-12">
													<div class="dataTables_filter" id="datatable_filter">
														<label>
															<?php $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : ''; ?>
															<input type="text" aria-controls="datatable" class="form-control fleft" placeholder="Search" name="keyword" value="<?php echo $keyword;?>" style="width:auto;"/>
															<button type="submit" class="btn btn-primary fleft" style="margin-left:0px;"><span class="fa fa-search"></span></button>
														</label>
													</div>
                                                    <div class="pull-left">
                                                        <div id="datatable_length" class="dataTables_length">
                                                            <label>
                                                                <?php $this->paging_model->writeLimitBox(); ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
												<div class="clearfix"></div>
											</div>
										</div>
										<table class="table table-bordered" id="datatable" >
										   	<thead>
										   		<tr>
													<th width="20%">Title</th>
                                                    <th>Type</th>
													<th>Entry</th>
                                                    <th>Status</th>
													<th></th>
												</tr>
										   	</thead>
										   	<tbody>

										   <?php 
										   	if(!empty($rows)){
										   	foreach ($rows as $key => $val) { ?>
										   		<tr>
													<td><?php echo $val['v_title']; ?></td>
                                                    <td><?php echo $val['v_type']; ?></td>
													<td><?php echo $val['page_title']; ?></td>
                                                    <td><?php echo $val['e_status']; ?></td>
													<td>
														<div class="btn-group action_btns">
															<a class="btn btn-primary btn-xs" title="Edit" href="<?php echo base_url().'admin/meta_fields/edit/'.$val["id"].'/';?>"><span class="fa fa-edit"></span></a>

															<?php if($val['e_status'] == 'inactive'){?>
															<a class="btn btn-success btn-xs" title="Active" href="<?php echo base_url().'admin/meta_fields/active/'.$val["id"].'/';?>"><span class="fa fa-eye"></span></a>
																<?php }else{?>

															<a class="btn btn-warning btn-xs" title="Inactive" href="<?php echo base_url().'admin/meta_fields/inactive/'.$val["id"].'/';?>"><span class="fa fa-eye-slash"></span></a>
																<?php } ?>

															<a class="btn btn-danger btn-xs" title="Delete" href="javascript:void(0)" class="md-trigger delete-confirmation" onClick="showConfirmBox('<?php echo base_url().'admin/meta_fields/delete/'.$val["id"].'/';?>')" ><span class="fa fa-trash"></span></a>
														</div>
														<div class="btn-group">
	                                                        <a href="<?php echo base_url().'admin/meta_field_items/view/'.$val["id"].'/';?>">
	                                                        	<button class="btn btn-default btn-xs" type="button">Manage Fields</button>
	                                                        	<button class="btn btn-xs btn-primary" type="button"><i class="fa fa-eye" aria-hidden="true"></i>
		                                                        <span class="sr-only"></span>
		                                                        </button>
	                                                        </a>
														</div>
													</td>
												</tr>
											<?php } }else{?>

                                                <tr><td colspan="4">No Record found.</td></tr>

                                            <?php }?>
										   </tbody>
										</table>	
										<div class="row">
											<div class="col-sm-12">
												<div class="pull-left"> <?php echo $this->paging_model->getPagesCounter();?> </div>
												<div class="pull-right">
													<div class="dataTables_paginate paging_bs_normal">
														<ul class="pagination">
															<?php $this->paging_model->writePagesLinks(); ?>
														</ul>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
										<input type="hidden" name="a" value="<?php echo @$_REQUEST['a'];?>" />
										<input type="hidden" name="st" value="<?php echo @$_REQUEST['st'];?>" />
										<input type="hidden" name="sb" value="<?php echo @$_REQUEST['sb'];?>" />
									</form>
								</div>
								
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
	
?>