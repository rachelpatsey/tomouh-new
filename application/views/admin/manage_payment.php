<?php include("inc/header.php"); ?>

<?php include("inc/header-top.php"); ?>

<?php include("jsfunctions/jsfunctions.php"); ?>

<div id="cl-wrapper" class="fixed-menu">
<?php include("inc/sidebar.php"); ?>
	
	<div class="container-fluid" id="pcont">
		<div class="page-head">
			<h2><?php echo $page_title;?></h2>
		</div>
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">
					<div class="block-flat">
						<div class="header">
							<h3> 
	                        	<?php echo ($script == 'list') ? 'List Of '.' '.ucfirst($page) : ucfirst($script).' '.ucfirst($page);  ?> 
	                        </h3>
						</div>
						<div id="response">
							<?php 
							if(!($this->form_validation->error_array())){
								if(isset($_GET['msg']) && $_GET['msg'] !=''){
									if($_GET['succ']==1){
										echo $this->messages_model->getSuccessMsg($_GET['msg']);
									}
									else if($_GET['succ']==0){
										echo $this->messages_model->getErrorMsg($_GET['msg']);
									}
								}
							}?>
                        	<?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							<div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>
						</div>
						<?php 
							if($script == 'edit'){ ?>

							<div class="row">
								<div class="col-md-12">
									<div class="content">
										<form role="form" action="" method="post" enctype="multipart/form-data" novalidate="">
											<?php 
	    										if(!empty($subscription_data)){
	              									$total_edu = sizeof($subscription_data);
	            									foreach($subscription_data as $row){ 
                                            ?>
                                       		<div class="form-group">
                                                <label>User ID<span style="color:red;">*</span></label>
                                                <input type="text" class="form-control" name="user_id" value="<?php echo $row->user_id; ?>" required="required" data-parsley-trigger="keyup">
                                            </div>

                                            <div class="form-group">
                                                <label>Subscription ID<span style="color:red;">*</span></label>
                                                <input type="text" class="form-control" name="v_subscription_id" value="<?php echo isset($members_data['v_subscription_id']) ? $members_data['v_subscription_id'] : ''; ?>" required="required" data-parsley-trigger="keyup">
                                            </div>
                                             
                                            <div class="form-group">
                                                <label>First Name<span style="color:red;">*</span></label>
                                                <input type="text" class="form-control" name="v_firstname" value="<?php echo isset($members_data['v_firstname']) ? $members_data['v_firstname'] : ''; ?>" required="required" data-parsley-trigger="keyup">
                                            </div>

                                            <div class="form-group">
                                                <label>Last Name<span style="color:red;">*</span></label>
                                                <input type="text" class="form-control" name="v_lastname" value="<?php echo isset($members_data['v_lastname']) ? $members_data['v_lastname'] : ''; ?>" required="required" data-parsley-trigger="keyup">
                                            </div>

                                            <div class="form-group">
                                                <label>Email<span style="color:red;">*</span></label>
                                                <input type="text" class="form-control" name="v_email" value="<?php echo isset($members_data['v_email']) ? $members_data['v_email'] : ''; ?>" required="required" data-parsley-trigger="keyup">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Subscription Date<span style="color:red;">*</span></label>
                                                <input type="text" class="form-control datetime" name="d_subscription_date" value="<?php echo $row->d_subscription_date; ?>" required="required" data-parsley-trigger="keyup">
                                            </div>

                                            <div class="form-group">
                                                <label>Expiration Date<span style="color:red;">*</span></label>
                                                <input type="text" class="form-control datetime" name="d_subscription_exp_date" value="<?php echo $row->d_subscription_exp_date?>" required="required" data-parsley-trigger="keyup">
                                            </div>
                                             
                                            <?php	} } ?>
                                            
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit" name="submit_btn" value="<?php echo ($script=='edit')?'Update':'Submit';?>"><?php echo ($script=='edit')?'Update':'Submit';?> </button>
                                                <a href="<?php echo base_url().'admin/manage_payment/';?>">
                                                <button class="btn fright" type="button" name="submit_btn">Cancel</button>
                                                </a> 
                                            </div>
				                   		</form>
									</div>
								</div>
							</div>
						<?php }else{ ?>	

							<div class="row">
								<div class="col-md-12">
									<div class="content">
										<form name="frm_payment" action="<?php echo base_url().'admin/manage_payment/';?>" method="get">
										  	<div class="table-responsive">
												<div class="row">
													<div class="col-sm-12">
														<div class="dataTables_filter" id="datatable_filter">
															<label>
																<?php $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : ''; ?>
																<input type="text" aria-controls="datatable" class="form-control fleft" placeholder="Search" name="keyword" value="<?php echo $keyword;?>" style="width:auto;"/>
																<button type="submit" class="btn btn-primary fleft" style="margin-left:0px;"><span class="fa fa-search"></span></button>
															</label>
														</div>
	                                                    <div class="pull-left">
	                                                        <div id="datatable_length" class="dataTables_length">
	                                                            <label>
	                                                                <?php $this->paging_model->writeLimitBox(); ?>
	                                                            </label>
	                                                        </div>
	                                                    </div>
	                                                </div>
													<div class="clearfix"></div>
												</div>
											</div>
											<table class="table table-bordered" id="datatable" >
											   	<thead>
											   		<tr>
														<th>Name</th>
														<th>Email</th>
														<th>Subscription ID</th>
														<th>Subscription Expiry Date</th>
	                                                    <th>Subscription Date</th>
														<th></th>
													</tr>
											   	</thead>
											   	<tbody>

											   	<?php 
											   	if(!empty($rows)){
											   	foreach ($rows as $key => $val) { ?>
											   		
											   		<tr>
														<td><?php echo $val['v_firstname'].' '.$val['v_lastname']; ?></td>
	                                                    <td><?php echo $val['v_email']; ?></td>
	                                                    <td><?php echo $val['v_subscription_id'] ?></td>
	                                                    <td><?php echo $val['d_subscription_exp_date']; ?></td>
														<td><?php echo $val['d_subscription_date']; ?></td>
														<td>
															<div class="btn-group action_btns">
																<a class="btn btn-primary btn-xs" title="Edit" href="<?php echo base_url().'admin/manage_payment/edit/'.$val["id"].'/';?>"><span class="fa fa-edit"></span></a>

																<a class="btn btn-danger btn-xs" title="Delete" href="javascript:void(0)" class="md-trigger delete-confirmation" onClick="showConfirmBox('<?php echo base_url().'admin/manage_payment/delete/'.$val["id"].'/';?>')" ><span class="fa fa-trash"></span></a>
															</div>
														</td>
													</tr>
												<?php } }else{?>

	                                                <tr><td colspan="4">No Record found.</td></tr>

	                                            <?php }?>
											   </tbody>
											</table>	
											<div class="row">
												<div class="col-sm-12">
													<div class="pull-left"> <?php echo $this->paging_model->getPagesCounter();?> </div>
													<div class="pull-right">
														<div class="dataTables_paginate paging_bs_normal">
															<ul class="pagination">
																<?php $this->paging_model->writePagesLinks(); ?>
															</ul>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
											<input type="hidden" name="a" value="<?php echo @$_REQUEST['a'];?>" />
											<input type="hidden" name="st" value="<?php echo @$_REQUEST['st'];?>" />
											<input type="hidden" name="sb" value="<?php echo @$_REQUEST['sb'];?>" />
										</form>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
