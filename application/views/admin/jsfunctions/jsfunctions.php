<script type="text/javascript">
	function redirectTo(path){
		window.location.href=path;
	}

	function showConfirmBox(path){
    var delete_path= path
    $("#colored-primary").niftyModal("show"); 
    $("#delete-record").attr('href', path)
  }

  $(function(){
	  
    $(document).on('click', '.btn-add', function(e){
        e.preventDefault();

        var controlForm = $('.controls form:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
    $(this).parents('.entry:first').remove();

    e.preventDefault();
    return false;
  });
});
  
</script>

         <div id="colored-primary" class="md-modal colored-header md-effect-10">
            <div class="md-content">
              <div class="modal-header">
               <h3>Confirm Delete Record ?</h3>
                <button type="button" class="close md-close" data-dismiss="modal" aria-hidden="true">&times;</button>
              </div>
              <div class="modal-body">
                <div class="text-center">
                  <div class="i-circle primary"><i class="fa fa-question"></i></div>
                  <h4>Delete!</h4>
                  <p>Are you sure you want to delete this <span id="rec_msg">record</span>?</p>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat md-close" data-dismiss="modal">Cancel</button>
                <a href="" id="delete-record" class="btn btn-primary btn-flat md-close">Delete</a>
              </div>
            </div>
	       </div> 
 <div class="md-overlay"></div>