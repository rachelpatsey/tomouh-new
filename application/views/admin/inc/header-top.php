<?php
  $site_logo = $this->admin_model->getSetting('SITE_LOGO');
  $site_url = $this->admin_model->getSetting('SITE_URL');
?>
<?php $login_data = $this->session->userdata["admin"]; ?>
<div id="head-nav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="fa fa-gear"></span>
        </button>
        <!-- <a class="navbar-brand" href="#"><span><?php echo $this->admin_model->getSetting('SITE_NAME');?></span></a> -->
        <a class="navbar-brand" href="<?php echo $site_url;?>"><img class="img-responsive" src="<?php echo base_url().'assets/frontend/images/'.$site_logo;?>"> </a>
      </div>
      <div class="navbar-collapse collapse">
        
        <ul class="nav navbar-nav navbar-right user-nav">
          <li class="dropdown profile_menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span><?php echo $login_data['v_username']; ?></span> <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo $this->config->config['base_url'].'admin/logout/'; ?>">Sign Out</a></li>
            </ul>
          </li>
        </ul>		
      </div><!--/.nav-collapse animate-collapse -->
    </div>
</div>