<?php
$viewMethod = $this->router->fetch_method();
$basefile = $viewMethod.".php";
$user_data = $this->session->userdata["admin"];
		
?>
<div class="cl-sidebar" data-position="right" data-step="1">
  <div class="cl-toggle"><i class="fa fa-bars"></i></div>
  <div class="cl-navblock">
    <div class="menu-space">
      <div class="content">
        <div class="side-user">
          <div class="avatar"></div>
          <div class="info">
            <img src="<?php echo $this->config->config['base_url'].'/assets/images/state_online.png'; ?>" alt="Status" /> <span>Online <a href="<?php echo $this->config->config['base_url'].'admin/logout/';?>"><span class="">( Logout )</span></a></span>
            <div class="clear"></div><span class="fleft"><a href="<?php echo $this->config->config['base_url']; ?>" target="_blank">View Site</a></span>
          </div>
        </div>
        <ul class="cl-vnavigation">
        <li class="<?php if($basefile == "dashboard.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/dashboard"><i class="fa fa-tachometer" aria-hidden="true"></i><span>Dashboard</span></a></li>

          <li><a href="#"><i class="fa fa-home"></i><span>Manage Settings</span></a>
            <ul class="sub-menu">
              
              <?php if($user_data['i_super'] == 1){?>
              
              <li class="<?php if($basefile=='settings.php') echo "active";?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/settings/"><i class="fa fa-cogs" aria-hidden="true"></i><span>General Settings</span></a></li>
              <li class="<?php if($basefile=='manage_admin.php') echo "active";?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_admin/"><i class="fa fa-user" aria-hidden="true"></i><span>Manage Admin</span></a></li>
              <?php }?>
             
              <li class="<?php if($basefile == "email_template.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/email_template"><i class="fa fa-envelope nav-icon"></i><span>Manage Email Templates</span></a></li>

              <li class="<?php if($basefile == "manage_menu.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_menu"><i class="fa fa-list"></i><span>Manage Menu</span></a></li>

              <li class="<?php if($basefile == "home_slider.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/home_slider"><i class="fa fa-sliders "></i><span>Manage Slider</span></a></li>
              <li class="<?php if($basefile == "import_members.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/import_members"><i class="fa  fa-stack-exchange"></i><span>Import Members</span></a></li>

            </ul>
          </li>
          <li class=""><a href="<?php echo $this->config->config['base_url']; ?>admin/pages/"><i class="fa fa-file"></i><span>Manage Pages</span></a>
            <ul class="sub-menu">
              <li class="<?php if($basefile=='pages.php') echo "active";?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/pages/"><i class="fa fa-file"></i><span>Manage Pages</span></a></li>
                <li class="<?php if($basefile=='meta_fields.php') echo "active";?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/meta_fields/"><i class="fa fa-file"></i><span>Manage Meta Fields</span></a></li>
            </ul>
           </li>
           <li class=""><a href="<?php echo $this->config->config['base_url']; ?>admin/pages/"><i class="fa fa-align-justify"></i><span>Manage Dropdowns</span></a>
            <ul class="sub-menu">
              <li class="<?php if($basefile=='manage_country.php') echo "active";?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_country"><i class="fa fa-align-justify"></i><span>Manage Country</span></a></li>
                <li class="<?php if($basefile=='manage_industry.php') echo "active";?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_industry/"><i class="fa fa-align-justify"></i><span>Manage Industry</span></a></li>
            </ul>
           </li>
          <li class="<?php if($basefile == "team.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/team"><i class="fa fa-users" aria-hidden="true"></i><span>Manage Team</span></a></li>

          <li class="<?php if($basefile == "members.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/members"><i class="fa fa-user" aria-hidden="true"></i><span>Manage Members</span></a></li>

          <li class="<?php if($basefile == "manage_events.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_events"><i class="fa fa-file" aria-hidden="true"></i><span>Manage Events</span></a></li>

          <li class="<?php if($basefile == "members_logs.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/members_logs"><i class="fa fa-user" aria-hidden="true"></i><span>Members Logs</span></a></li>
          <li class="<?php if($basefile == "members_update.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/members_update"><i class="fa fa-users" aria-hidden="true"></i><span>Manage Members Update</span></a></li>

          <li class="<?php if($basefile == "manage_initiatives.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_initiatives"><i class="fa fa-cogs" aria-hidden="true"></i><span>Manage Initiatives</span></a></li>

          <?php /*?><li class="<?php if($basefile == "manage_user_request.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_user_request"><i class="fa fa-database" aria-hidden="true"></i><span>Manage User Request Data</span></a></li><?php */?>

          <li class="<?php if($basefile == "books.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/books"><i class="fa fa-book" aria-hidden="true"></i><span>Manage Books</span></a></li>

          <li class="<?php if($basefile == "photos.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/photos"><i class="fa fa-picture-o" aria-hidden="true"></i><span>Manage Photos</span></a></li>

          <li class="<?php if($basefile == "videos.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/videos"><i class="fa fa-video-camera" aria-hidden="true"></i><span>Manage Videos</span></a></li>
                     
           <li class="<?php if($basefile == "manage_news.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_news"><i class="fa fa-file-text" aria-hidden="true"></i><span>Manage News</span></a></li>
           <!-- <li class="<?php if($basefile == "manage_fellows.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_fellows"><i class="fa fa-file-text" aria-hidden="true"></i><span>Manage Awards</span></a></li> -->
           
            <li class="<?php if($basefile=='manage_fellows.php') echo "active";?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_fellows"><i class="fa fa-align-justify"></i><span>Manage Awards</span></a></li>
              
           <li class="<?php if($basefile == "manage_founders.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_founders"><i class="fa fa-file-text" aria-hidden="true"></i><span>Manage Founders</span></a></li>
          

           <li class="<?php if($basefile == "manage_faqs.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_faqs"><i class="fa fa-question-circle" aria-hidden="true"></i><span>Manage FAQ</span></a></li>

           <li class="<?php if($basefile == "manage_testimonials.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_testimonials"><i class="fa fa-suitcase" aria-hidden="true"></i><span>Manage Testimonials</span></a></li>

           <!-- <li class="<?php if($basefile == "manage_payment.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_payment"><i class="fa fa-credit-card-alt" aria-hidden="true"></i><span>Manage Payment</span></a></li> -->
           
           <!-- <li class="<?php if($basefile == "manage_coupons.php"){ echo "active"; } ?>"><a href="<?php echo $this->config->config['base_url']; ?>admin/manage_coupons"><i class="fa fa-money" aria-hidden="true"></i><span>Manage Coupons</span></a></li> -->
                       
        </ul>
      </div>
    </div>
    <div class="text-right collapse-button" style="padding:7px 9px;">
      <button id="sidebar-collapse" class="btn btn-default" style=""><i style="color:#fff;" class="fa fa-angle-left"></i></button>
    </div>
  </div>
</div>