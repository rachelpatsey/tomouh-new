<?php error_reporting(0); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- <link rel="shortcut icon" href="images/favicon.png"> -->
<link rel="icon" type="image/png" href="<?php echo base_url(); ?>/favicon.png">

<title><?php echo $page_title; ?></title>

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->config['base_url'].'assets/css/admin/AdminLTE.css';?>" />

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->config['base_url'].'assets/js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css';?>" />

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="<?php echo $this->config->config['base_url'].'assets/js/bootstrap/dist/css/bootstrap.css';?>">
<link rel="stylesheet" href="<?php echo $this->config->config['base_url'].'assets/fonts/font-awesome-4/css/font-awesome.min.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->config['base_url'].'assets/js/jquery.nanoscroller/nanoscroller.css';?>" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->config['base_url'].'assets/js/jquery.easypiechart/jquery.easy-pie-chart.css';?>" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->config['base_url'].'assets/js/bootstrap.switch/bootstrap-switch.css';?>" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->config['base_url'].'assets/js/jquery.select2/select2.css';?>" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->config['base_url'].'assets/js/bootstrap.slider/css/slider.css';?>" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->config['base_url'].'assets/js/jquery.datatables/bootstrap-adapter/css/datatables.css';?>" />
<link rel="stylesheet" href="<?php echo $this->config->config['base_url'].'assets/js/jquery.icheck/skins/square/blue.css';?>" >
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->config['base_url'].'assets/js/dropzone/css/dropzone.css';?>" />
<link rel="stylesheet" href="<?php echo $this->config->config['base_url'].'assets/js/jquery.niftymodals/css/component.css';?>"  /> 

<!-- Custom styles for this template -->
<link rel="stylesheet" href="<?php echo $this->config->config['base_url'].'assets/css/admin/style.css';?>"/> 
<!-- Conform Dialoge Box -->


<link rel="stylesheet" href="<?php echo $this->config->config['base_url'].'video-js/video-js.css';?>" />
<script src="<?php echo $this->config->config['base_url'].'video-js/videojs-ie8.min.js'; ?>"></script>
<script src="<?php echo $this->config->config['base_url'].'video-js/video.js'; ?>"></script>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="<?php echo $this->config->config['base_url'].'assets/js/jquery.js';?>"></script> 

<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.parsley/parsley.js';?>" ></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.nanoscroller/jquery.nanoscroller.js';?>"></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.sparkline/jquery.sparkline.min.js';?>"></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.easypiechart/jquery.easy-pie-chart.js';?>"></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/behaviour/general.js';?>"></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.ui/jquery-ui.js';?>" ></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.nestable/jquery.nestable.js';?>"></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/bootstrap.switch/bootstrap-switch.min.js';?>"></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js';?>"></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.select2/select2.min.js';?>" ></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/bootstrap.slider/js/bootstrap-slider.js';?>" ></script> 

<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.datatables/jquery.datatables.min.js';?>"></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.datatables/bootstrap-adapter/js/datatables.js';?>"></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.icheck/icheck.min.js';?>"></script>

<script src="<?php echo $this->config->config['base_url'].'assets/js/cropper/cropper.min.js';?>"></script>
<link rel="stylesheet" href="<?php echo $this->config->config['base_url'].'assets/js/cropper/cropper.min.css';?>">
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/dropzone/dropzone.js';?>"></script>

<?php /*?><script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/dropzone-main.js';?>"></script><?php */?>
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/ckeditor/ckeditor.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/ckeditor/adapters/jquery.js';?>"></script> 

<script type="text/javascript">
    $(document).ready(function(){
      //initialize the javascript
      App.init();

   $('form').parsley();
       
  });
    jQuery(document).ready(function(){
		setTimeout(function(){
			jQuery('audio').css('display','inline-block');
		},700);

	});
</script> 

 

<!-- Bootstrap core JavaScript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 

<script src="<?php echo $this->config->config['base_url'].'assets/js/behaviour/voice-commands.js';?>"></script> 
<script src="<?php echo $this->config->config['base_url'].'assets/js/bootstrap/dist/js/bootstrap.min.js';?>"></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.flot/jquery.flot.js';?>"></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.flot/jquery.flot.pie.js';?>"></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.flot/jquery.flot.resize.js';?>"></script> 
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.flot/jquery.flot.labels.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->config->config['base_url'].'assets/js/jquery.niftymodals/js/jquery.modalEffects.js';?>"></script>




</head>
<body>