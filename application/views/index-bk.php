<?php
    $query_params =array();
    $url = $meta['home_page_video'];
    $query_str = parse_url($url, PHP_URL_QUERY);
    parse_str($query_str, $query_params);  

    $latitude = json_encode($lan_lat); 

include("inc/header_top.php");
include("inc/header.php"); ?>

<style type="text/css">
  .video_section .video_main{
    position: relative;
    display: block;
  }
  .video_section .video_texts{
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    text-align: center;
  }
  .video_section .video_texts img{
    margin: 210px auto 0;
  }
  /* @media(max-width: 575px){
	.map_section h1 {
		margin-bottom: 10px !important;
	}
  } */
  .home_slider .carousel .carousel-inner {
    margin-top: 125px !important;
    position: relative;
  }

  .slider_text{
      color: #fff !important;
      position: absolute;
      z-index: 9;
      top: 45%;
      left: 50%;
      transform: translate(-50%);
      font-size: 40px;
      line-height: normal;
      text-align: center;
      text-shadow: 1px 2px black;
  }

</style>

</head>

<body>
<div class="wrapper">
  <div class="home_slider">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">       
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <?php
       
        if(isset($sliders)){
            $length = sizeof($sliders);
            for($i=0;$i<$length;$i++){
         ?>        
        <div class="item<?php if($i == 0){?> active <?php } ?>">
        <img src="<?php echo base_url(); ?>assets/frontend/images/<?php if(isset($sliders[$i]['v_image'])){ echo $sliders[$i]['v_image']; }  ?>"><p class="white-text slider_text"><?php if(isset($sliders[$i]['v_text'])){ echo $sliders[$i]['v_text']; }  ?></p>
        </div>
        <?php
         }
        }
        ?>
      </div>
  </div>
  <?php if(isset($main_description) && !empty($main_description)){?>
  <section class="red_section">
    <p class="white-text" id="home-dec"><?php echo $main_description; ?></p>
  </section>
  <?php } ?>
  <section class="map_section">
    <br/>
    <div class="page_head" style="float:none !important;">
      <h1 class="red-text text-center font36">Global Presence</h1>
    </div>
    <div id="visualization" class="chart-div"></div> 
  </section>  
</div>

 <script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>
 <script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script>
    // media query event handler
    var mql = window.matchMedia("screen and (max-width: 767px)");
    if (mql.matches){ // if media query matches
    //alert("Window is 1199px or wider")
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
        margin:0,
        dots:false,
        loop :true,
        autoplay:true,
        slideSpeed:500, 
                responsive:{
          0:{
            items:1
          },
                    375:{
            items:2
          },
          600:{
            items:4
          },
          1000:{
            items:4
          }     
        }
        });
      });
    }
    else{
    // do something else
    }
</script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/client/js/owl.carousel.js"></script> 
<script type="text/javascript">
  
  $(document).ready(function(){
    $('.video_main').click(function(){
      $(this).removeClass('video_main');
      $("#video")[0].src += "&autoplay=1";
      $('.video_texts').css('display','none');
    });
    if ($('#home-dec').contents().find("body").length == 0){
      $('.red_section').remove();
    }
  });    
</script>
<script>
$(window).scroll(function() {    
  var scroll = $(window).scrollTop();
  if (scroll >= 100) {
    $("#menu").addClass("menufixed", 2000);
  }  
  var scroll = $(window).scrollTop();
  if (scroll <= 100) {
    $("#menu").removeClass("menufixed");
  }
  });
        $(window).scroll(function() {    
  var scroll = $(window).scrollTop();
  if (scroll >= 100) {
    $("#menu1").addClass("menufixed", 2000);
  }  
  var scroll = $(window).scrollTop();
  if (scroll <= 100) {
    $("#menu1").removeClass("menufixed");
  }
});

// New Map
google.load('visualization', {packages: ['geochart']});
function drawVisualization() {
  var lang_lat = '<?php echo $latitude; ?>';
  var locations = jQuery.parseJSON(lang_lat); 
  var width, height, selectedRegion, resolution;
  width = 1284;
  height = 600;
  var i;  
  var data = new google.visualization.DataTable();
  <?php
  $_is_show=$this->tomouh_model->getSetting('IS_SHOW');
  if(isset($_is_show) && $_is_show == 1){?>
      // data.addColumn('number', 'Lat');                                
      // data.addColumn('number', 'Long');
      data.addColumn('string', 'country of residence');
      data.addColumn('number', 'Members'); 
    for (i = 0; i < locations.length; i++) {  
      // console.log(locations);
      if( locations[i]['country_name'] != "Qatar"){
        data.addRows([
        [        
          // parseFloat(locations[i]['latitude']),
          // parseFloat(locations[i]['longitude']),
          locations[i]['country_name'],
          parseInt(locations[i]['total_members']),  
        ]
      ]);
      }
    }<?php
  }else{?>
    // data.addColumn('number', 'Lat');                                
    // data.addColumn('number', 'Long');
    data.addColumn('string', 'country of residence');
    data.addColumn('number', 'Members'); 
    data.addColumn({type: 'string', role: 'tooltip'});
    for (i = 0; i < locations.length; i++) { 
      if( locations[i]['country_name'] != "Qatar"){
        data.addRows([
        [        
          // parseFloat(locations[i]['latitude']),
          // parseFloat(locations[i]['longitude']),
          locations[i]['country_name'],
          parseInt(locations[i]['total_members']),    
          '',    
        ]
      ]);
      }
    }<?php
  }?>

 
  var options = {
    //height: height,
    // width: width,
    colorAxis: {values: [1, 50, 250, 500], colors: ['#001933', '#004c99', '#990000', '#ed1c24'],},
		backgroundColor: '#ffffff',
		datalessRegionColor: '#999999',
		defaultColor: '#f5f5f5',
    legend: 'none',
    tooltip:{textStyle: {color: '#800000'}, showColorCode: true},
  }
  if (window.matchMedia("(max-width: 575px)").matches){
    options['height']='';
  }else{
    options['height']=height;
  }
  var geochart = new google.visualization.GeoChart(document.getElementById('visualization'));
  geochart.draw(data, options);
}
google.setOnLoadCallback(drawVisualization);
</script>
<script src="<?php echo base_url(); ?>assets/client/js/custom.js"></script>
<?php include('inc/footer.php') ?>