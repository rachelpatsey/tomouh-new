<?php include("inc/header_top.php");
include("inc/header.php"); ?>
<div class="after-login">
  <div class="inner_wrapper">
    <div class="inner_header_div">
      <div class="container">
        <div class="row">
          <div class="col-sm-7 col-md-8 col-xs-12">
            <div class="left-div"> <a href="<?php echo base_url(); ?>my_home" class="">My Home</a> <a href="<?php echo base_url(); ?>members">Members</a> <a href="<?php echo base_url(); ?>event">Events</a> <a href="<?php echo base_url(); ?>initiative">Initiatives</a> </div>
          </div>
          <div class="col-sm-5 col-md-4  col-xs-12">
            <div class="right-div"> <a href="<?php echo base_url(); ?>account">Account</a> <a href="<?php echo base_url(); ?>profile" class="active">Profile</a> </div>
          </div>
        </div>
      </div>
    </div>
    <div class="profile_page" id="profile_page">
      <section class="event_cal">
        <div class="sec_banner">
          <div class="container">
            <div class="page_head">
              <h1 class="red-text text-center font36">Profile</h1>
            </div>
            <div class="profile_info">
              <div class="profile_tabs">
                <ul class="nav nav-tabs nav-justified">
                  <li class="active"><a data-toggle="tab" href="#personal_info">PERSONAL</a></li>
                  <li><a data-toggle="tab" href="#edu_info">EDUCATION & Professional</a></li>
                </ul>
                <div class="tab-content">
                  <div id="personal_info" class="tab-pane fade in active">
                    <div class="border-box">
                      <div class="personal_info">
                        <div class="col-sm-3 col-md-4 col-xs-12 img-div">
                          <div class="table-cell"> <img src="<?php echo base_url(); ?>assets/frontend/images/personal_info.png" class="img-responsive" alt="">
                            <p><strong>Personal Information</strong></p>
                          </div>
                        </div>
                        <div class="col-sm-9 col-md-8 col-xs-12 profile_detail">
                          <div class="table-cell">
                            <?php if (!isset($id)) { ?>
                              <div class="row">
                                <div class="col-md-4 col-sm-6 col-xs-6">
                                  <p>First Name</p>
                                </div>
                                <div class="col-md-8 col-sm-6 col-xs-6">
                                  <p><strong><?php echo $user_data['v_firstname']; ?></strong></p>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-4 col-sm-6 col-xs-6">
                                  <p>Last Name</p>
                                </div>
                                <div class="col-md-8 col-sm-6 col-xs-6">
                                  <p><strong><?php echo $user_data['v_lastname']; ?></strong></p>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-4 col-sm-6 col-xs-6">
                                  <p>Gender</p>
                                </div>
                                <div class="col-md-8 col-sm-6 col-xs-6">
                                  <p><strong><?php echo $user_data['e_gender']; ?></strong></p>
                                </div>
                              </div>
                            <?php } else { ?>
                              <div class="row">
                                <div class="col-md-4 col-sm-6 col-xs-6">
                                  <p>Name</p>
                                </div>
                                <div class="col-md-8 col-sm-6 col-xs-6">
                                  <p><strong><?php echo $user_data['v_firstname'] . ' ' . $user_data['v_lastname']; ?></strong></p>
                                </div>
                              </div>
                              <?php /*?><div class="row">
                              <div class="col-md-4 col-sm-6 col-xs-6">
                                <p>Type</p>
                              </div>
                              <div class="col-md-8 col-sm-6 col-xs-6">
                                <p><strong><?php echo $user_data['v_label']; ?></strong></p>

                              </div>
                            </div><?php */ ?>
                            <?php } ?>
                            <?php if (!isset($id)) { ?>
                              <div class="row">
                                <div class="col-md-4 col-sm-6 col-xs-6">
                                  <p>Date</p>
                                </div>
                                <div class="col-md-8 col-sm-6 col-xs-6">
                                  <p><strong><?php echo $user_data['d_dob']; ?></strong></p>
                                </div>
                              </div>
                            <?php } ?>
                          </div>
                        </div>
                      </div>
                      <div class="contact_info">
                        <div class="col-sm-3 col-md-4 col-xs-12 img-div">
                          <div class="table-cell"> <img src="<?php echo base_url(); ?>assets/frontend/images/contact_info.png" class="img-responsive" alt="">
                            <p><strong>Contact Information</strong></p>
                          </div>
                        </div>
                        <div class="col-sm-9 col-md-8 col-xs-12 profile_detail">
                          <div class="table-cell">
                            <div class="row">
                              <div class="col-md-4 col-sm-6 col-xs-6">
                                <p>Home Country</p>
                              </div>
                              <div class="col-md-8 col-sm-6 col-xs-6">
                                <p><strong><?php echo $user_data['v_home_country']; ?></strong></p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-4 col-sm-6 col-xs-6">
                                <p>Home City</p>
                              </div>
                              <div class="col-md-8 col-sm-6 col-xs-6">
                                <p><strong><?php echo $user_data['v_home_city']; ?></strong></p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-4 col-sm-6 col-xs-6">
                                <p>Country of Residence</p>
                              </div>
                              <div class="col-md-8 col-sm-6 col-xs-6">
                                <p><strong><?php echo $user_data['v_residence_country']; ?></strong></p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-4 col-sm-6 col-xs-6">
                                <p>City of Residence</p>
                              </div>
                              <div class="col-md-8 col-sm-6 col-xs-6">
                                <p><strong><?php echo $user_data['v_residence_city']; ?></strong></p>
                              </div>
                            </div>
                            <?php /*?>
                            <div class="row">
                              <div class="col-md-4 col-sm-6 col-xs-6">
                                <p>Mobile Number</p>
                              </div>
                              <div class="col-md-8 col-sm-6 col-xs-6">
                                <p><strong><?php echo $user_data['v_telephone']; ?></strong></p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-4 col-sm-6 col-xs-6">
                                <p>Mobile Number 2</p>
                              </div>
                              <div class="col-md-8 col-sm-6 col-xs-6">
                                <p><strong><?php echo $user_data['v_telephone_2']; ?></strong></p>
                              </div>
                            </div>
                            <?php */ ?>
                          </div>
                        </div>
                      </div>
                      <div class="social_info">
                        <div class="col-sm-3 col-md-4 col-xs-12 img-div">
                          <div class="table-cell"> <img src="<?php echo base_url(); ?>assets/frontend/images/social.png" class="img-responsive" alt="">
                            <p><strong>Social</strong></p>
                          </div>
                        </div>
                        <div class="col-sm-9 col-md-8 col-xs-12 profile_detail">
                          <div class="table-cell">
                            <div class="row">
                              <div class="col-md-4 col-sm-6 col-xs-6">
                                <p>Twitter Link</p>
                              </div>
                              <div class="col-md-8 col-sm-6 col-xs-6">
                                <p><strong>
                                    <?php if ($user_data['v_twitter_link'] != '') { ?>
                                      <a href="<?php echo $user_data['v_twitter_link']; ?>" target="_blank">
                                        <?php echo $user_data['v_twitter_link']; ?>
                                      </a>
                                    <?php } ?>
                                  </strong>
                                </p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-4 col-sm-6 col-xs-6">
                                <p>LinkedIn Link</p>
                              </div>
                              <div class="col-md-8 col-sm-6 col-xs-6">
                                <p><strong>
                                    <?php if ($user_data['v_linkedin_link'] != '') { ?>
                                      <a href="<?php echo $user_data['v_linkedin_link']; ?>" target="_blank">
                                        <?php echo $user_data['v_linkedin_link']; ?>
                                      </a>
                                    <?php } ?>
                                  </strong></p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-4 col-sm-6 col-xs-6">
                                <p>Instagram Link</p>
                              </div>
                              <div class="col-md-8 col-sm-6 col-xs-6">
                                <p><strong>
                                    <?php if ($user_data['v_instagram_link'] != '') { ?>
                                      <a href="<?php echo $user_data['v_instagram_link']; ?>" target="_blank">
                                        <?php echo $user_data['v_instagram_link']; ?>
                                      </a>
                                    <?php } ?>
                                  </strong></p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php
                    if (!isset($id)) {
                      ?>
                      <div class="btn-div"> <a href="<?php echo base_url(); ?>edit_profile" class="btn gray-border-btn">Edit</a> </div>
                    <?php
                    }
                    ?>
                  </div>
                  <div id="edu_info" class="tab-pane fade">
                    <div class="border-box">
                      <div class="education_info">
                        <p class="form-head">Educational Information</p>
                        <div class="custom">
                          <div class="table-responsive">
                            <table class="table">
                              <thead>
                                <tr>
                                  <th>University</th>
                                  <th>Degree</th>
                                  <th>Major</th>
                                  <th>Year</th>
                                  <!-- <th style="text-align: center;">Main Occupation</th> -->
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($educational_data as $row) { ?>
                                  <tr style="height: 55px;">
                                    <td><?php echo $row['v_university']; ?></td>
                                    <td><?php echo $row['v_degree']; ?></td>
                                    <td><?php echo $row['v_major']; ?></td>
                                    <td><?php echo $row['i_passing_year']; ?></td>
                                    <!-- <td><div class="radio" style="margin-left: 9%;">
                                      <input type="radio" <?php if ($row['i_main_occupation'] == 1) { ?> checked <?php } ?> >
                                      <label for="radio3"></label>
                                    </div></td> -->
                                  </tr>
                                <?php } ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="border-box">
                      <div class="professional_info">
                        <p class="form-head">Professional Information</p>
                        <div class="custom">
                          <div class="table-responsive">
                            <table class="table">
                              <thead>
                                <tr>
                                  <th>Company</th>
                                  <th>Job Title</th>
                                  <th>Industry</th>
                                  <th>From</th>
                                  <th>To</th>
                                  <!-- <th style="text-align: center;">Main Occupation</th> -->
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($professional_data as $row) { ?>
                                  <tr style="height: 55px;">
                                    <td><?php echo $row['v_company']; ?></td>
                                    <td><?php echo $row['v_job_title']; ?></td>
                                    <td><?php echo $row['v_industry']; ?></td>
                                    <td><?php echo $row['i_company_from']; ?></td>
                                    <td><?php echo $row['i_company_to']; ?></td>
                                    <!-- <td><div class="radio" style="margin-left: 9%;">
                                      <input type="radio" <?php if ($row['i_main_occupation'] == 1) { ?> checked <?php } ?> >
                                      <label for="radio6"></label>
                                    </div>
                                  </td> -->
                                  </tr>
                                <?php } ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="border-box">
                      <div class="achievement_info">
                        <p class="form-head" style="padding-bottom: 7px;">Achievements, Awards and Honors</p>
                        <ul style="padding-top: 0px;padding-bottom: 0px;">
                          <?php foreach ($achievement_data as $row) { ?>

                            <li><?php echo $row['v_achievement']; ?></li>

                          <?php } ?>
                        </ul>
                      </div>
                    </div>
                    <div class="border-box">
                      <div class="achievement_info">
                        <p class="form-head" style="padding-bottom: 7px;">Passions and Interests</p>
                        <p style="margin-left: 15px;"><?php echo $user_data['t_passion_interest']; ?></p>
                      </div>
                    </div>
                    <?php
                    if (!isset($id)) {
                      ?>
                      <div class="btn-div"> <a href="<?php echo base_url(); ?>edit_profile?edu_info=1" class="btn gray-border-btn">Edit</a> </div>
                    <?php
                    }
                    ?>

                  </div>
                  <?php if (isset($id)) { ?>
                    <div class="btn-div">
                      <a class="btn gray-border-btn" download target="_blank" href="<?php echo base_url(); ?>profile/download/<?php echo $user_data['id'] ?>"><i class="fa fa-file-pdf-o"></i> Download Pdf</a>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
<?php include('inc/footer.php'); ?>