<?php
include('inc/header_top.php');
include('inc/header.php');
?>
<div class="inner_wrapper">
  <div class="blog_page">
    <div class="sec_banner">
      <div class="container">
        <div class="page_head">
          <h1 class="red-text text-center font36"><?php if(isset($text['l_value'])){ echo $text['l_value']; }?></h1>
        </div>
        <div class="row">
        <div class="col-md-12">
            <div class="blog">
              <div class="blog_img" style="background-image: url('<?php echo base_url(); ?>assets/images/<?php if(isset($image['l_value'])){ echo $image['l_value']; }?>');"></div>
              <p class="blog_name spectral-font"><?php if(isset($text['l_value'])){ echo $text['l_value']; }?></p>
              <div class="blog_desc"><?php if(isset($description['l_value'])){ echo $description['l_value']; }?></div>
            </div>
          	</div>
      	   </div>
  	      </div>
	    </div>
	  </div>
 </div>
<?php include('inc/footer.php'); ?>