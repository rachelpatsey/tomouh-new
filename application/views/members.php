<?php

include("inc/header_top.php");
include("inc/header.php");
?>
<style type="text/css">
  body.after-login{
    padding-right: 0px !important;
  }
  input.parsley-error {
    border: 1px solid red;
  }

  .parsley-error-list {
      color: red;
  }
  .error_box{display: inline-block;width: 100%;padding-top: 20px;padding-left: 10%;padding-right: 10%;}
  .error_box .icon{float: left;padding-right: 10px;}
</style>
<div class="after-login">
<div class="inner_wrapper">
              <?php
              if(!($this->form_validation->error_array())){
                if(isset($_GET['msg']) && $_GET['msg'] !=''){ ?>
                <div id="response" class="error_box">
                <?php  if($_GET['succ']==1){

                    echo $this->messages_model->getSuccessMsg($_GET['msg']);
                  }
                  else if($_GET['succ']==0){

                    echo $this->messages_model->getErrorMsg($_GET['msg']);
                  } ?>
              </div>
              <?php  }
              }?>
                          <?php echo validation_errors('<div class="alert alert-danger alert-white rounded">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <div class="icon"><i class="fa fa-times-circle"></i></div>', '</div>');?>
  <div class="inner_header_div">
    <div class="container">
      <div class="row">
        <div class="col-sm-7 col-md-8 col-xs-12">
          <div class="left-div"> <a href="<?php echo base_url(); ?>my_home" class="">My Home</a> <a href="<?php echo base_url(); ?>members" class="active">Members</a> <a href="<?php echo base_url(); ?>event">Events</a> <a href="<?php echo base_url(); ?>initiative">Initiatives</a> </div>
        </div>
        <div class="col-sm-5 col-md-4  col-xs-12">
          <div class="right-div"> <a href="<?php echo base_url(); ?>account" >Account</a> <a href="<?php echo base_url(); ?>profile">Profile</a> </div>
        </div>
      </div>
    </div>
  </div>
  <div class="member_search_page">
    <div class="container">
      <div class="gray-border-box">
      <form action="" method="get" name="frm_members">
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <p class="form-head">Search a member</p>
          </div>
          <div class="col-xs-12 col-sm-6">
            <p class="text-right note">*All search fields are optional</p>
          </div>
        </div>
        <div class="control-group after-add-more">
          <div class="row">
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>First name</label>
                <input class="form-control" name="firstname" id="firstname" value="<?php if(isset($post_data['firstname'])){ echo $post_data['firstname']; } ?>" type="text">
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Last name</label>
                <input class="form-control" name="lastname" id="lastname" value="<?php if(isset($post_data['lastname'])){ echo $post_data['lastname']; } ?>" type="text">
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Keyword</label>
                <input class="form-control" name="keyword" id="keyword" value="<?php if(isset($post_data['keyword'])){ echo $post_data['keyword']; } ?>" type="text">
              </div>
            </div>
          </div>
        </div>
      <div id="advance_search" style="<?php if(isset($flag) && $flag == 1) { ?> display: block;<?php }else{ ?>display: none; <?php } ?>">
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <p class="form-head">Address</p>
          </div>
        </div>
        <div class="control-group after-add-more">
          <div class="row">
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <input type="hidden" name="advance_search_flag" id="advance_search_flag">
                <input type="hidden" name="search_form_flag" id="search_form_flag" value="<?php if(isset($search_form_flag)){ echo $search_form_flag; } ?>">
                <label>Country of residence</label>
               <div class="select">
                  <select class="selectpicker" name="residence_country" id="residence_country">
                    <option value="">Select Country</option>
                    <?php foreach($countries as $country) {?>
                    <option <?php if(isset($post_data)){ if(!empty($post_data['residence_country']) && $post_data['residence_country'] == $country->country_name ){ ?> selected <?php } }?>><?php echo $country->country_name; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>City of residence</label>
                <input class="form-control" name="residence_city" id="residence_city" value="<?php if(isset($post_data['residence_city'])){ echo $post_data['residence_city']; } ?>" type="text">
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Home country</label>
                 <div class="select">
                  <select class="selectpicker" name="home_country" id="home_country">
                     <option value="">Select Country</option>
                    <?php foreach($countries as $country) {?>
                    <option <?php if(isset($post_data)){ if(!empty($post_data['home_country']) && $post_data['home_country'] == $country->country_name ){ ?> selected <?php } }?>><?php echo $country->country_name; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Home city</label>
                <input class="form-control" name="home_city" id="home_city" value="<?php if(isset($post_data['home_city'])){ echo $post_data['home_city']; } ?>" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <p class="form-head">Education</p>
          </div>
        </div>
        <div class="control-group after-add-more">
          <div class="row">
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>University</label>
               <input class="form-control" name="university" id="university" value="<?php if(isset($post_data['university'])){ echo $post_data['university']; } ?>" type="text">
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Major</label>
                <input class="form-control" name="major" id="major" value="<?php if(isset($post_data['major'])){ echo $post_data['major']; } ?>" type="text">
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Degree</label>
                <input class="form-control" name="degree" id="degree" value="<?php if(isset($post_data['degree'])){ echo $post_data['degree']; } ?>" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <p class="form-head">Career</p>
          </div>
        </div>
        <div class="control-group after-add-more">
          <div class="row">
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Industry</label>
               <input class="form-control" name="industry" id="industry" value="<?php if(isset($post_data['industry'])){ echo $post_data['industry']; } ?>" type="text">
              </div>
            </div>
            <div class="col-sm-3 col-xs-12">
              <div class="form-group">
                <label>Company</label>
                <input class="form-control" name="company" id="company" value="<?php if(isset($post_data['company'])){ echo $post_data['company']; } ?>" type="text">
              </div>
            </div>
          </div>
        </div>
      </div>
        <div class="row">
          <div class="col-sm-12 col-xs-12 pull-right btns"><input type="submit" name="search" value="Search" class="btn dark-gray-btn"> <button type="button" class="btn gray-border-btn bsc_search">Advance Search</button><button type="button" style="margin-left: 5px;" class="btn gray-border-btn clearsearch">Clear</button></div>
        </div>
      </form>
      </div>
      <div class="search_result">
        <div class="gray-border-box">
          <?php
            $i =1;
            if(isset($members['result']) && !empty($members['result'])){
            foreach($members['result'] as $member){
          ?>
          <div class="search_member_box">
            <div class="row">
              <div class="col-sm-4 col-md-3 col-xs-12">
                <!-- <div class="member_no"><?php echo $i; ?></div> -->
                <div class="member_name">
                  <p class="red-text"><?php echo $member['v_firstname'].' '.$member['v_lastname']; ?></p>
                </div>
              </div>
              <div class="col-sm-4  col-md-5 col-xs-12">
                <div class="member_addr"><?php if(isset($member['v_main_occupation']) && !empty($member['v_main_occupation'])){ echo $member['v_main_occupation']; } ?></div>
                <div class="member_addr"><?php if(isset($member['v_residence_city']) && !empty($member['v_residence_city'])){ echo $member['v_residence_city']; } ?><?php if(!empty($member['v_residence_city']) && !empty($member['v_residence_country'])){ echo ", ";} ?><?php if(isset($member['v_residence_country']) && !empty($member['v_residence_country'])){ echo $member['v_residence_country']; } ?></div>
              </div>
              <div class="col-sm-4 col-md-4 col-xs-12">
                <div class="member_btns"> <a class="btn gray-border-btn" href="<?php echo base_url(); ?>profile/<?php echo $member['id']; ?>" target="_blank">View Profile</a> <a class="btn gray-border-btn" data-toggle="modal" data-target="#contact" data-id="<?php echo $member['v_email']; ?>" id="getcontacts">Contact</a> </div>
              </div>
            </div>
          </div>
          <?php
            $i++;
            } 
                $total_page = ceil($members['total_row']/$limit);
                $next_page = $page + 1;
                $pre_page = $page - 1;
            	unset($post_data['page']);
				
				
				
				
			?>
          <div class="pagination_div"> 
          <a <?php if(isset($page) && $page == 1){  }else{?> href="<?php echo base_url(); ?>members?page=1<?php if(isset($post_data)){ foreach($post_data as $key => $value){ echo '&'.$key.'='.$value; } }?>" <?php } ?> class="gray-border-btn back-btn">&lt;&lt;</a>
          <a <?php if(isset($page) && $page == 1){  }else{?> href="<?php echo base_url(); ?>members?page=<?php echo $pre_page; ?><?php if(isset($post_data)){ foreach($post_data as $key => $value){ echo '&'.$key.'='.$value; } }?>" <?php } ?> class="gray-border-btn back-btn">&lt;</a>
            <ul class="custom_pagination">
               
			   <?php
			   
			   
			   if(($total_page - $page) > 8){
			   
			   	 if($page > 2)
			   	 	$startpage = $page-2;
				 else
				 	$startpage = 1;
				  
			   }else{
			   		
					if($total_page > 10)
						$startpage = ($total_page - 10);
			   		else
						$startpage = 1;
			   }
			   
			   if(($total_page - $page) > 8){
				   for( $i=$startpage;$i<($startpage+5);$i++){?>
					<li <?php if($page == $i){ ?> class="active" <?php } ?>><a class="form_submit_page" href="<?php echo base_url(); ?>members?page=<?php echo $i; ?><?php if(isset($post_data)){ foreach($post_data as $key => $value){ echo '&'.$key.'='.$value; } }?>"><?php echo $i; ?></a></li>
				  <?php } ?>
                  <li>..</li>
                  <?php
                  for( $i=($total_page-5); $i<=$total_page; $i++){?>
					<li <?php if($page == $i){ ?> class="active" <?php } ?>><a class="form_submit_page" href="<?php echo base_url(); ?>members?page=<?php echo $i; ?><?php if(isset($post_data)){ foreach($post_data as $key => $value){ echo '&'.$key.'='.$value; } }?>"><?php echo $i; ?></a></li>
				  <?php } ?>
               <?php
			   }else{
                for( $i=$startpage; $i<=$total_page; $i++){
               ?>
              	<li <?php if($page == $i){ ?> class="active" <?php } ?>><a class="form_submit_page" href="<?php echo base_url(); ?>members?page=<?php echo $i; ?><?php if(isset($post_data)){ foreach($post_data as $key => $value){ echo '&'.$key.'='.$value; } }?>"><?php echo $i; ?></a></li>
              <?php } ?>
       		  <?php }?>
            </ul>
            <a <?php if(isset($page) && $page == $total_page){ }else{?> href="<?php echo base_url(); ?>members?page=<?php echo $next_page; ?><?php if(isset($post_data)){ foreach($post_data as $key => $value){ echo '&'.$key.'='.$value; } }?>" <?php } ?>  class="gray-border-btn next-btn">&gt;</a>
            <a <?php if(isset($page) && $page == $total_page){ }else{?> href="<?php echo base_url(); ?>members?page=<?php echo $total_page; ?><?php if(isset($post_data)){ foreach($post_data as $key => $value){ echo '&'.$key.'='.$value; } }?>" <?php } ?>  class="gray-border-btn next-btn">&gt;&gt;</a>
          </div>
        <?php 
      
         }else{
          ?>
          <div class="search_member_box">
            <div class="row">
              <div class="col-xs-12">
                <h3 class="text-center">NO RESULT FOUND</h3>
              </div>
            </div>
          </div>
          <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="contact" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <font color="black" >All * is required Field</font>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        </div>
        <div class="modal-body">
          <div id="user-detail">
            <form action="" method="post" parsley-validate>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="message" class="form-control-label">Subject:<span style="color:red;">*</span></label>
                    <br>
                    <input class="form-control" type="text" name="subject" id="subject" value="" required="required">
                    <input type="hidden" name="member_email" id="member_email">
                  </div>
                  <div class="form-group">
                    <label for="message" class="form-control-label">Message:<span style="color:red;">*</span></label>
                    <textarea class="form-control" rows="5" id="message" name="message" required="required"></textarea>
                  </div>
                  <p>* For quality purposes, all communications are monitored by the administration.</p>
                  <div class="modal-footer">
                    <button type="submit" name="send_mail" value="send_mail" class="btn btn-primary" style="background: #800000;border-color: #800000;">Send</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
   </div>
</div>
</div>

<script type="text/javascript">

    $('.bsc_search').click(function(){

        $('#advance_search').toggle();

        var st = $('#advance_search').attr('style');

        if(st.length){

          $(this).text('Advance Search');
          $('#advance_search_flag').attr('value',0);
        }else{

          $(this).text('Basic Search');
          $('#advance_search_flag').attr('value',1);
        }
    });

    $('.close').click(function(){
        $('body').css('padding-right','0px');
    });



</script>
<script type="text/javascript">

  $(document).ready(function(){
    $(document).on('click', '#getcontacts', function(e){
      e.preventDefault();
      var customer_email = $(this).data('id');
      $('#member_email').attr('value',customer_email);
      });

    $(document).on('click', '#form_submit_page', function(e){
      e.preventDefault();
      var form_type = $('search_form_flag').val();
      if(form_type == 1){
        var val =$(this).val();
        var url = '<?php echo base_url(); ?>members?page='+val; 
        $('frm_members').attr('action',url);
        $('frm_members').submit();  
      }
      });
  });

  $('.clearsearch').on('click',function(){

    var red_url = "<?php echo base_url(); ?>members";

    window.location.href = red_url;

  });
</script>

<script type="text/javascript">
  function get_contact(_this){
    $('body').css('padding-right','0px');
    var email = $(_this).attr('id');

    // var url = '<?php //echo base_url(); ?>';

    // $.ajax({
    //     type: "POST",
    //     crossDomain: true,
    //     url: url+'profile/contact',
    //     data: {'data':'efsfsesdfs'},
    //     cache: false,
    //     dataType: 'json',
    //     success: function(dataStr){

    //       var obj = jQuery.parseJSON(JSON.stringify(dataStr));


    //       $('body').append(obj['pop_str']);


    //     }
    //   });
  }
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/client/js/bootstrap-select.min.js"></script>
<!--slider js-->
<?php include("inc/footer.php"); ?>