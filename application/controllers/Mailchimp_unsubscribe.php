<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./zoho/vendor/autoload.php');
use CristianPontes\ZohoCRMClient\ZohoCRMClient;

require_once('./zoho_v2/vendor/autoload.php');

// require_once('./application/mailchimp/MCAPI.class.php');

require_once('./application/helpers/general_helper.php');

class Mailchimp_unsubscribe extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('tomouh_model');
		$this->load->model('general_model');
		$this->load->model('messages_model');
		$this->load->model('admin/admin_model');
		$this->load->model('paging_model');
		$this->load->library('form_validation'); 
		$this->load->library('session');

		ZCRMRestClient::initialize();
	}
	public function index(){

		
		$moduleIns=ZCRMRestClient::getInstance()->getModuleInstance("Contacts");  //To get module instance
		$modified_since["If-Modified-Since"]=date('c', strtotime('-1 days'));//time in 8601 format
        $response=$moduleIns->getRecords(null, null, null, null,null,$modified_since); //To get module records
	
		$records=$response->getData();
		
		// $l_id = $this->tomouh_model->getSetting('MAILCHIMP_LIST_ID');
		
		foreach($records as $record){

			$member_data = $record->getData();
			
			if($member_data['Member_Status'] == 'Cancelled'){

				$merge_vars['EMAIL'] = $member_data['Email'];
				$merge_vars['LNAME'] = $member_data['Name'];
				$merge_vars['FNAME'] = $member_data['Last_Name'];

				$res = mailChimpUnsubscribe($merge_vars);

				if($res == 1){

	            }else{

	        	}
			}
			// echo "<pre>"; print_r($member_data); exit;	
		}
		
		$this->updateMembersLabelToRenewal();
	}

	public function updateMembersLabelToRenewal(){

		$expired_members = $this->tomouh_model->getExpiredMembers();

		foreach($expired_members as $expired_member){

			if(isset($expired_member['v_crm_contact_id']) && !empty($expired_member['v_crm_contact_id'])){


				$zcrmRecordIns = ZCRMRecord::getInstance("Contacts", $expired_member['v_crm_contact_id']);
				$zcrmRecordIns->setFieldValue("Label", "Did not pay renewal");
				$apiResponse=$zcrmRecordIns->update();

                //$this->tomouh_model->deleteMember($expired_member['v_email']); 
			}
		}
	}
}
?>