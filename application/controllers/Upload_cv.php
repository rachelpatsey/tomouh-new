<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./zoho/vendor/autoload.php');
use CristianPontes\ZohoCRMClient\ZohoCRMClient;

require_once('./zoho_v2/vendor/autoload.php');

class Upload_cv extends CI_Controller
{
	public function __construct(){
		parent::__construct();	
		$this->load->model('tomouh_model');
		$this->load->model('admin/admin_model');
		$this->load->model('messages_model');
		$this->load->library('session');
		$this->load->library('form_validation');

		ZCRMRestClient::initialize();
	}
	public function index(){
		
		$data['meta_title'] = 'Upload_CV';
		$data['meta_keyword'] = 'Upload_CV';
		$data['meta_description'] = 'Upload_CV';
			
		$crm_email = isset($_GET['ref'])?$_GET['ref']:'';

		if(!empty($crm_email)){

			$member_data = $this->tomouh_model->getCrmContactData($crm_email);

			if(empty($member_data)){
			
				$member = $this->getZohoCRMContacts($crm_email,1);
				

				if(!empty($member)){

					$contact_id = $member['contact_id'];

					$data['name'] = $member['member']['First_Name'].' '.$member['member']['Last_Name'];
					// echo "<pre>";print_r($data);exit;
					$insert_array = array(
							'v_contact_id'=>$contact_id,
							'v_email'=>$crm_email,
							'l_contact_data'=>json_encode($member['member']),
							);
					$this->admin_model->add_entry($insert_array,"tbl_crm_contact_data");
				}
			}else{

				$member = json_decode($member_data['l_contact_data'],true);

				$data['name'] = $member['First_Name'].' '.$member['Last_Name'];

				$contact_id = $member_data['v_contact_id'];

			}
			
			if($this->input->post()){
				
				$allowed =  array('doc','docx','pdf');
				$filename = $_FILES['cv']['name'];
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				
				$new_filename = $data['name'].'.'.$ext;
				
				if(!in_array($ext,$allowed) ) {
					redirect(base_url().'upload_cv?ref='.$crm_email.'&succ=0&msg=valid_doc');
					exit;
				}

				$targetfolder = getcwd()."/assets/cms_uploads/";
				$targetfolder = $targetfolder.$new_filename;
				
				
				move_uploaded_file($_FILES["cv"]["tmp_name"], $targetfolder);
				

				$record=ZCRMRecord::getInstance("Contacts",$contact_id);
				$record->uploadAttachment($targetfolder); 
				$record->setFieldValue("Label",'Candidate');
			
				$apiResponse=$record->update();
				
				// $filePath - absolute path of the attachment to be uploaded.				  
				
				 $email_to = $crm_email;
				 
				 $email_from ='';	
				 $template = $this->tomouh_model->getEmailTemplate(8);

				 $email_subject = $template['v_subject'];
				 $content = $template['l_body'];

				 $link  = '<a href="'.base_url().'assets/cms_uploads/'.$new_filename.'" target="_blank">CLICK HERE</a>';
			  
			   	 $content = str_replace("LINK", $link, $content);

			   	 $sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content , $attachments = array() );
				 	
				 //unlink('./assets/crm_uploads/'.$_FILES['cv']['name']);	

				redirect(base_url().'upload_cv?ref='.$crm_email.'&succ=1&msg=cv_submitted');
				exit;			  
			}

		}

		$this->load->view('upload_cv',$data);
	}

	public function getZohoCRMContacts($email,$index){

		
		$zcrmModuleIns = ZCRMModule::getInstance("Contacts");
		$bulkAPIResponse=$zcrmModuleIns->getRecords('','Modified_Time','desc',$index,$index+199,$modified_since);
						
		$recordsArray = $bulkAPIResponse->getData();
		
		if(!empty($recordsArray)){
			foreach ($recordsArray as $rcad) {
				$theRsp = $rcad->getData();
				if(isset($theRsp['Email'])){
					if($theRsp['Email'] == $email){
						$data_members['member'] = $theRsp;
						$data_members['contact_id'] = $rcad->getEntityId();
						return $data_members;
					}
				}
			}
		}else{

			return 0;
		}

		$index = $index+200;

		$result = $this->getZohoCRMContacts($email,$index);

		if(!empty($result)){

			return $result;
		}
	}
}