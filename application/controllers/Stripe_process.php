<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once './zoho/vendor/autoload.php';

require_once './application/mailchimp/MCAPI.class.php';

// include('PayPal/bootstrap.php');
// use PayPal\Api\Payment;
//use PayPal\Api\Agreement;
require_once './application/helpers/general_helper.php';
require_once './zoho_v2/vendor/autoload.php';

require_once 'application/libraries/stripe-php/init.php';

class Stripe_process extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
        $this->load->model('tomouh_model');
        $this->load->model('general_model');
        $this->load->model('messages_model');
        $this->load->model('admin/admin_model');
        $this->load->model('paging_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        ZCRMRestClient::initialize();
    }

    public function index()
    {

        $data = array();
        $user_step1 = $this->session->userdata('user');

        $user_step2 = $this->session->userdata('user_step2');

        if (!isset($user_step1['email']) || $user_step1['email'] == '') {

            redirect(base_url() . 'signup');
            exit;
        }
        if (isset($user_step1['is_member'])) {

            // $data['member_temp_id'] = $user_step1['email'];
            $member_data = $this->admin_model->select_member_data("tbl_members", $user_step1['email']);
            if (isset($member_data) && count($member_data) && isset($member_data['id']) && !empty($member_data['id'])) {
                $member_id = $member_data['id'];
                $data['member_temp_id'] = $member_id;
                $data['is_previous_subscription'] = 1;
            }
        } else {

            $email = $user_step1['email'];
            $member_array = array(
                'v_email' => $email,
                'l_user_step1_data' => json_encode($user_step1),
                'l_user_step2_data' => json_encode($user_step2),
            );

            $member_temp_id = $this->admin_model->add_entry($member_array, "tbl_members_temp");

            $data['member_temp_id'] = $member_temp_id;
        }

        // $data['business_name'] = $this->tomouh_model->getSetting('PAYPAL_BUSINESS_NAME');

        $data['is_test'] = $this->tomouh_model->getSetting('IS_STRIPE_TEST');
        $data['charges_amount'] = $this->tomouh_model->getSetting('STRIPE_CHARGES_AMOUNT');
        $data['stripe_secret_key'] = $this->tomouh_model->getSetting('STRIPE_SECRET_KEY');
        $data['stripe_publishable_key'] = $this->tomouh_model->getSetting('STRIPE_PUBLISHABLE_KEY');

        $data['meta_title'] = "Stripe_process";
        $data['meta_keyword'] = "Stripe_process";
        $data['meta_description'] = "Stripe_process";

        $this->load->view('stripe_process', $data);
    }

    public function isValidEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }
    public function stripePost()
    {

        $tempMemberId = $this->input->post('member_temp_id');
        \Stripe\Stripe::setApiKey($this->tomouh_model->getSetting('STRIPE_SECRET_KEY'));
        $token = $this->input->post('stripeToken');
        $productID = $this->tomouh_model->getSetting('STRIPE_PRODUCT_ID');
        $preCustomerID='';

        // Canceled previous subscription
        $is_previous_subscription = $this->input->post('is_previous_subscription');
        if (isset($is_previous_subscription) && !empty($is_previous_subscription) && $is_previous_subscription == 1) {
            $previousSubscriptionData = $this->admin_model->select_member_subscription_data("tbl_subscription_data", $tempMemberId);
        }
        if (isset($previousSubscriptionData) && count($previousSubscriptionData)) {
            if(isset($previousSubscriptionData['e_payment_type']) && !empty($previousSubscriptionData['e_payment_type'])){
                $pre_e_payment_type=$previousSubscriptionData['e_payment_type'];
            }
            if(isset($previousSubscriptionData['v_subscription_id']) && !empty($previousSubscriptionData['v_subscription_id'])){
                $pre_v_subscription_id=$previousSubscriptionData['v_subscription_id'];
            }
            if(isset($previousSubscriptionData['v_customer_id']) && !empty($previousSubscriptionData['v_customer_id'])){
                $preCustomerID=$previousSubscriptionData['v_customer_id'];
            }
            if(isset($previousSubscriptionData['user_id']) && !empty($previousSubscriptionData['user_id'])){
                $preMemberId=$previousSubscriptionData['user_id'];
            }
            if(isset($pre_e_payment_type) && !empty($pre_e_payment_type) && $pre_e_payment_type == 'stripe' && isset($pre_v_subscription_id) && !empty($pre_v_subscription_id)){
                $retrive_subscription=\Stripe\Subscription::retrieve($pre_v_subscription_id);
                // $cancel_subscription= \Stripe\Subscription::cancel($pre_v_subscription_id);
                $retrive_subscription->delete();
            }
        }
        // End

        if (isset($is_previous_subscription) && !empty($is_previous_subscription) && $is_previous_subscription == 1) {
            // Renewal Subscription >>
            if(isset($pre_e_payment_type) && !empty($pre_e_payment_type) && $pre_e_payment_type == 'stripe'){
                if(isset($preCustomerID) && !empty($preCustomerID)){
                    $customerID = $preCustomerID;
                }
            }else if(isset($pre_e_payment_type) && !empty($pre_e_payment_type) && $pre_e_payment_type == 'paypal'){
                $previousMemberData = $this->admin_model->select_entry("tbl_members", $tempMemberId);
                $tempMemberEmail=$memberFName=$memberLName='';
                if(isset($previousMemberData) && !empty($previousMemberData)){
                    $tempMemberEmail=$previousMemberData['v_email'];
                    $memberFName=$previousMemberData['v_firstname'];
                    $memberLName=$previousMemberData['v_lastname'];
                }
                $memberFullName=$memberFName.'-'.$memberLName;
                $customer = \Stripe\Customer::create([
                    'email' => $tempMemberEmail,
                    'name' => $memberFullName,
                    'source' => $token,
                ]);
                if (isset($customer) && empty($customer)) {
                    redirect('/stripe_process?succ=0&msg=Payment declined. Please use another card or recheck the entered information is correct.');
                }
                $customerID = $customer->id;
            }else{
                $existMemberData = $this->admin_model->getRow("tbl_members", $tempMemberId);
                $tempMemberEmail=$memberFName=$memberLName='';
                if(isset($existMemberData) && !empty($existMemberData) && count($existMemberData)){

                    $tempMemberEmail=$existMemberData['v_email'];
                    $memberFName=$existMemberData['v_firstname'];
                    $memberLName=$existMemberData['v_lastname'];
                    $memberFullName=$memberFName.'-'.$memberLName;
                    $customer = \Stripe\Customer::create([
                        'email' => $tempMemberEmail,
                        'name' => $memberFullName,
                        'source' => $token,
                    ]);
                    $preMemberId=$existMemberData['id'];
                    if (isset($customer) && empty($customer)) {
                        redirect('/stripe_process?succ=0&msg=Payment declined. Please use another card or recheck the entered information is correct.');
                    }
                    $customerID = $customer->id;
                }
            }
            if(isset($customerID) && !empty($customerID)){
                $subscription = \Stripe\Subscription::create(array(
                    "customer" => $customerID,
                    "items" => array(
                        array(
                            "plan" => $productID,
                        ),
                    ),
                ));
            }
            // << End
        }else{
            // new  Subscription >>
            $tempMember = $this->tomouh_model->getTempUserById($tempMemberId);
            $tempMemberFname = $tempMemberLname = $tempMemberEmail = '';
            if (isset($tempMember) && count($tempMember)) {
                $userData = json_decode($tempMember['l_user_step1_data']);

                if (isset($userData->firstname)) {
                    $tempMemberFname = $userData->firstname;
                }
                if (isset($userData->lastname)) {
                    $tempMemberLname = $userData->lastname;
                }
                if (isset($userData->email)) {
                    $tempMemberEmail = $userData->email;
                }

            }
            $memberFullName = $tempMemberFname . ' ' . $tempMemberLname;

            $customer = \Stripe\Customer::create([
                'email' => $tempMemberEmail,
                'name' => $memberFullName,
                // 'address'=>[
                //     // "city"=>$tempMemberCity,
                //     // "country"=>$tempMemberCountry,            //
                //     // "line1"=>$customerAddress['line1'],
                //     // "postal_code"=>$customerAddress['postal_code'],
                //     // "state"=>$customerAddress['state']
                // ],
                'source' => $token,
            ]);
            if (isset($customer) && empty($customer)) {
                redirect('/stripe_process?succ=0&msg=Payment declined. Please use another card or recheck the entered information is correct.');
            } else if (isset($customer) && !empty($customer) && isset($customer->id)) {
                $customerID = $customer->id;
                $subscription = \Stripe\Subscription::create(array(
                    "customer" => $customerID,
                    "items" => array(
                        array(
                            "plan" => $productID,
                        ),
                    ),
                ));
            }
            // << End
        }

        if (isset($subscription) && !empty($subscription) && isset($subscription->id) && !empty($subscription->id) && isset($customerID) && !empty($customerID)) {
            $sub_id = $subscription->id;
        } else {
            redirect('/stripe_process?succ=0&msg=Payment declined. Please use another card or recheck the entered information is correct.');
        }
        if (isset($subscription) && !empty($subscription) && isset($subscription->type)) {
            if ($subscription->type == 'invoice.payment_failed') {
                redirect('/stripe_process?succ=0&msg=Payment declined. Please use another card or recheck the entered information is correct.');
            }
        }

        if(isset($is_previous_subscription) && !empty($is_previous_subscription) && $is_previous_subscription == 1 && isset($sub_id) && !empty($sub_id) && isset($preMemberId) && !empty($preMemberId)) {

             // update exist  Member data  >>>
            $update_data = array(
                'e_plan_type' => 'paid',
                'e_payment_type' => 'stripe',
                'd_subscription_exp_date' => date('Y-m-d', strtotime('+1 year')),
                'd_last_profile_update' => date('Y-m-d'),
            );
            $tbl = "tbl_members";
            $update_user_data = $this->admin_model->update_entry($preMemberId, $update_data, $tbl);

            $subscription_array = array(
                'user_id' => $preMemberId,
                'v_customer_id' => $customerID,
                'v_subscription_id' => $sub_id,
                'l_subscription_data' => json_encode($subscription),
                'd_subscription_date' => date("Y-m-d H:i:s"),
                'e_payment_type' => 'stripe',
            );
            $this->admin_model->add_entry($subscription_array, "tbl_subscription_data");
            $this->update_zoho_crm($preMemberId, $sub_id);
            $this->load->view('payment_success', ['is_stripe' => 1]);
             // << End

        }else if(isset($sub_id) && !empty($sub_id) && $is_previous_subscription != 1) {

            // New Member subscription  >>>
            $member_temp_data = $this->tomouh_model->getTempUserById($tempMemberId);

            $user_data = json_decode($member_temp_data['l_user_step1_data'], true);

            $data = json_decode($member_temp_data['l_user_step2_data'], true);
            $latitude = '';
            $longitude = '';
            $main_occupation_description = isset($data['main_occu']) ? $data['main_occu'] : '';

            $main_occupation_data = isset($data['occupation']) ? $data['occupation'] : '';

            $int_main_occupation = intval(preg_replace('/[^0-9]+/', '', $main_occupation_data), 10);

            $passion_interest = $data['passions_interests'];

            if ($user_data['referral'] == 0) {

                $referral_email = '';
            } else {

                $referral_email = $user_data['referral_email'];
            }

            $user = $user_data['email'];

            $email_to = $user;
            $email_from = "";
            $template = $this->tomouh_model->getEmailTemplate(2);
            $email_subject = $template['v_subject'];
            $content = $template['l_body'];

            $activation_link = '<a href="' . base_url() . 'login/activation/' . md5($user_data['email']) . '" target="_blank">link here</a>';

            $content = str_replace("link here", $activation_link, $content);

            $sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content, $attachments = array());
            $sent = 1;
            $tel1 = $tel2 = '';
            if (isset($user_data['tele_country_code']) && isset($user_data['telephone'])) {
                $tel1 = $user_data['tele_country_code'] . '-' . $user_data['telephone'];
            }
            if (isset($user_data['tele_2_country_code']) && isset($user_data['telephone_2'])) {
                $tel2 = $user_data['tele_2_country_code'] . '-' . $user_data['telephone_2'];
            }
            if ($sent) {

                $update_data = array(
                    'v_email' => isset($user_data['email']) ? $user_data['email'] : '',
                    'v_password' => isset($user_data['password']) ? md5($user_data['password']) : '',
                    'v_firstname' => isset($user_data['firstname']) ? $user_data['firstname'] : '',
                    'v_lastname' => isset($user_data['lastname']) ? $user_data['lastname'] : '',
                    'd_dob' => isset($user_data['dob']) ? date('Y-m-d', strtotime($user_data['dob'])) : '',
                    'e_gender' => isset($user_data['gender']) ? $user_data['gender'] : '',
                    'v_home_country' => isset($user_data['home_country']) ? $user_data['home_country'] : '',
                    'v_home_city' => isset($user_data['home_city']) ? $user_data['home_city'] : '',
                    'v_residence_country' => isset($user_data['residence_country']) ? $user_data['residence_country'] : '',
                    'v_residence_city' => isset($user_data['residence_city']) ? $user_data['residence_city'] : '',
                    'v_telephone' => $tel1,
                    'v_telephone_2' => $tel2,
                    'v_twitter_link ' => isset($user_data['twitter_link']) ? $user_data['twitter_link'] : '',
                    'v_linkedin_link' => isset($user_data['linkedin_link']) ? $user_data['linkedin_link'] : '',
                    'v_instagram_link' => isset($user_data['instagram_link']) ? $user_data['instagram_link'] : '',
                    'e_referral' => isset($user_data['referral']) ? $user_data['referral'] : '',
                    'v_referral_email' => isset($referral_email) ? $referral_email : '',
                    't_passion_interest' => isset($passion_interest) ? $passion_interest : '',
                    't_description' => '',
                    'v_main_occupation' => $main_occupation_description,
                    'v_longitude' => $longitude,
                    'v_latitude' => $latitude,
                    'e_status' => 'inactive',
                    'e_member_of_month' => 0,
                    'v_subscription_id' => $sub_id,
                    'e_type' => isset($user_data['type']) ? $user_data['type'] : '',
                    'v_label' => 'Member',
                    'e_plan_type' => 'paid',
                    'v_crm_contact_id' => $user_data['contact_id'],
                    'e_payment_type' => 'stripe',
                    'd_subscription_exp_date' => date('Y-m-d', strtotime('+1 year')),
                    'd_last_profile_update' => date('Y-m-d'),
                );

                $tbl = "tbl_members";
                $add_user_data = $this->admin_model->update_member_entry($user_data['contact_id'], $update_data, $tbl);

                // $add_user_data = $this->admin_model->add_entry($update_data,$tbl);

                // $this->add_zoho_crm($add_user_data, $tempMemberId);

                if ($user_data['type'] == 'member') {

                    $this->add_mailchimp($user_data['email'], $user_data['firstname'], $user_data['lastname']);
                }

                $subscription_array = array(
                    'user_id' => $add_user_data,
                    'v_customer_id' => $customerID,
                    'v_subscription_id' => $sub_id,
                    'l_subscription_data' => json_encode($subscription),
                    'd_subscription_date' => date("Y-m-d H:i:s"),
                    'e_payment_type' => 'stripe',
                );
                $this->admin_model->add_entry($subscription_array, "tbl_subscription_data");

                $educ_data_length = sizeof($data['university']);

                for ($i = 0; $i < $educ_data_length; $i++) {
                    if (isset($data['university'][$i]) && !empty($data['university'][$i])) {
                        if ($int_main_occupation <= 10) {

                            if (($i + 1) == $int_main_occupation) {
                                $main_occupation = 1;
                            } else {
                                $main_occupation = 0;
                            }
                        } else {
                            $main_occupation = 0;
                        }

                        $insert_array = array(

                            'user_id' => $add_user_data,
                            'v_university' => $data['university'][$i],
                            'v_degree' => $data['degree'][$i],
                            'v_major' => $data['major'][$i],
                            'i_passing_year' => $data['graduation_year'][$i],
                            'i_main_occupation' => $main_occupation,
                        );
                        $tbl = "tbl_educational_data";
                        $this->admin_model->add_entry($insert_array, $tbl);
                    }
                }

                $company_data_length = sizeof($data['company']);

                for ($i = 0; $i < $company_data_length; $i++) {

                    if (isset($data['company'][$i]) && !empty($data['company'][$i])) {
                        if ($int_main_occupation >= 10) {

                            if (($i + 11) == $int_main_occupation) {
                                $main_occupation = 1;
                            } else {
                                $main_occupation = 0;
                            }
                        } else {
                            $main_occupation = 0;
                        }

                        $insert_array = array(

                            'user_id' => $add_user_data,
                            'v_company' => $data['company'][$i],
                            'v_job_title' => $data['job_title'][$i],
                            'v_industry' => $data['industry'][$i],
                            'i_company_from' => $data['company_from'][$i],
                            'i_company_to' => $data['company_to'][$i],
                            'i_main_occupation' => $main_occupation,
                        );
                        $tbl = "tbl_professional_data";
                        $this->admin_model->add_entry($insert_array, $tbl);
                    }
                }

                $award_data_length = sizeof($data['award_details']);

                for ($i = 0; $i < $award_data_length; $i++) {

                    if (isset($data['award_details'][$i]) && !empty($data['award_details'][$i])) {
                        $insert_array = array(

                            'user_id' => $add_user_data,
                            'v_achievement' => $data['award_details'][$i],
                        );
                        $tbl = "tbl_achievements_data";
                        $this->admin_model->add_entry($insert_array, $tbl);
                    }
                }

                // $res = setcookie('user_step1', '', time() - 2592000, '/');

                // $res = setcookie('user_step2', '', time() - 2592000, '/');

                $res = setcookie('user_step1', '', time() - 31536000, '/');

                $res = setcookie('user_step2', '', time() - 31536000, '/');

                $this->session->unset_userdata('user');
                $this->session->unset_userdata('user_step2');
            }
            $this->load->view('payment_success', ['is_stripe' => 1]);
            // <<  End
        } else {
            redirect('/stripe_process?succ=0&msg=Payment declined. Please use another card or recheck the entered information is correct.');
        }

    }
    public function returnWebhookStripe()
    {

        //    echo "test";exit;
        $json = file_get_contents('php://input');

        // // Converts it into a PHP object
        $response = json_decode($json);
        // echo $response->data->object->subscription;exit;
        // echo "<pre>";
        // print_r($response);exit;

        $handle = fopen('./test_paypal.txt', 'a+');
        $logtext = "******************" . date('Y-m-d H:i:s') . "******************\n\n";

        $logtext .= "========== Stripe parameter list ======== ";

        $logtext .= "<pre>" . print_r($response, true) . "</pre>";

        $logtext .= "\n\n**********************************************************************\n\n";
        $errorlog = fwrite($handle, $logtext);
        fclose($handle);

        // exit();

        if (isset($response) && !empty($response)) {

            $sub_id = $response->data->object->subscription;
            $v_customer_id = $response->data->object->customer;
            if (isset($sub_id) && !empty($sub_id)) {

                $sub_data = $this->tomouh_model->getSubscriptionBySubId($sub_id);

                $sub_date = isset($sub_data['d_subscription_date']) ? $sub_data['d_subscription_date'] : '';

                $original_date = '';

                if ($sub_date != '') {

                    $original_date = date('Y-m-d', strtotime($sub_date));
                }

                if (isset($original_date) && !empty($original_date) && strtotime($original_date) >= strtotime(date('Y-m-d') . ' -1 day')) {

                    exit();

                } else {

                    $member = $this->tomouh_model->getUserBySubscriptionId($sub_id);

                    if (!empty($member)) {

                        $subscription_array = array(
                            'user_id' => $member['id'],
                            'v_customer_id' => $v_customer_id,
                            'v_subscription_id' => $sub_id,
                            'l_subscription_data' => json_encode($response),
                            'd_subscription_date' => date("Y-m-d H:i:s"),
                            'e_payment_type' => 'stripe',
                        );
                        $this->admin_model->add_entry($subscription_array, "tbl_subscription_data");

                        $update_member = array(
                            'd_subscription_exp_date' => date('Y-m-d', strtotime('+1 year')),
                        );

                        $this->admin_model->update_entry($member['id'], $update_member, "tbl_members");

                        $this->update_zoho_crm($member['id'], $sub_id);

                    }
                }
            }
        }
    }

    public function add_zoho_crm($user_id, $temp_user_id)
    {

        $member_data = $this->tomouh_model->getUserByUserId($user_id);

        $payment_data = 'Payment Type : ' . $member_data['e_payment_type'] . ', Subscription Id : ' . $member_data['v_subscription_id'] . ', Activation Date : ' . date('Y-m-d') . ', Expiration Date : ' . $member_data['d_subscription_exp_date'];

        $member_temp_data = $this->tomouh_model->getTempUserById($temp_user_id);

        $user_step1 = json_decode($member_temp_data['l_user_step1_data'], true);

        $user_step2 = json_decode($member_temp_data['l_user_step2_data'], true);

        if (isset($user_step2['university']) && !empty($user_step2['university'])) {

            $educ_data_length = sizeof($user_step2['university']);

            $education_info = array();

            for ($i = 0; $i < $educ_data_length; $i++) {

                if (isset($user_step2['university'][$i]) && !empty($user_step2['university'][$i])) {

                    $education_info[] = 'University : ' . $user_step2['university'][$i] . ', Degree : ' . $user_step2['degree'][$i] . ', Major : ' . $user_step2['major'][$i] . ', Year : ' . $user_step2['graduation_year'][$i];

                }
            }
        }

        if (isset($user_step2['company']) && !empty($user_step2['company'])) {

            $company_data_length = sizeof($user_step2['company']);

            $experience_info = array();

            for ($i = 0; $i < $company_data_length; $i++) {

                if (isset($user_step2['company'][$i]) && !empty($user_step2['company'][$i])) {

                    $experience_info[] = 'Company : ' . $user_step2['company'][$i] . ', Job Title : ' . $user_step2['job_title'][$i] . ', Industry : ' . $user_step2['industry'][$i] . ', Period : ' . $user_step2['company_from'][$i] . '-' . $user_step2['company_to'][$i];

                }
            }
        }

        if (isset($user_step2['award_details']) && !empty($user_step2['award_details'])) {

            $award_data_length = sizeof($user_step2['award_details']);

            $award_detail = '';

            for ($i = 0; $i < $award_data_length; $i++) {

                if (isset($user_step2['award_details'][$i]) && !empty($user_step2['award_details'][$i])) {

                    $award_detail .= $user_step2['award_details'][$i] . ',';

                }
            }
        }

        if (isset($award_detail)) {

            $AAH = rtrim($award_detail, ',');

        } else {
            $AAH = '';
        }

        $educational_info = implode(' %0A ', $education_info);

        $exp_info = implode(' %0A ', $experience_info);

        $cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');

        $contact_id = $user_step1['contact_id'];

        $zcrmRecordIns = ZCRMRecord::getInstance("Contacts", $contact_id);
        $zcrmRecordIns->setFieldValue("Last_Name", $user_step1['lastname']);
        $zcrmRecordIns->setFieldValue("Email", $user_step1['email']);
        $zcrmRecordIns->setFieldValue("First_Name", $user_step1['firstname']);
        $zcrmRecordIns->setFieldValue("Date_of_Birth", date('Y-m-d', strtotime($user_step1['dob'])));
        $zcrmRecordIns->setFieldValue("Gender", $user_step1['gender']);
        $zcrmRecordIns->setFieldValue("Country", $user_step1['home_country']);
        $zcrmRecordIns->setFieldValue("Home_City", $user_step1['home_city']);
        $zcrmRecordIns->setFieldValue("Mobile_No", $user_step1['tele_country_code'] . '-' . $user_step1['telephone']);
        $zcrmRecordIns->setFieldValue("Country_of_Residence", $user_step1['residence_country']);
        $zcrmRecordIns->setFieldValue("City_Of_Residence", $user_step1['residence_city']);
        $zcrmRecordIns->setFieldValue("Mobile_No_2", $user_step1['tele_2_country_code'] . '-' . $user_step1['telephone']);
        $zcrmRecordIns->setFieldValue("Educational_Info", isset($educational_info) ? urldecode($educational_info) : '');
        $zcrmRecordIns->setFieldValue("Experience_Info", isset($exp_info) ? urldecode($exp_info) : '');
        $zcrmRecordIns->setFieldValue("AAH", $AAH);
        $zcrmRecordIns->setFieldValue("Passions_and_interests", $user_step2['passions_interests']);
        $zcrmRecordIns->setFieldValue("Member_Status", "Active");
        $zcrmRecordIns->setFieldValue("Plan", "Paid");
        $zcrmRecordIns->setFieldValue("Label", "Member");
        $zcrmRecordIns->setFieldValue("Profile_Expiry", date('Y-m-d', strtotime('+1 year')));
        $zcrmRecordIns->setFieldValue("Subscription_Info", $payment_data);
        $apiResponse = $zcrmRecordIns->update();

    }

    public function update_zoho_crm($user_id, $sub_id)
    {

        $results = $this->tomouh_model->getUserByUserId($user_id);

        $payment_data = $this->tomouh_model->getAllSubscriptionByUserId($user_id);

        $payment_info = [];

        if ($payment_data) {

            foreach ($payment_data as $data) {

                $payment_info[] = 'Payment Type : Stripe, Subscription : ' . $data['v_subscription_id'] . ', Expiration Date : ' . date("m/d/Y", strtotime(date("Y-m-d", strtotime($data['d_subscription_date'])) . " + 1 year"));

            }
        }

        $payment_info[] = 'Payment Type : Stripe, Subscription Id : ' . $sub_id . ', Expiration Date : ' . date('m/d/Y', strtotime('+1 year'));

        $pay_info = implode(' %0A ', $payment_info);

        $member_crm_id = $results['v_crm_contact_id'];

        $zcrmRecordMemIns = ZCRMRecord::getInstance("Contacts", $member_crm_id);
        $zcrmRecordMemIns->setFieldValue("Plan", 'Paid');
        $zcrmRecordMemIns->setFieldValue("Profile_Expiry", date('Y-m-d', strtotime('+1 year')));
        $zcrmRecordMemIns->setFieldValue("Subscription_Info", $pay_info);
        $apiResponse = $zcrmRecordMemIns->update();

    }

    public function add_mailchimp($email, $firstname, $lastname)
    {

        // mailchimp api call to subscribe start

        $merge_vars = array(
            'EMAIL' => $email,
            'FNAME' => $firstname,
            'LNAME' => $lastname,
        );

        mailChimpSubscribe($merge_vars);

        // mailchimp api call to subscribe end
    }

    public function file_get_contents_curl($url)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
}
