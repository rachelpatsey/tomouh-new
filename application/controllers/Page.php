<?php
defined('BASEPATH') or exit('No direct script access allowed');

// require_once('./zoho/vendor/autoload.php');
// use CristianPontes\ZohoCRMClient\ZohoCRMClient;

require_once('./zoho_v2/vendor/autoload.php');

// require_once('./application/mailchimp/MCAPI.class.php');
require_once('./application/helpers/general_helper.php');

class Page extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->model('tomouh_model');
		$this->load->model('general_model');
		$this->load->model('messages_model');
		$this->load->model('admin/admin_model');
		$this->load->model('paging_model');
		$this->load->library('form_validation');
		$this->load->library('session');

		ZCRMRestClient::initialize();
	}

	public function profile_upto_date()
	{
		$data =[];
		$this->load->view("profile_upto_date",$data);
	}
	public function view($slug = "")
	{


		$data = array();

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			$btn_submit = $this->input->post("btn_submit");

			if ($btn_submit && $btn_submit == 'CONTACT US') {

				$insert_data["v_user_name"] = $this->input->post("v_user_name");
				$insert_data["v_user_email"] = $this->input->post("v_user_email");
				$insert_data["l_user_message"] = $this->input->post("l_user_message");

				$contact_info =  $this->tomouh_model->getSetting('CONTACT_INFO');

				$tbl = "tbl_contact_us";
				$add_contact_data = $this->admin_model->add_entry($insert_data, $tbl);

				$email_to = $contact_info;
				$email_from = '';
				$email_subject = 'You have received a new inquiry';
				$template = $this->tomouh_model->getEmailTemplate(5);

				$content = $template['l_body'];

				$email_content = '<p>Hello,</p>
								  <p>An inquiry has been received from the website. See the details below.</p>';
				$email_content .= '<table cellpadding="6" align="left">
									<tr>
										<th align="left">Name</th>
										<td>' . $insert_data["v_user_name"] . '</td>
									</tr>
									<tr>
										<th align="left">Email</th>
										<td>' . $insert_data["v_user_email"] . '</td>
									</tr>
									<tr>
										<th align="left">Message</th>
										<td>' . $insert_data["l_user_message"] . '</td>
									</tr>
								  </table>';

				$content = str_replace("EMAIL_CONTENT", $email_content, $content);
				// print_r($content); exit();
				$sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content, $attachments = array());

				redirect(base_url() . 'contact_us/?succ=1&msg=inqsent');
				exit;
			}
		}

		$result = $this->tomouh_model->getPageBySlug($slug);
		$data = $result;
		$data['meta'] = $this->tomouh_model->getPageMeta($result['id']);
		$data['meta_title'] = ($result['v_meta_title']) ? $result['v_meta_title'] : $result['v_title'];
		$data['meta_keyword'] = $result['v_meta_keyword'];
		$data['meta_description'] = $result['l_meta_description'];
		$data['main_description'] = $result['l_description'];
		$data['body_class'] = 'home page-' . $result['id'];
		$data['title'] = $result['v_title'];

		if ($slug == 'about_us') {

			// $template = $this->tomouh_model->getEmailTemplate(2);
   //          $email_subject = $template['v_subject'];
   //          $content = $template['l_body'];
   //          echo $content; exit;
		    
			$data['address'] =  $this->tomouh_model->getSetting('ADDRESS');			
			$this->load->view("about_us", $data);
		} elseif ($slug == 'tcl-fellow') {
			
			$arg['e_status'] = 'active';
			$arg['d_year'] = $_GET['d_year'];
			$arg['e_type'] = 'TCL Fellows';
			$rows=$this->admin_model->getFellowList($arg);

			if(isset($rows['rows']) && count($rows['rows'])){
				foreach ($rows['rows'] as $key => $value) {
					$yearlydata[$value['d_year']][]=$value;
				}
			}
			// _p($yearlydata);exit;
			// $data['fellows'] = $rows['rows'];
			$data['fellows'] = isset($yearlydata) ? $yearlydata : [];
			$data['years'] = $rows['years'];
			$data['selectedYear'] = $rows['selectedYear'];

			$this->load->view("tcl_fellow", $data);
		}elseif ($slug == 'tcl-awards') {
			
			$arg['e_status'] = 'active';
			$arg['d_year'] = $_GET['d_year'];
			$arg['e_type'] = 'TCL Awards';
			$rows=$this->admin_model->getFellowList($arg);

			if(isset($rows['rows']) && count($rows['rows'])){
				foreach ($rows['rows'] as $key => $value) {
					$yearlydata[$value['d_year']][]=$value;
				}
			}

			$data['selectedYear'] = $rows['selectedYear'];
			// $data['fellows'] = $rows['rows'];
			$data['fellows'] = isset($yearlydata) ? $yearlydata : [];
			$data['years'] = $rows['years'];			
			$this->load->view("tcl_awards", $data);
		}elseif ($slug == 'sponsorship') {
			$this->load->view("sponsorship", $data);
		}  
		elseif ($slug == 'founders') {
			$arg['e_status'] = 'active';
			$rows=$this->admin_model->getFounderList($arg);
			
			$data['founders'] = $rows['rows'];
			$this->load->view("founders", $data);
		} 
		elseif ($slug == 'our_members') {
			$data['members_update'] = $this->tomouh_model->getMembersUpdate();
			$data['member_of_month'] = $this->tomouh_model->getMemberOfMonth();
			$data['books'] = $this->tomouh_model->getBooks();
			$this->load->view("our_members", $data);
		} 
		elseif ($slug == 'contact_us') {
			$data['contact_info'] =  $this->tomouh_model->getSetting('CONTACT_INFO');
			$this->load->view("contact_us", $data);
		} elseif ($slug == 'faq') {
			$data['faqs'] = $this->tomouh_model->getFaqs();
			$this->load->view("faq", $data);
		} elseif ($slug == 'my_home') {
			if ($this->session->has_userdata('logged_user')) {
				$data['books'] = $this->tomouh_model->getBooks();
				$data['members'] = $this->tomouh_model->memberWithProfessionData();
				$data['members_update'] = $this->tomouh_model->getMembersUpdate();

				$this->load->view("my_home", $data);
			} else {
				redirect(base_url() . 'login');
				exit();
			}
		} elseif ($slug == 'why_join_tomouh') {
			$this->load->view("why_join", $data);
		} elseif ($slug == 'initiative') {
			if ($this->session->has_userdata('logged_user')) {
				$data['initiative_data'] = $this->tomouh_model->getInitiativeData();
				$this->load->view("initiative", $data);
			} else {
				redirect(base_url() . 'login');
				exit();
			}
		} elseif ($slug == 'membership_process') {
			$this->load->view("membership_process", $data);
		} elseif ($slug == 'team') {
			$data['teams'] = $this->tomouh_model->getTomouhTeam('Team');
			
			$this->load->view("team", $data);
		}elseif ($slug == 'honorary_member') {
			$data['honorary_members'] = $this->tomouh_model->getTomouhTeam('Honorary');
			$this->load->view("honorary_member", $data);
		}elseif ($slug == 'council-of-senior-advisers') {
			$data['council_of_senior_advisers'] = $this->tomouh_model->getTomouhTeam('CouncilofSeniorAdvisers');
			$this->load->view("council_of_senior_advisers", $data);
		} 
		elseif ($slug == 'board-members') {
			// $result= $this->tomouh_model->getTomouhTeam('Board');
			// if(isset($result) && count($result)){
			// 	$data['board_members'] =array_chunk($result,2);
			// }else{
			// 	$data['board_members']=[];
			// }
			$data['board_members']=$this->tomouh_model->getTomouhTeam('Board');
			$this->load->view("board_members", $data);
		} 
		elseif ($slug == 'membership_committee') {
			$data['memberships'] = $this->tomouh_model->getTomouhTeam('Membership');
			// _p($data);exit;
			$this->load->view("membership_committee", $data);
		} elseif ($slug == 'media') {

			$data['limit'] = $limit = 9;

			if ($this->input->get('page') && $this->input->get('page') != '') {

				$data['page'] = $this->input->get('page');
				$limitstart = $limit * ($this->input->get('page') - 1);
			} else {

				$data['page'] = 1;
				$limitstart = 0;
			}

			if ($this->input->get('type') && $this->input->get('type') != '') {

				$data['type'] = $type = $this->input->get('type');
			} else {

				$data['type'] = $type = 'image';
			}

			if ($type == 'image') {

				$photo = $this->tomouh_model->getChildPhotosPagination(0, $limit, $limitstart);

				$data['total_row'] = $photo['total_row'];

				foreach ($photo['result'] as $ph) {
					$photo_id = $ph['id'];
					$category_name = $ph['v_name'];
					$image = $this->tomouh_model->getPhoto($photo_id);
					$img = $image[0]['v_image'];
					$photos[] = array(

						'id' => $photo_id,
						'v_slug' => $ph['v_slug'],
						'v_name' => $category_name,
						'v_image' => $img,
					);
				}

				$data['photos'] = $photos;
				$data['videos'] = $this->tomouh_model->getvideosPagination($limit, 0);
			} else {

				$photo = $this->tomouh_model->getChildPhotosPagination(0, $limit, 0);

				$data['total_row'] = $photo['total_row'];

				foreach ($photo['result'] as $ph) {
					$photo_id = $ph['id'];
					$category_name = $ph['v_name'];
					$image = $this->tomouh_model->getPhoto($photo_id);
					$img = $image[0]['v_image'];
					$photos[] = array(

						'id' => $photo_id,
						'v_slug' => $ph['v_slug'],
						'v_name' => $category_name,
						'v_image' => $img,
					);
				}

				$data['photos'] = $photos;
				$data['videos'] = $this->tomouh_model->getvideosPagination($limit, $limitstart);
			}
			$this->load->view("media", $data);
		} else {

			$this->load->view("page", $data);
		}
	}

	public function signup($slug = "signup")
	{
		// echo "<pre>";
		// print_r($_COOKIE);
		// exit;
		// $this->db->select('*');
		// $this->db->from('tbl_members');
		// // $this->db->where('id', 45);
		// $this->db->order_by('id', 'DESC')->limit(5);
		// $query = $this->db->get();
		// $row = $query->result_array();

		// foreach ($row as $key => $value) {
		// 	$user_id = $value['id'];
		// 	// echo $user_id; 
		// 	// achivement data
		// 	$this->db->select('*');
		// 	$this->db->from('tbl_achievements_data');
		// 	$this->db->where('user_id', $user_id);
		// 	$query = $this->db->get();
		// 	$rowa = $query->result_array();
		// 	$achvdata = [];
		// 	$achv = '';
		// 	if (count($rowa)) {
		// 		foreach ($rowa as $ka => $va) {
		// 			$achvdata[] = $va['v_achievement'];
		// 		}
		// 		$achv = implode(',', $achvdata);
		// 	}


		// 	// educational information
		// 	$this->db->select('*');
		// 	$this->db->from('tbl_educational_data');
		// 	$this->db->where('user_id', $user_id);
		// 	$query = $this->db->get();
		// 	$rowe = $query->result_array();
		// 	$education_info = [];
		// 	$educational = '';
		// 	if (count($rowe)) {
		// 		foreach ($rowe as $ke => $ve) {
		// 			$education_info[] = 'University : ' . $ve['v_university'] . ', Degree : ' . $ve['v_degree'] . ', Major : ' . $ve['v_major'] . ', Year : ' . $ve['i_passing_year'];
		// 		}
		// 		$educational = implode(' %0A ', $education_info);
		// 		$educational = urldecode($educational);
		// 	}

		// 	// professional information
		// 	$this->db->select('*');
		// 	$this->db->from('tbl_professional_data');
		// 	$this->db->where('user_id', $user_id);
		// 	$query = $this->db->get();
		// 	$rowp = $query->result_array();
		// 	$experience_info = [];
		// 	$experience = '';
		// 	if (count($rowp)) {
		// 		foreach ($rowp as $kex => $vex) {
		// 			$experience_info[] = 'Company : ' . $vex['v_company'] . ', Job Title : ' . $vex['v_job_title'] . ', Industry : ' . $vex['v_industry'] . ', Period : ' . $vex['i_company_from'] . '-' . $vex['i_company_to'];
		// 		}
		// 		$experience = implode(' %0A ', $experience_info);
		// 		$experience = urldecode($experience);
		// 	}

		// 	// subscription information
		// 	$this->db->select('*');
		// 	$this->db->from('tbl_subscription_data');
		// 	$this->db->where('user_id', $user_id);
		// 	$query = $this->db->get();
		// 	$rowsub = $query->result_array();
		// 	$payment_info = [];
		// 	$payement = '';
		// 	if (count($rowsub)) {
		// 		foreach ($rowsub as $kex => $vex) {
		// 			$payment_info[] = 'Payment Type : ' . $vex['e_payment_type'] . ', Subscription : ' . $vex['v_subscription_id'] . ', Expiration Date : ' . date('m/d/Y', strtotime($vex['d_subscription_date']));
		// 		}
		// 		$payement = implode(' %0A ', $payment_info);
		// 		$payement = urldecode($payement);
		// 	}


			// $apiResponse=ZCRMModule::getInstance('Contacts')->getRecord($contact_id); 
			// $record=$apiResponse->getData();

			// $contactdata=$record->getData();

			//insert contact in zoho start

			//to get the instance of the module

		// 	$moduleContactIns = ZCRMRestClient::getInstance()->getModuleInstance("Contacts");
		// 	$recordsContact = array();

		// 	//To get ZCRMRecord instance
		// 	$recordContact = ZCRMRecord::getInstance("Contacts", null);

		// 	//This function use to set FieldApiName and value similar to all other FieldApis and Custom field
		// 	$recordContact->setFieldValue("Last_Name", isset($value['v_lastname']) ? $value['v_lastname'] : '');
		// 	$recordContact->setFieldValue("Email", isset($value['v_email']) ? $value['v_email'] : '');
		// 	$recordContact->setFieldValue("First_Name", isset($value['v_firstname']) ? $value['v_firstname'] : '');
		// 	$recordContact->setFieldValue("Date_of_Birth", date('Y-m-d', strtotime($value['d_dob'])));
		// 	$recordContact->setFieldValue("Gender", $value['e_gender']);

		// 	// $recordContact->setFieldValue("Expired_On","10/10/2020");
		// 	$recordContact->setFieldValue("Home_Country", isset($value['v_home_country']) ? $value['v_home_country'] : '');
		// 	$recordContact->setFieldValue("Home_City", isset($value['v_home_city']) ? $value['v_home_city'] : '');
		// 	$recordContact->setFieldValue("Mobile", isset($value['v_telephone']) && !empty($value['v_telephone']) ?  $value['v_telephone'] : '');
		// 	$recordContact->setFieldValue("Mobile_No", isset($value['v_telephone']) && !empty($value['v_telephone']) ? $value['v_telephone'] : '');

		// 	$recordContact->setFieldValue("Country_Of_Residence", isset($value['v_residence_country']) ? $value['v_residence_country'] : '');
		// 	$recordContact->setFieldValue("City_Of_Residence", isset($value['v_residence_city']) ? $value['v_residence_city'] : '');
		// 	$recordContact->setFieldValue("Mobile_No_2", isset($value['v_telephone_2']) && !empty($value['v_telephone_2']) ? $value['v_telephone_2'] : '');
		// 	$recordContact->setFieldValue("Educational_Info", $educational);

		// 	$recordContact->setFieldValue("Experience_Info", $experience);
		// 	$recordContact->setFieldValue("AAH", $achv);
		// 	$recordContact->setFieldValue("Passions_and_interests", isset($value['t_passion_interest']) ? $value['t_passion_interest'] : '');
		// 	$recordContact->setFieldValue("Subscription_Info", $payement);

		// 	$recordContact->setFieldValue("Lead_Source", isset($value['lead_source']) ? $value['lead_source'] : '');
		// 	$recordContact->setFieldValue("Label", $value['v_label']);
		// 	$recordContact->setFieldValue("Plan", $value['e_plan_type']);

		// 	array_push($recordsContact, $recordContact); //pushing the record to the array 

		// 	$responseIn = $moduleContactIns->createRecords($recordsContact);
		// 	$responseIn = $responseIn->getEntityResponses();
		// 	$responseIn = $responseIn[0]->getDetails();
		// 	$responseCrmID = $responseIn['id'];
		// 	// $user['contact_id'] = $responseCrmID;

		// 	$update_array  = array(
		// 		'v_crm_contact_id' => $responseCrmID,
		// 	);

		// 	$this->admin_model->update_entry($user_id, $update_array, "tbl_members");
		// }

		// echo "<pre>";
		// print_r('added');
		// // print_r($row);
		// exit;

		$result = $this->tomouh_model->getPageBySlug($slug);

		if ($this->session->has_userdata('logged_user')) {

			redirect(base_url() . 'profile');
			exit;
		}

		$data                     = $result;
		$data['meta']             = $this->tomouh_model->getPageMeta($result['id']);
		$data['meta_title']       = ($result['v_meta_title']) ? $result['v_meta_title'] : $result['v_title'];
		$data['meta_keyword']     = $result['v_meta_keyword'];
		$data['meta_description'] = $result['l_meta_description'];
		$data['main_description'] = $result['l_description'];
		$data['body_class']       = $slug . ' page-' . $result['id'];
		$data['title']            = $result['v_title'];

		// echo "<pre>"; print_r($data); exit;

		$crm_email = isset($_GET['ref']) ? $_GET['ref'] : '';
		
		if (!empty($crm_email)) {

			// $res = setcookie('user_step1', '', time() - 2592000, '/');

			// $res = setcookie('user_step2', '', time() - 2592000, '/');
			
			// $res = setcookie('user_step1', '', time() - 31536000, '/');// 

			// $res = setcookie('user_step2', '', time() - 31536000, '/');// 

			if (isset($_COOKIE['user_step1']) && !empty($_COOKIE['user_step1'])) {
				
				$data['user_step1'] = json_decode($_COOKIE['user_step1'], true);
			
			}else{
				$res = setcookie('user_step1', '', time() - 31536000, '/');
			}
			if (isset($_COOKIE['user_step2']) && !empty($_COOKIE['user_step2'])) {

				$data['user_step2'] = json_decode($_COOKIE['user_step2'], true);
			}else{
				$res = setcookie('user_step2', '', time() - 31536000, '/');//
			}

			$member_data = $this->tomouh_model->getBeforeMemberData($crm_email);
			
			$member_data_new = $this->tomouh_model->getMemberData($crm_email);
			
				
			if(count($member_data_new) && !empty($member_data_new)){
			    if($member_data_new['v_label'] == 'Acceptance expired'){
                    print('<p>Your Acceptance has been expired!</p>');
                    print('<b>Please contact us at <a href="mailto:info@tomouh.net">info@tomouh.net</a> for more details.</b>');
                    exit;
                }
			}
			// 28-2020
			if (isset($member_data['l_user_step1_data']) && !empty($member_data['l_user_step1_data'])) {
				$data['user_step1'] = json_decode($member_data['l_user_step1_data'], true);
			}  
			/* if (isset($_COOKIE['user_step1']) && !empty($_COOKIE['user_step1'])) {
				$data['user_step1'] = json_decode($_COOKIE['user_step1'], true);
			}*/
			// end
			// $data['user_step1'] =  isset($member_data['l_user_step1_data']) ? json_decode($member_data['l_user_step1_data'], true) : '';

			if (!empty($member_data)) {


				$user['contact_id'] = $member_data['v_contact_crm_id'];
			
				$data['member'] = $this->getZohoCRMContactsById($member_data['v_contact_crm_id']);

				$this->session->set_userdata($user);
			}
			else if (!empty($member_data_new)) {


				$user['contact_id'] = $member_data_new['v_crm_contact_id'];
				// echo "<pre>"; print_r($member_data); exit;

				$data['member'] = $this->getZohoCRMContactsById($member_data_new['v_crm_contact_id']);
				
				// echo "<pre>";
				// print_r($data['member']);
				// exit;

				$this->session->set_userdata($user);
			}  
			else {

				$data['member'] = $this->getZohoCRMContacts($crm_email, 1);
			
				$user['contact_id'] = $data['member']['contact_id'];

				$this->session->set_userdata($user);
			}
		} else {
			$data['member'] = '';
			if (isset($_COOKIE['user_step1']) && !empty($_COOKIE['user_step1'])) {
				$data['user_step1'] = json_decode($_COOKIE['user_step1'], true);
			}
			// echo "<pre>";
			// print_r($this->input->post());
			// exit;
			$email = $this->input->post('email');

			$results = $this->tomouh_model->getAllMembers();

			foreach ($results as $result) {

				$saved_email = $result['v_email'];

				if ($email != '' && $email == $saved_email) {

					redirect(base_url() . 'signup/?succ=0&msg=sameemail');
					exit;
				}
			}
		}

		$data['plan_type'] = isset($_GET['plan_type']) ? $_GET['plan_type'] : '';
		if ($this->input->post()) {

			$data = $this->input->post();

			// setcookie('user_step1', json_encode($data), time() + (86400 * 30), "/");
			setcookie('user_step1', json_encode($data), time() + 31536000, "/");
		
			

			$password = $this->input->post('password');
			$confirm_password = $this->input->post('confirm_password');

			if ($password != $confirm_password) {
				redirect(base_url() . 'signup/?succ=0&msg=mispass');
				exit;
			}

			// $email = $this->input->post('email');

			// $results = $this->tomouh_model->getAllMembers();

			//    foreach($results as $result){

			//      $saved_email = $result['v_email'];

			//      if($email == $saved_email){

			//      		redirect(base_url().'signup/?succ=0&msg=sameemail');
			// 		exit;
			//      }

			//    }

			$login_data['user'] = $this->input->post();
			$this->session->set_userdata($login_data);

				
			redirect(base_url() . 'signup_2');
			exit();
		}

		$data['countries'] = $this->tomouh_model->getCountry();
		
		
		
		$this->load->view('signup', $data);
	}
	public function signup_2($slug = "signup")
	{
		
		if ($this->session->has_userdata('logged_user')) {

			redirect(base_url() . 'profile');
			exit;
		}
        
      
		$result = $this->tomouh_model->getPageBySlug($slug);

		$data                     = $result;
		$data['meta']             = $this->tomouh_model->getPageMeta($result['id']);
		$data['meta_title']       = ($result['v_meta_title']) ? $result['v_meta_title'] : $result['v_title'];
		$data['meta_keyword']     = $result['v_meta_keyword'];
		$data['meta_description'] = $result['l_meta_description'];
		$data['main_description'] = $result['l_description'];
		$data['body_class']       = $slug . ' page-' . $result['id'];
		$data['title']            = $result['v_title'];

		$this->load->helper('url');

		$user_id  = $this->session->userdata("logged_user");
		
		if ($this->input->post()) {

			$data = $this->input->post();


			// setcookie('user_step2', json_encode($data), time() + (86400 * 30), "/");
			// setcookie('user_step2', json_encode($data), time() + 31536000, "/");

			// 28-2020
			

			if (isset($_COOKIE['user_step2']) && !empty($_COOKIE['user_step2'])) {
				setcookie('user_step2', json_encode($data), time() + 31536000, "/");
				$data['user_step2'] = json_decode($_COOKIE['user_step2'], true);
			}else{
				setcookie('user_step2', json_encode($data), time() + 31536000, "/");
			}
			// end
			$user_data = $this->session->userdata('user');
		


			if (isset($user_data['plan_type']) && $user_data['plan_type'] == 'free') {

				$next_subscription_date = date('Y-m-d', strtotime('+1 year'));
                	$latitude = '';
					$longitude = '';
				// $address = $user_data['residence_city'] . ' ' . $user_data['residence_country'];

				// $google_api_key = $this->tomouh_model->getSetting('GOOGLE_MAP_KEY');

				// $geo = $this->file_get_contents_curl('https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&key=' . $google_api_key);

				// $geo = json_decode($geo, true);

				// if (isset($geo['status']) && ($geo['status'] == 'OK')) {

				// 	$latitude = $geo['results'][0]['geometry']['location']['lat'];
				// 	$longitude = $geo['results'][0]['geometry']['location']['lng'];
				// } else {

				// 	$latitude = '';
				// 	$longitude = '';
				// }

				$main_occupation_description = isset($data['main_occu']) ? $data['main_occu'] : '';

				$main_occupation_data = isset($data['occupation']) ? $data['occupation'] : '';

				$int_main_occupation = intval(preg_replace('/[^0-9]+/', '', $main_occupation_data), 10);

				$passion_interest = $data['passions_interests'];

				if ($user_data['referral'] == 0) {

					$referral_email = '';
				} else {

					$referral_email = $user_data['referral_email'];
				}

				/*
						email part
						*/

				$user = $user_data['email'];

				$email_to = $user;
				$email_from = "";

				$template = $this->tomouh_model->getEmailTemplate(2);
				$email_subject = $template['v_subject'];
				$content = $template['l_body'];

				$activation_link = '<a href="' . base_url() . 'login/activation/' . md5($user_data['email']) . '" target="_blank">link here</a>';

				$content = str_replace("link here", $activation_link, $content);

				$content = str_replace("../../../../", base_url(), $content);
	
				$sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content, $attachments = array());
				$sent = 1;
				if ($sent) {

					$update_data = array(

						'v_email'   	 => $user_data['email'],
						'v_password' 	 => md5($user_data['password']),
						'v_firstname'	 => $user_data['firstname'],
						'v_lastname' 	 => $user_data['lastname'],
						'd_dob'      	 => date('Y-m-d', strtotime($user_data['dob'])),
						'e_gender'    	 => $user_data['gender'],
						'v_home_country' => $user_data['home_country'],
						'v_home_city'    => $user_data['home_city'],
						'v_residence_country' => $user_data['residence_country'],
						'v_residence_city'    => $user_data['residence_city'],
						'v_telephone' 	 => $user_data['tele_country_code'] . '-' . $user_data['telephone'],
						'v_telephone_2'  => $user_data['tele_2_country_code'] . '-' . $user_data['telephone_2'],
						'v_twitter_link ' => $user_data['twitter_link'],
						'v_linkedin_link' => $user_data['linkedin_link'],
						'e_referral' 	 => $user_data['referral'],
						'v_referral_email' => $referral_email,
						't_passion_interest'  => isset($passion_interest) ? $passion_interest : '',
						't_description'  => '',
						'v_main_occupation' => $main_occupation_description,
						'v_longitude' => $longitude,
						'v_latitude' => $latitude,
						'e_status' => 'inactive',
						'e_member_of_month' => 0,
						'v_subscription_id' => '',
						'e_type' => $user_data['type'],
						'v_label' => 'Member',
						'e_plan_type' => 'free',
						'v_crm_contact_id' => $user_data['contact_id'],
						'e_payment_type' => '',
						'd_subscription_exp_date' => date("Y-m-d", strtotime('-1 day')),
					);

					$tbl = "tbl_members";
					// $add_user_data = $this->admin_model->update_member_entry($user_data['contact_id'], $update_data, $tbl);
					$add_user_data = $this->admin_model->add_entry($update_data,$tbl);
					$user_id = $add_user_data;
					
					$this->admin_model->delete_data($add_user_data,'tbl_educational_data');

					$this->admin_model->delete_data($add_user_data,'tbl_professional_data');
					$this->admin_model->delete_data($add_user_data,'tbl_achievements_data');
					
					$educ_data_length = sizeof($data['university']);

				for ($i = 0; $i < $educ_data_length; $i++) {
					if (isset($data['university'][$i]) && !empty($data['university'][$i])) {
						if ($int_main_occupation <= 10) {

							if (($i + 1) == $int_main_occupation) {
								$main_occupation = 1;
							} else {
								$main_occupation = 0;
							}
						} else {
							$main_occupation = 0;
						}

						$insert_array = array(

							'user_id' => $user_id,
							'v_university' => $data['university'][$i],
							'v_degree' => $data['degree'][$i],
							'v_major' => $data['major'][$i],
							'i_passing_year' => $data['graduation_year'][$i],
							'i_main_occupation' => $main_occupation
						);
						$tbl = "tbl_educational_data";
						$this->admin_model->add_entry($insert_array, $tbl);
					}
				}

				$company_data_length = sizeof($data['company']);

				for ($i = 0; $i < $company_data_length; $i++) {

					if (isset($data['company'][$i]) && !empty($data['company'][$i])) {
						if ($int_main_occupation >= 10) {

							if (($i + 11) == $int_main_occupation) {
								$main_occupation = 1;
							} else {
								$main_occupation = 0;
							}
						} else {
							$main_occupation = 0;
						}

						$insert_array = array(

							'user_id' => $user_id,
							'v_company' => $data['company'][$i],
							'v_job_title' => $data['job_title'][$i],
							'v_industry' => $data['industry'][$i],
							'i_company_from' => $data['company_from'][$i],
							'i_company_to' => $data['company_to'][$i],
							'i_main_occupation' => $main_occupation
						);
						$tbl = "tbl_professional_data";
						$this->admin_model->add_entry($insert_array, $tbl);
					}
				}

				$award_data_length = sizeof($data['award_details']);

				for ($i = 0; $i < $award_data_length; $i++) {

					if (isset($data['award_details'][$i]) && !empty($data['award_details'][$i])) {
						$insert_array = array(

							'user_id' => $user_id,
							'v_achievement' => $data['award_details'][$i],
						);
						$tbl = "tbl_achievements_data";
						$this->admin_model->add_entry($insert_array, $tbl);
					}
				}


					// $add_user_data = $this->admin_model->add_entry($insert_data,$tbl);

					$login_data['user_step2'] = $this->input->post();

					$this->session->set_userdata($login_data);

					$this->add_zoho_crm_plan($user_id);

					// if ($user_data['type'] == 'member') {

					// 	$this->add_mailchimp($user_data['email'], $user_data['firstname'], $user_data['lastname']);
					// }


					// $member_type = $user_data['type'];

					// if($member_type == 'member'){

					// 	$contact_id = $user_data['contact_id'];

					// 	$this->updateContactLabel($contact_id);

					// }

					$this->session->unset_userdata('user');

					// $res = setcookie('user_step1', '', time() - 2592000, '/');

					// $res = setcookie('user_step2', '', time() - 2592000, '/');
					$res = setcookie('user_step1', '', time() - 31536000, '/');//5 Days

					$res = setcookie('user_step2', '', time() - 31536000, '/'); //5 Days

					redirect(base_url() . 'login?succ=1&msg=notverify');
					exit();
				}
			} elseif (isset($user_data['contact_id']) && $user_data['contact_id'] == '') {

				$before_array = array(
					'v_email'			=> $user_data['email'],
					'l_user_step1_data' => json_encode($user_data),
					'l_user_step2_data'	=> json_encode($data),
					'd_added' 			=> date('Y-m-d H:i:s'),
				);

				$inserted_id = $this->admin_model->add_entry($before_array, 'tbl_before_member');

				$user_data['lead_source'] = 'Via Website';
				
				$con_id = $this->addZohoContactMember($user_data, $data, $inserted_id);
			
				// $contactdata = $this->session->userdata('contactdata');

				$insert_data = array(

					'v_email'   	 => $user_data['email'],
					'v_password' 	 => md5($user_data['password']),
					'v_firstname'	 => $user_data['firstname'],
					'v_lastname' 	 => $user_data['lastname'],
					'd_dob'      	 => date('Y-m-d', strtotime($user_data['dob'])),
					'e_gender'    	 => $user_data['gender'],
					'v_home_country' => $user_data['home_country'],
					'v_home_city'    => $user_data['home_city'],
					'v_residence_country' => $user_data['residence_country'],
					'v_residence_city'    => $user_data['residence_city'],
					'v_telephone' 	 => $user_data['tele_country_code'] . '-' . $user_data['telephone'],
					'v_telephone_2'  => $user_data['tele_2_country_code'] . '-' . $user_data['telephone_2'],
					'v_twitter_link ' => $user_data['twitter_link'],
					'v_linkedin_link' => $user_data['linkedin_link'],
					'e_referral' 	 => $user_data['referral'],
					'v_referral_email' => $referral_email,
					't_passion_interest'  => isset($passion_interest) ? $passion_interest : '',
					't_description'  => '',
					'v_main_occupation' => $main_occupation_description,
					'v_longitude' => $longitude,
					'v_latitude' => $latitude,
					'e_status' => 'inactive',
					'e_member_of_month' => 0,
					'v_subscription_id' => '',
					'e_type' => $user_data['type'],
					'v_label' => 'Candidate',
					'e_plan_type' => '',
					'v_crm_contact_id' => $con_id,
					'e_payment_type' => '',
					// 'd_subscription_exp_date'=>date("Y-m-d", strtotime('+1 year')),
				);

				$tbl = "tbl_members";
				$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);

				$educ_data_length = sizeof($data['university']);

				for ($i = 0; $i < $educ_data_length; $i++) {
					if (isset($data['university'][$i]) && !empty($data['university'][$i])) {
						if ($int_main_occupation <= 10) {

							if (($i + 1) == $int_main_occupation) {
								$main_occupation = 1;
							} else {
								$main_occupation = 0;
							}
						} else {
							$main_occupation = 0;
						}

						$insert_array = array(

							'user_id' => $add_user_data,
							'v_university' => $data['university'][$i],
							'v_degree' => $data['degree'][$i],
							'v_major' => $data['major'][$i],
							'i_passing_year' => $data['graduation_year'][$i],
							'i_main_occupation' => $main_occupation
						);
						$tbl = "tbl_educational_data";
						$this->admin_model->add_entry($insert_array, $tbl);
					}
				}

				$company_data_length = sizeof($data['company']);

				for ($i = 0; $i < $company_data_length; $i++) {

					if (isset($data['company'][$i]) && !empty($data['company'][$i])) {
						if ($int_main_occupation >= 10) {

							if (($i + 11) == $int_main_occupation) {
								$main_occupation = 1;
							} else {
								$main_occupation = 0;
							}
						} else {
							$main_occupation = 0;
						}

						$insert_array = array(

							'user_id' => $add_user_data,
							'v_company' => $data['company'][$i],
							'v_job_title' => $data['job_title'][$i],
							'v_industry' => $data['industry'][$i],
							'i_company_from' => $data['company_from'][$i],
							'i_company_to' => $data['company_to'][$i],
							'i_main_occupation' => $main_occupation
						);
						$tbl = "tbl_professional_data";
						$this->admin_model->add_entry($insert_array, $tbl);
					}
				}

				$award_data_length = sizeof($data['award_details']);

				for ($i = 0; $i < $award_data_length; $i++) {

					if (isset($data['award_details'][$i]) && !empty($data['award_details'][$i])) {
						$insert_array = array(

							'user_id' => $add_user_data,
							'v_achievement' => $data['award_details'][$i],
						);
						$tbl = "tbl_achievements_data";
						$this->admin_model->add_entry($insert_array, $tbl);
					}
				}


				// $res = setcookie('user_step1', '', time() - 2592000, '/');

				// $res = setcookie('user_step2', '', time() - 2592000, '/');

				$res = setcookie('user_step1', '', time() - 31536000, '/');

				$res = setcookie('user_step2', '', time() - 31536000, '/');

				

				$this->session->unset_userdata('user');

				$this->session->unset_userdata('user_step2');

				redirect(base_url() . 'signup_success');
				exit;
			} else {

				$login_data['user_step2'] = $this->input->post();

				$this->session->set_userdata($login_data);

				$member_type = $user_data['type'];

				$us_data['last_page'] = "signup_2";

				$this->session->set_userdata($us_data);

				redirect(base_url() . 'payment');
				exit;
			}
		}

		$login_data = $this->session->userdata('user');

		if (empty($login_data)) {

			redirect(base_url() . 'signup');
			exit;
		} else {

			if (!isset($user_id)) {

				$user_id = 0;
			}

			$user_email = isset($login_data['email']) ? $login_data['email'] : '';

			if (!empty($user_email)) {

				$edu_pro_data = $this->tomouh_model->getBeforeMemberData($user_email);

				// $data['user_step2'] = isset($edu_pro_data['l_user_step2_data']) ? json_decode($edu_pro_data['l_user_step2_data'], true) : '';
				// 28-2020
				/* if(isset($edu_pro_data['l_user_step2_data']) && !empty($edu_pro_data['l_user_step2_data'])){
					$data['user_step2'] = json_decode($edu_pro_data['l_user_step2_data'], true);
				}else{
					if (isset($_COOKIE['user_step2']) && !empty($_COOKIE['user_step2'])) {

						$data['user_step2'] = json_decode($_COOKIE['user_step2'], true);
					}
				} */
				if (isset($_COOKIE['user_step2']) && !empty($_COOKIE['user_step2'])) {
					$data['user_step2'] = json_decode($_COOKIE['user_step2'], true);
				}
				// End
			} else {

				if (isset($_COOKIE['user_step2']) && !empty($_COOKIE['user_step2'])) {

					$data['user_step2'] = json_decode($_COOKIE['user_step2'], true);
				}
			}

			$data['industry_data'] = $this->tomouh_model->getIndustryActiveData();
		}

		$this->load->view('signup_2', $data);
	}
	public function edit_profile($slug = "edit_profile")
	{

		$this->load->helper('url');
		// if(isset($_GET['update_info']) && $_GET['update_info'] == 1){
			if(!$this->session->has_userdata('logged_user')){
				$this->session->set_userdata('last_page', current_url().'?update_info=1');
				redirect('/login?succ=0&msg=logfirst');
				exit;
			}
		// }
		$data = [];
		
		$data['meta_title'] = "Edit Profile";
		$data['meta_keyword'] = "Edit Profile";
		$data['meta_description'] = "Edit Profile";

		$user_id  = $this->session->userdata("logged_user");

		if (!$this->session->has_userdata('logged_user')) {

			redirect(base_url() . 'login/?succ=0&msg=logfirst');
			exit;
		}

		if ($this->input->post()) {

			$data = $this->input->post();

			$user_data = $this->tomouh_model->getUserByUserId($user_id);
			// echo "<pre>";
			// print_r($data);
			// // print_r($user_data);
			// exit;
			$old_data = [];
			$new_data = [];
			// if($this->input->post('update_info') == 1){
				if(!empty($data) && !empty($user_data)){

					if($data['firstname'] != $user_data['v_firstname']){
						$old_data['firstname']= $user_data['v_firstname'];
						$new_data['firstname']= $data['firstname'];
					}
					if($data['lastname'] != $user_data['v_lastname']){
						$old_data['lastname']= $user_data['v_lastname'];
						$new_data['lastname']= $data['lastname'];
					}
					if(date('Y-m-d',strtotime($data['dob']))  != $user_data['d_dob']){
						$old_data['date of birth']= $user_data['d_dob'];
						$new_data['date of birth']= date('Y-m-d',strtotime($data['dob']));
					}
					if($data['gender'] != $user_data['e_gender']){
						$old_data['gender']= $user_data['e_gender'];
						$new_data['gender']= $data['gender'];
					}
					if($data['email'] != $user_data['v_email']){
						$old_data['email']= $user_data['v_email'];
						$new_data['email']= $data['email'];
					}
					if($data['home_country'] != $user_data['v_home_country']){
						$old_data['home country']= $user_data['v_home_country'];
						$new_data['home country']= $data['home_country'];
					}
					if($data['home_city'] != $user_data['v_home_city']){
						$old_data['home city']= $user_data['v_home_city'];
						$new_data['home city']= $data['home_city'];
					}
					if($data['residence_country'] != $user_data['v_residence_country']){
						$old_data['residence country']= $user_data['v_residence_country'];
						$new_data['residence country']= $data['residence_country'];
					}
					if($data['residence_city'] != $user_data['v_residence_city']){
						$old_data['residence city']= $user_data['v_residence_city'];
						$new_data['residence city']= $data['residence_city'];
					}
					if($data['tele_country_code'].'-'.$data['telephone'] != $user_data['v_telephone']){
						$old_data['mobile']= $user_data['v_telephone'];
						$new_data['mobile']= $data['tele_country_code'].'-'.$data['telephone'];
					}
					if($data['tele_2_country_code'].'-'.$data['telephone_2'] != $user_data['v_telephone_2']){
						$old_data['mobile 2']= $user_data['v_telephone_2'];
						$new_data['mobile 2']= $data['tele_2_country_code'].'-'.$data['telephone_2'];
					}
					if($data['twitter_link'] != $user_data['v_twitter_link']){
						$old_data['twitter link']= $user_data['v_twitter_link'];
						$new_data['twitter link']= $data['twitter_link'];
					}
					if($data['linkedin_link'] != $user_data['v_linkedin_link']){
						$old_data['linkedin link']= $user_data['v_linkedin_link'];
						$new_data['linkedin link']= $data['linkedin_link'];
					}
					if($data['instagram link'] != $user_data['v_instagram_link']){
						$old_data['instagram link']= $user_data['v_instagram_link'];
						$new_data['instagram link']= $data['instagram_link'];
					}
	
					if($data['referral_email'] != $user_data['v_referral_email']){
						$old_data['referral email']= $user_data['v_referral_email'];
						$new_data['referral email']= $data['referral_email'];
					}
	
					if(count($new_data) && !empty($new_data)){
						$tbl ="tbl_members_log";
						$l_data = array(
							'member_id'=> $user_id, 
							'firstname'=> $user_data['v_firstname'],
							'lastname'=> $user_data['v_lastname'],
							'gender'=> $user_data['e_gender'],
						);
						$insert_data=array(
							'l_old_data'=>json_encode($old_data),
							'l_new_data'=>json_encode($new_data),
							'l_data' => json_encode($l_data),
							'd_modified'=>date('Y-m-d H:i:s'),
						); 
						$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
					
					}
					
					// print_r($old_data);
					// print_r($new_data);
					// print_r(json_encode($old_data));
					// print_r(json_encode($new_data));
					// exit;
				}
			// }
		


			$email = $user_data['v_email'];

			$results = $this->tomouh_model->getAllMembers();

			if ($email != $data['email']) {

				mailChimpChangeEmail($data['email'], $email);

				foreach ($results as $result) {

					$saved_email = $result['v_email'];

					if ($data['email'] == $saved_email) {

						redirect(base_url() . 'edit_personal_info/?succ=0&msg=sameemail');
						exit;
					}
				}
			}

			$date_of_birth = date('Y-m-d', strtotime($data['dob']));

			if ($data['referral'] == 0) {

				$referral_email = '';
			} else {

				$referral_email = $data['referral_email'];
			}

			$address = $data['residence_city'] . ' ' . $data['residence_country'];

			$google_api_key = $this->tomouh_model->getSetting('GOOGLE_MAP_KEY');

			$geo = $this->file_get_contents_curl('https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&key=' . $google_api_key);

			$geo = json_decode($geo, true);

			if (isset($geo['status']) && ($geo['status'] == 'OK')) {

				$latitude = $geo['results'][0]['geometry']['location']['lat'];
				$longitude = $geo['results'][0]['geometry']['location']['lng'];
			} else {
				$latitude = '';
				$longitude = '';
			}

			$v_telephone='';
			if(!empty($data['tele_country_code'])){
				$v_telephone=$data['tele_country_code'];
				if(!empty($data['telephone'])){
					$v_telephone.='-'.$data['telephone'];
				}
			}else{
				if(!empty($data['telephone'])){
					$v_telephone=$data['telephone'];
				}
			}
			
			$v_telephone_2='';
			if(!empty($data['tele_2_country_code'])){
				$v_telephone_2=$data['tele_2_country_code'];
				if(!empty($data['telephone_2'])){
					$v_telephone_2.='-'.$data['telephone_2'];
				}
			}else{
				if(!empty($data['telephone_2'])){
					$v_telephone_2=$data['telephone_2'];
				}
			}

			$update_data = array(
				'v_email'   	 => $data['email'],
				'v_firstname'	 => $data['firstname'],
				'v_lastname' 	 => $data['lastname'],
				'd_dob'      	 => $date_of_birth,
				'e_gender'    	 => $data['gender'],
				'v_home_country' => $data['home_country'],
				'v_home_city'    => $data['home_city'],
				'v_residence_country' => $data['residence_country'],
				'v_residence_city'    => $data['residence_city'],
				// 'v_telephone' 	 => $data['tele_country_code'] . '-' . $data['telephone'],
				// 'v_telephone_2'  => $data['tele_2_country_code'] . '-' . $data['telephone_2'],
				'v_telephone' 	 => $v_telephone,
				'v_telephone_2'  => $v_telephone_2,
				'v_twitter_link ' => $data['twitter_link'],
				'v_linkedin_link' => $data['linkedin_link'],
				'v_instagram_link' => $data['instagram_link'],
				'e_referral'     => $data['referral'],
				'v_referral_email' => $referral_email,
				'v_longitude' => $longitude,
				'v_latitude' => $latitude,
				'i_updated' => 1,
				'e_updated' => 'yes',
				'd_last_profile_update'=>date('Y-m-d'),
			);

			
			$tbl = "tbl_members";
			$updateee =$this->admin_model->update_entry($user_id, $update_data, $tbl);
		
			
			$this->update_crm_detail_step1($data, $user_id);
			
			// redirect(base_url() . 'profile');
			// exit();
		}


		if (isset($this->session->userdata["logged_user"])) {

			$user_data = $this->tomouh_model->getUserByUserId($user_id);

			$usr_data['user'] = $user_data;

			$this->session->set_userdata($usr_data);
		}

		$data['educational_data'] = $edu_data = $this->tomouh_model->getEducationalData($user_id);

		$data['professional_data'] = $prof_data = $this->tomouh_model->getProfessionalData($user_id);

		$data['achievement_data'] = $achi_data = $this->tomouh_model->getAchievementalData($user_id);

		$user_data = $this->tomouh_model->getUserByUserId($user_id);

		$data['industry_data'] = $this->tomouh_model->getIndustryActiveData();

		if ($this->input->post()) {

			$update_user = array(
				'e_updated' => 'yes',
				'd_last_profile_update'=>date('Y-m-d'),
			);
	
			$tbl = "tbl_members";
			$this->admin_model->update_entry($user_id, $update_user, $tbl);

			$old_data = [];
			$new_data = [];

			$data = $this->input->post();

			$data['edu_row_id'] = isset($data['edu_row_id']) ? $data['edu_row_id'] : array('0');

			if (isset($data['edu_row_id'])) {

				foreach ($edu_data as $row) {

					if (!in_array($row['id'], $data['edu_row_id'])) {
						$rowData = $this->admin_model->getRow("tbl_educational_data",$row['id']);
						
						$old_data=[];
						$new_data=[];

						$old_data['university']= $rowData['v_university'];
						$old_data['degree']= $rowData['v_degree'];
						$old_data['major']= $rowData['v_major'];
						$old_data['passing_year']= $rowData['i_passing_year'];
						
						$tbl ="tbl_members_log";
						$l_data = array(
							'member_id'=> $user_id, 
							'firstname'=> $user_data['v_firstname'],
							'lastname'=> $user_data['v_lastname'],
							'gender'=> $user_data['e_gender'],
						);
						$insert_data=array(
							'l_old_data'=>json_encode($old_data),
							'l_new_data'=>json_encode($new_data),
							'l_data' => json_encode($l_data),
							'is_education'=> 1,
							'd_modified'=>date('Y-m-d H:i:s'),
						); 
						$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
							
					
						$this->admin_model->delete_entry($row['id'], "tbl_educational_data");
					}
				}
			}

			$data['pro_row_id'] = isset($data['pro_row_id']) ? $data['pro_row_id'] : array('0');

			if (isset($data['pro_row_id'])) {

				foreach ($prof_data as $row) {

					if (!in_array($row['id'], $data['pro_row_id'])) {
						$old_data=[];
						$new_data=[];


						$rowData = $this->admin_model->getRow("tbl_professional_data",$row['id']);
						
							
							$old_data['company']= $rowData['v_company'];
							$old_data['job_title']= $rowData['v_job_title'];
							$old_data['industry']= $rowData['v_industry'];
							$old_data['company_from']= $rowData['i_company_from'];
							$old_data['company_to']= $rowData['i_company_to'];
							
							$tbl ="tbl_members_log";
							$l_data = array(
								'member_id'=> $user_id, 
								'firstname'=> $user_data['v_firstname'],
								'gender'=> $user_data['e_gender'],
								'lastname'=> $user_data['v_lastname']
							);
							$insert_data=array(
								'l_old_data'=>json_encode($old_data),
								'l_new_data'=>json_encode($new_data),
								'l_data' => json_encode($l_data),
								'is_professional'=>1,
								'd_modified'=>date('Y-m-d H:i:s'),
							); 
							$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
							
					
						$this->admin_model->delete_entry($row['id'], "tbl_professional_data");
					}
				}
			}

			$data['achi_row_id'] = isset($data['achi_row_id']) ? $data['achi_row_id'] : array('0');

			if (isset($data['achi_row_id'])) {

				foreach ($achi_data as $row) {

					if (!in_array($row['id'], $data['achi_row_id'])) {

						$rowData = $this->admin_model->getRow("tbl_achievements_data",$row['id']);
						
							$old_data=[];
							$new_data=[];

							$old_data['achievement']= $rowData['v_achievement'];
						
							$tbl ="tbl_members_log";
							$l_data = array(
								'member_id'=> $user_id, 
								'firstname'=> $user_data['v_firstname'],
								'gender'=> $user_data['e_gender'],
								'lastname'=> $user_data['v_lastname']
							);
							$insert_data=array(
								'l_old_data'=>json_encode($old_data),
								'l_new_data'=>json_encode($new_data),
								'l_data' => json_encode($l_data),
								'is_achievements'=>1,
								'd_modified'=>date('Y-m-d H:i:s'),
							); 
							$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
							
						
						$this->admin_model->delete_entry($row['id'], "tbl_achievements_data");
					}
				}
			}


			$main_occupation_data = $this->input->post('occupation');

			$int_main_occupation = intval(preg_replace('/[^0-9]+/', '', $main_occupation_data), 10);

			$passion_interest_data = array(
				't_passion_interest' => $data['passions_interests'],
				'v_main_occupation' => $data['main_occu'],
				'i_updated' => 1,
				'e_updated' => 'yes',
				// 'd_last_profile_update'=>date('Y-m-d'),
			);

				$old_data=[];
				$new_data=[];

				if($user_data['t_passion_interest'] != $data['passions_interests']){
					$old_data['Passions and interests']= $user_data['t_passion_interest'];
			
					$new_data['Passions and interests']= $data['passions_interests'];
					if(count($new_data) && !empty($new_data)){
						$tbl ="tbl_members_log";
						$l_data = array(
							'member_id'=> $user_id, 
							'firstname'=> $user_data['v_firstname'],
							'gender'=> $user_data['e_gender'],
							'lastname'=> $user_data['v_lastname']
						);
					}
					
					$insert_data=array(
						'l_old_data'=>json_encode($old_data),
						'l_new_data'=>json_encode($new_data),
						'l_data' => json_encode($l_data),
						'd_modified'=>date('Y-m-d H:i:s'),
					); 
					$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
				}
							
		
			$tbl = "tbl_members";
			$this->admin_model->update_entry($user_id, $passion_interest_data, $tbl);


			$educ_data_length = sizeof($data['university']);

			for ($i = 0; $i < $educ_data_length; $i++) {

				if (isset($data['university'][$i]) && !empty($data['university'][$i])) {

					if ($int_main_occupation <= 10) {

						if (($i + 1) == $int_main_occupation) {
							$main_occupation = 1;
						} else {
							$main_occupation = 0;
						}
					} else {
						$main_occupation = 0;
					}

					if (!empty($data['edu_row_id'][$i])) {

							$old_data=[];
							$new_data=[];

							$rowData = $this->admin_model->getRow("tbl_educational_data",$data['edu_row_id'][$i]);

							if(!empty($data) && !empty($user_data)){
			
								if($data['university'][$i] != $rowData['v_university']){
									$old_data['university']= $rowData['v_university'];
									$new_data['university']= $data['university'][$i];
								}
								if($data['degree'][$i] != $rowData['v_degree']){
									$old_data['degree']= $rowData['v_degree'];
									$new_data['degree']= $data['degree'][$i];
								}

								if($data['major'][$i] != $rowData['v_major']){
									$old_data['major']= $rowData['v_major'];
									$new_data['major']= $data['major'][$i];
								}
								if($data['graduation_year'][$i] != $rowData['i_passing_year']){
									$old_data['graduation_year']= $rowData['i_passing_year'];
									$new_data['graduation_year']= $data['graduation_year'][$i];
								}
							
								if(count($new_data) && !empty($new_data)){
									$tbl ="tbl_members_log";
									$l_data = array(
										'member_id'=> $user_id, 
										'firstname'=> $user_data['v_firstname'],
										'gender'=> $user_data['e_gender'],
										'lastname'=> $user_data['v_lastname']
									);
									$insert_data=array(
										'l_old_data'=>json_encode($old_data),
										'l_new_data'=>json_encode($new_data),
										'l_data' => json_encode($l_data),
										'is_education'=>1,
										'd_modified'=>date('Y-m-d H:i:s'),
									); 
									$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
								
								}
								
								// print_r($old_data);
								// print_r($new_data);
								// print_r(json_encode($old_data));
								// print_r(json_encode($new_data));
								// exit;
							}
					
						$update_array = array(
							'v_university' => $data['university'][$i],
							'v_degree' => $data['degree'][$i],
							'v_major' => $data['major'][$i],
							'i_passing_year' => $data['graduation_year'][$i],
							'i_main_occupation' => $main_occupation
						);

						$tbl = "tbl_educational_data";
						$this->admin_model->update_entry($data['edu_row_id'][$i], $update_array, $tbl);
					} else {

						if($this->input->post('update_info') == 1){
							if(!empty($data) && !empty($user_data)){
								$old_data=[];
								$new_data=[];
								$new_data['university']= $data['university'][$i];
								$new_data['degree']= $data['degree'][$i];
								$new_data['major']= $data['major'][$i];
								$new_data['graduation_year']= $data['graduation_year'][$i];
							
								if(count($new_data) && !empty($new_data)){
									$tbl ="tbl_members_log";
									$l_data = array(
										'member_id'=> $user_id, 
										'firstname'=> $user_data['v_firstname'],
										'gender'=> $user_data['e_gender'],
										'lastname'=> $user_data['v_lastname']
									);
									$insert_data=array(
										'l_old_data'=>json_encode($old_data),
										'l_new_data'=>json_encode($new_data),
										'l_data' => json_encode($l_data),
										'is_education'=>1,
										'd_modified'=>date('Y-m-d H:i:s'),
									); 
									$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
								
								}
								
							}
						}


						$insert_array = array(

							'user_id' => $user_id,
							'v_university' => $data['university'][$i],
							'v_degree' => $data['degree'][$i],
							'v_major' => $data['major'][$i],
							'i_passing_year' => $data['graduation_year'][$i],
							'i_main_occupation' => $main_occupation
						);
						$tbl = "tbl_educational_data";
						$this->admin_model->add_entry($insert_array, $tbl);
					}
				}
			}

			$prof_data_length = sizeof($data['company']);

			for ($i = 0; $i < $prof_data_length; $i++) {
				if (isset($data['company'][$i]) && !empty($data['company'][$i])) {

					if ($int_main_occupation >= 10) {

						if (($i + 11) == $int_main_occupation) {
							$main_occupation = 1;
						} else {
							$main_occupation = 0;
						}
					} else {
						$main_occupation = 0;
					}

					if (!empty($data['pro_row_id'][$i])) {
							$old_data=[];
							$new_data=[];

							$rowData = $this->admin_model->getRow("tbl_professional_data",$data['pro_row_id'][$i]);

							if(!empty($data) && !empty($user_data)){
			
								if($data['company'][$i] != $rowData['v_company']){
									$old_data['company']= $rowData['v_company'];
									$new_data['company']= $data['company'][$i];
								}
								if($data['job_title'][$i] != $rowData['v_job_title']){
									$old_data['job_title']= $rowData['v_job_title'];
									$new_data['job_title']= $data['job_title'][$i];
								}

								if($data['industry'][$i] != $rowData['v_industry']){
									$old_data['industry']= $rowData['v_industry'];
									$new_data['industry']= $data['industry'][$i];
								}
								if($data['company_from'][$i] != $rowData['i_company_from']){
									$old_data['company_from']= $rowData['i_company_from'];
									$new_data['company_from']= $data['company_from'][$i];
								}
								if($data['company_to'][$i] != $rowData['i_company_to']){
									$old_data['company_to']= $rowData['i_company_to'];
									$new_data['company_to']= $data['company_to'][$i];
								}
							
								if(count($new_data) && !empty($new_data)){
									$tbl ="tbl_members_log";
									$l_data = array(
										'member_id'=> $user_id, 
										'firstname'=> $user_data['v_firstname'],
										'gender'=> $user_data['e_gender'],
										'lastname'=> $user_data['v_lastname']
									);
									$insert_data=array(
										'l_old_data'=>json_encode($old_data),
										'l_new_data'=>json_encode($new_data),
										'l_data' => json_encode($l_data),
										'is_professional'=>1,
										'd_modified'=>date('Y-m-d H:i:s'),
									); 
									$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
								
								}
								
							}
				

						$update_array = array(

							'v_company' => $data['company'][$i],
							'v_job_title' => $data['job_title'][$i],
							'v_industry' => $data['industry'][$i],
							'i_company_from' => $data['company_from'][$i],
							'i_company_to' => $data['company_to'][$i],
							'i_main_occupation' => $main_occupation
						);

						$tbl = "tbl_professional_data";
						$this->admin_model->update_entry($data['pro_row_id'][$i], $update_array, $tbl);
					} else {

							if(!empty($data) && !empty($user_data)){
								$old_data=[];
								$new_data=[];
								$new_data['company']= $data['company'][$i];
								$new_data['job_title']= $data['job_title'][$i];
								$new_data['industry']= $data['industry'][$i];
								$new_data['company_from']= $data['company_from'][$i];
								$new_data['company_to']= $data['company_to'][$i];
							
								if(count($new_data) && !empty($new_data)){
									$tbl ="tbl_members_log";
									$l_data = array(
										'member_id'=> $user_id, 
										'firstname'=> $user_data['v_firstname'],
										'gender'=> $user_data['e_gender'],
										'lastname'=> $user_data['v_lastname']
									);
									$insert_data=array(
										'l_old_data'=>json_encode($old_data),
										'l_new_data'=>json_encode($new_data),
										'l_data' => json_encode($l_data),
										'is_professional'=>1,
										'd_modified'=>date('Y-m-d H:i:s'),
									); 
									$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
								
								}
								
							}
						$insert_array = array(

							'user_id'           => $user_id,
							'v_company'         => $data['company'][$i],
							'v_job_title'       => $data['job_title'][$i],
							'v_industry'        => $data['industry'][$i],
							'i_company_from'    => $data['company_from'][$i],
							'i_company_to'      => $data['company_to'][$i],
							'i_main_occupation' => $main_occupation
						);
						$tbl = "tbl_professional_data";
						$this->admin_model->add_entry($insert_array, $tbl);
					}
				}
			}

			$achie_data_length = sizeof($data['award_details']);

			for ($i = 0; $i < $achie_data_length; $i++) {

				if (isset($data['award_details'][$i]) && !empty($data['award_details'][$i])) {
				

					if (!empty($data['achi_row_id'][$i])) {

							$old_data=[];
							$new_data=[];

							$rowData = $this->admin_model->getRow("tbl_achievements_data",$data['achi_row_id'][$i]);
	
							if(!empty($data) && !empty($user_data)){
			
								if($data['award_details'][$i] != $rowData['v_achievement']){
									$old_data['achievement']= $rowData['v_achievement'];
									$new_data['achievement']= $data['award_details'][$i];
								}
							
								if(count($new_data) && !empty($new_data)){
									$tbl ="tbl_members_log";
									$l_data = array(
										'member_id'=> $user_id, 
										'firstname'=> $user_data['v_firstname'],
										'gender'=> $user_data['e_gender'],
										'lastname'=> $user_data['v_lastname']
									);
									$insert_data=array(
										'l_old_data'=>json_encode($old_data),
										'l_new_data'=>json_encode($new_data),
										'l_data' => json_encode($l_data),
										'is_achievements'=>1,
										'd_modified'=>date('Y-m-d H:i:s'),
									); 
									$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
								
								}
								
							}
					
						$update_array = array(
							'v_achievement' => $data['award_details'][$i],
						);

						$tbl = "tbl_achievements_data";
						$this->admin_model->update_entry($data['achi_row_id'][$i], $update_array, $tbl);
					} else {

							if(!empty($data) && !empty($user_data)){
								$old_data=[];
								$new_data=[];
								$new_data['achievement']= $data['award_details'][$i];
							
								if(count($new_data) && !empty($new_data)){
									$tbl ="tbl_members_log";
									$l_data = array(
										'member_id'=> $user_id, 
										'firstname'=> $user_data['v_firstname'],
										'gender'=> $user_data['e_gender'],
										'lastname'=> $user_data['v_lastname']
									);
									$insert_data=array(
										'l_old_data'=>json_encode($old_data),
										'l_new_data'=>json_encode($new_data),
										'l_data' => json_encode($l_data),
										'is_achievements'=>1,
										'd_modified'=>date('Y-m-d H:i:s'),
									); 
									$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
								
								}
								
							}
					
						$insert_array = array(
							'user_id' => $user_id,
							'v_achievement' => $data['award_details'][$i],
						);
						$tbl = "tbl_achievements_data";
						$this->admin_model->add_entry($insert_array, $tbl);
					}
				}
			}

			$this->update_crm_detail_step2($data, $user_id);

			redirect(base_url() . 'profile');
			exit;
		}

		$data['t_passion_interest'] = isset($user_data['t_passion_interest']) ? $user_data['t_passion_interest'] : '';
		$data['v_main_occupation'] = isset($user_data['v_main_occupation']) ? $user_data['v_main_occupation'] : '';

		// $this->load->view('edit_profile_2', $data);
	

		// $user_data = $this->tomouh_model->getUserByUserId($user_id);
		//echo "<pre>"; print_r($user_data); exit;
		$data['user_data'] = $user_data;
		$data['countries'] = $this->tomouh_model->getCountry();
		$this->load->view('edit_profile', $data);
	}
	public function edit_profile_1($slug = "edit_profile_1")
	{

		$this->load->helper('url');
		// if(isset($_GET['update_info']) && $_GET['update_info'] == 1){
			if(!$this->session->has_userdata('logged_user')){
				$this->session->set_userdata('last_page', current_url().'?update_info=1');
				redirect('/login?succ=0&msg=logfirst');
				exit;
			}
		// }
		
		$data['meta_title'] = "Edit Profile";
		$data['meta_keyword'] = "Edit Profile";
		$data['meta_description'] = "Edit Profile";

		$user_id  = $this->session->userdata("logged_user");

		if (!$this->session->has_userdata('logged_user')) {

			redirect(base_url() . 'login/?succ=0&msg=logfirst');
			exit;
		}

		if ($this->input->post()) {

			$data = $this->input->post();

			$user_data = $this->tomouh_model->getUserByUserId($user_id);
			// echo "<pre>";
			// print_r($data);
			// print_r($user_data);
			// exit;
			$old_data = [];
			$new_data = [];
			// if($this->input->post('update_info') == 1){
				if(!empty($data) && !empty($user_data)){

					if($data['firstname'] != $user_data['v_firstname']){
						$old_data['firstname']= $user_data['v_firstname'];
						$new_data['firstname']= $data['firstname'];
					}
					if($data['lastname'] != $user_data['v_lastname']){
						$old_data['lastname']= $user_data['v_lastname'];
						$new_data['lastname']= $data['lastname'];
					}
					if(date('Y-m-d',strtotime($data['dob']))  != $user_data['d_dob']){
						$old_data['date of birth']= $user_data['d_dob'];
						$new_data['date of birth']= date('Y-m-d',strtotime($data['dob']));
					}
					if($data['gender'] != $user_data['e_gender']){
						$old_data['gender']= $user_data['e_gender'];
						$new_data['gender']= $data['gender'];
					}
					if($data['email'] != $user_data['v_email']){
						$old_data['email']= $user_data['v_email'];
						$new_data['email']= $data['email'];
					}
					if($data['home_country'] != $user_data['v_home_country']){
						$old_data['home country']= $user_data['v_home_country'];
						$new_data['home country']= $data['home_country'];
					}
					if($data['home_city'] != $user_data['v_home_city']){
						$old_data['home city']= $user_data['v_home_city'];
						$new_data['home city']= $data['home_city'];
					}
					if($data['residence_country'] != $user_data['v_residence_country']){
						$old_data['residence country']= $user_data['v_residence_country'];
						$new_data['residence country']= $data['residence_country'];
					}
					if($data['residence_city'] != $user_data['v_residence_city']){
						$old_data['residence city']= $user_data['v_residence_city'];
						$new_data['residence city']= $data['residence_city'];
					}
					if($data['tele_country_code'].'-'.$data['telephone'] != $user_data['v_telephone']){
						$old_data['mobile']= $user_data['v_telephone'];
						$new_data['mobile']= $data['tele_country_code'].'-'.$data['telephone'];
					}
					if($data['tele_2_country_code'].'-'.$data['telephone_2'] != $user_data['v_telephone_2']){
						$old_data['mobile 2']= $user_data['v_telephone_2'];
						$new_data['mobile 2']= $data['tele_2_country_code'].'-'.$data['telephone_2'];
					}
					if($data['twitter_link'] != $user_data['v_twitter_link']){
						$old_data['twitter link']= $user_data['v_twitter_link'];
						$new_data['twitter link']= $data['twitter_link'];
					}
					if($data['linkedin_link'] != $user_data['v_linkedin_link']){
						$old_data['linkedin link']= $user_data['v_linkedin_link'];
						$new_data['linkedin link']= $data['linkedin_link'];
					}
					if($data['instagram link'] != $user_data['v_instagram_link']){
						$old_data['instagram link']= $user_data['v_instagram_link'];
						$new_data['instagram link']= $data['instagram_link'];
					}
	
					if($data['referral_email'] != $user_data['v_referral_email']){
						$old_data['referral email']= $user_data['v_referral_email'];
						$new_data['referral email']= $data['referral_email'];
					}
	
					if(count($new_data) && !empty($new_data)){
						$tbl ="tbl_members_log";
						$l_data = array(
							'member_id'=> $user_id, 
							'firstname'=> $user_data['v_firstname'],
							'lastname'=> $user_data['v_lastname'],
							'gender'=> $user_data['e_gender'],
						);
						$insert_data=array(
							'l_old_data'=>json_encode($old_data),
							'l_new_data'=>json_encode($new_data),
							'l_data' => json_encode($l_data),
							'd_modified'=>date('Y-m-d H:i:s'),
						); 
						$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
					
					}
					
					// print_r($old_data);
					// print_r($new_data);
					// print_r(json_encode($old_data));
					// print_r(json_encode($new_data));
					// exit;
				}
			// }
		


			$email = $user_data['v_email'];

			$results = $this->tomouh_model->getAllMembers();

			if ($email != $data['email']) {

				mailChimpChangeEmail($data['email'], $email);

				foreach ($results as $result) {

					$saved_email = $result['v_email'];

					if ($data['email'] == $saved_email) {

						redirect(base_url() . 'edit_personal_info/?succ=0&msg=sameemail');
						exit;
					}
				}
			}

			$date_of_birth = date('Y-m-d', strtotime($data['dob']));

			if ($data['referral'] == 0) {

				$referral_email = '';
			} else {

				$referral_email = $data['referral_email'];
			}

			$address = $data['residence_city'] . ' ' . $data['residence_country'];

			$google_api_key = $this->tomouh_model->getSetting('GOOGLE_MAP_KEY');

			$geo = $this->file_get_contents_curl('https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&key=' . $google_api_key);

			$geo = json_decode($geo, true);

			if (isset($geo['status']) && ($geo['status'] == 'OK')) {

				$latitude = $geo['results'][0]['geometry']['location']['lat'];
				$longitude = $geo['results'][0]['geometry']['location']['lng'];
			} else {
				$latitude = '';
				$longitude = '';
			}

			$update_data = array(
				'v_email'   	 => $data['email'],
				'v_firstname'	 => $data['firstname'],
				'v_lastname' 	 => $data['lastname'],
				'd_dob'      	 => $date_of_birth,
				'e_gender'    	 => $data['gender'],
				'v_home_country' => $data['home_country'],
				'v_home_city'    => $data['home_city'],
				'v_residence_country' => $data['residence_country'],
				'v_residence_city'    => $data['residence_city'],
				'v_telephone' 	 => $data['tele_country_code'] . '-' . $data['telephone'],
				'v_telephone_2'  => $data['tele_2_country_code'] . '-' . $data['telephone_2'],
				'v_twitter_link ' => $data['twitter_link'],
				'v_linkedin_link' => $data['linkedin_link'],
				'v_instagram_link' => $data['instagram_link'],
				'e_referral'     => $data['referral'],
				'v_referral_email' => $referral_email,
				'v_longitude' => $longitude,
				'v_latitude' => $latitude,
				'i_updated' => 1,
				'e_updated' => 'yes',
				'd_last_profile_update'=>date('Y-m-d'),
			);

		
			$tbl = "tbl_members";
			$updateee =$this->admin_model->update_entry($user_id, $update_data, $tbl);
		

			$this->update_crm_detail_step1($data, $user_id);

			redirect(base_url() . 'profile');
			exit();
		}


		$user_data = $this->tomouh_model->getUserByUserId($user_id);
		//echo "<pre>"; print_r($user_data); exit;
		$data['user_data'] = $user_data;
		$data['countries'] = $this->tomouh_model->getCountry();
		$this->load->view('edit_profile_1', $data);
	}

	public function edit_profile_2($slug = "edit_profile_2")
	{

		$data = array();

		$data['meta_title'] = "Edit Profile";
		$data['meta_keyword'] = "Edit Profile";
		$data['meta_description'] = "Edit Profile";

		$user_id  = $this->session->userdata("logged_user");

		if (!$this->session->has_userdata('logged_user')) {

			redirect(base_url() . 'login/?succ=0&msg=logfirst');
			exit;
		}

		$this->load->helper('url');

		if (isset($this->session->userdata["logged_user"])) {

			$user_data = $this->tomouh_model->getUserByUserId($user_id);

			$usr_data['user'] = $user_data;

			$this->session->set_userdata($usr_data);
		}

		$data['educational_data'] = $edu_data = $this->tomouh_model->getEducationalData($user_id);

		$data['professional_data'] = $prof_data = $this->tomouh_model->getProfessionalData($user_id);

		$data['achievement_data'] = $achi_data = $this->tomouh_model->getAchievementalData($user_id);

		$user_data = $this->tomouh_model->getUserByUserId($user_id);

		$data['industry_data'] = $this->tomouh_model->getIndustryActiveData();

		if ($this->input->post()) {

			$update_user = array(
				'e_updated' => 'yes',
				'd_last_profile_update'=>date('Y-m-d'),
			);
	
			$tbl = "tbl_members";
			$this->admin_model->update_entry($user_id, $update_user, $tbl);

			$old_data = [];
			$new_data = [];

			$data = $this->input->post();

			$data['edu_row_id'] = isset($data['edu_row_id']) ? $data['edu_row_id'] : array('0');

			if (isset($data['edu_row_id'])) {

				foreach ($edu_data as $row) {

					if (!in_array($row['id'], $data['edu_row_id'])) {
						$rowData = $this->admin_model->getRow("tbl_educational_data",$row['id']);
						
						$old_data=[];
						$new_data=[];

						$old_data['university']= $rowData['v_university'];
						$old_data['degree']= $rowData['v_degree'];
						$old_data['major']= $rowData['v_major'];
						$old_data['passing_year']= $rowData['i_passing_year'];
						
						$tbl ="tbl_members_log";
						$l_data = array(
							'member_id'=> $user_id, 
							'firstname'=> $user_data['v_firstname'],
							'lastname'=> $user_data['v_lastname'],
							'gender'=> $user_data['e_gender'],
						);
						$insert_data=array(
							'l_old_data'=>json_encode($old_data),
							'l_new_data'=>json_encode($new_data),
							'l_data' => json_encode($l_data),
							'is_education'=> 1,
							'd_modified'=>date('Y-m-d H:i:s'),
						); 
						$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
							
					
						$this->admin_model->delete_entry($row['id'], "tbl_educational_data");
					}
				}
			}

			$data['pro_row_id'] = isset($data['pro_row_id']) ? $data['pro_row_id'] : array('0');

			if (isset($data['pro_row_id'])) {

				foreach ($prof_data as $row) {

					if (!in_array($row['id'], $data['pro_row_id'])) {
						$old_data=[];
						$new_data=[];


						$rowData = $this->admin_model->getRow("tbl_professional_data",$row['id']);
						
							
							$old_data['company']= $rowData['v_company'];
							$old_data['job_title']= $rowData['v_job_title'];
							$old_data['industry']= $rowData['v_industry'];
							$old_data['company_from']= $rowData['i_company_from'];
							$old_data['company_to']= $rowData['i_company_to'];
							
							$tbl ="tbl_members_log";
							$l_data = array(
								'member_id'=> $user_id, 
								'firstname'=> $user_data['v_firstname'],
								'gender'=> $user_data['e_gender'],
								'lastname'=> $user_data['v_lastname']
							);
							$insert_data=array(
								'l_old_data'=>json_encode($old_data),
								'l_new_data'=>json_encode($new_data),
								'l_data' => json_encode($l_data),
								'is_professional'=>1,
								'd_modified'=>date('Y-m-d H:i:s'),
							); 
							$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
							
					
						$this->admin_model->delete_entry($row['id'], "tbl_professional_data");
					}
				}
			}

			$data['achi_row_id'] = isset($data['achi_row_id']) ? $data['achi_row_id'] : array('0');

			if (isset($data['achi_row_id'])) {

				foreach ($achi_data as $row) {

					if (!in_array($row['id'], $data['achi_row_id'])) {

						$rowData = $this->admin_model->getRow("tbl_achievements_data",$row['id']);
						
							$old_data=[];
							$new_data=[];

							$old_data['achievement']= $rowData['v_achievement'];
						
							$tbl ="tbl_members_log";
							$l_data = array(
								'member_id'=> $user_id, 
								'firstname'=> $user_data['v_firstname'],
								'gender'=> $user_data['e_gender'],
								'lastname'=> $user_data['v_lastname']
							);
							$insert_data=array(
								'l_old_data'=>json_encode($old_data),
								'l_new_data'=>json_encode($new_data),
								'l_data' => json_encode($l_data),
								'is_achievements'=>1,
								'd_modified'=>date('Y-m-d H:i:s'),
							); 
							$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
							
						
						$this->admin_model->delete_entry($row['id'], "tbl_achievements_data");
					}
				}
			}


			$main_occupation_data = $this->input->post('occupation');

			$int_main_occupation = intval(preg_replace('/[^0-9]+/', '', $main_occupation_data), 10);

			$passion_interest_data = array(
				't_passion_interest' => $data['passions_interests'],
				'v_main_occupation' => $data['main_occu'],
				'i_updated' => 1,
				'e_updated' => 'yes',
				// 'd_last_profile_update'=>date('Y-m-d'),
			);

				$old_data=[];
				$new_data=[];

				if($user_data['t_passion_interest'] != $data['passions_interests']){
					$old_data['Passions and interests']= $user_data['t_passion_interest'];
			
					$new_data['Passions and interests']= $data['passions_interests'];
					if(count($new_data) && !empty($new_data)){
						$tbl ="tbl_members_log";
						$l_data = array(
							'member_id'=> $user_id, 
							'firstname'=> $user_data['v_firstname'],
							'gender'=> $user_data['e_gender'],
							'lastname'=> $user_data['v_lastname']
						);
					}
					
					$insert_data=array(
						'l_old_data'=>json_encode($old_data),
						'l_new_data'=>json_encode($new_data),
						'l_data' => json_encode($l_data),
						'd_modified'=>date('Y-m-d H:i:s'),
					); 
					$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
				}
							
		
			$tbl = "tbl_members";
			$this->admin_model->update_entry($user_id, $passion_interest_data, $tbl);


			$educ_data_length = sizeof($data['university']);

			for ($i = 0; $i < $educ_data_length; $i++) {

				if (isset($data['university'][$i]) && !empty($data['university'][$i])) {

					if ($int_main_occupation <= 10) {

						if (($i + 1) == $int_main_occupation) {
							$main_occupation = 1;
						} else {
							$main_occupation = 0;
						}
					} else {
						$main_occupation = 0;
					}

					if (!empty($data['edu_row_id'][$i])) {

							$old_data=[];
							$new_data=[];

							$rowData = $this->admin_model->getRow("tbl_educational_data",$data['edu_row_id'][$i]);

							if(!empty($data) && !empty($user_data)){
			
								if($data['university'][$i] != $rowData['v_university']){
									$old_data['university']= $rowData['v_university'];
									$new_data['university']= $data['university'][$i];
								}
								if($data['degree'][$i] != $rowData['v_degree']){
									$old_data['degree']= $rowData['v_degree'];
									$new_data['degree']= $data['degree'][$i];
								}

								if($data['major'][$i] != $rowData['v_major']){
									$old_data['major']= $rowData['v_major'];
									$new_data['major']= $data['major'][$i];
								}
								if($data['graduation_year'][$i] != $rowData['i_passing_year']){
									$old_data['graduation_year']= $rowData['i_passing_year'];
									$new_data['graduation_year']= $data['graduation_year'][$i];
								}
							
								if(count($new_data) && !empty($new_data)){
									$tbl ="tbl_members_log";
									$l_data = array(
										'member_id'=> $user_id, 
										'firstname'=> $user_data['v_firstname'],
										'gender'=> $user_data['e_gender'],
										'lastname'=> $user_data['v_lastname']
									);
									$insert_data=array(
										'l_old_data'=>json_encode($old_data),
										'l_new_data'=>json_encode($new_data),
										'l_data' => json_encode($l_data),
										'is_education'=>1,
										'd_modified'=>date('Y-m-d H:i:s'),
									); 
									$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
								
								}
								
								// print_r($old_data);
								// print_r($new_data);
								// print_r(json_encode($old_data));
								// print_r(json_encode($new_data));
								// exit;
							}
					
						$update_array = array(
							'v_university' => $data['university'][$i],
							'v_degree' => $data['degree'][$i],
							'v_major' => $data['major'][$i],
							'i_passing_year' => $data['graduation_year'][$i],
							'i_main_occupation' => $main_occupation
						);

						$tbl = "tbl_educational_data";
						$this->admin_model->update_entry($data['edu_row_id'][$i], $update_array, $tbl);
					} else {

						if($this->input->post('update_info') == 1){
							if(!empty($data) && !empty($user_data)){
								$old_data=[];
								$new_data=[];
								$new_data['university']= $data['university'][$i];
								$new_data['degree']= $data['degree'][$i];
								$new_data['major']= $data['major'][$i];
								$new_data['graduation_year']= $data['graduation_year'][$i];
							
								if(count($new_data) && !empty($new_data)){
									$tbl ="tbl_members_log";
									$l_data = array(
										'member_id'=> $user_id, 
										'firstname'=> $user_data['v_firstname'],
										'gender'=> $user_data['e_gender'],
										'lastname'=> $user_data['v_lastname']
									);
									$insert_data=array(
										'l_old_data'=>json_encode($old_data),
										'l_new_data'=>json_encode($new_data),
										'l_data' => json_encode($l_data),
										'is_education'=>1,
										'd_modified'=>date('Y-m-d H:i:s'),
									); 
									$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
								
								}
								
							}
						}


						$insert_array = array(

							'user_id' => $user_id,
							'v_university' => $data['university'][$i],
							'v_degree' => $data['degree'][$i],
							'v_major' => $data['major'][$i],
							'i_passing_year' => $data['graduation_year'][$i],
							'i_main_occupation' => $main_occupation
						);
						$tbl = "tbl_educational_data";
						$this->admin_model->add_entry($insert_array, $tbl);
					}
				}
			}

			$prof_data_length = sizeof($data['company']);

			for ($i = 0; $i < $prof_data_length; $i++) {
				if (isset($data['company'][$i]) && !empty($data['company'][$i])) {

					if ($int_main_occupation >= 10) {

						if (($i + 11) == $int_main_occupation) {
							$main_occupation = 1;
						} else {
							$main_occupation = 0;
						}
					} else {
						$main_occupation = 0;
					}

					if (!empty($data['pro_row_id'][$i])) {
							$old_data=[];
							$new_data=[];

							$rowData = $this->admin_model->getRow("tbl_professional_data",$data['pro_row_id'][$i]);

							if(!empty($data) && !empty($user_data)){
			
								if($data['company'][$i] != $rowData['v_company']){
									$old_data['company']= $rowData['v_company'];
									$new_data['company']= $data['company'][$i];
								}
								if($data['job_title'][$i] != $rowData['v_job_title']){
									$old_data['job_title']= $rowData['v_job_title'];
									$new_data['job_title']= $data['job_title'][$i];
								}

								if($data['industry'][$i] != $rowData['v_industry']){
									$old_data['industry']= $rowData['v_industry'];
									$new_data['industry']= $data['industry'][$i];
								}
								if($data['company_from'][$i] != $rowData['i_company_from']){
									$old_data['company_from']= $rowData['i_company_from'];
									$new_data['company_from']= $data['company_from'][$i];
								}
								if($data['company_to'][$i] != $rowData['i_company_to']){
									$old_data['company_to']= $rowData['i_company_to'];
									$new_data['company_to']= $data['company_to'][$i];
								}
							
								if(count($new_data) && !empty($new_data)){
									$tbl ="tbl_members_log";
									$l_data = array(
										'member_id'=> $user_id, 
										'firstname'=> $user_data['v_firstname'],
										'gender'=> $user_data['e_gender'],
										'lastname'=> $user_data['v_lastname']
									);
									$insert_data=array(
										'l_old_data'=>json_encode($old_data),
										'l_new_data'=>json_encode($new_data),
										'l_data' => json_encode($l_data),
										'is_professional'=>1,
										'd_modified'=>date('Y-m-d H:i:s'),
									); 
									$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
								
								}
								
							}
				

						$update_array = array(

							'v_company' => $data['company'][$i],
							'v_job_title' => $data['job_title'][$i],
							'v_industry' => $data['industry'][$i],
							'i_company_from' => $data['company_from'][$i],
							'i_company_to' => $data['company_to'][$i],
							'i_main_occupation' => $main_occupation
						);

						$tbl = "tbl_professional_data";
						$this->admin_model->update_entry($data['pro_row_id'][$i], $update_array, $tbl);
					} else {

							if(!empty($data) && !empty($user_data)){
								$old_data=[];
								$new_data=[];
								$new_data['company']= $data['company'][$i];
								$new_data['job_title']= $data['job_title'][$i];
								$new_data['industry']= $data['industry'][$i];
								$new_data['company_from']= $data['company_from'][$i];
								$new_data['company_to']= $data['company_to'][$i];
							
								if(count($new_data) && !empty($new_data)){
									$tbl ="tbl_members_log";
									$l_data = array(
										'member_id'=> $user_id, 
										'firstname'=> $user_data['v_firstname'],
										'gender'=> $user_data['e_gender'],
										'lastname'=> $user_data['v_lastname']
									);
									$insert_data=array(
										'l_old_data'=>json_encode($old_data),
										'l_new_data'=>json_encode($new_data),
										'l_data' => json_encode($l_data),
										'is_professional'=>1,
										'd_modified'=>date('Y-m-d H:i:s'),
									); 
									$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
								
								}
								
							}
						$insert_array = array(

							'user_id'           => $user_id,
							'v_company'         => $data['company'][$i],
							'v_job_title'       => $data['job_title'][$i],
							'v_industry'        => $data['industry'][$i],
							'i_company_from'    => $data['company_from'][$i],
							'i_company_to'      => $data['company_to'][$i],
							'i_main_occupation' => $main_occupation
						);
						$tbl = "tbl_professional_data";
						$this->admin_model->add_entry($insert_array, $tbl);
					}
				}
			}

			$achie_data_length = sizeof($data['award_details']);

			for ($i = 0; $i < $achie_data_length; $i++) {

				if (isset($data['award_details'][$i]) && !empty($data['award_details'][$i])) {
				

					if (!empty($data['achi_row_id'][$i])) {

							$old_data=[];
							$new_data=[];

							$rowData = $this->admin_model->getRow("tbl_achievements_data",$data['achi_row_id'][$i]);
	
							if(!empty($data) && !empty($user_data)){
			
								if($data['award_details'][$i] != $rowData['v_achievement']){
									$old_data['achievement']= $rowData['v_achievement'];
									$new_data['achievement']= $data['award_details'][$i];
								}
							
								if(count($new_data) && !empty($new_data)){
									$tbl ="tbl_members_log";
									$l_data = array(
										'member_id'=> $user_id, 
										'firstname'=> $user_data['v_firstname'],
										'gender'=> $user_data['e_gender'],
										'lastname'=> $user_data['v_lastname']
									);
									$insert_data=array(
										'l_old_data'=>json_encode($old_data),
										'l_new_data'=>json_encode($new_data),
										'l_data' => json_encode($l_data),
										'is_achievements'=>1,
										'd_modified'=>date('Y-m-d H:i:s'),
									); 
									$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
								
								}
								
							}
					
						$update_array = array(
							'v_achievement' => $data['award_details'][$i],
						);

						$tbl = "tbl_achievements_data";
						$this->admin_model->update_entry($data['achi_row_id'][$i], $update_array, $tbl);
					} else {

							if(!empty($data) && !empty($user_data)){
								$old_data=[];
								$new_data=[];
								$new_data['achievement']= $data['award_details'][$i];
							
								if(count($new_data) && !empty($new_data)){
									$tbl ="tbl_members_log";
									$l_data = array(
										'member_id'=> $user_id, 
										'firstname'=> $user_data['v_firstname'],
										'gender'=> $user_data['e_gender'],
										'lastname'=> $user_data['v_lastname']
									);
									$insert_data=array(
										'l_old_data'=>json_encode($old_data),
										'l_new_data'=>json_encode($new_data),
										'l_data' => json_encode($l_data),
										'is_achievements'=>1,
										'd_modified'=>date('Y-m-d H:i:s'),
									); 
									$add_user_data = $this->admin_model->add_entry($insert_data, $tbl);
								
								}
								
							}
					
						$insert_array = array(
							'user_id' => $user_id,
							'v_achievement' => $data['award_details'][$i],
						);
						$tbl = "tbl_achievements_data";
						$this->admin_model->add_entry($insert_array, $tbl);
					}
				}
			}

			$this->update_crm_detail_step2($data, $user_id);

			redirect(base_url() . 'profile');
			exit;
		}

		$data['t_passion_interest'] = isset($user_data['t_passion_interest']) ? $user_data['t_passion_interest'] : '';
		$data['v_main_occupation'] = isset($user_data['v_main_occupation']) ? $user_data['v_main_occupation'] : '';

		$this->load->view('edit_profile_2', $data);
	}

	public function isValidEmail($email)
	{
		return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
	}

	public function payment_success($slug = "payment_success")
	{

		$data = array();

		$response = $_POST;

		if (isset($response) && !empty($response)) {

			$sub_id = $response['subscr_id'];

			if (!empty($sub_id)) {

				$sub_data = $this->tomouh_model->getSubscriptionBySubId($sub_id);

				$sub_date = isset($sub_data['d_subscription_date']) ? $sub_data['d_subscription_date'] : '';

				$original_date = '';

				if ($sub_date != '') {

					$original_date = date('Y-m-d', strtotime($sub_date));
				}

				// $original_date = '2018-05-10';

				if (isset($original_date) && !empty($original_date) && strtotime($original_date) >= strtotime(date('Y-m-d') . ' -1 day')) { } else {

					$member = $this->tomouh_model->getUserBySubscriptionId($sub_id);

					if (!empty($member)) {

						$subscription_array = array(
							'user_id' => $member['id'],
							'v_customer_id' => $response['payer_id'],
							'v_subscription_id' => $sub_id,
							'l_subscription_data' => json_encode($response),
							'd_subscription_date' => date("Y-m-d H:i:s"),
							'e_payment_type' => 'paypal',
						);
						$this->admin_model->add_entry($subscription_array, "tbl_subscription_data");

						$this->update_zoho_crm($member['id'], $sub_id);

						$update_member = array(
							'd_subscription_exp_date' => date('Y-m-d', strtotime('+1 year')),
						);

						$this->admin_model->update_entry($member['id'], $update_member, "tbl_members");
					} else {

						$is_member = $this->isValidEmail($response['custom']);

						if ($is_member == 1) {

							$mem_data = $this->tomouh_model->getUserByEmail($response['custom']);

							if ($mem_data) {

								$subscription_array = array(
									'user_id' => $mem_data['id'],
									'v_customer_id' => $response['payer_id'],
									'v_subscription_id' => $sub_id,
									'l_subscription_data' => json_encode($response),
									'd_subscription_date' => date("Y-m-d H:i:s"),
									'e_payment_type' => 'paypal',
								);
								$this->admin_model->add_entry($subscription_array, "tbl_subscription_data");

								$update_member = array(
									'e_plan_type' => 'paid',
									'd_subscription_exp_date' => date('Y-m-d', strtotime('+1 year')),
								);

								$this->admin_model->update_entry($mem_data['id'], $update_member, "tbl_members");

								$this->update_zoho_crm($mem_data['id'], $sub_id);

								if (isset($mem_data['e_status']) && $mem_data['e_status'] == 'active') {

									$data['is_member'] = 1;
								} else {

									$data['is_member'] = 0;
								}
							}
						} else {

							$member_temp_data = $this->tomouh_model->getTempUserById($response['custom']);

							$user_data = json_decode($member_temp_data['l_user_step1_data'], true);

							$data = json_decode($member_temp_data['l_user_step2_data'], true);


							if (isset($user_data['residence_city'])) {

								$address = $user_data['residence_city'] . ' ' . $user_data['residence_country'];

								$google_api_key = $this->tomouh_model->getSetting('GOOGLE_MAP_KEY');

								$geo = $this->file_get_contents_curl('https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&key=' . $google_api_key);

								$geo = json_decode($geo, true);

								if (isset($geo['status']) && ($geo['status'] == 'OK')) {

									$latitude = $geo['results'][0]['geometry']['location']['lat'];
									$longitude = $geo['results'][0]['geometry']['location']['lng'];
								} else {

									$latitude = '';
									$longitude = '';
								}
							} else {
								$latitude = '';
								$longitude = '';
							}

							$main_occupation_description = isset($data['main_occu']) ? $data['main_occu'] : '';

							$main_occupation_data = isset($data['occupation']) ? $data['occupation'] : '';

							$int_main_occupation = intval(preg_replace('/[^0-9]+/', '', $main_occupation_data), 10);

							$passion_interest = $data['passions_interests'];

							if ($user_data['referral'] == 0) {

								$referral_email = '';
							} else {

								$referral_email = $user_data['referral_email'];
							}

							$user = $user_data['email'];

							$email_to = $user;
							$email_from = "";

							$template = $this->tomouh_model->getEmailTemplate(2);
							$email_subject = $template['v_subject'];
							$content = $template['l_body'];

							$activation_link = '<a href="' . base_url() . 'login/activation/' . md5($user_data['email']) . '" target="_blank">link here</a>';

							$content = str_replace("link here", $activation_link, $content);

							$sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content, $attachments = array());

							if ($sent) {

								$update_data = array(
									'v_email'                 => $user_data['email'],
									'v_password'              => md5($user_data['password']),
									'v_firstname'             => $user_data['firstname'],
									'v_lastname'              => $user_data['lastname'],
									'd_dob'                   => date('Y-m-d', strtotime($user_data['dob'])),
									'e_gender'                => $user_data['gender'],
									'v_home_country'          => $user_data['home_country'],
									'v_home_city'             => $user_data['home_city'],
									'v_residence_country'     => $user_data['residence_country'],
									'v_residence_city'        => $user_data['residence_city'],
									'v_telephone'             => $user_data['tele_country_code'] . '-' . $user_data['telephone'],
									'v_telephone_2'           => $user_data['tele_2_country_code'] . '-' . $user_data['telephone_2'],
									'v_twitter_link '         => $user_data['twitter_link'],
									'v_linkedin_link'         => $user_data['linkedin_link'],
									'v_instagram_link'        => $user_data['instagram_link'],
									'e_referral'              => $user_data['referral'],
									'v_referral_email'        => $referral_email,
									't_passion_interest'      => isset($passion_interest) ? $passion_interest : '',
									't_description'           => '',
									'v_main_occupation'       => isset($main_occupation_description) ? $main_occupation_description : '',
									'v_longitude'             => $longitude,
									'v_latitude'              => $latitude,
									'e_status'                => 'inactive',
									'e_member_of_month'       => 0,
									'v_subscription_id'       => $sub_id,
									'e_type'                  => $user_data['type'],
									'v_label'             	  => 'Member',
									'e_plan_type'             => 'paid',
									'v_crm_contact_id'        => $user_data['contact_id'],
									'e_payment_type'          => 'paypal',
									'd_subscription_exp_date' => date('Y-m-d', strtotime('+1 year')),
								);

								$tbl = "tbl_members";
								// $add_user_data = $this->admin_model->add_entry($insert_data,$tbl);

								$this->admin_model->update_member_entry($user_data['contact_id'], $update_data, $tbl);

								$this->add_zoho_crm($add_user_data, $response['custom']);

								if ($user_data['type'] == 'member') {

									$this->add_mailchimp($user_data['email'], $user_data['firstname'], $user_data['lastname']);
								}

								$subscription_array = array(
									'user_id' => $add_user_data,
									'v_customer_id' => $response['payer_id'],
									'v_subscription_id' => $sub_id,
									'l_subscription_data' => json_encode($response),
									'd_subscription_date' => date("Y-m-d H:i:s"),
									'e_payment_type' => 'paypal',
								);
								$this->admin_model->add_entry($subscription_array, "tbl_subscription_data");

								$educ_data_length = sizeof($data['university']);

								for ($i = 0; $i < $educ_data_length; $i++) {
									if (isset($data['university'][$i]) && !empty($data['university'][$i])) {
										if ($int_main_occupation <= 10) {

											if (($i + 1) == $int_main_occupation) {
												$main_occupation = 1;
											} else {
												$main_occupation = 0;
											}
										} else {
											$main_occupation = 0;
										}

										$insert_array = array(

											'user_id' => $add_user_data,
											'v_university' => $data['university'][$i],
											'v_degree' => $data['degree'][$i],
											'v_major' => $data['major'][$i],
											'i_passing_year' => $data['graduation_year'][$i],
											'i_main_occupation' => $main_occupation
										);
										$tbl = "tbl_educational_data";
										$this->admin_model->add_entry($insert_array, $tbl);
									}
								}

								$company_data_length = sizeof($data['company']);

								for ($i = 0; $i < $company_data_length; $i++) {

									if (isset($data['company'][$i]) && !empty($data['company'][$i])) {
										if ($int_main_occupation >= 10) {

											if (($i + 11) == $int_main_occupation) {
												$main_occupation = 1;
											} else {
												$main_occupation = 0;
											}
										} else {
											$main_occupation = 0;
										}

										$insert_array = array(

											'user_id' => $add_user_data,
											'v_company' => $data['company'][$i],
											'v_job_title' => $data['job_title'][$i],
											'v_industry' => $data['industry'][$i],
											'i_company_from' => $data['company_from'][$i],
											'i_company_to' => $data['company_to'][$i],
											'i_main_occupation' => $main_occupation
										);
										$tbl = "tbl_professional_data";
										$this->admin_model->add_entry($insert_array, $tbl);
									}
								}

								$award_data_length = sizeof($data['award_details']);

								for ($i = 0; $i < $award_data_length; $i++) {

									if (isset($data['award_details'][$i]) && !empty($data['award_details'][$i])) {
										$insert_array = array(

											'user_id' => $add_user_data,
											'v_achievement' => $data['award_details'][$i],
										);
										$tbl = "tbl_achievements_data";
										$this->admin_model->add_entry($insert_array, $tbl);
									}
								}
							}
						}
					}
				}
			}
		}

		$data['meta_title'] = "Payment Success Page";
		$data['meta_keyword'] = "Payment Success Page";
		$data['meta_description'] = "Payment Success Page";

		$res = setcookie('user_step1', '', time() - 2592000, '/');

		$res = setcookie('user_step2', '', time() - 2592000, '/');

		$this->session->unset_userdata('user');
		$this->session->unset_userdata('user_step2');

		$this->load->view('payment_success', $data);
	}

	public function logout()
	{
        $this->session->unset_userdata('logged_user');
		$this->session->unset_userdata('last_page');
		if (isset($_COOKIE['tomouh_logged_data']) && !empty($_COOKIE['tomouh_logged_data'])) {
			setcookie('tomouh_logged_data', '', time() - 31536000, '/');
		}
		redirect(base_url() . 'login/?succ=1&msg=logout');
		exit;
	}

	public function getZohoCRMContacts($email, $index)
	{
        try{
            $zcrmModuleIns = ZCRMModule::getInstance("Contacts");
    		$bulkAPIResponse = $zcrmModuleIns->getRecords('', 'Modified_Time', 'desc', $index, $index + 199);
    
    		$recordsArray = $bulkAPIResponse->getData();
    
    		if (!empty($recordsArray)) {
    			foreach ($recordsArray as $rcad) {
    				$theRsp = $rcad->getData();
    				if (isset($theRsp['Email'])) {
    					if ($theRsp['Email'] == $email) {
    						$data_members['member'] = $theRsp;
    						$data_members['contact_id'] = $rcad->getEntityId();
    						return $data_members;
    					}
    				}
    			}
    		} else {
    
    			return 0;
    		}
    
    		$index = $index + 200;
    
    		$result = $this->getZohoCRMContacts($email, $index);
    
    		if (!empty($result)) {
    
    			return $result;
    		}
        }
        catch (\Throwable $th) {
			//throw $th;
		}
		

		// $cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');

		// $client = new ZohoCRMClient('Contacts', $cms_token, "eu");
		// $records = $client->getRecords()
		// 	    ->fromIndex($index)
		// 	    ->toIndex($index+199)
		// 	    ->sortBy('Modified Time')
		// 	    ->sortDesc()
		// 	    ->request();

		// if(!empty($records)){

		// 	foreach($records as $record){
		// 		echo "<pre>";
		// 			print_r($record->data);exit;

		// 		if(isset($record->data['Email'])){


		// 			if($record->data['Email'] == $email){

		// 				$data_members = $record->data;



		// 				return $data_members;

		// 			}
		// 		}
		// 	}
		// }else{

		// 		return 0;
		// }

		// $index = $index+200;

		// $result = $this->getZohoCRMContacts($email,$index);

		// if(!empty($result)){

		// 	return $result;
		// }
	}

	public function add_zoho_crm($user_id, $temp_user_id)
	{

		$member_data = $this->tomouh_model->getUserByUserId($user_id);

		$payment_data_str = 'Payment Type : ' . $member_data['e_payment_type'] . ', Subscription Id : ' . $member_data['v_subscription_id'] . ', Activation Date : ' . date('Y-m-d') . ', Expiration Date : ' . $member_data['d_subscription_exp_date'];

		$member_temp_data = $this->tomouh_model->getTempUserById($temp_user_id);

		$user_step1 = json_decode($member_temp_data['l_user_step1_data'], true);

		$user_step2 = json_decode($member_temp_data['l_user_step2_data'], true);

		if (isset($user_step2['university']) && !empty($user_step2['university'])) {

			$educ_data_length = sizeof($user_step2['university']);

			$education_info = array();

			for ($i = 0; $i < $educ_data_length; $i++) {

				if (isset($user_step2['university'][$i]) && !empty($user_step2['university'][$i])) {

					$education_info[] = 'University : ' . $user_step2['university'][$i] . ', Degree : ' . $user_step2['degree'][$i] . ', Major : ' . $user_step2['major'][$i] . ', Year : ' . $user_step2['graduation_year'][$i];
				}
			}
		}

		if (isset($user_step2['company']) && !empty($user_step2['company'])) {

			$company_data_length = sizeof($user_step2['company']);

			$experience_info = array();

			for ($i = 0; $i < $company_data_length; $i++) {

				if (isset($user_step2['company'][$i]) && !empty($user_step2['company'][$i])) {

					$experience_info[] = 'Company : ' . $user_step2['company'][$i] . ', Job Title : ' . $user_step2['job_title'][$i] . ', Industry : ' . $user_step2['industry'][$i] . ', Period : ' . $user_step2['company_from'][$i] . '-' . $user_step2['company_to'][$i];
				}
			}
		}

		if (isset($user_step2['award_details']) && !empty($user_step2['award_details'])) {

			$award_data_length = sizeof($user_step2['award_details']);

			$award_detail = '';

			for ($i = 0; $i < $award_data_length; $i++) {

				if (isset($user_step2['award_details'][$i]) && !empty($user_step2['award_details'][$i])) {

					$award_detail .= $user_step2['award_details'][$i] . ',';
				}
			}
		}

		if (isset($award_detail)) {

			$AAH = rtrim($award_detail, ',');
		} else {
			$AAH = '';
		}

		$educational_info = implode(' %0A ', $education_info);

		$exp_info = implode(' %0A ', $experience_info);

		// $cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');

		// insert into member

		//to get the instance of the module
		// $moduleMemberIns=ZCRMRestClient::getInstance()->getModuleInstance("Members");

		// $recordsMember=array();
		// //To get ZCRMRecord instance
		// $recordMember=ZCRMRecord::getInstance("Members",null); 
		// //This function use to set FieldApiName and value similar to all other FieldApis and Custom field
		// $recordMember->setFieldValue("Last_Name",$user_step1['lastname']);
		// $recordMember->setFieldValue("Email",$user_step1['email']);
		// $recordMember->setFieldValue("Name",$user_step1['firstname']);
		// $recordMember->setFieldValue("Date_Of_Birth",date('Y-m-d', strtotime($user_step1['dob'])));
		// $recordMember->setFieldValue("Gender",$user_step1['gender']);
		// $recordMember->setFieldValue("Expired_On",date('m/d/Y',strtotime('+1 year')));
		// $recordMember->setFieldValue("Home_Country",$user_step1['home_country']);
		// $recordMember->setFieldValue("Home_City",$user_step1['home_city']);
		// $recordMember->setFieldValue("Mobile_No",$user_step1['tele_country_code'].'-'.$user_step1['telephone']); 
		// $recordMember->setFieldValue("Country_of_Residence",$user_step1['residence_country']);
		// $recordMember->setFieldValue("City_Of_Residence'",$user_step1['residence_city']);
		// $recordMember->setFieldValue("Mobile_No_2",$user_step1['tele_2_country_code'].'-'.$user_step1['telephone_2']);
		// $recordMember->setFieldValue("Educational_Info_1",isset($educational_info)? urldecode($educational_info):''); 
		// $recordMember->setFieldValue("Experience_Info_1",isset($exp_info)? urldecode($exp_info):'');
		// $recordMember->setFieldValue("AAH",$AAH);
		// $recordMember->setFieldValue("Passions_and_interests",$user_step2['passions_interests']);
		// $recordMember->setFieldValue("Subscription_Info",$payment_data_str);
		// $recordMember->setFieldValue("Status'","Active");
		// $recordMember->setFieldValue("Plan","Paid");
		// $recordMember->setFieldValue("Label","Member");

		// array_push($recordsMember, $recordMember);//pushing the record to the array 
		// $responseIn=$moduleMemberIns->createRecords($recordsMember);
		// $responseIn = $responseIn->getEntityResponses();
		// $responseIn = $responseIn[0]->getDetails();
		// $responseCrmID = $responseIn['id'];

		//       $update_array  = array(
		//               'v_member_crm_id'=>$responseCrmID,
		//               );

		//       $this->admin_model->update_entry($user_id,$update_array,"tbl_members");

		if (isset($user_step1['contact_id']) && !empty($user_step1['contact_id'])) {

			$contact_id = $user_step1['contact_id'];
			//UPDATE CONTACT DATA
			$zcrmRecordContactIns = ZCRMRecord::getInstance("Contacts", $contact_id);

			$zcrmRecordContactIns->setFieldValue("Last_Name", $user_step1['lastname']);
			$zcrmRecordContactIns->setFieldValue("Email", $user_step1['email']);
			$zcrmRecordContactIns->setFieldValue("First_Name", $user_step1['firstname']);
			$zcrmRecordContactIns->setFieldValue("Date_of_Birth", date('Y-m-d', strtotime($user_step1['dob'])));
			$zcrmRecordContactIns->setFieldValue("Gender", $user_step1['gender']);

			$zcrmRecordContactIns->setFieldValue("Country", $user_step1['home_country']);
			$zcrmRecordContactIns->setFieldValue("Home_City", $user_step1['home_city']);
			// $zcrmRecordContactIns->setFieldValue("Mobile", $user_step1['tele_country_code'] . '-' . $user_step1['telephone']);
			$zcrmRecordContactIns->setFieldValue("Mobile_No", $user_step1['tele_country_code'] . '-' . $user_step1['telephone']);

			$zcrmRecordContactIns->setFieldValue("Country_of_Residence", $user_step1['residence_country']);
			$zcrmRecordContactIns->setFieldValue("City_Of_Residence", $user_step1['residence_city']);
			$zcrmRecordContactIns->setFieldValue("Mobile_No_2", $user_step1['tele_2_country_code'] . '-' . $data['telephone_2']);

			$zcrmRecordContactIns->setFieldValue("Educational_Info", isset($educational_info) ? urldecode($educational_info) : '');
			$zcrmRecordContactIns->setFieldValue("AAH", $AAH);
			$zcrmRecordContactIns->setFieldValue("Passions_and_interests", $data['passions_interests']);
			$zcrmRecordContactIns->setFieldValue("Experience_Info", isset($exp_info) ? urldecode($exp_info) : '');
			$zcrmRecordContactIns->setFieldValue("Subscription_Info", $payment_data_str);
			// $zcrmRecordContactIns->setFieldValue("Member_Status", "Active");
			$zcrmRecordContactIns->setFieldValue("Plan", "Paid");
			$zcrmRecordContactIns->setFieldValue("Label", 'Member');

			$apiResponse = $zcrmRecordContactIns->update();
		}
	}

	public function add_mailchimp($email, $firstname, $lastname)
	{

		// mailchimp api call to subscribe start

		$merge_vars = array(
			'EMAIL' => $email,
			'FNAME' => $firstname,
			'LNAME' => $lastname,
		);

		mailChimpSubscribe($merge_vars);

		// mailchimp api call to subscribe end	

		// if($api->listSubscribe($list_id, $EmailTo, $merge_vars, 'html', false) === true) {

		// }else{

		// }
	}

	public function updateContactLabel($contact_id)
	{

		$zcrmRecordIns = ZCRMRecord::getInstance("Contacts", $contact_id);
		$zcrmRecordIns->setFieldValue("Label", "Candidate");
		$apiResponse = $zcrmRecordIns->update();
	}


	public function getZohoCRMContactsById($contact_id)
	{
		// echo $contact_id; exit;
        try{
            $apiResponse = ZCRMModule::getInstance('Contacts')->getRecord($contact_id);
    		$record = $apiResponse->getData();
    
    		$memberdata['memberdata'] = $record->getData();
    		$memberdata['contact_id'] = $record->getEntityId();
    
    		return $memberdata;
        }
        catch (\Throwable $th) {
			//throw $th;
		}
	
	}

	public function add_zoho_crm_plan($user_id)
	{

		$member_data = $this->tomouh_model->getUserByUserId($user_id);

		$payment_data = '';

		$user_step1 = $this->session->userdata('user');

		$user_step2 = $this->session->userdata('user_step2');

		if (isset($user_step2['university']) && !empty($user_step2['university'])) {

			$educ_data_length = sizeof($user_step2['university']);

			$education_info = array();

			for ($i = 0; $i < $educ_data_length; $i++) {

				if (isset($user_step2['university'][$i]) && !empty($user_step2['university'][$i])) {

					$education_info[] = 'University : ' . $user_step2['university'][$i] . ', Degree : ' . $user_step2['degree'][$i] . ', Major : ' . $user_step2['major'][$i] . ', Year : ' . $user_step2['graduation_year'][$i];
				}
			}
		}

		if (isset($user_step2['company']) && !empty($user_step2['company'])) {

			$company_data_length = sizeof($user_step2['company']);

			$experience_info = array();

			for ($i = 0; $i < $company_data_length; $i++) {

				if (isset($user_step2['company'][$i]) && !empty($user_step2['company'][$i])) {

					$experience_info[] = 'Company : ' . $user_step2['company'][$i] . ', Job Title : ' . $user_step2['job_title'][$i] . ', Industry : ' . $user_step2['industry'][$i] . ', Period : ' . $user_step2['company_from'][$i] . '-' . $user_step2['company_to'][$i];
				}
			}
		}

		if (isset($user_step2['award_details']) && !empty($user_step2['award_details'])) {

			$award_data_length = sizeof($user_step2['award_details']);

			$award_detail = '';

			for ($i = 0; $i < $award_data_length; $i++) {

				if (isset($user_step2['award_details'][$i]) && !empty($user_step2['award_details'][$i])) {

					$award_detail .= $user_step2['award_details'][$i] . ',';
				}
			}
		}

		if (isset($award_detail)) {

			$AAH = rtrim($award_detail, ',');
		} else {
			$AAH = '';
		}

		$educational_info = implode(' %0A ', $education_info);

		$exp_info = implode(' %0A ', $experience_info);

		// $cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');
		// insert into member

		//to get the instance of the module
		// $moduleMemberIns=ZCRMRestClient::getInstance()->getModuleInstance("Members");

		// $recordsMember=array();
		// //To get ZCRMRecord instance
		// $recordMember=ZCRMRecord::getInstance("Members",null); 
		// //This function use to set FieldApiName and value similar to all other FieldApis and Custom field
		// $recordMember->setFieldValue("Last_Name",$user_step1['lastname']);
		// $recordMember->setFieldValue("Email",$user_step1['email']);
		// $recordMember->setFieldValue("Name",$user_step1['firstname']);
		// $recordMember->setFieldValue("Date_Of_Birth",date('Y-m-d', strtotime($user_step1['dob'])));
		// $recordMember->setFieldValue("Gender",$user_step1['gender']);
		// $recordMember->setFieldValue("Expired_On","");
		// $recordMember->setFieldValue("Home_Country",$user_step1['home_country']);
		// $recordMember->setFieldValue("Home_City",$user_step1['home_city']);
		// $recordMember->setFieldValue("Mobile_No",$user_step1['tele_country_code'].'-'.$user_step1['telephone']); 
		// $recordMember->setFieldValue("Country_of_Residence",$user_step1['residence_country']);
		// $recordMember->setFieldValue("City_Of_Residence",$user_step1['residence_city']);
		// $recordMember->setFieldValue("Mobile_No_2",$user_step1['tele_2_country_code'].'-'.$user_step1['telephone_2']);
		// $recordMember->setFieldValue("Educational_Info_1",isset($educational_info)? urldecode($educational_info):''); 
		// $recordMember->setFieldValue("Experience_Info_1",isset($exp_info)? urldecode($exp_info):'');
		// $recordMember->setFieldValue("AAH",$AAH);
		// $recordMember->setFieldValue("Passions_and_interests",$user_step2['passions_interests']);
		// $recordMember->setFieldValue("Subscription_Info","");
		// $recordMember->setFieldValue("Status","Active");
		// $recordMember->setFieldValue("Plan","Free");
		// $recordMember->setFieldValue("Label","Member");

		// array_push($recordsMember, $recordMember);//pushing the record to the array 
		// $responseIn=$moduleMemberIns->createRecords($recordsMember);
		// $responseIn = $responseIn->getEntityResponses();
		// $responseIn = $responseIn[0]->getDetails();
		// $responseCrmID = $responseIn['id'];  

		// $update_array  = array(
		// 		'v_member_crm_id'=>$responseCrmID,
		// 		);

		// $this->admin_model->update_entry($user_id,$update_array,"tbl_members");

		if (isset($user_step1['contact_id']) && !empty($user_step1['contact_id'])) {

			$contact_id = $user_step1['contact_id'];

			$zcrmRecordIns = ZCRMRecord::getInstance("Contacts", $contact_id);
			$zcrmRecordIns->setFieldValue("Last_Name", $user_step1['lastname']);
			$zcrmRecordIns->setFieldValue("Email", $user_step1['email']);
			$zcrmRecordIns->setFieldValue("First_Name", $user_step1['firstname']);
			$zcrmRecordIns->setFieldValue("Date_of_Birth", date('Y-m-d', strtotime($user_step1['dob'])));
			$zcrmRecordIns->setFieldValue("Gender", $user_step1['gender']);
			$zcrmRecordIns->setFieldValue("Profile_Expiry", "");
			$zcrmRecordIns->setFieldValue("Country", $user_step1['home_country']);
			$zcrmRecordIns->setFieldValue("Home_City", $user_step1['home_city']);
			$zcrmRecordIns->setFieldValue("Mobile_No", $user_step1['tele_country_code'] . '-' . $user_step1['telephone']);
			$zcrmRecordIns->setFieldValue("Country_of_Residence", $user_step1['residence_country']);
			$zcrmRecordIns->setFieldValue("City_Of_Residence", $user_step1['residence_city']);
			$zcrmRecordIns->setFieldValue("Mobile_No_2", $user_step1['tele_2_country_code'] . '-' . $user_step1['telephone_2']);
			$zcrmRecordIns->setFieldValue("Educational_Info", isset($educational_info) ? urldecode($educational_info) : '');
			$zcrmRecordIns->setFieldValue("Experience_Info", isset($exp_info) ? urldecode($exp_info) : '');
			$zcrmRecordIns->setFieldValue("AAH", $AAH);
			$zcrmRecordIns->setFieldValue("Passions_and_interests", $user_step2['passions_interests']);
			$zcrmRecordIns->setFieldValue("Subscription_Info", "");
			// $zcrmRecordIns->setFieldValue("Member_Status", "Active");
			$zcrmRecordIns->setFieldValue("Plan", "Free");
			$zcrmRecordIns->setFieldValue("Label", "Member");
			$apiResponse   = $zcrmRecordIns->update();
		}
	}

	public function getCountryFromName()
	{

		$response = array();

		if (isset($_POST['country']) && !empty($_POST['country'])) {

			$country = $_POST['country'];

			$country_data = $this->tomouh_model->getCountryCode($country);

			$response['phonecode'] = $country_data['phonecode'];
		}

		echo json_encode($response);
		exit();
	}

	public function signup_success($slug = "signup_success")
	{

		$data['meta_title'] = "Signup Success Page";
		$data['meta_keyword'] = "Signup Success Page";
		$data['meta_description'] = "Signup Success Page";

		$this->load->view('signup_success', $data);
	}

	public function addZohoContactMember($personal_data, $educational_data, $insert_id)
	{

		// echo "<pre>";
		// print_r($personal_data);
		// print_r($educational_data); 
		// exit;

		if (isset($educational_data['university']) && !empty($educational_data['university'])) {

			$educ_data_length = sizeof($educational_data['university']);

			$education_info = array();

			for ($i = 0; $i < $educ_data_length; $i++) {

				if (isset($educational_data['university'][$i]) && !empty($educational_data['university'][$i])) {

					$education_info[] = 'University : ' . $educational_data['university'][$i] . ', Degree : ' . $educational_data['degree'][$i] . ', Major : ' . $educational_data['major'][$i] . ', Year : ' . $educational_data['graduation_year'][$i];
				}
			}
		}

		if (isset($educational_data['company']) && !empty($educational_data['company'])) {

			$company_data_length = sizeof($educational_data['company']);

			$experience_info = array();

			for ($i = 0; $i < $company_data_length; $i++) {

				if (isset($educational_data['company'][$i]) && !empty($educational_data['company'][$i])) {

					$experience_info[] = 'Company : ' . $educational_data['company'][$i] . ', Job Title : ' . $educational_data['job_title'][$i] . ', Industry : ' . $educational_data['industry'][$i] . ', Period : ' . $educational_data['company_from'][$i] . '-' . $educational_data['company_to'][$i];
				}
			}
		}

		if (isset($educational_data['award_details']) && !empty($educational_data['award_details'])) {

			$award_data_length = sizeof($educational_data['award_details']);

			$award_detail = '';

			for ($i = 0; $i < $award_data_length; $i++) {

				if (isset($educational_data['award_details'][$i]) && !empty($educational_data['award_details'][$i])) {

					$award_detail .= $educational_data['award_details'][$i] . ',';
				}
			}
		}

		if (isset($award_detail)) {

			$AAH = rtrim($award_detail, ',');
		} else {
			$AAH = '';
		}

		$educational_info = implode(' %0A ', $education_info);

		$exp_info = implode(' %0A ', $experience_info);

		//insert contact in zoho start

		//to get the instance of the module

		$moduleContactIns = ZCRMRestClient::getInstance()->getModuleInstance("Contacts");
		$recordsContact = array();

		//To get ZCRMRecord instance
		$recordContact = ZCRMRecord::getInstance("Contacts", null);



		//This function use to set FieldApiName and value similar to all other FieldApis and Custom field
		$recordContact->setFieldValue("Last_Name", isset($personal_data['lastname']) ? $personal_data['lastname'] : '');
		$recordContact->setFieldValue("Email", isset($personal_data['email']) ? $personal_data['email'] : '');
		$recordContact->setFieldValue("First_Name", isset($personal_data['firstname']) ? $personal_data['firstname'] : '');
		$recordContact->setFieldValue("Date_of_Birth", date('Y-m-d', strtotime($personal_data['dob'])));
		$recordContact->setFieldValue("Gender", $personal_data['gender']);

		// $recordContact->setFieldValue("Expired_On","10/10/2020");
		$recordContact->setFieldValue("Country", isset($personal_data['home_country']) ? $personal_data['home_country'] : '');
		$recordContact->setFieldValue("Home_City", isset($personal_data['home_city']) ? $personal_data['home_city'] : '');
		// $recordContact->setFieldValue("Mobile", isset($personal_data['telephone']) && !empty($personal_data['telephone']) ? $personal_data['tele_country_code'] . '-' . $personal_data['telephone'] : '');
		$recordContact->setFieldValue("Mobile_No", isset($personal_data['telephone']) && !empty($personal_data['telephone']) ? $personal_data['tele_country_code'] . '-' . $personal_data['telephone'] : '');

		$recordContact->setFieldValue("Country_of_Residence", isset($personal_data['residence_country']) ? $personal_data['residence_country'] : '');
		$recordContact->setFieldValue("City_Of_Residence", isset($personal_data['residence_city']) ? $personal_data['residence_city'] : '');
		$recordContact->setFieldValue("Mobile_No_2", isset($personal_data['telephone_2']) && !empty($personal_data['telephone_2']) ? $personal_data['tele_2_country_code'] . '-' . $personal_data['telephone_2'] : '');
		$recordContact->setFieldValue("Educational_Info", isset($educational_info) ? urldecode($educational_info) : '');

		$recordContact->setFieldValue("Experience_Info", isset($exp_info) ? urldecode($exp_info) : '');
		$recordContact->setFieldValue("AAH", $AAH);
		$recordContact->setFieldValue("Passions_and_interests", isset($educational_data['passions_interests']) ? $educational_data['passions_interests'] : '');
		// $recordContact->setFieldValue("Subscription_Info","test");

		$recordContact->setFieldValue("Lead_Source", isset($personal_data['lead_source']) ? $personal_data['lead_source'] : '');
		$recordContact->setFieldValue("Label", "Candidate");

		// 12-03-2020  rachel>>
		// $value['v_owner_id'] = 72650000001752217;
		// $user = ZCRMUser::getInstance($value['v_owner_id'], null);	
			
		// $recordContact->setOwner($user);
		// $recordContact->setCreatedBy($user);

		// <<12-03-2020
		
		array_push($recordsContact, $recordContact); //pushing the record to the array 

		$responseIn = $moduleContactIns->createRecords($recordsContact);

		$responseIn = $responseIn->getEntityResponses();
		$responseIn = $responseIn[0]->getDetails();
		$responseCrmID = $responseIn['id'];
		// echo "<pre>";
		// print_r($responseIn);exit;
		// $user['contact_id'] = $responseCrmID;

		$update_array  = array(
			'v_contact_crm_id' => $responseCrmID,
		);

		$this->admin_model->update_entry($insert_id, $update_array, "tbl_before_member");

		return $responseCrmID;

		// $this->session->set_userdata($user);

		// insert contact in zoho end

	}

	public function delete_account()
	{

		$user_id = $this->session->userdata("logged_user");

		if (isset($user_id) && !empty($user_id)) {

			$this->delete_crm_data($user_id);

			$usr_data = $this->tomouh_model->getUserByUserId($user_id);

			if (isset($usr_data['v_email']) && !empty($usr_data['v_email'])) {

				$this->admin_model->delete_member_temp_data($usr_data['v_email'], 'tbl_members_temp');

				$this->admin_model->delete_member_before_data($usr_data['v_email'], 'tbl_before_member');
			}

			$delete_result = $this->tomouh_model->deleteUserAccount($user_id);

			if ($delete_result) {

				$this->session->unset_userdata('logged_user');

				if (isset($_COOKIE['tomouh_logged_data']) && !empty($_COOKIE['tomouh_logged_data'])) {
					setcookie('tomouh_logged_data', '', time() - 31536000, '/');
				}

				redirect(base_url() . 'login?succ=1&msg=delete_account');
				exit;
			}
		}

		redirect(base_url() . 'login');
		exit;
	}

	public function request_website_data()
	{

		$user_id = $this->session->userdata("logged_user");

		if (isset($user_id) && !empty($user_id)) {

			$user_data = $this->tomouh_model->getUserByUserId($user_id);

			if (!empty($user_data)) {

				$email_content = '<p>Hello ' . $user_data["v_firstname"] . ' ' . $user_data["v_lastname"] . ',</p>
						   <p>
We have recently received your request. Please see the information below that is on the Tomouh\'s website.</p>';

				$email_content .= '<table cellpadding="10" align="center">
						<tr>
							<th align="left">Name</th>
							<td>' . $user_data["v_firstname"] . ' ' . $user_data["v_lastname"] . '</td>
						</tr>
						<tr>
							<th align="left">Email</th>
							<td>' . $user_data["v_email"] . '</td>
						</tr>
						<tr>
							<th align="left">Date Of Birth</th>
							<td>' . $user_data["d_dob"] . '</td>
						</tr>
						<tr>
							<th align="left">Gender</th>
							<td>' . $user_data["e_gender"] . '</td>
						</tr>
						<tr>
							<th align="left">Home Country</th>
							<td>' . $user_data["v_home_country"] . '</td>
						</tr>
						<tr>
							<th align="left">Home City</th>
							<td>' . $user_data["v_home_city"] . '</td>
						</tr>
						<tr>
							<th align="left">Residence Country</th>
							<td>' . $user_data["v_residence_country"] . '</td>
						</tr>
						<tr>
							<th align="left">Residence City</th>
							<td>' . $user_data["v_residence_city"] . '</td>
						</tr>
						<tr>
							<th align="left">Telephone</th>
							<td>' . $user_data["v_telephone"] . '</td>
						</tr>
						<tr>
							<th align="left">Telephone 2</th>
							<td>' . $user_data["v_telephone_2"] . '</td>
						</tr>
						<tr>
							<th align="left">Twitter Link</th>
							<td>' . $user_data["v_twitter_link"] . '</td>
						</tr>
						<tr>
							<th align="left">LinkedIn Link</th>
							<td>' . $user_data["v_linkedin_link"] . '</td>
						</tr>
						<tr>
							<th align="left">Instagram Link</th>
							<td>' . $user_data["v_instagram_link"] . '</td>
						</tr>
						<tr>
							<th align="left">Passion And Interest</th>
							<td>' . $user_data["t_passion_interest"] . '</td>
						</tr>
					</table>';

				$edu_data = $this->tomouh_model->getEducationalData($select_user['user_id']);

				if (!empty($edu_data)) {

					$email_content .= '<table cellpadding="10" style="text-align: center;width:100%;" border="1px;" align="left">
							<tr>
								<th colspan="4">
									Educational Data
								</th>
							</tr>
							<tr>
								<th>University</th>
								<th>Degree</th>
								<th>Major</th>
								<th>Graduation Year</th>
							</tr>';

					foreach ($edu_data as $edu) {

						$email_content .=  '<tr>
											<td>' . $edu['v_university'] . '</td>
											<td>' . $edu['v_degree'] . '</td>
											<td>' . $edu['v_major'] . '</td>
											<td>' . $edu['i_passing_year'] . '</td>
										</tr>';
					}

					$email_content .= '</table>';
				}

				$pro_data = $this->tomouh_model->getProfessionalData($select_user['user_id']);

				if (!empty($pro_data)) {

					$email_content .= '<table cellpadding="10" style="margin-top:10px; text-align: center;width:100%;" border="1px;" align="left">

							<tr>
								<th colspan="5">
									Professional Data
								</th>
							</tr>
							<tr>
								<th>Company</th>
								<th>Job Title</th>
								<th>Industry</th>
								<th>From</th>
								<th>To</th>
							</tr>';

					foreach ($pro_data as $pro) {

						$email_content .=  '<tr>
											<td>' . $pro['v_company'] . '</td>
											<td>' . $pro['v_job_title'] . '</td>
											<td>' . $pro['v_industry'] . '</td>
											<td>' . $pro['i_company_from'] . '</td>
											<td>' . $pro['i_company_to'] . '</td>
										</tr>';
					}

					$email_content .= '</table>';
				}

				$award_data = $this->tomouh_model->getAchievementalData($select_user['user_id']);

				if (!empty($award_data)) {

					$email_content .= '<table cellpadding="10" style="margin-top:10px;width:100%;text-align:center;" border="1px;" align="left">
										<tr>
											<th>
												Achievements, Awards and Honors
											</th>
										</tr>';

					foreach ($award_data as $awa) {

						$email_content .= '<tr>
												<td>' . $awa['v_achievement'] . '</td>
										   </tr>';
					}

					$email_content .= "</table>";
				}

				$email_to = $user_data["v_email"];
				$email_from = '';
				$template = $this->tomouh_model->getEmailTemplate(9);
				$email_subject = $template['v_subject'];

				$content = isset($template['l_body']) ? $template['l_body'] : '';


				$content = str_replace("REQUESTED_DATA", $email_content, $content);


				$sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content, $attachments = array());
			}

			/*
			$entry_id = $this->tomouh_model->check_request_data($user_id);

			if($entry_id){

				$request_data = array(

					'e_sended'    => '0',
					'd_added'     => date('Y-m-d H:i:s'),

					); 

				$this->admin_model->update_entry($entry_id,$request_data, 'tbl_request_user_data');		

			}else{

				$user_data = $this->tomouh_model->getUserByUserId($user_id);			

				$request_data = array(

					'user_id'     => $user_id,
					'v_email'     => isset($user_data['v_email']) ? $user_data['v_email'] : '',
					'v_firstname' => isset($user_data['v_firstname']) ? $user_data['v_firstname'] : '',
					'v_lastname'  => isset($user_data['v_lastname']) ? $user_data['v_lastname'] : '',
					'd_added'     => date('Y-m-d H:i:s'),

					); 

				$this->admin_model->add_entry($request_data,"tbl_request_user_data");

			}
			
			*/

			redirect(base_url() . 'account?succ=1&msg=request_data');
			exit;
		}

		redirect(base_url() . 'login');
		exit;
	}

	public function update_crm_detail_step1($data, $user_id)
	{

		$user_data = $this->tomouh_model->getUserByUserId($user_id);
		// echo "<pre>";
		// print_r($user_data);exit;
		// if(isset($user_data['v_member_crm_id']) && !empty($user_data['v_member_crm_id'])){

		// 	$member_id = $user_data['v_member_crm_id'];
		// 	// UPDATE MEMBER DATA
		// 	$zcrmRecordMemberIns = ZCRMRecord::getInstance("Members", $member_id);

		// 	$zcrmRecordMemberIns->setFieldValue("Last_Name", $data['lastname']);
		// 	$zcrmRecordMemberIns->setFieldValue("Email",$data['email']);
		// 	$zcrmRecordMemberIns->setFieldValue("Name",$data['firstname']);
		// 	$zcrmRecordMemberIns->setFieldValue("Date_Of_Birth",date('Y-m-d', strtotime($data['dob'])));
		// 	$zcrmRecordMemberIns->setFieldValue("Gender",$data['gender']);

		// 	$zcrmRecordMemberIns->setFieldValue("Home_Country",$data['home_country']);
		// 	$zcrmRecordMemberIns->setFieldValue("Home_City",$data['home_city']);
		// 	$zcrmRecordMemberIns->setFieldValue("Mobile_No",$data['tele_country_code'].'-'.$data['telephone']); 

		// 	$zcrmRecordMemberIns->setFieldValue("Country_of_Residence",$data['residence_country']);
		// 	$zcrmRecordMemberIns->setFieldValue("City_Of_Residence'",$data['residence_city']);
		// 	$zcrmRecordMemberIns->setFieldValue("Mobile_No_2",$user_step1['tele_2_country_code'].'-'.$data['telephone_2']);

		// 	$apiResponse=$zcrmRecordMemberIns->update();


		// }

		if (isset($user_data['v_crm_contact_id']) && !empty($user_data['v_crm_contact_id'])) {

			$contact_id = $user_data['v_crm_contact_id'];

			//UPDATE CONTACT DATA
			$zcrmRecordContactIns = ZCRMRecord::getInstance("Contacts", $contact_id);

			$zcrmRecordContactIns->setFieldValue("Last_Name", $data['lastname']);
			$zcrmRecordContactIns->setFieldValue("Email", $data['email']);
			$zcrmRecordContactIns->setFieldValue("First_Name", $data['firstname']);
			$zcrmRecordContactIns->setFieldValue("Date_of_Birth", date('Y-m-d', strtotime($data['dob'])));
			$zcrmRecordContactIns->setFieldValue("Gender", $data['gender']);

			$zcrmRecordContactIns->setFieldValue("Country", $data['home_country']);
			$zcrmRecordContactIns->setFieldValue("Home_City", $data['home_city']);
			// $zcrmRecordContactIns->setFieldValue("Mobile", $data['tele_country_code'] . '-' . $data['telephone']);
			
			$zcrmRecordContactIns->setFieldValue("Mobile_No", strval($user_data['v_telephone']));

			$zcrmRecordContactIns->setFieldValue("Country_of_Residence", $data['residence_country']);
			$zcrmRecordContactIns->setFieldValue("City_Of_Residence", $data['residence_city']);
			$Mobile_No_2=strval($data['tele_2_country_code']) . '-' . strval($data['telephone_2']);
			$zcrmRecordContactIns->setFieldValue("Mobile_No_2",strval($user_data['v_telephone_2']));
			// $zcrmRecordContactIns->setFieldValue("Mobile_No_2", $user_data['v_telephone_2']);
			$zcrmRecordContactIns->setFieldValue("Last_profile_update", date('Y-m-d'));

			$apiResponse = $zcrmRecordContactIns->update();
			// echo "<pre>";print_r($apiResponse); exit;

		}
	}


	public function update_crm_detail_step2($data, $user_id)
	{

		$user_data = $this->tomouh_model->getUserByUserId($user_id);

		if (isset($data['university']) && !empty($data['university'])) {

			$educ_data_length = sizeof($data['university']);

			$education_info = array();

			for ($i = 0; $i < $educ_data_length; $i++) {

				if (isset($data['university'][$i]) && !empty($data['university'][$i])) {

					$education_info[] = 'University : ' . $data['university'][$i] . ', Degree : ' . $data['degree'][$i] . ', Major : ' . $data['major'][$i] . ', Year : ' . $data['graduation_year'][$i];
				}
			}
		}

		if (isset($data['company']) && !empty($data['company'])) {

			$company_data_length = sizeof($data['company']);

			$experience_info = array();

			for ($i = 0; $i < $company_data_length; $i++) {

				if (isset($data['company'][$i]) && !empty($data['company'][$i])) {

					$experience_info[] = 'Company : ' . $data['company'][$i] . ', Job Title : ' . $data['job_title'][$i] . ', Industry : ' . $data['industry'][$i] . ', Period : ' . $data['company_from'][$i] . '-' . $data['company_to'][$i];
				}
			}
		}

		if (isset($data['award_details']) && !empty($data['award_details'])) {

			$award_data_length = sizeof($data['award_details']);

			$award_detail = '';

			for ($i = 0; $i < $award_data_length; $i++) {

				if (isset($data['award_details'][$i]) && !empty($data['award_details'][$i])) {

					$award_detail .= $data['award_details'][$i] . ',';
				}
			}
		}

		if (isset($award_detail)) {

			$AAH = rtrim($award_detail, ',');
		} else {
			$AAH = '';
		}

		$educational_info = implode(' %0A ', $education_info);

		$exp_info = implode(' %0A ', $experience_info);

		// $cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');

		//      if(isset($user_data['v_member_crm_id']) && !empty($user_data['v_member_crm_id'])){ 

		//      	$member_id = $user_data['v_member_crm_id'];
		// // UPDATE MEMBER DATA
		// $zcrmRecordMemberIns = ZCRMRecord::getInstance("Members", $member_id);

		// $zcrmRecordMemberIns->setFieldValue("Educational_Info_1",isset($educational_info)?urldecode($educational_info):'');
		// $zcrmRecordMemberIns->setFieldValue("AAH",$AAH);
		// $zcrmRecordMemberIns->setFieldValue("Passions_and_interests",$data['passions_interests']);
		// $zcrmRecordMemberIns->setFieldValue("Experience_Info_1",isset($exp_info)?urldecode($exp_info):'');

		// $apiResponse=$zcrmRecordMemberIns->update();
		//      }

		if (isset($user_data['v_crm_contact_id']) && !empty($user_data['v_crm_contact_id'])) {

			$contact_id = $user_data['v_crm_contact_id'];

			//UPDATE CONTACT DATA
			$zcrmRecordContactIns = ZCRMRecord::getInstance("Contacts", $contact_id);

			$zcrmRecordContactIns->setFieldValue("Educational_Info", isset($educational_info) ? urldecode($educational_info) : '');
			$zcrmRecordContactIns->setFieldValue("AAH", $AAH);
			$zcrmRecordContactIns->setFieldValue("Passions_and_interests", $data['passions_interests']);
			$zcrmRecordContactIns->setFieldValue("Experience_Info", isset($exp_info) ? urldecode($exp_info) : '');
			$zcrmRecordContactIns->setFieldValue("Last_profile_update", date('Y-m-d'));


			$apiResponse = $zcrmRecordContactIns->update();
		}
	}

	public function delete_crm_data($user_id)
	{


		$user_data = $this->tomouh_model->getUserByUserId($user_id);

		// $cms_token = $this->tomouh_model->getSetting('ZOHO_CMS_TOKEN');

		//   	if(isset($user_data['v_member_crm_id']) &&  !empty($user_data['v_member_crm_id'])){

		//   		$this->admin_model->delete_crm_member($user_data['v_member_crm_id'],"tbl_crm_members_data");

		//    	$zcrmRecordIns = ZCRMRecord::getInstance("Members", $user_data['v_member_crm_id']); 
		// 	$apiResponse=$zcrmRecordIns->delete();
		// }

		if (isset($user_data['v_crm_contact_id']) &&  !empty($user_data['v_crm_contact_id'])) {

			$this->admin_model->delete_crm_contact($user_data['v_crm_contact_id'], "tbl_crm_contact_data");

			$zcrmRecordIns = ZCRMRecord::getInstance("Contacts", $user_data['v_crm_contact_id']);
			$apiResponse = $zcrmRecordIns->delete();
		}
	}

	public function file_get_contents_curl($url)
	{

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);

		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	}

	public function update_zoho_crm($user_id, $sub_id)
	{

		$results = $this->tomouh_model->getUserByUserId($user_id);

		$payment_data =  $this->tomouh_model->getAllSubscriptionByUserId($user_id);

		$payment_info = [];

		if ($payment_data) {

			foreach ($payment_data as $data) {

				$payment_info[] = 'Payment Type : paypal, Subscription : ' . $data['v_subscription_id'] . ', Expiration Date : ' . date("m/d/Y", strtotime(date("Y-m-d", strtotime($data['d_subscription_date'])) . " + 1 year"));
			}
		}

		$payment_info[] = 'Payment Type : paypal, Subscription Id : ' . $sub_id . ', Expiration Date : ' . date('m/d/Y', strtotime('+1 year'));

		$pay_info = implode(' %0A ', $payment_info);

		$contact_crm_id = $results['v_crm_contact_id'];

		// UPDATE MEMBER DATA
		$zcrmRecordMemberIns = ZCRMRecord::getInstance("Contacts", $contact_crm_id);

		$zcrmRecordMemberIns->setFieldValue("Plan", 'Paid');
		$zcrmRecordMemberIns->setFieldValue("Profile_Expiry", date('Y-m-d', strtotime('+1 year')));
		$zcrmRecordMemberIns->setFieldValue("Subscription_Info", $pay_info);

		$apiResponse = $zcrmRecordMemberIns->update();
	}
}
