<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once('./zoho_v2_live/vendor/autoload.php');

class GetLiveMembers extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('tomouh_model');
		$this->load->model('messages_model');
		$this->load->model('admin/admin_model');
		$this->load->library('session');
		$this->load->library('form_validation');

		ZCRMRestClient::initialize();
	}
	public function index()
	{	
	
		$this->db->select('*');
		$this->db->from('tbl_members');
		// $this->db->where('id', 45);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get();
		$rows = $query->result_array();
		$email = '';
		$LeadSource = '';
		
		// $emaildata = ['mohammad@almojelgroup.com', 'hussein.attar@gmail.com', 'ghadamuhanna@gmail.com','abeer.almoajil@gmail.com'];
		foreach ($rows as $key => $value) {
		// foreach ($emaildata as $key => $value) {
			// echo $value;
			// exit;
			$email = $value['v_email'];
			echo $email; 
			// $email = 'hussein.attar@gmail.com';
			// $email = $value;

			try {

				$moduleIns = ZCRMRestClient::getInstance()->getModuleInstance("Contacts");  //To get module instance
				$response = $moduleIns->searchRecordsByCriteria("(Email:equals:$email)");  //To get module records that match the criteria
				$contactData=$response->getData(); 
				$contactId =  $contactData[0]->getEntityId();
				$contactData = $contactData[0]->getData(); 
				echo "<pre>";
				print_r($contactId); 
				print_r($contactData);
				// exit;

				$moduleIns = ZCRMRestClient::getInstance()->getModuleInstance("Members");  //To get module instance
				$response = $moduleIns->searchRecordsByCriteria("(Email:equals:$email)");  //To get module records that match the criteria
				$memberData=$response->getData(); 
				// $ownerId =  $memberData[0]->getOwner()->getId();
				$memberData = $memberData[0]->getData(); 
				echo "<pre>";
				print_r($memberData); 
				// exit;

				//UPDATE CONTACT DATA
				$zcrmRecordContactIns = ZCRMRecord::getInstance("Contacts", $contactId);

				$zcrmRecordContactIns->setFieldValue("Last_Name", $memberData['Last_Name']);
				// $zcrmRecordContactIns->setFieldValue("Email", $memberData['Email']);
				$zcrmRecordContactIns->setFieldValue("First_Name", $memberData['Name']);
				$zcrmRecordContactIns->setFieldValue("Date_of_Birth", date('Y-m-d', strtotime($memberData['Date_Of_Birth'])));
				$zcrmRecordContactIns->setFieldValue("Gender", $memberData['Gender']);

				$zcrmRecordContactIns->setFieldValue("Country", $memberData['Home_Country']);
				$zcrmRecordContactIns->setFieldValue("Home_City", $memberData['Home_City']);
				$zcrmRecordContactIns->setFieldValue("Mobile", $memberData['Mobile_No']);
				$zcrmRecordContactIns->setFieldValue("Mobile_No", $memberData['Mobile_No']);

				$zcrmRecordContactIns->setFieldValue("Country_of_Residence", $memberData['Country_of_Residence']);
				$zcrmRecordContactIns->setFieldValue("City_Of_Residence", $memberData['City_Of_Residence']);
				$zcrmRecordContactIns->setFieldValue("Mobile_No_2", $memberData['Mobile_No_2']);

				$zcrmRecordContactIns->setFieldValue("Educational_Info", $memberData['Educational_Info_1']);
				$zcrmRecordContactIns->setFieldValue("AAH", $memberData['AAH']);
				$zcrmRecordContactIns->setFieldValue("Passions_and_interests", $memberData['Passions_and_interests']);
				$zcrmRecordContactIns->setFieldValue("Experience_Info", $memberData['Experience_Info_1']);
				$zcrmRecordContactIns->setFieldValue("Subscription_Info", $memberData['Subscription_Info']);
				$zcrmRecordContactIns->setFieldValue("Member_Status", $memberData['Status']);
				$zcrmRecordContactIns->setFieldValue("Plan", $memberData['Plan']);
				$zcrmRecordContactIns->setFieldValue("Label", $memberData['Label']);

				$zcrmRecordContactIns->setFieldValue("GCC_or_not", $memberData['GCC_or_Not']);
				$zcrmRecordContactIns->setFieldValue("Rank", $memberData['Rank']);
				$zcrmRecordContactIns->setFieldValue("Last_ranked", $memberData['Last_ranked']);
				$zcrmRecordContactIns->setFieldValue("Last_Industry", $memberData['Industry']);
				$zcrmRecordContactIns->setFieldValue("Profile_Expiry", $memberData['Expired_On']);
				$zcrmRecordContactIns->setFieldValue("Spoke_at_event", $memberData['Spoke_at_event']);
				$zcrmRecordContactIns->setFieldValue("Moderator", $memberData['Moderator']);
				$zcrmRecordContactIns->setFieldValue("Membership_Committee", $memberData['Membership_Committee']);

				$zcrmRecordContactIns->setFieldValue("Age", $memberData['Age']);
				$zcrmRecordContactIns->setFieldValue("Last_profile_update", $memberData['Last_profile_update']);
				$zcrmRecordContactIns->setFieldValue("Exception", $memberData['Exception']);
				$zcrmRecordContactIns->setFieldValue("Secondary_Email", $memberData['Secondary_Email']);
				$zcrmRecordContactIns->setFieldValue("Member_of_the_month", $memberData['Member_of_the_month']);
			
				$apiResponse = $zcrmRecordContactIns->update();
				echo "added CRM";

				$update_array  = array(
					'v_crm_contact_id' => $contactId,
				);
				// echo "<pre>";
				// print_r($update_array);
				// exit;
				// echo $user_id; exit;
				$this->admin_model->update_entry($value['id'], $update_array, "tbl_members");
				// exit;
				echo "updated";
			} catch (Exception $e) {
				// echo 'Caught exception: ',  $e->getMessage(), "\n";
			}
		}
		echo  "DONE";
			
	}

}
