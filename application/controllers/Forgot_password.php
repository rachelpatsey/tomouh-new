<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Forgot_password extends CI_Controller
{
	public function __construct(){
		parent::__construct();

		$this->load->model('admin/admin_model');
		$this->load->model('tomouh_model');
		$this->load->model('messages_model');
		$this->load->library('session');
		$this->load->library('form_validation');
	}
	public function index(){

			$this->load->helper('url');

			if($this->session->has_userdata('logged_user')){
					redirect(base_url().'profile');
					exit;
			}

			if($this->input->post()){

				$email = $this->input->post('email');

				$row = '';

				if($email != ""){
					$row = $this->tomouh_model->isEmailExist($email);
				}

				if(!empty($row)){
			
					$email_to = $email;
					$email_from = ""; 
					
					$template = $this->tomouh_model->getEmailTemplate(3);
	
					$email_subject = $template['v_subject'];
					$content = $template['l_body'];
					
					$activation_link = '<a href="'.base_url().'forgot_password/changepassword/'.md5($email).'" target="_blank">CLICK HERE</a>';

					$content = str_replace("Click here", $activation_link, $content);

					$sent = $this->tomouh_model->sent_email($email_to, $email_from, $email_subject, $content , $attachments = array() );
					if($sent){
						redirect(base_url().'forgot_password/?succ=1&msg=Forgot_Mail_Sent');
						exit();
					}else{
						redirect(base_url().'forgot_password/?succ=0&msg=emailnotsent');
						exit();
					}
				}else{
						redirect(base_url().'forgot_password/?succ=0&msg=emailnotexist');
						exit();
				}	
			}

			$data['meta_keyword'] = 'Forgotten Password';
			$data['meta_description'] = 'Forgotten Password';
			$data['meta_title'] = 'Forgotten Password';
	
			$this->load->view('forgot_password',$data);
	}

	public function changepassword($code=""){
		
		$data['code'] = $code;
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			 
			$new_password = $this->input->post("new_password");
			$confirm_password = $this->input->post("confirm_password");
			
			if($new_password != '' && $new_password == $confirm_password){
				$data = $this->tomouh_model->getUserByMd5Email($code);
				if(count($data)){
					$ins['v_password'] = md5($new_password);
					$reset = $this->tomouh_model->updateUser($data['id'],$ins);
					if($reset !=''){
						redirect(base_url().'login/?succ=1&msg=chpass');
						exit();
					}
				}else{
						redirect(base_url().'forgot_password/changepassword?succ=0&msg=config_email');
						exit();
				}
			}else{
				redirect(base_url().'forgot_password/changepassword/'.$code.'?succ=0&msg=passwordmismatch');
				exit();
			}
		}

		$data['meta_title'] = 'Reset Password';
		$data['meta_keyword'] = 'Reset Password';
		$data['meta_description'] = 'Reset Password';

		$this->load->view("reset_password", $data);

	}
}
?>